<?php
/*
 * php code///////////**********************************************************
 */
$title = 'ร้านอาหาร Online';

require 'library/pagination.php';

$db = new database();
$option_cat = array(
    "table" => "product_categories"
);
$query_cat = $db->select($option_cat); // catgorie

$option_shop = array(
    "table" => "users",
    "condition" => "Cclose='1'"
);
$query_shop = $db->select($option_shop); // shop

$sql_pc = "SELECT * FROM v_product ";
$query_pc = $db->query($sql_pc);
// echo $sql_pc;
$pagination = new Zebra_Pagination();

$rows_pc = $db->rows($query_pc);

$per_page = 32;
$page_start = (($pagination->get_page() - 1) * $per_page);
$sql_pc .= "ORDER BY cat ASC LIMIT {$page_start},{$per_page} ";
$pagination->records($rows_pc);
$pagination->records_per_page($per_page);
$pagination->base_url('', FALSE);
$query_pc_page = $db->query($sql_pc);

$page = ($page_start != 0) ? $page_start : "1";
$pages = ceil($rows_pc / $per_page);

$uri = $_SERVER['REQUEST_URI']; // url
/*
 * php code///////////**********************************************************
 */

/*
 * header***********************************************************************
 */
require 'template/front/header.php';
/*
 * header***********************************************************************
 */
?>
    <div class="col-md-8">
<?php
$i = 0;
while ($rs_pc = $db->get($query_pc_page)) {
    $tr = ($i % 2 == 0) ? "odd" : "even";
    $dis = ($rs_pc['priceP'] - $rs_pc['price']);
    if ($rs_pc['priceP'] > 0) {
        $ddis = ($dis * 100) / $rs_pc['priceP'];
    }
    ?>


    <!-- SHOW PC -->

    <div class="col-md-4 hidden-xs  ">
        <form class="form-inline" role="form"
              action="<?php echo base_url(); ?>/cart/update/<?php echo $rs_pc['id']; ?>" method="post">

            <div class="thumbnail">
                <button type="submit" class="order-add-button">
                    <span class="glyphicon glyphicon-plus-sign"></span> สั่งอาหาร
                </button>
                <?php if ($dis > 0) { ?>
                    <div class="shop-item-card__badge-wrapper">
                        <div class="shop-badge shop-badge--fixed-width shop-badge--promotion">
                            <div class="shop-badge--promotion__label-wrapper shop-badge--promotion__label-wrapper--th-TH">
                                        <span style="font-size: 18px;"
                                              class="shop-badge--promotion__label-wrapper__off-label shop-badge--promotion__label-wrapper__off-label--th-TH">ลด</span>
                                <span style="font-size: 18px;"
                                      class="percent"><?php echo number_format($ddis, 2); ?>%</span>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                <a href="<?php echo $baseUrl; ?>/product/view/<?php echo $rs_pc['id']; ?>">
                    <!--                            <img style="height: 160px!important;"-->
                    <img style="height: 160px!important;"
                         src="<?php echo $baseUrl; ?>/upload/product/thumb_<?php echo $rs_pc['image']; ?>"
                         alt="<?php echo $rs_pc['pname']; ?>">
                </a>
                <div class="caption">
                    <a href="<?php echo $baseUrl; ?>/product/view/<?php echo $rs_pc['id']; ?>"
                       style="font-size: 16px;"><?php $str_check = strlen($rs_pc['pname']);
                        if ($str_check >= 100) {
                            $sheck_num = mb_substr($rs_pc['pname'], 0, 100);
                            echo $sheck_num . "...";
                        } else {
                            echo $rs_pc['pname'];
                        }
                        ?></a>
                    <p style="font-size: 16px;font-weight: bold;color: red;">฿
                        : <?php echo number_format($rs_pc['price']) . ".-"; ?>
                        <strike><?php if ($rs_pc['priceP'] > $rs_pc['price']) {
                                echo number_format($rs_pc['priceP']) . ".-";
                            } ?></strike></p>
                    <p style="font-size: 16px;width=100%">
                        <input type="hidden" class="form-control" id="text2" autocomplete="off" name="qty"
                               placeholder="ใส่จำนวน" data-validation="number"
                               data-validation-allowing="float" value="1"></p>

                </div>
            </div>
        </form>


    <!-- END SHOW PC -->


    <!-- SHOW MOBILE -->


    <div class="visible-xs col-xs-offset-2 col-xs-10 ">
        <form class="form-inline" role="form" action="<?php echo base_url(); ?>/cart/update/<?php echo $rs_pc['id']; ?>"
              method="post">

            <div class="thumbnail" style="height: 110px!important;">
                <button type="submit" class="order-add-button">
                    <span class="glyphicon glyphicon-plus-sign"></span> สั่งอาหาร
                </button>


                <div class="visible-xs col-xs-3 ">
                    <?php if ($dis > 0) { ?>
                        <div class="shop-item-card__badge-wrapper">
                            <div class="shop-badge shop-badge--fixed-width shop-badge--promotion">
                                <div class="shop-badge--promotion__label-wrapper shop-badge--promotion__label-wrapper--th-TH">
                                        <span style="font-size: 18px;"
                                              class="shop-badge--promotion__label-wrapper__off-label shop-badge--promotion__label-wrapper__off-label--th-TH">ลด</span>
                                    <span style="font-size: 18px;"
                                          class="percent"><?php echo number_format($ddis, 2); ?>%</span>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    <a href="<?php echo $baseUrl; ?>/product/view/<?php echo $rs_pc['id']; ?>">
                        <img style="height: 60px; width:80px;"
                             src="<?php echo $baseUrl; ?>/upload/product/thumb_<?php echo $rs_pc['image']; ?>"
                             alt="<?php echo $rs_pc['pname']; ?>">
                    </a>
                </div>
                <div class="visible-xs  col-xs-5 ">
                    <div class="caption">
                        <a href="<?php echo $baseUrl; ?>/product/view/<?php echo $rs_pc['id']; ?>"
                           style="font-size: 12px;"><?php $str_check = strlen($rs_pc['pname']);
                            if ($str_check >= 100) {
                                $sheck_num = mb_substr($rs_pc['pname'], 0, 100);
                                echo $sheck_num . "...";
                            } else {
                                echo $rs_pc['pname'];
                            }
                            ?></a></div>
                </div>
                <div class="visible-xs  col-xs-4 ">
                    <div class="caption" style="font-size: 14px;font-weight: bold;color: red;">
                        ฿ <?php echo number_format($rs_pc['price']) . ".-"; ?>
                        <strike><?php if ($rs_pc['priceP'] > $rs_pc['price']) {
                                echo number_format($rs_pc['priceP']) . ".-";
                            } ?></strike>
                        <input type="hidden" class="form-control" id="text2" autocomplete="off" name="qty"
                               placeholder="ใส่จำนวน" data-validation="number"
                               data-validation-allowing="float" value="1">
                    </div>

                </div>
            </div>
    </div>
    </form>




    <!-- END SHOW MOBILE -->

    </div>

<?php } ?>

    </div>  <!-- This close div from <div class="row"> in header.php line : 76 -->


<?php $pagination->render(); ?>


<?php
/*
 * footer***********************************************************************
 */
require 'template/front/footer.php';
/*
 * footer***********************************************************************
 */