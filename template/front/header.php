<script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.js"></script>
    <script src="js/system.js"></script>
<script language="javascript">
function getLocation()
  {  if (navigator.geolocation)    {    navigator.geolocation.getCurrentPosition(showPosition);   }
  else{document.getElementById("location").value ="Geolocation is not supported by this browser.";}  }function showPosition(position)
  { document.getElementById("location").value=" " + position.coords.latitude +   ", " + position.coords.longitude;      }
 
</script>
<?php
$strCurrentURL = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <?php date_default_timezone_set("Asia/Bangkok"); ?>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@Food ร้านอาหารออนไลน์</title>
    <link href="<?php echo $baseUrl; ?>/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $baseUrl; ?>/css/main.menu.css" rel="stylesheet">
    <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo $baseUrl; ?>/images/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo $baseUrl; ?>/images/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo $baseUrl; ?>/images/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo $baseUrl; ?>/images/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo $baseUrl; ?>/images/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo $baseUrl; ?>/images/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo $baseUrl; ?>/images/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo $baseUrl; ?>/images/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="<?php echo $baseUrl; ?>/images/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo $baseUrl; ?>/images/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo $baseUrl; ?>/images/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo $baseUrl; ?>/images/favicon-16x16.png">
    <link rel="manifest" href="<?php echo $baseUrl; ?>/images/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php echo $baseUrl; ?>/images/ms-icon-144x144.png">
    <link rel="stylesheet" href="<?php echo $baseUrl; ?>/css/font-awesome.min.css">
</head>

<body background="<?php echo $baseUrl; ?>/img/background.jpg" onLoad="JavaScript:getLocation();">
<!-- Navigation -->

<div class="row">

    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand visible-xs" href="#"><img src="<?php echo $baseUrl; ?>/images/wm2.jpg" width="20" ></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li><a href="<?php echo base_url(); ?>">หน้าหลัก</a></li>
                    <li><a href="<?php echo base_url(); ?>/categorie/34">อาหารเช้า</a></li>
                    <li><a href="<?php echo base_url(); ?>/categorie/35">อาหารกลางวัน</a></li>
                    <li><a href="<?php echo base_url(); ?>/categorie/36">เครื่องดื่ม/ขนมหวาน</a></li>
                    <li><a href="<?php echo $baseUrl; ?>/front/product/index?name=%E0%B9%80%E0%B8%9B%E0%B9%87%E0%B8%94">+เมนูเป็ด+</a>
                    </li>
                    <li><a href="<?php echo base_url(); ?>/aboutus">ติดต่อเรา</a></li>


                    <li>
                        <?php if (!empty(@$_SESSION[_ef . 'username'])) { ?>

                    <li class="bg-info">
                        <a href="" style="color:#000000">
                            ชื่อผู้ใช้ : <?php echo @$_SESSION[_ef . 'fullname']; ?>
                        </a>
                    </li>
                    <li class="bg-success">
                        <a style="color:#000000" href="<?php echo base_url(); ?>/admin">
                            จัดการร้าน
                        </a>
                    </li>
                    <li>
                        <?php if (@$_SESSION[_ef . 'levelaccess'] == ('admin' || 'employee' || 'fin' || 'chef')) { ?>
                            <a style="color:#000000" href="" data-toggle="modal" data-target="#TBM" class="bg-info">
                                <span class="glyphicon glyphicon-open"></span> จองโต๊ะ
                            </a>
                        <?php } ?>
                    </li>


                    <?php } else { ?>

                        <li>
                            <a href="<?php echo base_url(); ?>/admin">
                                เข้าสู่ระบบ
                            </a>
                        </li>
                    <?php } ?>

                    </li>

                    <li class="hidden-sm row">
                        <div class="col-sm">
                            <form class=" navbar-right col-sm-12" id="yw0"
                                  action="<?php echo $baseUrl; ?>/front/product/index" method="get">
                                <input class="col-xs-8" style="height: 50px" type="" placeholder="ค้นหาสินค้า, ร้านค้า"
                                       name="name"
                                       value="<?php echo @$_REQUEST['name']; ?>">
                                <button class="btn btn-default col-xs-4" style="height: 49px">
                                    <svg width="15px" height="15px">
                                        <path d="M11.618 9.897l4.224 4.212c.092.09.1.23.02.312l-1.464 1.46c-.08.08-.222.072-.314-.02L9.868 11.66M6.486 10.9c-2.42 0-4.38-1.955-4.38-4.367 0-2.413 1.96-4.37 4.38-4.37s4.38 1.957 4.38 4.37c0 2.412-1.96 4.368-4.38 4.368m0-10.834C2.904.066 0 2.96 0 6.533 0 10.105 2.904 13 6.486 13s6.487-2.895 6.487-6.467c0-3.572-2.905-6.467-6.487-6.467 "></path>
                                    </svg>
                                </button>
                            </form>
                        </div>
                    </li>
                </ul>

            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
</div>

<div class="row">  <!-- This div close on end blog >>home>index.php line: 123 -->

    <div class="col-md-3 xblog visible-md visible-lg">
        <div class="row">

            <div class="col-md-5">
                <div class="logo-wowped">
                    <a href="<?php echo base_url(); ?>/">
                        <img src="<?php echo $baseUrl; ?>/images/wm2.jpg">
                    </a>
                </div>
            </div>

            <div class="col-md-7">
                <div style="margin-top: 75px">
                    <a class="cart-button" href="<?php echo base_url(); ?>/cart">

                        สั่งอาหาร
                        <br>
                        <span class="item-number "><?php echo count(@$_SESSION[_ef . 'cart']); ?></span>
                        <br>
                        รายการ
                    </a>
                </div>
            </div>
        </div>

        <div class="row" style="margin-top: 10px">

            <?php
            $db = new database();
            $sql_tbh = " SELECT * FROM v_table ";
            $sql_tbh .= "WHERE 1=1 ORDER BY tb ";
            $query_tbh = $db->query($sql_tbh);
            while ($rs_tbh = $db->get($query_tbh)) {
                ?>

                <div class="board-table-col-3">
                    <div class="board-table-col-3">
                        <input type="hidden" name="id" value="<?php echo $rs_tbh['id']; ?>">
                    </div>
                    <?php if ($rs_tbh['open'] == 1||$rs_tbh['open'] == 9) { ?>
                        <form class="form-inline" role="form"
                              action="<?php echo base_url(); ?>/back/order/Vpayment/<?php echo $rs_tbh['que']; ?>"
                              method="post">
                            <button type="submit"
                                    class="cc-button"><?php echo "# " . $rs_tbh['tb'] . "<br>" . $rs_tbh['nameC'] . "<br>" . number_format($rs_tbh['total']) . "฿"; ?>  </button>
                        </form>
                    <?php } else if ($rs_tbh['open'] == 0) { ?>
                        <button type="submit" class="oo-button"><?php echo $rs_tbh['tb'] . "<br>"; ?> </button>
                    <?php } else if ($rs_tbh['open'] == 2) { ?>
                        <form class="form-inline" role="form"
                              action="<?php echo base_url(); ?>/back/order/Vpayment/<?php echo $rs_tbh['que']; ?>"
                              method="post">
                            <button type="submit"
                                    class="pp-button"><?php echo "# " . $rs_tbh['tb'] . "<br>" . $rs_tbh['nameC'] . "<br>" . number_format($rs_tbh['total']) . "฿"; ?>  </button>
                        </form>
                    <?php } else if ($rs_tbh['open'] == 3) { ?>
                        <form class="form-inline" role="form"
                              action="<?php echo base_url(); ?>/back/order/Vpayment/<?php echo $rs_tbh['que']; ?>"
                              method="post">
                            <button type="submit"
                                    class="pp-button"><?php echo "# " . $rs_tbh['tb'] . "<br>" . $rs_tbh['nameC'] . "<br>" . number_format($rs_tbh['total']) . "฿"; ?>  </button>
                        </form>
                    <?php } ?>
                </div>
            <?php } ?>
        </div>
    </div>

    <div class="order-menu visible-md">
        <div style="margin-top: 20px">
            <!--            <a href="#"><img src="--><?php //echo $baseUrl; ?><!--/images/wm2.jpg"></a>-->
            <!--            <br>-->
            <a class="cart-button" style="height: 40px" href="<?php echo base_url(); ?>/cart">
                <svg class="cart-svg " width="16 " height="16 " viewBox="0 0 16 16 ">
                    <path d="M15.32 2.405H4.887C3 2.405 2.46.805 2.46.805L2.257.21C2.208.085 2.083 0 1.946 0H.336C.1 0-.064.24.024.46l.644 1.945L3.11 9.767c.047.137.175.23.32.23h8.418l-.493 1.958H3.768l.002.003c-.017 0-.033-.003-.05-.003-1.06 0-1.92.86-1.92 1.92s.86 1.92 1.92 1.92c.99 0 1.805-.75 1.91-1.712l5.55.076c.12.922.91 1.636 1.867 1.636 1.04 0 1.885-.844 1.885-1.885 0-.866-.584-1.593-1.38-1.814l2.423-8.832c.12-.433-.206-.86-.655-.86 "
                          fill="#fff "></path>
                </svg>
                สั่งซื้อ
                <span class="item-number "><?php echo count(@$_SESSION[_ef . 'cart']); ?></span>
            </a>
        </div>
    </div>


    <!--    Responsive For Mobile-->
    <div class="visible-xs order-menu2 ">
        <a class="btn btn-wowped" style="height: 40px" href="<?php echo base_url(); ?>/cart">
            รายการสั่งอาหาร :
            <span class="item-number "><?php echo count(@$_SESSION[_ef . 'cart']); ?></span>
        </a>
    </div>

     <div class="mobile-menu hidden-md hidden-lg bg-wowped">
<!--    <div class="mobile-menu hidden-md hidden-lg ">-->
<!---->
<!--        <a href="--><?php //echo base_url(); ?><!--/" style="float: left">-->
<!--            <img src="--><?php //echo $baseUrl; ?><!--/images/wm2.jpg" width="60">-->
<!--        </a>-->

        <button class="btn btn-info" style="font-size: 13px;float: right" id="show">เรียกดูโต๊ะ</button>

        <div class="board-table">
            <div class="row ">
                <button class="btn bg-inverse pull-right" id="hide"
                        style="margin-right: 20px;margin-top: 5px;margin-bottom: 5px">ปิด
                </button>
            </div>

            <?php
            $db = new database();

            $sql_tbh = " SELECT * FROM v_table ";
            $sql_tbh .= "WHERE 1=1 ORDER BY tb ";

            $query_tbh = $db->query($sql_tbh);
            while ($rs_tbh = $db->get($query_tbh)) {
                ?>

                <div class="board-table-col-3">
                    <div>
                        <input type="hidden" name="id" value="<?php echo $rs_tbh['id']; ?>">
                    </div>
                    <?php if ($rs_tbh['open'] == 1) { ?>
                        <form class="form-inline" role="form"
                              action="<?php echo base_url(); ?>/back/order/Vpayment/<?php echo $rs_tbh['que']; ?>"
                              method="post">
                            <button type="submit"
                                    class="cc-button bg-inverse"><?php echo "# " . $rs_tbh['tb'] . "<br>" . $rs_tbh['nameC'] . "<br>" . number_format($rs_tbh['total']) . "฿"; ?>  </button>
                        </form>

                    <?php } else if ($rs_tbh['open'] == 0) { ?>
                        <button type="submit" class="oo-button"><?php echo $rs_tbh['tb'] . "<br>"; ?> </button>
                    <?php } else if ($rs_tbh['open'] == 2) { ?>
                        <form class="form-inline" role="form"
                              action="<?php echo base_url(); ?>/back/order/Vpayment/<?php echo $rs_tbh['que']; ?>"
                              method="post">
                            <button type="submit"
                                    class="pp-button"><?php echo "# " . $rs_tbh['tb'] . "<br>" . $rs_tbh['nameC'] . "<br>" . number_format($rs_tbh['total']) . "฿"; ?>  </button>
                        </form>
                    <?php } else if ($rs_tbh['open'] == 3) { ?>
                        <form class="form-inline" role="form"
                              action="<?php echo base_url(); ?>/back/order/Vpayment/<?php echo $rs_tbh['que']; ?>"
                              method="post">
                            <button type="submit"
                                    class="pp-button"><?php echo "# " . $rs_tbh['tb'] . "<br>" . $rs_tbh['nameC'] . "<br>" . number_format($rs_tbh['total']) . "฿"; ?>  </button>
                        </form>
                    <?php } ?>

                </div>
            <?php } ?>


        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="TBM" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">

        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span
                                aria-hidden="true">&times;</span><span
                                class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel">เลือกโต๊ะ</h4>
                </div>
                <?php
                $sql_tb = " SELECT * FROM v_table ";
                $sql_tb .= "WHERE 1=1 ORDER by id asc ";

                $query_tb = $db->query($sql_tb);
                while ($rs_tb = $db->get($query_tb)) {
                    ?>

                    <div class="col-sm-4 col-md-4 col-xs-4" style="height: 150px!important" ; class="modal-body"
                         ;>
                        <div class="thumbnail" style="height: 140px!important;">
                            <form class="form-inline" role="form"
                                  action="<?php echo base_url(); ?>/cart/utable1/<?php echo $rs_tb['tb']; ?>"
                                  method="post">
                                <div class="">
                                    <input type="hidden" name="tb" value="<?php echo $rs_tb['tb']; ?>">
                                    <input type="hidden" name="open" value="<?php echo $rs_tb['open']; ?>">
                                    <input type="hidden" id="que" name="que"
                                           value="<?php echo $rs_tb['que']; ?>">
                                    <p><input type="text" class="form-control" id="nameC" name="nameC"
                                              placeholder="ชื่อ.." value="<?php echo $rs_tb['nameC']; ?>"
                                              size="6">
                                    </p>
                                    <p><input type="text" class="form-control" id="tt" name="tt"
                                              placeholder="ที่นั่ง.." value="<?php echo $rs_tb['tt']; ?>"
                                              size="6">
                                    </p>
                                </div>
                                <?php if ($rs_tb['open'] == 0) { ?>
                                    <button type="submit"
                                            class="carto-button"><?php echo $rs_tb['tb'] . "<br>"; ?> </button>
                                <?php } else if ($rs_tb['open'] == 1) { ?>
                                    <button type="submit"
                                            class="cartT-button"><?php echo $rs_tb['tb'] . "<br>"; ?>  </button>
                                <?php } else if ($rs_tb['open'] == 2) { ?>
                                    <div class="cartc-button"><?php echo $rs_tb['tb'] . "<br>"; ?> </div>

                                <?php } else if ($rs_tb['open'] == 3) { ?>
                                    <div class="cartc-button"><?php echo $rs_tb['tb'] . "<br>"; ?> </div>
                                <?php } ?>
                            </form>
                        </div>
                    </div>
                <?php } ?>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>

    <!-- <div style="margin-top:300px;background-color: rgba(148, 147, 147, 1);">

    <p>พัฒนาระบบ @Food โดย </br>
    ©Addpay Service Point Co., Ltd. </br>
    โทร.045-317123, 061-8182888 Fax.045-317678</br>
      <p>ติดต่อ : ร้านอาหาร Wow เป็ด </p>

      </div>  -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="<?php echo $baseUrl; ?>/js/jquery.1.11.1.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo $baseUrl; ?>/template/js/bootstrap.min.js"></script>
    <script src="<?php echo $baseUrl; ?>/template/js/mainmenu.js"></script>
    <script src="<?php echo $baseUrl; ?>/template/js/slide.js"></script>
    <script src="<?php echo $baseUrl; ?>/css/font-awesome.min.css"></script>
    <!--ส่วนของหน้าเดิม-->


</body>
</html>

<script>
    $(document).ready(function () {
        var maxHeight = 0;
        $(".xblog").each(function () {
            if ($(this).height() > maxHeight) {
                maxHeight = $(this).height();
            }
        });
        $(".xblog").height(maxHeight);


        $("#hide").click(function () {
            $(".board-table").fadeOut();
        });
        $("#show").click(function () {
            $(".board-table").fadeIn();
        });

    });

    $(document).scroll(function () {
        var y = $(this).scrollTop();

//        if (y > 100) {
//            $('.mobile-menu').fadeIn();
//        } else {
//            $('.mobile-menu').fadeOut();
//        }
        if (y > 800) {
            $('.order-menu').fadeIn();
        } else {
            $('.order-menu').fadeOut();
        }

    });


</script>




