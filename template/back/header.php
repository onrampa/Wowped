 <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="<?php echo $baseUrl; ?>/images/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo $baseUrl; ?>/images/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="<?php echo $baseUrl; ?>/images/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="<?php echo $baseUrl; ?>/images/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="<?php echo $baseUrl; ?>/images/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="<?php echo $baseUrl; ?>/images/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="<?php echo $baseUrl; ?>/images/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="<?php echo $baseUrl; ?>/images/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo $baseUrl; ?>/images/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="<?php echo $baseUrl; ?>/images/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="<?php echo $baseUrl; ?>/images/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo $baseUrl; ?>/images/favicon-16x16.png">
<link rel="manifest" href="<?php echo $baseUrl; ?>/images/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="<?php echo $baseUrl; ?>/images/ms-icon-144x144.png">


 <?php 

 if($_SESSION[_ef . 'levelaccess']== 'admin'){
        require 'template/back/admin.php';
 }
 else if($_SESSION[_ef . 'levelaccess']== 'employee'){
        require 'template/back/employee.php';
 }
 else if($_SESSION[_ef . 'levelaccess']== 'chef'){
    require 'template/back/chef.php';
}
else if($_SESSION[_ef . 'levelaccess']== 'cashier'){
    require 'template/back/cashier.php';
}
 else if($_SESSION[_ef . 'levelaccess']== 'user'){
        require 'template/back/user.php';
 }
 ?>
 <?php	date_default_timezone_set("Asia/Bangkok");?>
