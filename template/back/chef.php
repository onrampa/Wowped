<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="language" content="en" />
        <link rel="stylesheet" type="text/css" href="<?php echo $baseUrl; ?>/css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo $baseUrl; ?>/css/bootstrap-theme.min.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo $baseUrl; ?>/css/defaults.css" />
        <!-- EVIL TEMP -->
         <link rel="stylesheet" type="text/css" href="<?php echo $baseUrl; ?>/template/evillage/css/main.css" />
         <link type="text/css"  href="<?php echo $baseUrl; ?>/template/css/main.menu.css" rel="stylesheet">

        <script type="text/javascript" src="<?php echo $baseUrl; ?>/js/jquery.1.11.1.min.js"></script>
        <script type="text/javascript" src="<?php echo $baseUrl; ?>/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php echo $baseUrl; ?>/js/loading.js"></script>
        <title><?php echo @$title; ?></title>
        <style>
            /* CSS used here will be applied after bootstrap.css */
/*            body { 
                background: url('<?php echo $baseUrl; ?>/images/bg_suburb.jpg') no-repeat center center fixed; 
                -webkit-background-size: cover;
                -moz-background-size: cover;
                -o-background-size: cover;
                background-size: cover;
            }*/

           .panel-default {
                opacity: 0.9;
                margin-top:30px;
            }
            .form-group.last {
                margin-bottom:0px;
            }
        </style>
   
</head>

    <body class="evillage-body">
        <!-- <div id="warpper">   -->
              
              <!-- <div class="container"> -->
     <div class=""> <!-- EVIL EDIT -->


            <nav class="navbar navbar-default navbar-fixed-top navbar-evillage-header" style="margin-bottom: 0;" role="navigation">
                
                <div class="navbar-header">
                    <a class="navbar-brand" href="<?php echo $baseUrl; ?>/back">C O O K : </a>
                </div>

                <ul class="nav navbar-top-links navbar-right border-none ">
                
                    <li> <?php echo $_SESSION[_ef . 'fullname']; ?> </li>
                
                    <li class="dropdown">
                        <a class="dropdown-toggle" href="#" data-toggle="dropdown">
                            <i class="glyphicon glyphicon-user"></i> <span class="caret"></span>
                        </a>
                        <ul class="dropdown dropdown-menu">
                            <li>
                                <a href="<?php echo $baseUrl; ?>/back/user/profile"><i class="glyphicon glyphicon-user"></i>
                                    User Profile 
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="<?php echo $baseUrl; ?>/back/user/logout"><i class="glyphicon glyphicon-log-out"></i>
                                    Logout
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
                
                <div class="navbar-default navbar-static-side navbar-evillage-menu navbar-evillage" role="navigation">
                    <div class="sidebar-collapse">
                    <ul id="side-menu" class="nav">

                    <li>
                    <a href="<?php echo $baseUrl; ?>/back/orderd/index1"><i class="glyphicon glyphicon-align-justify"></i> 1. New อาหาร</a>
                    </li>
                    <li>
                    <a href="<?php echo $baseUrl; ?>/back/orderd/index3"><i class="glyphicon glyphicon-align-justify"></i> 2. เสิร์ฟแล้ว</a>
                    </li>
                    <li>
                        <a href="<?php echo $baseUrl; ?>"><i class="glyphicon glyphicon-share-alt"></i> กลับไปเว็บไซต์</a>
                    </li>

                </ul>
            </div>
                </div>
            </nav>
