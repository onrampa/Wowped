<!DOCTYPE html>
<html lang="en">
    <head>
    <!-- <meta HTTP-EQUIV='Refresh' CONTENT='10' charset="utf-8"> -->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="language" content="en" />
        <link rel="stylesheet" type="text/css" href="<?php echo $baseUrl; ?>/css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo $baseUrl; ?>/css/bootstrap-theme.min.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo $baseUrl; ?>/css/defaults.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo $baseUrl; ?>/jui/jquery-ui.css">
        <script type="text/javascript" src="<?php echo $baseUrl; ?>/js/jquery.1.11.1.min.js"></script>
        <script type="text/javascript" src="<?php echo $baseUrl; ?>/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php echo $baseUrl; ?>/js/loading.js"></script>
          <script type="text/javascript" src="<?php echo $baseUrl; ?>/jui/jquery-ui.js"></script>
        <style>
            /* CSS used here will be applied after bootstrap.css */
           /* body { 
                background: url('<?php echo $baseUrl; ?>/images/bg_suburb.jpg') no-repeat center center fixed; 
                -webkit-background-size: cover;
                -moz-background-size: cover;
                -o-background-size: cover;
                background-size: cover;
            }*/

            .panel-default {
                opacity: 0.9;
                margin-top:30px;
            }
            .form-group.last {
                margin-bottom:0px;
            }
        </style>
        
        <title>@Food <?php echo $title; ?></title>
    </head>

    <body>
    <body class="evillage-body">

        <!-- <div id="warpper"> -->
        <!-- <div class="container"> -->
            <nav class="navbar navbar-default navbar-fixed-top" style="margin-bottom: 0;" role="navigation">
                <div class="navbar-header">
                    <a class="navbar-brand" href="<?php echo $baseUrl; ?>/back">A D M I N</a>
                </div>
                <ul class="nav navbar-top-links navbar-right">
  <li>
                <?php echo $_SESSION[_ef . 'fullname']; ?>
                </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" href="#" data-toggle="dropdown">
                            <i class="glyphicon glyphicon-user"></i> <span class="caret"></span>
                        </a>
                        <ul class="dropdown dropdown-menu">                                       
                            <li>
                                <a href="<?php echo $baseUrl; ?>/back/user/profile"><i class="glyphicon glyphicon-user"></i>
                                    User Profile 
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="<?php echo $baseUrl; ?>/back/user/logout"><i class="glyphicon glyphicon-log-out"></i>
                                    Logout
                                </a>
                            </li>
                        </ul>
                        <li>
                                            <?php echo date('Y-m-d H:i:s'); ?>
                         </li>
                    </li>
                </ul>
                <div class="navbar-default navbar-static-side" role="navigation">
                    <div class="sidebar-collapse">
                    <ul id="side-menu" class="nav">
                    <li>
                        <a href="<?php echo $baseUrl; ?>/admin"><i class="glyphicon glyphicon-home"></i> หน้าแรก</a>
                    </li>
                    <li>
                        <a href="<?php echo $baseUrl; ?>/back/product"><i class="glyphicon glyphicon-book"></i> เมนูอาหาร</a>
                    </li>
                    <li>
                        <a href="<?php echo $baseUrl; ?>/back/user"><i class="glyphicon glyphicon-user"></i> ผู้ใช้</a>
                    </li>
                    <li>
                        <a href="<?php echo $baseUrl; ?>/back/uzone"><i class="glyphicon glyphicon-cutlery"></i> โต๊ะอาหาร</a>
                    </li>
                    <li>
                        <a href="<?php echo $baseUrl; ?>/back/productcategorie"><i class="glyphicon glyphicon-th-list"></i> ประเภทอาหาร</a>
                    </li>
                    <li>
                        <a href="<?php echo $baseUrl; ?>/back/stock_cat"><i class="glyphicon glyphicon-th-list"></i> คลังวัตถุดิบ</a>
                    </li>
                    <li>
                        <a href="<?php echo $baseUrl; ?>/back/order/index1"><i class="glyphicon glyphicon-align-justify"></i> 1.รายการ Order</a>
                    </li>
                    <li>
                        <a href="<?php echo $baseUrl; ?>/back/order/index2"><i class="glyphicon glyphicon-align-justify"></i> 2.รายการยังไม่ชำระเงิน </a>
                    </li>
                    <li>
                        <a href="<?php echo $baseUrl; ?>/back/order/index3"><i class="glyphicon glyphicon-align-justify"></i> 3.ชำระเงินเสร็จสิ้น </a>
                    </li>
                    <li>
                    <a href="<?php echo $baseUrl; ?>/back/order/view3_print"><i class="glyphicon glyphicon-print"></i> PRINT สรุปรายวัน </a>
                    </li>  
                    <li>
                        <a href="<?php echo $baseUrl; ?>/back/order"><i class="glyphicon glyphicon-folder-open"></i> เรียกดูทุก Order  </a>
                    </li> 
                    <li>
                        <a href="<?php echo $baseUrl; ?>/back/order/indexq"><i class="glyphicon glyphicon-folder-open"></i> เคลียร์ใบเสร็จเพื่อ ยกเลิก  </a>
                    </li>                          
                    <li>
                        <a href="<?php echo $baseUrl; ?>/back/howtopay"><i class="glyphicon glyphicon-list-alt"></i> วิธีการสั่งซื้อ</a>
                    </li>
                    <li>
                        <a href="<?php echo $baseUrl; ?>/back/aboutus"><i class="glyphicon glyphicon-hdd"></i> เกี่ยวกับเรา</a>
                    </li>
                    <li>
                        <a href="<?php echo $baseUrl; ?>"><i class="glyphicon glyphicon-share-alt"></i> กลับไปเว็บไซต์</a>
                    </li>
                </ul>
            </div>
                </div>
            </nav>
