<?php
date_default_timezone_set("Asia/Bangkok");

include 'config.php';


$create_date = date('Y-m-d H:i:s');
	
$query0 = "INSERT INTO regis_market (province_regis, date_regis, name, sername, id_card, type_person, incom_per, address, sub_district, district, province, zipcode, phone_num, line_id, e_mail, status_market, s_product, n_product, g_product, org_agency, create_date) VALUES ('" . $_POST["province_regis"] . "', '" . $_POST["date_regis"] . "', '" . $_POST["name"] . "', '" . ($_POST["sername"]) . "', '" . $_POST["id_card"] . "', '" . $_POST["type_person"] . "', '" . $_POST["income_per"] . "', '" . $_POST["address"] . "', '" . $_POST["sub_district"] . "', '" . $_POST["district"] . "', '" . $_POST["province"] . "', '" . $_POST["zipcode"] . "', '" . $_POST["phone_num"] . "', '" . $_POST["line_id"] . "', '" . $_POST["e_mail"] . "', '" . $_POST["status_market"] . "', '" . $_POST["s_product"] . "', '" . $_POST["n_product"] . "', '" . $_POST["g_product"] . "', '" . $_POST["org_agency"] . "', '" . $create_date . "')";



$con = mysqli_connect($Host, $User, $Password,$Database) ;

$con->set_charset("utf8");

$result = mysqli_query($con,$query0) ;
?>



<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>ระบบลงทะเบียนร้านค้า ผู้ประกอบการ</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link href="css/main.menu.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    
	<div id="flipkart-navbar">
    <div class="container">
        <div class="row row1">
            <ul class="largenav pull-right">
                <li class="upper-links"><a class="links" href="index.html">หน้าหลัก</a></li>
                <li class="upper-links"><a class="links" href="#">สินค้า</a></li>
                <li class="upper-links"><a class="links" href="cart.html">ตะกร้าสินค้า</a></li>
                <li class="upper-links"><a class="links" href="#">วิธีซื้อสินค้า</a></li>
                <li class="upper-links"><a class="links" href="#">เกี่ยวกับเรา</a></li>
                <li class="upper-links">
                   
                </li>
            </ul>
        </div>

        <div class="row row2">
            <div class="col-sm-2">
                <h2 style="margin:0px;"><span class="smallnav menu" onclick="openNav()">☰ AddPay Shops</span></h2>
                <div style="margin-top:-10px; margin-left:25px;"><a href="#"><img src="images/logo/logo_addpay1.png" class="largenav"></a></div>              
            </div>
            
            
            <div class="flipkart-navbar-search smallsearch col-sm-10 col-xs-11">
                <div class="row">
                    <input class="flipkart-navbar-input col-xs-11" type="" placeholder="ค้นหา" name="">
                    <button class="flipkart-navbar-button col-xs-1">
                        <svg width="15px" height="15px">
                            <path d="M11.618 9.897l4.224 4.212c.092.09.1.23.02.312l-1.464 1.46c-.08.08-.222.072-.314-.02L9.868 11.66M6.486 10.9c-2.42 0-4.38-1.955-4.38-4.367 0-2.413 1.96-4.37 4.38-4.37s4.38 1.957 4.38 4.37c0 2.412-1.96 4.368-4.38 4.368m0-10.834C2.904.066 0 2.96 0 6.533 0 10.105 2.904 13 6.486 13s6.487-2.895 6.487-6.467c0-3.572-2.905-6.467-6.487-6.467 "></path>
                        </svg>
                    </button>
                </div>
            </div>
          
        </div>
    </div>
</div>




<div id="mySidenav" class="sidenav">
    <div class="container" style="background-color: #9c1313; padding-top: 10px;">
        <span class="sidenav-heading">เมนู</span>
        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">×</a>
    </div>
    <a href="#">หน้าหลัก</a>
	<a href="#">อิเล็กทรอนิกส์</a>
	<a href="#">แฟชั่นสุภาพสตรี</a>
	<a href="#">แฟชั่นสุภาพบุรุษ</a>
	<a href="#">เครื่องใช้ไฟฟ้าและของในบ้าน</a>
	<a href="#">สุขภาพและความงาม</a>
	<a href="#">เด็กและของเล่น</a>
	<a href="#">กีฬาและการเดินทาง</a>
	<a href="#">ยานยนต์</a>
	<a href="#">สัตว์เลี้ยงและสื่อบันเทิง</a>
</div>
    

    <!-- Page Content -->
    <div class="container">

        <div class="row top10">

            <div class="col-md-2 hidden-xs">
                <p class="lead">AddPay Shops</p>
                <div class="list-group">
                    <a href="#" class="list-group-item">อุปกรณ์อิเล็กทรอนิกส์</a>
                    <a href="#" class="list-group-item">อาหาร</a>
                    <a href="#" class="list-group-item">ของฝาก</a>
                    <a href="#" class="list-group-item">สุขภาพและความงาม</a>
                    <a href="#" class="list-group-item">แฟชั่นสุภาพสตรี</a>
                    <a href="#" class="list-group-item">แฟชั่นสุภาพบุรุษ</a>
                    <a href="#" class="list-group-item">สินค้าอื่นๆ</a>
                </div>
            </div>

            <div class="col-md-9">
                

                <div class="row top10">

                    <div class="col-sm-4 col-lg-4 col-md-4">
                        <div class='container'>
    <div class='row' style='padding-top:25px; padding-bottom:25px;'>
        <div class='col-md-12'>
            <div id='mainContentWrapper'>
                <div class="col-md-8 col-md-offset-0">
                    <h2 style="text-align: center;">
                    <img src="images/market.png" width="100" height="97"> <img src="images/logo_thongchom.png" width="150"> <img src="images/IMG_31_110860_20160322_091552.jpg" width="100"><br> แบบคำขอลงทะเบียนผู้ประกอบการ โครงการตลาดต้องชม</h2>
               
                    
                    
                       
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-default">
                                    
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            <div class="items">
                                                <div class="col-md-12">
                                                    
                                       
                                        	
                                        <table class="table table-striped" style="font-weight: bold;">
                                             
                                      
                                            <tr>
                                                <td colspan="2" style="width: 175px; align-content:center;">
<?php

if($result)
 {
   echo "ข้อมูลของท่านได้รับการบันทึกเรียบร้อยแล้ว";
 }
else
 {
  echo "บันทึกข้อมูลไม่สำเร็จ";
}
?>
                                                
                                                
                                                
                                                
                                                
                                                                                                 </td>
                                            </tr>

                                        </table>
                                     <br/>
                                        <a href="regis.php"><button type="submit" class="btn btn-success btn-lg" style="width:100%;">กลับหน้าหลัก
                          </button></a>
                                        
                                        

                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                         
                           
        </div>
    </div>



                    </div>


               
                </div>

            </div>

        </div>

    </div>
    <!-- /.container -->

    <div class="container">

        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <p>© 2016 Addpay Service Point Co., Ltd. All rights reserved.</p>
<p>บริษัท แอดเพย์ เซอร์วิสพอยท์ จำกัด เลขที่ 406 หมู่ 18 ต.ขามใหญ่ อ.เมือง จ.อุบลราชธานี 34000</p>
<p>โทร. 045-317123,061-8182888 Fax.045-317678</p>
            </div>
        </footer>

    </div>
    <!-- /.container -->





    
    

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
        <script src="js/mainmenu.js"></script>
        <script src="http://momentjs.com/downloads/moment-with-locales.js"></script>
		<script src="http://momentjs.com/downloads/moment-timezone-with-data.js"></script>

  </body>
</html>