<?php
date_default_timezone_set("Asia/Bangkok");


?>



<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>ระบบลงทะเบียนร้านค้า ผู้ประกอบการ</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link href="css/main.menu.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    
	<div id="flipkart-navbar">
    <div class="container">
        <div class="row row1">
            <ul class="largenav pull-right">
                <li class="upper-links"><a class="links" href="index.html">หน้าหลัก</a></li>
                <li class="upper-links"><a class="links" href="#">สินค้า</a></li>
                <li class="upper-links"><a class="links" href="cart.html">ตะกร้าสินค้า</a></li>
                <li class="upper-links"><a class="links" href="#">วิธีซื้อสินค้า</a></li>
                <li class="upper-links"><a class="links" href="#">เกี่ยวกับเรา</a></li>
                <li class="upper-links">
                   
                </li>
            </ul>
        </div>

        <div class="row row2">
            <div class="col-sm-2">
                <h2 style="margin:0px;"><span class="smallnav menu" onclick="openNav()">☰ AddPay Shops</span></h2>
                <div style="margin-top:-10px; margin-left:25px;"><a href="#"><img src="images/logo/logo_addpay1.png" class="largenav"></a></div>              
            </div>
            
            
            <div class="flipkart-navbar-search smallsearch col-sm-10 col-xs-11">
                <div class="row">
                    <input class="flipkart-navbar-input col-xs-11" type="" placeholder="ค้นหา" name="">
                    <button class="flipkart-navbar-button col-xs-1">
                        <svg width="15px" height="15px">
                            <path d="M11.618 9.897l4.224 4.212c.092.09.1.23.02.312l-1.464 1.46c-.08.08-.222.072-.314-.02L9.868 11.66M6.486 10.9c-2.42 0-4.38-1.955-4.38-4.367 0-2.413 1.96-4.37 4.38-4.37s4.38 1.957 4.38 4.37c0 2.412-1.96 4.368-4.38 4.368m0-10.834C2.904.066 0 2.96 0 6.533 0 10.105 2.904 13 6.486 13s6.487-2.895 6.487-6.467c0-3.572-2.905-6.467-6.487-6.467 "></path>
                        </svg>
                    </button>
                </div>
            </div>
          
        </div>
    </div>
</div>




<div id="mySidenav" class="sidenav">
    <div class="container" style="background-color: #9c1313; padding-top: 10px;">
        <span class="sidenav-heading">เมนู</span>
        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">×</a>
    </div>
    <a href="#">หน้าหลัก</a>
	<a href="#">อิเล็กทรอนิกส์</a>
	<a href="#">แฟชั่นสุภาพสตรี</a>
	<a href="#">แฟชั่นสุภาพบุรุษ</a>
	<a href="#">เครื่องใช้ไฟฟ้าและของในบ้าน</a>
	<a href="#">สุขภาพและความงาม</a>
	<a href="#">เด็กและของเล่น</a>
	<a href="#">กีฬาและการเดินทาง</a>
	<a href="#">ยานยนต์</a>
	<a href="#">สัตว์เลี้ยงและสื่อบันเทิง</a>
</div>
    

    <!-- Page Content -->
    <div class="container">

        <div class="row top10">

            

            <div class="col-md-9">
                

                <div class="row top10">

                    <div class="col-sm-4 col-lg-4 col-md-4">
                        <div class='container'>
    <div class='row' style='padding-top:25px; padding-bottom:25px;'>
        <div class='col-md-12'>
            <div id='mainContentWrapper'>
                <div class="col-md-12 col-md-offset-0">
                    <h2 style="text-align: center;">
                    <img src="images/market.png" width="100" height="97"> <img src="images/logo_thongchom.png" width="150"> <img src="images/IMG_31_110860_20160322_091552.jpg" width="100"><br> แบบคำขอลงทะเบียนผู้ประกอบการ โครงการตลาดต้องชม</h2>
               
                    
                    
                       
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-default">
                                    
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            <div class="items">
                                                <div class="col-md-12">
     

<center>
        <h2><b>รายชื่อผู้ลงทะเบียน</b></h2></center>
  
       
  <table width="400" border="0" align="center">
    <tr>
      <td>ค้นหาจาก :</td>
      <td><select name="zone" width="150">
        <option value="<?php $record ["province_regis"];?>" selected="selected">วัน เดือน ปี</option>
        <option value=" ">ชื่อผู้เข้าอบรม</option>
        <option value=" ">รายการประชุม</option>
        <option value=" ">สถานที่</option>
      </select></td>
      <td><input type="submit" value="ค้นหา" /></td>
    </tr>
  </table>
 <hr>

<?php
$objConnect = mysqli_connect("localhost","regis_all","go2122242362487","admin_addpay_regis_all") or die("Error Connect to Database");
// $objDB = mysqli_select_db("admin_addpay_regis_all");
$strSQL = "SELECT * FROM regis_market ";
$objConnect->set_charset("utf8");

// $result = mysqli_query($con,$query0) ;
$objQuery = mysqli_query($objConnect,$strSQL) or die ("Error Query [".$strSQL."]");
$Num_Rows = mysqli_num_rows($objQuery);

$Per_Page = 25;

$Page = $_GET["Page"];
if(!$_GET["Page"])
{
	$Page=1;
}

$Prev_Page = $Page-1;
$Next_Page = $Page+1;

$Page_Start = (($Per_Page*$Page)-$Per_Page);
if($Num_Rows<=$Per_Page)
{
	$Num_Pages =1;
}
else if(($Num_Rows % $Per_Page)==0)
{
	$Num_Pages =($Num_Rows/$Per_Page) ;
}
else
{
	$Num_Pages =($Num_Rows/$Per_Page)+1;
	$Num_Pages = (int)$Num_Pages;
}

$strSQL .=" order  by id ASC LIMIT $Page_Start , $Per_Page";
$objQuery  = mysqli_query($objConnect,$strSQL);
?>

<table width="100%" border="1" align="center" cellpadding="0" cellspacing="0" class="font" style="border-color:#dddddd">
        <thead>
            <tr>
            <th width="" height="30" align="center">ลำดับที่</th>
            <th width="" align="center">จังหวัดที่ลงทะเบียน</th>
            <th width="" align="center">วันที่ยื่นคำขอ</th>
            <th width="" align="center">ชื่อ - นามสกุล</th>
            <th width="" align="center">หมายเลขบัตรประจำตัวประชาชน</th>
            <th width="" align="center">ประเภทของธุรกิจ</th>
            <th width="" align="center">รายได้ต่อเดือน</th>
            <th width="" align="center">ที่อยู่ปัจจุบัน</th>
            <th width="" align="center">หมายเลขโทรศัพท์</th>
            <th width="" align="center">ไลน์ไดดี</th>
            <th width="" align="center">อีเมลล์</th>
            <th width="" align="center">สถานภาพผู้ประกอบการ</th>
            <th width="" align="center">ชื่อร้านค้า</th>
            <th width="" align="center">สินค้าที่จำหน่าย</th>
            <th width="" align="center">กลุ่มสินค้าที่ท่านต้องการจำหน่าย </th>
            <th width="" align="center">หน่วยงานที่รับลงทะเบียน</th>
      </thead>

<?php
$i = 1;
if($Page > 1)
{
	$i = ($Per_Page * ($Page-1)) + 1; 
}
while($objResult = mysqli_fetch_array($objQuery))
{
?>

<tr>
            <td height="31" align="center"><?php echo $i;?></td>
            <td align="center">   <?php echo $objResult ["province_regis"];?></td>
            <td height="26">  <?php echo $objResult ["date_regis"];?></td>
            <td align="center">   <?php echo $objResult ["name"];?> <?php echo $objResult ['sername']; ?></td>
            <td align="center"   ><?php echo $objResult ["id_card"];?></td>
            <td align="center">   <?php echo $objResult ["type_person"];?></td>
            <td align="center">   <?php echo $objResult ["incom_per"];?></td>
            <td align="center">   <?php echo $objResult ["address"];?> <?php echo $objResult ["sub_district"];?> <?php echo $objResult ["district"];?> <?php echo $objResult ["province"];?> <?php echo $objResult ["zipcode"];?></td>
            <td align="center">   <?php echo $objResult ["phone_num"];?></td>
            <td align="center">   <?php echo $objResult ["line_id"];?></td>
            <td align="center">   <?php echo $objResult ["e_mail"];?></td>
            <td align="center">   <?php echo $objResult ["status_market"];?></td>
            <td align="center">   <?php echo $objResult ["s_product"];?></td>
            <td align="center">   <?php echo $objResult ["n_product"];?></td>
            <td align="center">   <?php echo $objResult ["g_product"];?></td>
            <td align="center">   <?php echo $objResult ["org_agency"];?></td>
        </tr>

<?php
		$i++;
}
?>
</table>

<br>
Total <?php echo $Num_Rows;?> Record : <?php echo $Num_Pages;?> Page :
<?php
if($Prev_Page)
{
	echo " <a href='$_SERVER[SCRIPT_NAME]?Page=$Prev_Page'><< Back</a> ";
}

for($i=1; $i<=$Num_Pages; $i++){
	if($i != $Page)
	{
		echo "[ <a href='$_SERVER[SCRIPT_NAME]?Page=$i'>$i</a> ]";
	}
	else
	{
		echo "<b> $i </b>";
	}
}
if($Page!=$Num_Pages)
{
	echo " <a href ='$_SERVER[SCRIPT_NAME]?Page=$Next_Page'>Next>></a> ";
}
mysqli_close($objConnect);
?>        
        
              

                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                         
                           
        </div>
    </div>



                    </div>


               
                </div>

            </div>

        </div>

    </div>
    <!-- /.container -->

    <div class="container">

        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <p>© 2016 Addpay Service Point Co., Ltd. All rights reserved.</p>
<p>บริษัท แอดเพย์ เซอร์วิสพอยท์ จำกัด เลขที่ 406 หมู่ 18 ต.ขามใหญ่ อ.เมือง จ.อุบลราชธานี 34000</p>
<p>โทร. 045-317123,061-8182888 Fax.045-317678</p>
            </div>
        </footer>

    </div>
    <!-- /.container -->





    
    

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
        <script src="js/mainmenu.js"></script>
        <script src="http://momentjs.com/downloads/moment-with-locales.js"></script>
		<script src="http://momentjs.com/downloads/moment-timezone-with-data.js"></script>

  </body>
</html>