<?php

/**
	Author : Witoon Pomipon
	Company : Codebee Company Limited
	Website : www.codebee.co.th
	Email : witoon@codebee.co.th
*/

defined('BASEPATH') OR exit('No direct script access allowed');

class Payment extends CI_Controller {

	
	public function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{
		
	}
	public function summary()
	{
		// เมธอด หลังจากชำระเงิน
	}
	public function ordercomplete()
	{
		// เมธอด เมื่อการสั่งซื้อสำเร็จ ไว้รับค่าตัวแปรจาก paypal
	}
	
	public function pay()
	{
		$config['business'] 			= 'onrampa.s@gmail.com'; // อีเมลบัญชีบิสิเนสผู้รับเงิน
		$config['cpp_header_image'] 	= base_url("assets/img/logo.png");  // รูปที่จะแสดงในหน้าทำธุรกรรม
		$config['return'] 				= site_url("payment/summary"); // หน้าที่จะแสดงหลังจากทำธุรกรรมเสร็จ
		$config['cancel_return'] 		= site_url("payment"); // หน้าที่จะแสดงกรณีกดปุ่มยกเลิก
		$config['notify_url'] 			= site_url("payment/ordercomplete"); // หน้าที่เราจะใช้รับข้อมูลในกรณีการสั่งซื้อสำเร็จเสร็จสิ้นด้วยดี
		$config['production'] 			= FALSE; // ระบุการทดสอบว่าอยู่ในขั้นตอนพัฒนาหรือใช้งานจริง
		$config["invoice"]				= "IV0001";  // ระบุเลขที่ใบแจ้งชำระเงิน
		$config["currency_code"]		= "THB"; // ระบุค่าเงินในการทำธุรธรรม
		
		$config["first_name"] 		= $row->fname; // ชื่อจริงผู้สั่งซื้อ
		$config["last_name"] 		= $row->lname; // นามสกุลผู้สั่งซื้อ
		$config["address1"] 		= $row->address; // ที่อยู่ช่องแรก
		$config["address2"] 		= $row->company; // ที่อยู่ช่องสอง ถ้ามี
		$config["city"] 			= $row->city; // เมือง
		$config["state"] 			= $row->state; // รัฐ
		$config["zip"] 				= $row->postcode; // รหัสไปรษณีย์
		$config["email"] 			= $row->email; // อีเมลผู้สั่งซื้อ
		$config["night_phone_a"] 	= $row->phone; // เบอร์โทร 1
		$config["night_phone_b"] 	= $row->mobile; // เบอร์โทร 2
		
		$this->load->library('paypal',$config); // โหลด paypal library มาใช้งานและกำหนดการตั้งค่าใหม่
		$this->paypal->add("ชื่อสินค้า","ราคาต่อหนวย","จำนวนรวม"); // ระบุชื่อสินค้า จำนวน และราคา
		$this->paypal->pay(); // เริ่มส่งข้อมูลไปให้ paypal เพื่อเริ่มขั้นตอนการชำระเงิน
	}
	
}
