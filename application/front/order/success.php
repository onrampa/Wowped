<?php
// if ($_SERVER['HTTP_REFERER'] != base_url() . "/order" OR $_SESSION[_ef . 'mform'] != "ict.addpay.co.th") {
//     header("location:" . base_url());
// }
// unset($_SESSION[_ef . 'mform']);
/*
 * php code///////////**********************************************************
 */
$title = 'ทำรายการสั่งซื้อสำเร็จ ';

/*
 * php code///////////**********************************************************
 */

/*
 * header***********************************************************************
 */
require 'template/front/header.php';
/*
 * header***********************************************************************
 */
?>
<div class="container">
<div class="container-body">
    <div class="panel-body">
    <div class="col-sm-9 blog-main">
    <div class="panel panel-default">
                <div class="panel-body">
            <div class="row" style="font-size:13px;">
                <div class="alert alert-success" role="alert" style="margin:15px;">
                    <p><strong>ทำรายการสั่งซื้อสำเร็จ!</strong></p>
                    <p>หมายเลขสั่งซื้อของคุณคือ <strong><?php echo @$_SESSION[_ef . 'order_id'];?></strong></p>
                    <p>หากต้องการกลับหน้าแรก
                    <a href="<?php echo base_url(); ?>" class="alert-link"><h3>คลิกที่นี่</h3></a>เพื่อกลับไปหน้าหลัก
                </div>
            </div>
        </div><!-- /.blog-main -->

    </div><!-- /.row -->

</div><!-- /.container -->
<?php
/*
 * footer***********************************************************************
 */
unset($_SESSION[_ef . 'order_id']);
require 'template/front/footer.php';
/*
 * footer***********************************************************************
 */