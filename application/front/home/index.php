
<?php
/*
 * php code///////////**********************************************************
 */
$title = 'ร้านอาหาร Online';

require 'library/pagination.php';

$db = new database();
$option_cat = array(
    "table" => "product_categories"
);
$query_cat = $db->select($option_cat); // catgorie

$option_shop = array(
    "table" => "users",
    "condition" => "Cclose='1'"
);
$query_shop = $db->select($option_shop); // shop

$sql_pc = "SELECT * FROM v_product ";
$query_pc = $db->query($sql_pc);
// echo $sql_pc;
$pagination = new Zebra_Pagination();

$rows_pc = $db->rows($query_pc);

$per_page = 40;
$page_start = (($pagination->get_page() - 1) * $per_page);
$sql_pc .= "ORDER BY cat, pname ASC LIMIT {$page_start},{$per_page} ";
$pagination->records($rows_pc);
$pagination->records_per_page($per_page);
$pagination->base_url('', FALSE);
$query_pc_page = $db->query($sql_pc);


$page = ($page_start != 0) ? $page_start : "1";
$pages = ceil($rows_pc / $per_page);

$uri = $_SERVER['REQUEST_URI']; // url
/*
 * php code///////////**********************************************************
 */

/*
 * header***********************************************************************
 */
require 'template/front/header.php';
/*
 * header***********************************************************************
 */
?>

    <!--content div -->
    <div class="col-md-8" style="margin-top: 60px">

    <div class="visible-md visible-lg" style="color:white">
        <div style="background-color: rgba(159, 27, 48, 0.9);margin-top: -60px;border-radius:10px;height: 40px">
            <div style="padding:10px;text-align: center;font-size: large"><p>เมนู ว้าวเป็ด</p>
            <input type="text" id="location" name="location"        />
            </div>
        </div>
    </div>


<?php
$i = 0;
while ($rs_pc = $db->get($query_pc_page)) {
    $tr = ($i % 2 == 0) ? "odd" : "even";
    $dis = ($rs_pc['priceP'] - $rs_pc['price']);
    if ($rs_pc['priceP'] > 0) {
        $ddis = ($dis * 100) / $rs_pc['priceP'];
    }
    ?>

    <div class="hidden-xs col-md-3 col-sm-3">
        <form class="form-inline" role="form"
              action="<?php echo base_url(); ?>/cart/update/<?php echo $rs_pc['id']; ?>" method="post">
            <div class="thumbnail">
                <?php if ($dis > 0) { ?>
                    <div class="shop-item-card__badge-wrapper">
                        <div class="shop-badge shop-badge--fixed-width shop-badge--promotion">
                            <div class="shop-badge--promotion__label-wrapper shop-badge--promotion__label-wrapper--th-TH">
                                        <span style="font-size: 18px;"
                                              class="shop-badge--promotion__label-wrapper__off-label shop-badge--promotion__label-wrapper__off-label--th-TH">ลด</span>
                                <span style="font-size: 18px;"
                                      class="percent"><?php echo number_format($ddis, 2); ?>%</span>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                <div class="jcrop">
                    <a href="<?php echo $baseUrl; ?>/product/view/<?php echo $rs_pc['id']; ?>">
                        <!--                            <img style="height: 160px!important;"-->
                        <img class="jresize"
                             src="<?php echo $baseUrl; ?>/upload/product/thumb_<?php echo $rs_pc['image']; ?>"
                             alt="<?php echo $rs_pc['pname']; ?>">
                    </a>
                </div>
                 <input type="checkbox"  id="home" name="home" value="1"> **กลับบ้าน
                <button type="submit" class="order-add-button">
                    <span class="glyphicon glyphicon-plus-sign"></span> <?php echo " " . $rs_pc['pname']; ?>
                </button>
                <div class="caption">
                    <p style="font-size: 16px;font-weight: bold;color: red;">ราคา :
                        <?php echo number_format($rs_pc['price'], 1) . " ฿"; ?>
                        <strike>
                            <?php if ($rs_pc['priceP'] > $rs_pc['price']) {
                                echo number_format($rs_pc['priceP'], 1) . "-";
                            } ?>
                        </strike>
                    </p>
                    <p style="font-size: 16px;width=100%">
                        <input type="hidden" class="form-control" id="text2" autocomplete="off" name="qty"
                               placeholder="ใส่จำนวน" data-validation="number"
                               data-validation-allowing="float" value="1"></p>
                </div>
        </form>
    </div>
    </div>



<?php } ?>

    <div class="visible-xs col-xs-12" style="margin-top: -40px">
        <table id="mytable" class="table table-striped table-bordered table-list " style="background: white">
            <thead>
            <th>รายการอาหาร</th>
            <th>ใส่ห่อ</th>
            <th>สั่งอาหาร</th>
            </thead>
            <tbody>
            <?php

            $sql_pcx .= "ORDER BY cat, pname ASC";
            $query_pc_pagex = $db->query($sql_pc);

            while ($rs_pcx = $db->get($query_pc_pagex)) {
                ?>
                <tr>
                    <form class="form-inline" role="form"
                          action="<?php echo base_url(); ?>/cart/update/<?php echo $rs_pcx['id']; ?>" method="post">
                        <td> <?php echo $rs_pcx['pname']; ?></td>
                        <td>
                        <input type="checkbox"  id="home" name="home" value="1">
                       
                        </td>
                        <td>
                            <button type="submit" class="order-add-button">
                                <span class="glyphicon glyphicon-plus-sign"></span>
                                ราคา :

                                <?php if ($rs_pcx['priceP'] > $rs_pcx['price']) {
                                    echo number_format($rs_pcx['priceP'], 0) . "-";
                                }else{
                                    echo number_format($rs_pcx['price'], 0) . " ฿";
                                }

                                ?>
                                <p style="font-size: 16px;width=100%">
                                    <input type="hidden" class="form-control" id="text2" autocomplete="off" name="qty"
                                           placeholder="ใส่จำนวน" data-validation="number"
                                           data-validation-allowing="float" value="1"></p>
                            </button>
                        </td>
                    </form>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>

    </div><!--  end content div-->


<?php $pagination->render(); ?>


<?php
/*
 * footer***********************************************************************
 */
require 'template/front/footer.php';
/*
 * footer***********************************************************************
 */