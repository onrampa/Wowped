<?php
/*
 * php code///////////**********************************************************
 */
$title = 'หน้าแรก ร้านค้าออนไลน์ สะดวกในการซื้อ ประหยัดในทุกการใช้จ่าย ปลอดภัยในทุกขั้นตอน';

require 'library/pagination.php';

$db = new database();
$option_cat = array(
    "table" => "product_categories"
);
$query_cat = $db->select($option_cat); // catgorie

$option_shop = array(
    "table" => "shop",
         "condition" => "Cclose <> 0 "
);
$query_shop = $db->select($option_shop); // shop

$sql_pc = "SELECT p.id, p.name AS pname, p.price , p.image, c.name AS cname, p.brandname,s.name AS shopName ";
$sql_pc .= "FROM products p INNER JOIN product_categories c ON p.product_categorie_id = c.id INNER JOIN shop s ON p.shop=s.id ";
$query_pc = $db->query($sql_pc);
$pagination = new Zebra_Pagination();

$rows_pc = $db->rows($query_pc);

$per_page = 21;
$page_start = (($pagination->get_page() - 1) * $per_page);
$sql_pc .= "ORDER BY id DESC LIMIT {$page_start},{$per_page} ";
$pagination->records($rows_pc);
$pagination->records_per_page($per_page);
$pagination->base_url('', FALSE);
$query_pc_page = $db->query($sql_pc);

$page = ($page_start!=0) ? $page_start : "1";
$pages = ceil($rows_pc/$per_page);

$uri = $_SERVER['REQUEST_URI']; // url
/*
 * php code///////////**********************************************************
 */

/*
 * header***********************************************************************
 */
require 'template/front/header.php';
/*
 * header***********************************************************************
 */
?>
<div class="container">

    <div class="blog-header">
        <h1 class="blog-title">ผลิตภัณฑ์ ร้านค้าออนไลน์</h1>
        <p class="lead blog-description">ร้านค้าออนไลน์ สะดวกในการซื้อ ประหยัดในทุกการใช้จ่าย ปลอดภัยในทุกขั้นตอน</p>
    </div>
    <div class="row">
        <div class="col-sm-3 blog-sidebar">
            <div class="panel panel-primary">
                <!-- Default panel contents -->
                <div class="panel-heading">ประเภทสินค้า</div>
                <!-- List group -->
                <ul class="list-group">
                    <?php
                    while ($rs_cat = $db->get($query_cat)) {
                        ?>
                        <li class="list-block"><a href="<?php echo $baseUrl; ?>/categorie/<?php echo $rs_cat['id']; ?>"><?php echo $rs_cat['name']; ?></a></li>
                    <?php } ?>
                </ul>
            </div>
      
         <div class="navbar-default navbar-static-side" role="navigation">
                    <div class="sidebar-collapse-primary">
                <div class="panel-heading">ชื่อร้านค้า</div>
                        <ul id="side-menu" class="nav">
                   <?php
                    while ($rs_shop = $db->get($query_shop)) {
                        ?>
                        <li class="list-block"><a href="<?php echo $baseUrl; ?>/shop/<?php echo $rs_shop['id']; ?>"><?php echo $rs_shop['name']; ?></a></li>
                    <?php } ?>
                      
                        </ul>
                    </div>
                    </div>
              </div><!-- /.blog-sidebar --> 
        <div class="col-sm-9 blog-main">
            <div class="panel panel-default">
                             
                <div class="panel-body">

                    <div class="row">
   <?php
                        $i = 0;
                        while ($rs_pc = $db->get($query_pc_page)) {
                            $tr = ($i % 2 == 0) ? "odd" : "even";
                            ?>

                            <div class="col-sm-6 col-md-4">
                                <div class="thumbnail" style="height: 330px!important;">
                                  <form class="form-inline" role="form" action="<?php echo base_url();?>/cart/update/<?php echo $rs_pc['id'];?>" method="post">
                                    <a href="<?php echo $baseUrl; ?>/product/view/<?php echo $rs_pc['id']; ?>">
                                        <img src="<?php echo $baseUrl; ?>/upload/product/thumb_<?php echo $rs_pc['image']; ?>" alt="<?php echo $rs_pc['pname']; ?>" height="170">
                                    </a>
                                    <div class="caption">
                                        <a href="<?php echo $baseUrl; ?>/product/view/<?php echo $rs_pc['id']; ?>"  style="font-size: 13px;"><?php echo $rs_pc['pname']; ?></a>
                                        <p style="font-size: 13px;font-weight: bold;color: red;">ราคา : <?php echo number_format($rs_pc['price']); ?> บาท</p>
                                        <p style="font-size: 13px;">ร้านค้า : <?php echo $rs_pc['shopName']; ?></p>
                                        <p>
                                    <a href="<?php echo $baseUrl; ?>/product/view/<?php echo $rs_pc['id']; ?>" class="btn btn-default" role="button">รายละเอียด</a>
                                    <input type="hidden" class="form-control" id="text2" autocomplete="off" name="qty" placeholder="ใส่จำนวน" data-validation="number" data-validation-allowing="float"value="1">
                                
                                <button type="submit" class="btn btn-success">Shop</button>
                                 </p>
                            </form>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                               
                    <?php $pagination->render(); ?>
                </div>
            </div>
        </div><!-- /.blog-main -->

    </div><!-- /.row -->

</div><!-- /.container -->
<?php
/*
 * footer***********************************************************************
 */
require 'template/front/footer.php';
/*
 * footer***********************************************************************
 */