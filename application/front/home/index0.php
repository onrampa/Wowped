<?php
/*
 * php code///////////**********************************************************
 */
$title = 'สินค้าชุมชน ขับเคลื่อนเศรษฐกิจฐานราก Village E-Commerce';

require 'library/pagination.php';

$db = new database();



$sql_pc = "SELECT * ";
$sql_pc .= "FROM v_product ";
$query_pc = $db->query($sql_pc);
// echo $sql_pc;
$pagination = new Zebra_Pagination();

$rows_pc = $db->rows($query_pc);

$per_page = 12;
$page_start = (($pagination->get_page() - 1) * $per_page);
$sql_pc .= "ORDER BY id DESC LIMIT {$page_start},{$per_page} ";
$pagination->records($rows_pc);
$pagination->records_per_page($per_page);
$pagination->base_url('', FALSE);
$query_pc_page = $db->query($sql_pc);

$page = ($page_start!=0) ? $page_start : "1";
$pages = ceil($rows_pc/$per_page);
$uri = $_SERVER['REQUEST_URI']; // url
/*
 * php code///////////**********************************************************
 */

/*
 * header***********************************************************************
 */
require 'template/front/header.php';
/*
 * header***********************************************************************
 */
?>



<!-- Item slider-->
<div class="blog-header">
    <div class="row" id="slider-text">
      <div class="col-md-12" >
      <center><h3>สินค้าแนะนำ</h3> </center>
      </div>
    </div>
<?php
$sql_s1 = "SELECT * ";
$sql_s1 .= "FROM v_product Where shop='10007421' ORDER BY id DESC  ";
$query_s1 = $db->query($sql_s1); 
?>
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="carousel carousel-showmanymoveone slide" id="itemslider">
        <div class="carousel-inner">
        <?php
        $s1=0;
        $i="active";
        while ($rs_s1 = $db->get($query_s1)) {
            $dis=($rs_s1['priceP']-$rs_s1['price']);
            $ddis=($dis*100)/$rs_s1['priceP'];
            
        ?>
          <div class="item <?php echo $i; ?>">
            <div class="col-xs-12 col-sm-6 col-md-2">
              <a href="#"><img src="<?php echo $baseUrl; ?>/upload/product/thumb_<?php echo $rs_s1['image']; ?>" alt="<?php echo $rs_s1['pname']; ?>" class="img-responsive center-block"></a>
              <?php if($dis>0){  ?>  <span class="badge"><?php echo number_format($ddis,0);  ?>%</span><?php } ?>
              <h4 class="text-center"><?php echo $rs_s1['pname']; ?></h4>
              <h5 class="text-center" style="font-size: 13px;font-weight: bold;color: red;">ราคา : <?php echo number_format($rs_s1['price']).".-"; ?> <strike><?php if($rs_s1['priceP']>$rs_s1['price']){echo number_format($rs_s1['priceP']).".-"; } ?></strike> / ชิ้น  </h5>
            </div>
          </div>
          <?php $i="";$s1++; } ?>

        </div>

        <div id="slider-control">
        <a class="left carousel-control" href="#itemslider" data-slide="prev"><img src="https://s12.postimg.org/uj3ffq90d/arrow_left.png" alt="Left" class="img-responsive"></a>
        <a class="right carousel-control" href="#itemslider" data-slide="next"><img src="https://s12.postimg.org/djuh0gxst/arrow_right.png" alt="Right" class="img-responsive"></a>
      </div>
      </div>
    </div>
  </div>
</div>
<!-- สิ้นสุด Slide สินค้า-->

<!-- Item slider-->
<div class="blog-slide">
<div class="row" id="slider-text">
      <div class="col-md-12" >

        <center><h3>   สินค้า HOT</h3> </center>
      </div>
    </div>
<?php

$sql_catN = "SELECT * ";
$sql_catN .= "FROM v_product Where sell >10  ORDER BY RAND()  ";
$query_catN = $db->query($sql_catN); 
?>
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="carousel carousel-showmanymoveone slide" id="itemslider1">
        <div class="carousel-inner">
        <?php
        $catN=0;
        $n="active";
        while ($rs_catN = $db->get($query_catN)) {
            $dis=($rs_catN['priceP']-$rs_catN['price']);
            $ddis=($dis*100)/$rs_catN['priceP'];
            
        ?>
          <div class="item <?php echo $n; ?>">
            <div class="col-xs-6 col-sm-6 col-md-2">
              <a href="#"><img src="<?php echo $baseUrl; ?>/upload/product/thumb_<?php echo $rs_catN['image']; ?>" alt="<?php echo $rs_catN['pname']; ?>" class="img-responsive center-block"></a>
              <span class="badge"> <?php if($dis>0){  ?>  <?php echo number_format($ddis,0);  ?>%</span><?php } ?>
             </br>
              <h4 class="text-center" style="font-size: 13px;"><?php echo $rs_catN['pname']; ?></h4>
              <h5 class="text-center" style="font-size: 13px;font-weight: bold;color: red;">ราคา : <?php echo number_format($rs_catN['price']).".-"; ?> <strike><?php if($rs_catN['priceP']>$rs_catN['price']){echo number_format($rs_catN['priceP']).".-"; } ?></strike> / ชิ้น  </h5>
            </div>
          </div>
          <?php $n="";$catN++; } ?>

        </div>

        <div id="slider-control">
        <a class="left carousel-control" href="#itemslider1" data-slide="prev"><img src="https://s12.postimg.org/uj3ffq90d/arrow_left.png" alt="Left" class="img-responsive"></a>
        <a class="right carousel-control" href="#itemslider1" data-slide="next"><img src="https://s12.postimg.org/djuh0gxst/arrow_right.png" alt="Right" class="img-responsive"></a>
      </div>
      </div>
    </div>
  </div>
</div>
<!-- สิ้นสุด Slide สินค้า-->

<div class="container">     
        
  <?php
  $i = 0;
  while ($rs_pc = $db->get($query_pc_page)) {
      $tr = ($i % 2 == 0) ? "odd" : "even";
      $dis=($rs_pc['priceP']-$rs_pc['price']);
      $ddis=($dis*100)/$rs_pc['priceP'];
      ?>

      <div class="col-md-3 col-md-3">
                            
    <div class="thumbnail" >
    <?php if($dis>0){  ?>    
    <div class="shop-item-card__badge-wrapper">
    <div class="shop-badge shop-badge--fixed-width shop-badge--promotion">
    <div class="shop-badge--promotion__label-wrapper shop-badge--promotion__label-wrapper--th-TH">                                                            
    <span style="font-size: 18px;" class="shop-badge--promotion__label-wrapper__off-label shop-badge--promotion__label-wrapper__off-label--th-TH">ลด</span>
    <span style="font-size: 18px;" class="percent"><?php echo number_format($ddis, 2);  ?>%</span>
    </div></div></div>
    <?php } ?>
      <form class="form-inline" role="form" action="<?php echo base_url();?>/cart/update/<?php echo $rs_pc['id'];?>" method="post">
        <a href="<?php echo $baseUrl; ?>/product/view/<?php echo $rs_pc['id']; ?>">
            <img  src="<?php echo $baseUrl; ?>/upload/product/thumb_<?php echo $rs_pc['image']; ?>" alt="<?php echo $rs_pc['pname']; ?>">
        </a>
                                    
                                    
      <div class="caption">
          <a href="<?php echo $baseUrl; ?>/product/view/<?php echo $rs_pc['id']; ?>"  style="font-size: 13px;"><?php $str_check = strlen($rs_pc['pname']);
 							if ($str_check >= 30) {
	 						$sheck_num = mb_substr($rs_pc['pname'], 0, 25);
	 						echo $sheck_num. "...";
 								}
 							else {
							echo $rs_pc['pname'];
								}
						?></a>
            <p style="font-size: 13px;font-weight: bold;color: red;">ราคา : <?php echo number_format($rs_pc['price']).".-"; ?> <strike><?php if($rs_pc['priceP']>$rs_pc['price']){echo number_format($rs_pc['priceP']).".-"; } ?></strike> / ชิ้น </p>
            <p style="font-size: 13px;">ร้านค้า : <?php echo $rs_pc['shopName']; ?></p>
            <p>
        <a href="<?php echo $baseUrl; ?>/product/view/<?php echo $rs_pc['id']; ?>" class="btn btn-default" role="button">รายละเอียด</a>
        <input type="hidden" class="form-control" id="text2" autocomplete="off" name="qty" placeholder="ใส่จำนวน" data-validation="number" data-validation-allowing="float"value="1">
        <button type="submit" class="btn btn-success">
                <svg class="cart-svg " width="16 " height="16 " viewBox="0 0 16 16 ">
                <path d="M15.32 2.405H4.887C3 2.405 2.46.805 2.46.805L2.257.21C2.208.085 2.083 0 1.946 0H.336C.1 0-.064.24.024.46l.644 1.945L3.11 9.767c.047.137.175.23.32.23h8.418l-.493 1.958H3.768l.002.003c-.017 0-.033-.003-.05-.003-1.06 0-1.92.86-1.92 1.92s.86 1.92 1.92 1.92c.99 0 1.805-.75 1.91-1.712l5.55.076c.12.922.91 1.636 1.867 1.636 1.04 0 1.885-.844 1.885-1.885 0-.866-.584-1.593-1.38-1.814l2.423-8.832c.12-.433-.206-.86-.655-.86 " fill="#fff "></path>
            </svg>หยิบ
        </button>


                                 </p>
                            </form>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    
                </div>
                               
                    <?php $pagination->render(); ?>
                </div>
            </div>
            </div>
        </div><!-- /.blog-main -->

    </div><!-- /.row -->

</div><!-- /.container -->
<?php
/*
 * footer***********************************************************************
 */
require 'template/front/footer.php';
/*
 * footer***********************************************************************
 */