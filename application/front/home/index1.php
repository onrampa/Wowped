<?php
/*
 * php code///////////**********************************************************
 */
$title = 'สินค้าชุมชน ขับเคลื่อนเศรษฐกิจฐานราก Village E-Commerce';

require 'library/pagination.php';

$db = new database();
$option_cat = array(
    "table" => "product_categories"
);
$query_cat = $db->select($option_cat); // catgorie

$option_shop = array(
    "table" => "users",
      "condition" => "Cclose='1'"
);
$query_shop = $db->select($option_shop); // shop

$sql_pc = "SELECT * ";
$sql_pc .= "FROM v_product ";
$query_pc = $db->query($sql_pc);
// echo $sql_pc;
$pagination = new Zebra_Pagination();

$rows_pc = $db->rows($query_pc);

$per_page = 30;
$page_start = (($pagination->get_page() - 1) * $per_page);
$sql_pc .= "ORDER BY id DESC LIMIT {$page_start},{$per_page} ";
$pagination->records($rows_pc);
$pagination->records_per_page($per_page);
$pagination->base_url('', FALSE);
$query_pc_page = $db->query($sql_pc);

$page = ($page_start!=0) ? $page_start : "1";
$pages = ceil($rows_pc/$per_page);

$uri = $_SERVER['REQUEST_URI']; // url
/*
 * php code///////////**********************************************************
 */

/*
 * header***********************************************************************
 */
require 'template/front/header.php';
/*
 * header***********************************************************************
 */
?>
<div class="container">
  <div class="row" id="slider-text">
    <div class="col-md-6" >
      <h3>สินค้ายอดฮิต</h3>
    </div>
  </div>
</div>

<!-- Item slider-->
<div class="container-fluid">

  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
      <div class="carousel carousel-showmanymoveone slide" id="itemslider">
        <div class="carousel-inner">

          <div class="item active">
            <div class="col-xs-12 col-sm-6 col-md-2">
              <a href="#"><img src="https://s12.postimg.org/655583bx9/item_1_180x200.png" class="img-responsive center-block"></a>
              <h4 class="text-center">MAYORAL SUKNJA</h4>
              <h5 class="text-center">4000,00 RSD</h5>
            </div>
          </div>

          <div class="item">
            <div class="col-xs-12 col-sm-6 col-md-2">
              <a href="#"><img src="https://s12.postimg.org/41uq0fc4d/item_2_180x200.png" class="img-responsive center-block"></a>
              <h4 class="text-center">MAYORAL KOŠULJA</h4>
              <h5 class="text-center">4000,00 RSD</h5>
            </div>
          </div>

          <div class="item">
            <div class="col-xs-12 col-sm-6 col-md-2">
              <a href="#"><img src="https://s12.postimg.org/dawwajl0d/item_3_180x200.png" class="img-responsive center-block"></a>
              <span class="badge">10%</span>
              <h4 class="text-center">PANTALONE TERI 2</h4>
              <h5 class="text-center">4000,00 RSD</h5>
              <h6 class="text-center">5000,00 RSD</h6>
            </div>
          </div>

          <div class="item">
            <div class="col-xs-12 col-sm-6 col-md-2">
              <a href="#"><img src="https://s12.postimg.org/5w7ki5z4t/item_4_180x200.png" class="img-responsive center-block"></a>
              <h4 class="text-center">CVETNA HALJINA</h4>
              <h5 class="text-center">4000,00 RSD</h5>
            </div>
          </div>

          <div class="item">
            <div class="col-xs-12 col-sm-6 col-md-2">
              <a href="#"><img src="https://s12.postimg.org/e2zk9qp7h/item_5_180x200.png" class="img-responsive center-block"></a>
              <h4 class="text-center">MAJICA FOTO</h4>
              <h5 class="text-center">4000,00 RSD</h5>
            </div>
          </div>

          <div class="item">
            <div class="col-xs-12 col-sm-6 col-md-2">
              <a href="#"><img src="https://s12.postimg.org/46yha3jfh/item_6_180x200.png" class="img-responsive center-block"></a>
              <h4 class="text-center">MAJICA MAYORAL</h4>
              <h5 class="text-center">4000,00 RSD</h5>
            </div>
          </div>

        </div>

        <div id="slider-control">
        <a class="left carousel-control" href="#itemslider" data-slide="prev"><img src="https://s12.postimg.org/uj3ffq90d/arrow_left.png" alt="Left" class="img-responsive"></a>
        <a class="right carousel-control" href="#itemslider" data-slide="next"><img src="https://s12.postimg.org/djuh0gxst/arrow_right.png" alt="Right" class="img-responsive"></a>
      </div>
      </div>
    </div>
  </div>
</div>
<!-- สิ้นสุด Slide สินค้า-->
<!--Start Shop Card-->
<div class="container">
     
        
        <div class="col-sm-12 blog-main">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
   <?php
                        $i = 0;
                        while ($rs_pc = $db->get($query_pc_page)) {
                            $tr = ($i % 2 == 0) ? "odd" : "even";
                            $dis=($rs_pc['priceP']-$rs_pc['price']);
                            $ddis=($dis*100)/$rs_pc['priceP'];
                            ?>
                            <div class="col-sm-5 col-md-2 col-xs-6"> 
                                <div class="thumbnail" style="height: 330px!important;">
                                <?php if($dis>0){  ?>    
                                    <div class="shop-item-card__badge-wrapper">
                                    <div class="shop-badge shop-badge--fixed-width shop-badge--promotion">
                                    <div class="shop-badge--promotion__label-wrapper shop-badge--promotion__label-wrapper--th-TH">                                                            
                                    <span style="font-size: 18px;" class="shop-badge--promotion__label-wrapper__off-label shop-badge--promotion__label-wrapper__off-label--th-TH">ลด</span>
                                    <span style="font-size: 18px;" class="percent"><?php echo number_format($ddis, 2);  ?>%</span>
                                    </div></div></div>
                                    <?php } ?>
                                  <form class="form-inline" role="form" action="<?php echo base_url();?>/cart/update/<?php echo $rs_pc['id'];?>" method="post">
                                    <a href="<?php echo $baseUrl; ?>/product/view/<?php echo $rs_pc['id']; ?>">
                                        <img style="height: 170px!important;" src="<?php echo $baseUrl; ?>/upload/product/thumb_<?php echo $rs_pc['image']; ?>" alt="<?php echo $rs_pc['pname']; ?>">
                                    </a>
                                    <div class="caption">
                                    <a href="<?php echo $baseUrl; ?>/product/view/<?php echo $rs_pc['id']; ?>"  style="font-size: 13px;">
                                    <?php $str_check = strlen($rs_pc['pname']);
                                                      if ($str_check > 20) {
                                                      $sheck_num = mb_substr($rs_pc['pname'], 0, 20);
                                                      echo $sheck_num. "...";                   }
                                                      else {     echo $rs_pc['pname'];          }                         
                                    ?>
                                    </a>
                                                                 
                                                                 
                          <p style="font-size: 13px;font-weight: bold;color: red;">฿ : <?php echo number_format($rs_pc['price']).".-"; ?> <strike><?php if($rs_pc['priceP']>$rs_pc['price']){echo number_format($rs_pc['priceP']).".-"; } ?></strike> /ชิ้น</p>
                          <p style="font-size: 13px;">: 
                          <?php $str_checkN = strlen($rs_pc['shopName']);
                                    if ($str_check >= 17) {
                                        $sheck_numN = mb_substr($rs_pc['shopName'], 0, 17);
                                                echo $sheck_numN. "...";                   }
                                else {     echo $rs_pc['shopName'];          }                         
                           ?>
                        
                          </p>
                          <p style="font-size: 14px;width=100%">
                          <input type="hidden" class="form-control" id="text2" autocomplete="off" name="qty" placeholder="ใส่จำนวน" data-validation="number" data-validation-allowing="float"value="1">
                                         <button type="submit" class="cart-button">
                                                 <svg class="cart-svg " width="16 " height="16 " viewBox="0 0 16 16 ">
                                                 <path d="M15.32 2.405H4.887C3 2.405 2.46.805 2.46.805L2.257.21C2.208.085 2.083 0 1.946 0H.336C.1 0-.064.24.024.46l.644 1.945L3.11 9.767c.047.137.175.23.32.23h8.418l-.493 1.958H3.768l.002.003c-.017 0-.033-.003-.05-.003-1.06 0-1.92.86-1.92 1.92s.86 1.92 1.92 1.92c.99 0 1.805-.75 1.91-1.712l5.55.076c.12.922.91 1.636 1.867 1.636 1.04 0 1.885-.844 1.885-1.885 0-.866-.584-1.593-1.38-1.814l2.423-8.832c.12-.433-.206-.86-.655-.86 " fill="#fff "></path>
                                             </svg>หยิบ
                                         </button>
                                                          </p>
                             </form>

                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                                   <div>
                    <?php $pagination->render(); ?>
                <!-- </div> -->
                </div>

            </div>

        </div><!-- /.blog-main -->

    </div><!-- /.row -->

</div><!-- /.container -->
<?php
/*
 * footer***********************************************************************
 */
require 'template/front/footer.php';
/*
 * footer***********************************************************************
 */