<?php

/*
 * php code///////////**********************************************************
 */
$db = new database();
$option_pc = array(
    "table" => "users"
);
$query_pc = $db->select($option_pc);

//$db = new database();
$option_s = array(
    "table" => "shop"
);
$query_s = $db->select($option_s);

$option_pv = array(
    "table" => "province"
);
$query_pv = $db->select($option_pv);

$title = 'สร้าง ผู้ใช้ใหม่';
/*
 * php code///////////**********************************************************
 */

/*
 * header***********************************************************************
 */
require 'template/front/header.php';
/*
 * header***********************************************************************
 */
?>
<script type="text/javascript" src="<?php echo $baseUrl; ?>/js/jquery.form-validator.min.js"></script>
<script type="text/javascript" src="<?php echo $baseUrl; ?>/ckeditor/ckeditor.js"></script>
<div class="container">
<div id="page-warpper">
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header">เพิ่มผู้ใช้ใหม่</h3>
        </div>
    </div>
  
    <div class="row">
   <div class="col-lg-10">
     <div class="form-horizontal" style="margin-top: 10px;">
         <form id="user-form" action="<?php echo $baseUrl; ?>/back/user/form_create" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                        <label for="Shop_image" class="col-sm-2 control-label">รูป สมาชิก</label>
                        <div class="col-sm-4">
                            <input type="file" name="image" id="image">
                        </div>
                    </div>
         <div class="form-group">
           <label class="col-sm-2 control-label required" for="User_username">Username <span class="required">*</span></label>
           <div class="col-sm-4">
                <input class="form-control input-sm" maxlength="50" name="username" id="username" type="text"  onkeyup="checkid(this.value)" data-validation="required"/>
          <font color='#FF0000'><b></b></font>

           <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
            <div id="txtHint4"></div>
             </div>
             </div>
        <div class="form-group">
            <label class="col-sm-2 control-label required" for="User_username">Password <span class="required">*</span></label>
            <div class="col-sm-4">
                <input class="form-control input-sm" maxlength="50" name="password" id="password" type="password"  data-validation="required"/>
             </div>
        </div>
          <div class="form-group">
           <label class="col-sm-2 control-label required" for="User_firstname">ชื่อจริง <span class="required">*</span></label>
           <div class="col-sm-4">
             <input maxlength="100" class="form-control input-sm" name="firstname" id="firstname" type="text" data-validation="required" />
           </div>
           </div>
         <div class="form-group">
            <label class="col-sm-2 control-label required" for="User_lastname">นามสกุล <span class="required">*</span></label>
            <div class="col-sm-4">
               <input class="form-control input-sm" maxlength="100" name="lastname" id="lastname" type="text"  data-validation="required"/>
            </div>
         </div>



        <div class="form-group">
             <label class="col-sm-2 control-label" for="User_email">อีเมล์<span class="required">*</span></label>
             <div class="col-sm-4">
             <input class="form-control input-sm" maxlength="100" name="email" id="email" type="text"  data-validation="required"/>
         </div>
         </div>
          <div class="form-group">
              <label class="col-sm-2 control-label" for="User_phone">โทรศัพท์<span class="required">*</span></label>
                        <div class="col-sm-4">
               <input class="form-control input-sm" maxlength="20" name="phone" id="phone" type="text" data-validation="required" />
             </div>
          </div>

         <div class="form-group">
                        <label class="col-sm-2 control-label" for="User_province">จังหวัด</label>
                        <div class="col-sm-4">
                            <select id="province" name="province" class="form-control input-sm" onchange="data_show(this.value,'amphur');">
                             <option value="">-- ไม่ระบุ --</option>
                                <?php while ($rs_pv = $db->get($query_pv)) { ?>
                                    <option value="<?php echo $rs_pv['PROVINCE_CODE']; ?>"><?php echo $rs_pv['PROVINCE_NAME']; ?></option>
                                <?php } ?>
                            </select>

                      </div>
                      </div>
        <div class="form-group">
                        <label class="col-sm-2 control-label" for="User_amphur">อำเภอ</label>
                        <div class="col-sm-4">
         
                            <select name='amphur' id='amphur'onchange="data_show(this.value,'district');" class="form-control input-sm">
                                <option value="">--- Pleaser Select Province ---</option>
                            </select>
                        </div>
         </div>

        <div class="form-group">
                        <label class="col-sm-2 control-label" for="User_district">ตำบล</label>
                        <div class="col-sm-4">
                            <input class="form-control input-sm" maxlength="6" name="district" id="district" type="text"/>
                    </div>
</div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="User_zipcode">รหัสไปรษณีย์</label>
                        <div class="col-sm-4">
                            <input class="form-control input-sm" maxlength="5" name="zipcode" id="zipcode" type="text" />
                        </div>
       </div>

       <div class="form-group">
                        <label class="col-sm-2 control-label" for="User_address">ที่อยู่</label>
                        <div class="col-sm-4">
                            <input class="form-control input-sm" maxlength="200" name="address" id="address" type="text"  />
            <input class="form-control input-sm" maxlength="200" name="user_type" id="user_type" type="hidden" value="user" />
            <input class="form-control input-sm" maxlength="200" name="shop" id="shop" type="hidden" value="0" />
                        </div>
             </div>

                </form>
                
            </div>
            
            <div class="col-lg-10" align="center">
            <div class="subhead">
                <a role="button" id="save" class="btn btn-success btn-xs new-data" href="#">
                    <i class="glyphicon glyphicon-floppy-save"></i>
                   &nbsp;&nbsp;&nbsp;ตกลง&nbsp;&nbsp;&nbsp;&nbsp;
                </a>&nbsp;&nbsp;&nbsp;&nbsp;
                <a role="button" class="search-button btn btn-default btn-xs" href="<?php echo $baseUrl; ?>/back/user">
                    <i class="glyphicon glyphicon-remove-circle"></i>
                    &nbsp;&nbsp;&nbsp;ยกเลิก&nbsp;&nbsp;&nbsp;&nbsp;
                </a>
            </div>
        </div>
            
            
        </div>
    </div>
</div>
</div>
</br>
</br>
<script type="text/javascript">
    $(document).ready(function () {
        $("#save").click(function () {
            $("#user-form").submit();
            return false;
        });
    });
    $.validate();

   function checkid(str) {

            if (str == "") {
                document.getElementById("txtHint4").innerHTML = "";
                return;
            }
            if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            }
            else {// code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    document.getElementById("txtHint4").innerHTML = xmlhttp.responseText;
                }
            }
            xmlhttp.open("GET", "/front/check/username/" + str, true);
        //    xmlhttp.open("GET", "/eshop/back/check/username/" + str, true);
        //    alert("/eshop/front/check/username/" + str);
            xmlhttp.send();
        }

function uzXmlHttp(){
    var xmlhttp = false;
    try{
        xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
    }catch(e){
        try{
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }catch(e){
            xmlhttp = false;
        }
    }
 
    if(!xmlhttp && document.createElement){
        xmlhttp = new XMLHttpRequest();
    }
    return xmlhttp;
}
// End XmlHttp Object

function data_show(select_id,result){
    
	var url = base_path() .'/library/data/select_id='+select_id+'&result='+result;
	 alert(url);
	
    xmlhttp = uzXmlHttp();
    xmlhttp.open("GET", url, false);
	xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded;charset=utf-8"); // set Header
    xmlhttp.send(null);
	document.getElementById(result).innerHTML =  xmlhttp.responseText;
}
//window.onLoad=data_show(5,'amphur'); 


</script>
<?php
/*
 * footer***********************************************************************
 */
require 'template/front/footer.php';
/*
 * footer***********************************************************************
 */

?>