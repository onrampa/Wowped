<?php
$db = new database();
$title = 'O R D E R อาหาร';
// $option_pc = array(
//     "table" => "utable",
//     "condition" => "open IN (1)"
// );
// $query_pc = $db->select($option_pc);

$item_count = isset($_SESSION[_ef . 'cart']) ? count($_SESSION[_ef . 'cart']) : 0;
if (isset($_SESSION[_ef . 'qty'])) {
    $me_qty = 0;
    foreach ($_SESSION[_ef . 'qty'] as $me_item) {
        $me_qty = $me_qty + $me_item;
    }
} else {
    $me_qty = 0;
}

if (isset($_SESSION[_ef . 'cart']) and $item_count > 0) {
    $items_id = "";
    foreach ($_SESSION[_ef . 'cart'] as $item_id) {
        $items_id = $items_id . $item_id . ",";
    }
    $input_item = rtrim($items_id, ",");
    $option_ct = array(
        "table" => "products",
        "condition" => "id IN ({$input_item})"
    );
    $query_ct = $db->select($option_ct);
    $query_ct0 = $db->select($option_ct);
    $query_ct1 = $db->select($option_ct);
    $query_ct2 = $db->select($option_ct);
    $me_count = $db->rows($query_ct);
    $me_count2 = $db->rows($query_ct2);
} else {
    $me_count = 0;
    $me_count2 = 0;
}

// -- header
require 'template/front/header.php';
?>

    <script type="text/javascript">
        $(document).on('click', '.number-spinner button', function () {
            var btn = $(this),
                oldValue = btn.closest('.number-spinner').find('input').val().trim(),
                newVal = 0;

            if (btn.attr('data-dir') == 'up') {
                newVal = parseInt(oldValue) + 1;
            } else {
                if (oldValue >= 1) {
                    newVal = parseInt(oldValue) - 1;
                } else {
                    newVal = 0;
                }
            }
            btn.closest('.number-spinner').find('input').val(newVal);
        });
    </script>


    <script type="text/javascript" src="<?php echo base_url(); ?>/js/imagelightbox.min.js"></script>
    <script type="text/javascript" src="<?php echo $baseUrl; ?>/js/jquery.form-validator.min.js"></script>


    <div class="col-md-8">
        <div class="panel panel-default panel-table">
            <div class="panel-heading">
                <div class="row">
                    <div class="col col-xs-6">
                        <h3 class="panel-title">สั่งอาหารด้วยพนักงาน</h3>
                    </div>
                    <div class="col col-xs-6 text-right">
                        <button type="button" class="btn btn-sm btn-primary btn-create">บันทึก</button>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <table class="table table-striped table-bordered table-list">
                    <thead>
                    <tr>
                        <th width="55%">รายการอาหาร</th>
                        <th id="jscale" width="30%">จำนวน</th>
                        <th width="10%">ราคา</th>
                        <th width="5%"><em class="fa fa-cog"></em></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>กระเพราเป็ดไข่ดาว</td>
                        <td>

                            <div class="input-group number-spinner">
				                <span class="input-group-btn">
					               <button class="btnj btn-default " data-dir="dwn"><span class="glyphicon glyphicon-minus"></span></button>
				                </span>
                                <input type="text" class="form-control text-center" value="0">
                                <span class="input-group-btn">
					               <button class="btnj btn-default " data-dir="up"><span class="glyphicon glyphicon-plus"></span></button>
				                </span>
                            </div>
                        </td>
                        <td>12฿</td>
                        <td align="center">
                            <!--                            <a class="btn btn-default"><em class="fa fa-pencil"></em></a>-->
                            <a class="btn btn-danger"><em class="fa fa-trash"></em></a>
                        </td>
                    </tr>

                    <tr>
                        <td>เป็ดสับมะเง๊ก</td>
                        <td>

                            <div class="input-group number-spinner">
                                <span class="input-group-btn">
                                   <button class="btnj btn-default " data-dir="dwn"><span class="glyphicon glyphicon-minus"></span></button>
                                </span>
                                <input type="text" class="form-control text-center" value="0">
                                <span class="input-group-btn">
                                   <button class="btnj btn-default " data-dir="up"><span class="glyphicon glyphicon-plus"></span></button>
                                </span>
                            </div>
                        </td>
                         <td>12฿</td>
                        <td align="center">
                            <!--                            <a class="btn btn-default"><em class="fa fa-pencil"></em></a>-->
                            <a class="btn btn-danger"><em class="fa fa-trash"></em></a>
                        </td>
                    </tr>

                    <tr>
                        <td>เป็ดคั่ว</td>
                        <td>

                            <div class="input-group number-spinner">
                                <span class="input-group-btn">
                                   <button class="btnj btn-default " data-dir="dwn"><span class="glyphicon glyphicon-minus"></span></button>
                                </span>
                                <input type="text" class="form-control text-center" value="0">
                                <span class="input-group-btn">
                                   <button class="btnj btn-default " data-dir="up"><span class="glyphicon glyphicon-plus"></span></button>
                                </span>
                            </div>
                        </td>
                         <td>12฿</td>
                        <td align="center">
                            <!--                            <a class="btn btn-default"><em class="fa fa-pencil"></em></a>-->
                            <a class="btn btn-danger"><em class="fa fa-trash"></em></a>
                        </td>
                    </tr>
                    </tbody>
                </table>

            </div>
            <div class="panel-footer">
                <div class="row">
                    <div class="col col-xs-4">Page 1 of 5
                    </div>
                    <div class="col col-xs-8">
                        <ul class="pagination hidden-xs pull-right">
                            <li><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                        </ul>
                        <ul class="pagination visible-xs pull-right">
                            <li><a href="#">«</a></li>
                            <li><a href="#">»</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>


    </div>

<?php

$pagination->render();

// -- footer
require 'template/front/footer.php';
