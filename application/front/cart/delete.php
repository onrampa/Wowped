<?php

$item_id = isset($_GET['item_id']) ? $_GET['item_id'] : "";
if (!isset($_SESSION[_ef . 'cart'])) {
    $_SESSION[_ef . 'cart'] = array();
    $_SESSION[_ef . 'qty'] = array();
}
$key = array_search($item_id, $_SESSION[_ef . 'cart']);
$_SESSION[_ef . 'qty'][$key] = "";
$_SESSION[_ef . 'cart'] = array_diff($_SESSION[_ef . 'cart'], array($item_id));
if($item_id==0){
    unset($_SESSION[_ef . 'cart']);
    unset($_SESSION[_ef . 'qty']);
}
header('location:' . base_url() . '/cart');
