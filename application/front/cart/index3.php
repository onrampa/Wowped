<?php
/*
 * php code///////////**********************************************************
 */
$db = new database();
$title = 'O R D E R อาหาร';
// $option_pc = array(
//     "table" => "utable",
//     "condition" => "open IN (1)"
// );
// $query_pc = $db->select($option_pc);

$item_count = isset($_SESSION[_ef . 'cart']) ? count($_SESSION[_ef . 'cart']) : 0;
if (isset($_SESSION[_ef . 'qty'])) {
    $me_qty = 0;
    foreach ($_SESSION[_ef . 'qty'] as $me_item) {
        $me_qty = $me_qty + $me_item;
    }
} else {
    $me_qty = 0;
}

if (isset($_SESSION[_ef . 'cart']) and $item_count > 0) {
    $items_id = "";
    foreach ($_SESSION[_ef . 'cart'] as $item_id) {
        $items_id = $items_id . $item_id . ",";
    }
    $input_item = rtrim($items_id, ",");
    $option_ct = array(
        "table" => "products",
        "condition" => "id IN ({$input_item})"
    );
    $query_ct = $db->select($option_ct);
    $query_ct0 = $db->select($option_ct);
    $query_ct1 = $db->select($option_ct);
    $query_ct2 = $db->select($option_ct);
    $me_count = $db->rows($query_ct);
    $me_count2 = $db->rows($query_ct2);
} else {
    $me_count = 0;
    $me_count2 = 0;
}


/*
 * php code///////////**********************************************************
 */

/*
 * header***********************************************************************
 */
require 'template/front/header.php';
/*
 * header***********************************************************************
 */
?>
    <script type="text/javascript" src="<?php echo base_url(); ?>/js/imagelightbox.min.js"></script>
    <script type="text/javascript" src="<?php echo $baseUrl; ?>/js/jquery.form-validator.min.js"></script>


    <div class="col-md-8"  style="margin-top: 95px">
        <div class="visible-md visible-lg" style="color:white;margin-bottom: 20px">
            <div style="background-color: rgba(159, 27, 48, 0.9);margin-top: -95px;border-radius:10px;height: 80px">
                <div style="padding:10px;text-align: center;font-size: large"><p style="margin-top: 15px">จองโต๊ะ ว้าวเป็ด</p></div>

            </div>

        </div>
        <!-- SHOW MOBILE -->
        <div class="visible-xs">
            <?php if ($me_count > 0) { ?>
                <form action="<?php echo base_url(); ?>/order/save" method="post" name="form" id="form" role="form"
                      class="form-horizontal" enctype="multipart/form-data">

                    <div style="background-color:rgba(255, 255, 255, 0.9); ">

                        <div class="row" style="padding:5px">
                            <div class="col-xs-7">
                                <input type="text" class="form-control" readonly="readonly" data-validation="required"
                                       name="tb" value="<?php echo @$_SESSION[_ef . 'tb']; ?>"
                                       placeholder="กรุณาเลือกโต๊ะ">
                                <input type="hidden" class="form-control" readonly="readonly" name="que"
                                       value="<?php echo @$_SESSION[_ef . 'que']; ?>">
                            </div>

                        </div>
                        <div class="visible-xs  col-xs-10">

                            <input type="hidden" class="form-control" name="nameC"
                                   value="<?php echo @$_SESSION[_ef . 'nameC']; ?>">
                            <?php if (@$_SESSION[_ef . 'levelaccess'] == ('admin' || 'employee' || 'fin' || 'chef')) { ?>
                                <button type="button" data-toggle="modal" data-target="#TBModal" class="btn btn-info">
                                    <span class="glyphicon glyphicon-open"></span> เลือกโต๊ะ
                                </button>
                            <?php } ?>
                            <?php
                            $i = 0;
                            $total_price0 = 0;
                            while ($rs_ct0 = $db->get($query_ct)) {
                                $key = array_search($rs_ct0['id'], $_SESSION[_ef . 'cart']);
                                $total_price0 = $total_price0 + ($rs_ct0['price'] * $_SESSION[_ef . 'qty'][$key]);
                                ?>
                                <input type="hidden" name="product_<?php echo $i; ?>"
                                       value="<?php echo $rs_ct0['id'] ?>">
                                <input type="hidden" name="price_<?php echo $i; ?>"
                                       value="<?php echo $rs_ct0['price']; ?>">
                                <input type="hidden" name="priceP_<?php echo $i; ?>"
                                       value="<?php echo $rs_ct0['priceP']; ?>">
                                <input type="hidden" name="qty_<?php echo $i; ?>"
                                       value="<?php echo $_SESSION[_ef . 'qty'][$key]; ?>">
                                <input type="hidden" name="detail_<?php echo $i; ?>"
                                       value="<?php echo @$_SESSION[_ef . 'detail'][$key]; ?>">

                                <?php
                                $i++;
                            }
                            ?>
                            <input type="hidden" name="total" value="<?php echo $total_price0; ?>">
                            <input type="hidden" name="count_item" value="<?php echo $i; ?>">
                            <button type="button" class="btn btn-success saveform"
                                    style="position: absolute;top:-38px;right:-40px">
                                <span class="glyphicon glyphicon-floppy-save"></span>
                                บันทึกรายการ
                            </button>

                        </div>


                    </div>
                </form>
                <form action="<?php echo base_url(); ?>/cart/update/0" method="post" name="cartform" id="cartform">
                    <table class="table table-bordered table-striped" style="background-color: #ffffff; ">
                        <thead>
                        <tr>
                            <th>รายการอาหาร</th>
                            <th>ชื่อ</th>

                            <th style="width: 100px;text-align: center;">จำนวน</th>
                            <th style="text-align:center;">จำนวนเงินรวม</th>

                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $i = 0;
                        $total_price = 0;
                        while ($rs_ct = $db->get($query_ct0)) {
                            $key = array_search($rs_ct['id'], $_SESSION[_ef . 'cart']);
                            $total_price = $total_price + ($rs_ct['price'] * $_SESSION[_ef . 'qty'][$key]);
                            ?>
                            <tr>
                                <td>
                                    <a href="<?php echo base_url(); ?>/upload/product/<?php echo $rs_ct['image']; ?>"
                                       data-imagelightbox="a">
                                        <img style="height: 50px!important;"
                                             src="<?php echo base_url(); ?>/upload/product/sm_<?php echo $rs_ct['image']; ?>"
                                             class="img-responsive" alt="Responsive image">
                                    </a>
                                </td>


                                <td>
                                    <?php echo $rs_ct['name']; ?>
                                </td>


                                <td style="text-align:center;">
                                    <input style="text-align:center;" type="number" name="qtyupdate[<?php echo $i; ?>]"
                                           value="<?php echo @$_SESSION[_ef . 'qty'][$key]; ?>"
                                           class="form-control input-sm" autocomplete="off" data-validation="number"
                                           data-validation-allowing="float">
                                    <input type="hidden" name="arr_key_<?php echo $i; ?>" value="<?php echo $key; ?>">
                                </td>


                                <td style="text-align:right;">
                                    <?php echo number_format($rs_ct['price'] * $_SESSION[_ef . 'qty'][$key], 2); ?><br>
                                    <button type="button" data-toggle="modal"
                                            data-target="#deleteModal<?php echo $rs_ct['id']; ?>"
                                            class="btn btn-danger">
                                        <span class="glyphicon glyphicon-trash"></span>
                                        ลบทิ้ง
                                    </button>
                                </td>
                            </tr>
                            <!-- Modal ลบทั้งหมด -->
                            <div class="modal fade" id="deleteAllModal" tabindex="-1" role="dialog"
                                 aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span
                                                        aria-hidden="true">&times;</span><span
                                                        class="sr-only">Close</span>
                                            </button>
                                            <h4 class="modal-title" id="myModalLabel">แจ้งเตือนการลบข้อมูล</h4>
                                        </div>
                                        <div class="modal-body">
                                            คุณยืนยันต้องการจะลบข้อมูลนี้ทั้งหมด ใช่หรือไม่?
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">ไม่ใช่
                                            </button>
                                            <a role="button" class="btn btn-primary"
                                               href="<?php echo $baseUrl; ?>/cart/delete/0">ใช่ ยืนยันการลบ</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Modal -->
                            <div class="modal fade" id="deleteModal<?php echo $rs_ct['id']; ?>" tabindex="-1"
                                 role="dialog"
                                 aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span
                                                        aria-hidden="true">&times;</span><span
                                                        class="sr-only">Close</span>
                                            </button>
                                            <h4 class="modal-title" id="myModalLabel">แจ้งเตือนการลบข้อมูล</h4>
                                        </div>
                                        <div class="modal-body">
                                            คุณยืนยันต้องการจะลบข้อมูลนี้ ใช่หรือไม่?
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">ไม่ใช่
                                            </button>
                                            <a role="button" class="btn btn-primary"
                                               href="<?php echo $baseUrl; ?>/cart/delete/<?php echo $rs_ct['id']; ?>">ใช่
                                                ยืนยันการลบ</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php $i++;
                        } ?>
                        <tr>
                            <td colspan="6" style="text-align: right;">
                                <h4>จำนวนเงินรวมทั้งหมด <?php echo number_format($total_price, 2); ?> บาท</h4>
                            </td>
                        </tr>
                        <tr>

                            <td colspan="6" style="text-align: right;">
                                <button type="button" class="btn btn-info recal">
                                    <span class="glyphicon glyphicon-refresh"></span>
                                    Refresh รายการ
                                </button>

                            </td>
                        </tr>
                        </tbody>
                    </table>
                </form>
            <?php } else { ?>
                <div class="alert alert-danger" role="alert" style="margin:15px;">
                    ไม่มี Order หากต้องการเลือกอาหาร
                    <a href="<?php echo base_url(); ?>/product" class="alert-link">คลิกที่นี้</a>
                </div>
            <?php } ?>
            <!-- Modal -->
            <div class="modal fade" id="TBModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                 aria-hidden="true">

                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span
                                        aria-hidden="true">&times;</span><span
                                        class="sr-only">Close</span></button>
                            <h4 class="modal-title" id="myModalLabel">เลือกโต๊ะ</h4>
                        </div>
                        <?php
                        $sql_tb = " SELECT * FROM v_table ";
                        $sql_tb .= "WHERE 1=1 ORDER by id asc ";

                        $query_tb = $db->query($sql_tb);
                        while ($rs_tb = $db->get($query_tb)) {
                            ?>

                            <div class="col-sm-2 col-md-4 col-xs-4" style="height: 150px!important" ; class="modal-body"
                                 ;>
                                <div class="thumbnail" style="height: 140px!important;">
                                    <form class="form-inline" role="form"
                                          action="<?php echo base_url(); ?>/cart/utable1/<?php echo $rs_tb['tb']; ?>"
                                          method="post">
                                        <div class="">
                                            <input type="hidden" name="tb" value="<?php echo $rs_tb['tb']; ?>">
                                            <input type="hidden" name="open" value="<?php echo $rs_tb['open']; ?>">
                                            <input type="hidden" id="que" name="que"
                                                   value="<?php echo $rs_tb['que']; ?>">
                                            <p><input type="text" class="form-control" id="nameC" name="nameC"
                                                      placeholder="ชื่อ.." value="<?php echo $rs_tb['nameC']; ?>"
                                                      size="6">
                                            </p>
                                            <p><input type="text" class="form-control" id="tt" name="tt"
                                                      placeholder="ที่นั่ง.." value="<?php echo $rs_tb['tt']; ?>"
                                                      size="6">
                                            </p>
                                        </div>
                                        <?php if ($rs_tb['open'] == 0) { ?>
                                            <button type="submit"
                                                    class="carto-button"><?php echo $rs_tb['tb'] . "<br>"; ?> </button>
                                        <?php } else if ($rs_tb['open'] == 1) { ?>
                                            <button type="submit"
                                                    class="cartT-button"><?php echo $rs_tb['tb'] . "<br>"; ?>  </button>
                                        <?php } else if ($rs_tb['open'] == 2) { ?>
                                            <div class="cartc-button"><?php echo $rs_tb['tb'] . "<br>"; ?> </div>
                                        <?php } ?>

                                    </form>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="modal-footer">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- END SHOW MOBILE -->

        <!-- SHOW PC -->
        <div class="visible-md visible-lg">
            <?php if ($me_count > 0) { ?>
                <form action="<?php echo base_url(); ?>/order/save" method="post" name="form" id="form" role="form"
                      class="form-horizontal" enctype="multipart/form-data">

                    <div style="background-color:rgba(255, 255, 255, 0.9); ">

                        <div class="row" style="padding:5px">
                            <div class="col-xs-7">
                                <input type="text" class="form-control" readonly="readonly" data-validation="required"
                                       name="tb" value="<?php echo @$_SESSION[_ef . 'tb']; ?>"
                                       placeholder="กรุณาเลือกโต๊ะ">
                                <input type="hidden" class="form-control" readonly="readonly" name="que"
                                       value="<?php echo @$_SESSION[_ef . 'que']; ?>">
                            </div>

                        </div>
                        <div class="visible-md visible-lg  col-xs-10">

                            <input type="hidden" class="form-control" name="nameC"
                                   value="<?php echo @$_SESSION[_ef . 'nameC']; ?>">
                            <?php if (@$_SESSION[_ef . 'levelaccess'] == ('admin' || 'employee' || 'fin' || 'chef')) { ?>
                                <button type="button" data-toggle="modal" data-target="#TBModal2" class="btn btn-info">
                                    <span class="glyphicon glyphicon-open"></span> เลือกโต๊ะ
                                </button>
                            <?php } ?>
                            <?php
                            $i = 0;
                            $total_price0 = 0;
                            while ($rs_ct0 = $db->get($query_ct1)) {
                                $key = array_search($rs_ct0['id'], $_SESSION[_ef . 'cart']);
                                $total_price0 = $total_price0 + ($rs_ct0['price'] * $_SESSION[_ef . 'qty'][$key]);
                                ?>
                                <input type="hidden" name="product_<?php echo $i; ?>"
                                       value="<?php echo $rs_ct0['id'] ?>">
                                <input type="hidden" name="price_<?php echo $i; ?>"
                                       value="<?php echo $rs_ct0['price']; ?>">
                                <input type="hidden" name="priceP_<?php echo $i; ?>"
                                       value="<?php echo $rs_ct0['priceP']; ?>">
                                <input type="hidden" name="qty_<?php echo $i; ?>"
                                       value="<?php echo $_SESSION[_ef . 'qty'][$key]; ?>">
                                <input type="hidden" name="detail_<?php echo $i; ?>"
                                       value="<?php echo @$_SESSION[_ef . 'detail'][$key]; ?>">

                                <?php
                                $i++;
                            }
                            ?>
                            <input type="hidden" name="total" value="<?php echo $total_price0; ?>">
                            <input type="hidden" name="count_item" value="<?php echo $i; ?>">
                            <button type="button" class="btn btn-success saveform"
                                    style="position: absolute;top:-38px;right:-40px">
                                <span class="glyphicon glyphicon-floppy-save"></span>
                                บันทึกรายการ
                            </button>

                        </div>


                    </div>
                </form>
                <form action="<?php echo base_url(); ?>/cart/update/0" method="post" name="cartform" id="cartform">
                    <table class="table table-bordered table-striped" style="background-color: #ffffff; ">
                        <thead>
                        <tr>
                            <th>รายการอาหาร</th>
                            <th>ชื่อ</th>

                            <th style="width: 100px;text-align: center;">จำนวน</th>
                            <th style="text-align:center;">จำนวนเงินรวม</th>

                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $i = 0;
                        $total_price2 = 0;
                        while ($rs_ct2 = $db->get($query_ct2)) {
                            $key = array_search($rs_ct2['id'], $_SESSION[_ef . 'cart']);
                            $total_price2 = $total_price2 + ($rs_ct2['price'] * $_SESSION[_ef . 'qty'][$key]);
                            ?>
                            <tr>
                                <td>
                                    <a href="<?php echo base_url(); ?>/upload/product/<?php echo $rs_ct2['image']; ?>"
                                       data-imagelightbox="a">
                                        <img style="height: 50px!important;"
                                             src="<?php echo base_url(); ?>/upload/product/sm_<?php echo $rs_ct2['image']; ?>"
                                             class="img-responsive" alt="Responsive image">
                                    </a>
                                </td>


                                <td>
                                    <?php echo $rs_ct2['name']; ?>
                                </td>


                                <td style="text-align:center;">
                                    <input style="text-align:center;" type="number" name="qtyupdate[<?php echo $i; ?>]"
                                           value="<?php echo @$_SESSION[_ef . 'qty'][$key]; ?>"
                                           class="form-control input-sm" autocomplete="off" data-validation="number"
                                           data-validation-allowing="float">
                                    <input type="hidden" name="arr_key_<?php echo $i; ?>" value="<?php echo $key; ?>">
                                </td>


                                <td style="text-align:right;">
                                    <?php echo number_format($rs_ct2['price'] * $_SESSION[_ef . 'qty'][$key], 2); ?><br>
                                    <button type="button" data-toggle="modal"
                                            data-target="#deleteModal<?php echo $rs_ct2['id']; ?>"
                                            class="btn btn-danger">
                                        <span class="glyphicon glyphicon-trash"></span>
                                        ลบทิ้ง
                                    </button>
                                </td>
                            </tr>
                            <!-- Modal ลบทั้งหมด -->
                            <div class="modal fade" id="deleteAllModal" tabindex="-1" role="dialog"
                                 aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span
                                                        aria-hidden="true">&times;</span><span
                                                        class="sr-only">Close</span>
                                            </button>
                                            <h4 class="modal-title" id="myModalLabel">แจ้งเตือนการลบข้อมูล</h4>
                                        </div>
                                        <div class="modal-body">
                                            คุณยืนยันต้องการจะลบข้อมูลนี้ทั้งหมด ใช่หรือไม่?
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">ไม่ใช่
                                            </button>
                                            <a role="button" class="btn btn-primary"
                                               href="<?php echo $baseUrl; ?>/cart/delete/0">ใช่ ยืนยันการลบ</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Modal -->
                            <div class="modal fade" id="deleteModal<?php echo $rs_ct2['id']; ?>" tabindex="-1"
                                 role="dialog"
                                 aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span
                                                        aria-hidden="true">&times;</span><span
                                                        class="sr-only">Close</span>
                                            </button>
                                            <h4 class="modal-title" id="myModalLabel">แจ้งเตือนการลบข้อมูล</h4>
                                        </div>
                                        <div class="modal-body">
                                            คุณยืนยันต้องการจะลบข้อมูลนี้ ใช่หรือไม่?
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">ไม่ใช่
                                            </button>
                                            <a role="button" class="btn btn-primary"
                                               href="<?php echo $baseUrl; ?>/cart/delete/<?php echo $rs_ct2['id']; ?>">ใช่
                                                ยืนยันการลบ</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php $i++;
                        } ?>
                        <tr>
                            <td colspan="6" style="text-align: right;">
                                <h4>จำนวนเงินรวมทั้งหมด <?php echo number_format($total_price2, 2); ?> บาท</h4>
                            </td>
                        </tr>
                        <tr>

                            <td colspan="6" style="text-align: right;">
                                <button type="button" class="btn btn-info recal">
                                    <span class="glyphicon glyphicon-refresh"></span>
                                    Refresh รายการ
                                </button>

                            </td>
                        </tr>
                        </tbody>
                    </table>
                </form>
            <?php } else { ?>
                <div class="alert alert-danger" role="alert" style="margin:15px;">
                    ไม่มี Order หากต้องการเลือกอาหาร
                    <a href="<?php echo base_url(); ?>/product" class="alert-link">คลิกที่นี้</a>
                </div>
            <?php } ?>
            <!-- Modal -->
            <div class="modal fade" id="TBModal2" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span
                                        aria-hidden="true">&times;</span><span
                                        class="sr-only">Close</span></button>
                            <h4 class="modal-title" id="myModalLabel">เลือกโต๊ะ</h4>
                        </div>
                        <?php
                        $sql_tb = " SELECT * FROM v_table ";
                        $sql_tb .= "WHERE 1=1 ORDER by id asc ";

                        $query_tb = $db->query($sql_tb);
                        while ($rs_tb = $db->get($query_tb)) {
                            ?>

                            <div class="col-sm-2 col-md-2 col-xs-2" style="height: 150px!important" ; class="modal-body"
                                 ;>
                                <div class="thumbnail" style="height: 140px!important;">
                                    <form class="form-inline" role="form"
                                          action="<?php echo base_url(); ?>/cart/utable1/<?php echo $rs_tb['tb']; ?>"
                                          method="post">
                                        <div class="">
                                            <input type="hidden" name="tb" value="<?php echo $rs_tb['tb']; ?>">
                                            <input type="hidden" name="open" value="<?php echo $rs_tb['open']; ?>">
                                            <input type="hidden" id="que" name="que"
                                                   value="<?php echo $rs_tb['que']; ?>">
                                            <p><input type="text" class="form-control" id="nameC" name="nameC"
                                                      placeholder="ชื่อ.." value="<?php echo $rs_tb['nameC']; ?>"
                                                      size="6">
                                            </p>
                                            <p><input type="text" class="form-control" id="tt" name="tt"
                                                      placeholder="ที่นั่ง.." value="<?php echo $rs_tb['tt']; ?>"
                                                      size="6">
                                            </p>
                                        </div>
                                        <?php if ($rs_tb['open'] == 0) { ?>
                                            <button type="submit"
                                                    class="carto-button"><?php echo $rs_tb['tb'] . "<br>"; ?> </button>
                                        <?php } else if ($rs_tb['open'] == 1) { ?>
                                            <button type="submit"
                                                    class="cartT-button"><?php echo $rs_tb['tb'] . "<br>"; ?>  </button>
                                        <?php } else if ($rs_tb['open'] == 2) { ?>
                                            <div class="cartc-button"><?php echo $rs_tb['tb'] . "<br>"; ?> </div>
                                        <?php } ?>

                                    </form>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="modal-footer">
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <!-- END SHOW PC -->
    </div>

    <script type="text/javascript">
        $(document).ready(function () {
            $('a').imageLightbox();
            $.validate();
            $('.delAll').click(function () {
                window.location = 'deliveryinformation/del';
            });
            $('.recal').click(function () {
                $('#cartform').submit();
            });
            $('.saveform').click(function () {
                $('#form').submit();
            });
        });
    </script>

<?php
/*
 * footer***********************************************************************
 */
require 'template/front/footer.php';
/*
 * footer***********************************************************************
 */
