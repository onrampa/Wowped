<?php
/*
 * php code///////////**********************************************************
 */
$db = new database();
$sql_pd = "SELECT p.*, c.name as cname, c.id as cid FROM products p ";
$sql_pd .= "INNER JOIN product_categories c ";
$sql_pd .= "ON p.product_categorie_id=c.id ";
$sql_pd .= "WHERE p.id='{$_GET['id']}' ";
$query_pd = $db->query($sql_pd);
$rs_pd = $db->get($query_pd);

$option_ps = array(
    "table" => "v_product",
    "fields" => "id,pname,price,image",
    "condition" => "cat='{$rs_pd['cid']}'",
    "limit" => 5
);
$query_ps = $db->select($option_ps);
/*
 * php code///////////**********************************************************
 */
$title = $rs_pd['name'];
$dis = ($rs_pd['priceP'] - $rs_pd['price']);
if ($rs_pd['priceP'] > 0) {
    $ddis = ($dis * 100) / $rs_pd['priceP'];
}
/*
 * header***********************************************************************
 */
require 'template/front/header.php';
/*
 * header***********************************************************************
 */
?>

    <div class="col-md-8" style="margin-top: 95px">

        <div class="visible-md visible-lg" style="color:white">
            <div style="background-color: rgba(159, 27, 48, 0.9);margin-top: -95px;border-radius:10px;height: 80px">
                <div style="padding:10px;text-align: center;font-size: large"><p style="margin-top: 15px">เมนู
                        ว้าวเป็ด</p></div>
            </div>
        </div>

        <div class="blog-header" style="margin-top: 20px">
            <ol class="breadcrumb">
                <li><a href="<?php echo base_url(); ?>">หน้าแรก</a></li>
                <li>
                    <a href="<?php echo base_url(); ?>/categorie/<?php echo $rs_pd['cid']; ?>"><?php echo $rs_pd['cname']; ?></a>
                </li>
                <li class="active"><?php echo $rs_pd['name']; ?></li>
            </ol>
        </div>

        <div class="row" style="padding: 0 15px">
            <div class="panel panel-default" style="border-radius: 10px 10px">
                <div class="panel-body">
                    <?php if ($dis > 0) { ?>
                        <div class="shop-item-card__badge-wrapper">
                            <div class="shop-badge shop-badge--fixed-width shop-badge--promotion">
                                <div class="shop-badge--promotion__label-wrapper shop-badge--promotion__label-wrapper--th-TH">
                                <span style="font-size: 18px;"
                                      class="shop-badge--promotion__label-wrapper__off-label shop-badge--promotion__label-wrapper__off-label--th-TH">ลด</span>
                                    <span style="font-size: 18px;"
                                          class="percent"><?php echo number_format($ddis, 2); ?>
                                        %</span>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="row">
                        <div class="col-sm-5">
                            <a href="<?php echo base_url(); ?>/upload/product/<?php echo $rs_pd['image']; ?>"
                               data-imagelightbox="a">
                                <img src="<?php echo base_url(); ?>/upload/product/thumb_<?php echo $rs_pd['image']; ?>"
                                     class="img-responsive" alt="Responsive image">
                            </a>
                        </div>
                        <div class="col-sm-7">
                            <h3><?php echo $rs_pd['name']; ?></h3>
                            <dl class="dl-horizontal">
                                <dt>คงเหลือ</dt>
                                <dd><?php echo $rs_pd['instock']; ?> ที่</dd>
                                <dt>ราคา</dt>
                                <dd><p style="font-size: 15px;font-weight: bold;color: red;">
                                <strike>
                                        <?php if ($rs_pd['priceP'] > $rs_pd['price']) {
                                            echo number_format($rs_pd['priceP']) . ".-";
                                        } ?>
                                </strike>
                                        <span style="font-size: 18px;font-weight: bold;color: navy;">
                                <?php echo number_format($rs_pd['price'], 2); ?> / ที่
                                    </p>
                                </dd>
                                </span>
                            </dl>
                            <hr>
                            <form class="form-inline" role="form"
                                  action="<?php echo base_url(); ?>/cart/update/<?php echo $rs_pd['id']; ?>"
                                  method="post">
                                <div class="form-group">
                                    <input type="number" class="form-control" id="text2" autocomplete="off" name="qty"
                                           placeholder="ใส่จำนวน" data-validation="number"
                                           data-validation-allowing="float"
                                           value="1">
                                </div>
                                <button type="submit" class="btn btn-success">ORDER</button>
                            </form>
                        </div>
                        <div id="fb-root"></div>
                        <script>(function (d, s, id) {
                                var js, fjs = d.getElementsByTagName(s)[0];
                                if (d.getElementById(id)) return;
                                js = d.createElement(s);
                                js.id = id;
                                js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8";
                                fjs.parentNode.insertBefore(js, fjs);
                            }(document, 'script', 'facebook-jssdk'));</script>


                    </div>
                    <div class="row" style="font-size: 14px;">
                        <div class="col-sm-12">
                            <ul class="nav nav-tabs">
                                <li role="presentation" class="active"><a href="#detail" onclick="return false;">รายละเอียด</a>
                                    <div id="detail">

                                        <?php echo $rs_pd['detail']; ?>
                                    </div>
                                </li>
                            </ul>

                        </div>
                    </div>
                </div>
            </div>

            <div class="">
                <div class="panel panel-primary">
                    <!-- Default panel contents -->
                    <div class="panel-heading">อาหารแนะนำ</div>
                    <div class="panel-body">
                        <!-- List group -->
                        <ul class="list-group">
                            <?php while ($rs_ps = $db->get($query_ps)) { ?>
                                <li class="col-md-3">
                                    <a href="<?php echo $baseUrl; ?>/product/view/<?php echo $rs_ps['id']; ?>">
                                        <img src="<?php echo base_url(); ?>/upload/product/thumb_<?php echo $rs_ps['image']; ?>"
                                             class="img-responsive" alt="Responsive image">
                                        <?php $str_check = strlen($rs_ps['pname']);
                                        if ($str_check >= 20) {
                                            $sheck_num = mb_substr($rs_ps['pname'], 0, 20);
                                            echo $sheck_num . "...";
                                        } else {
                                            echo $rs_ps['pname'];
                                        } ?>
                                        (<?php echo $rs_ps['price']; ?> บาท)
                                    </a>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div><!-- /.blog-sidebar -->
        </div><!-- /.row -->
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            $('a').imageLightbox();
            $.validate();
        });
    </script>
<?php
/*
 * footer***********************************************************************
 */
require 'template/front/footer.php';
/*
 * footer***********************************************************************
 */
