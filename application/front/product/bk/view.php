
<?php
/*
 * php code///////////**********************************************************
 */
$db = new database();
$sql_pd = "SELECT * FROM v_product  ";
$sql_pd .= "WHERE id='{$_GET['id']}' ";
$query_pd = $db->query($sql_pd);
$rs_pd = $db->get($query_pd);

$option_ps = array(
    "table" => "v_product",
    "fields" => "id,pname,price,image",
    "condition" => "cat='{$rs_pd['cat']}'",
    "limit" => 5
);
$query_ps = $db->select($option_ps);
/*
 * php code///////////**********************************************************
 */
$title = $rs_pd['name'];
$dis=($rs_pd['priceP']-$rs_pd['price']);
$ddis=($dis*100)/$rs_pd['priceP'];
/*
 * header***********************************************************************
 */
require 'template/front/header.php';
/*
 * header***********************************************************************
 */
?>
<script type="text/javascript" src="<?php echo base_url(); ?>/js/imagelightbox.min.js"></script>
<script type="text/javascript" src="<?php echo $baseUrl; ?>/js/jquery.form-validator.min.js"></script>
<style>
    #imagelightbox
    {
        position: fixed;
        z-index: 9999;

        -ms-touch-action: none;
        touch-action: none;
    }
</style>
<div class="container">

    <div class="blog-header">
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>">หน้าแรก</a></li>
            <li><a href="<?php echo base_url(); ?>/categorie/<?php echo $rs_pd['cat']; ?>"><?php echo $rs_pd['cname']; ?></a></li>
            <li class="active"><?php echo $rs_pd['pname']; ?></li>
        </ol>
    </div>

    <div class="row">
        <div class="col-sm-9 blog-main">
            <div class="panel panel-default">
                <div class="panel-body">
                <?php if($dis>0){  ?>    
                                    <div class="shop-item-card__badge-wrapper">
                                    <div class="shop-badge shop-badge--fixed-width shop-badge--promotion">
                                    <div class="shop-badge--promotion__label-wrapper shop-badge--promotion__label-wrapper--th-TH">                                                            
                                    <span style="font-size: 18px;" class="shop-badge--promotion__label-wrapper__off-label shop-badge--promotion__label-wrapper__off-label--th-TH">ลด</span>
                                    <span style="font-size: 18px;" class="percent"><?php echo number_format($ddis, 2);  ?>%</span>
                                    </div></div></div>
                                    <?php } ?>
                    <div class="row">
                        <div class="col-sm-5">
                            <a href="<?php echo base_url(); ?>/upload/product/<?php echo $rs_pd['image']; ?>" data-imagelightbox="a">
                                <img src="<?php echo base_url(); ?>/upload/product/thumb_<?php echo $rs_pd['image']; ?>" class="img-responsive" alt="Responsive image">
                            </a>
                        </div>
                        <div class="col-sm-7">
                            <h3><?php echo $rs_pd['pname']; ?></h3>
                            <dl class="dl-horizontal">
                                <dt>ยี่ห้อ</dt>
                                <dd><?php echo $rs_pd['brandname']; ?></dd>
                                <dt>มีสินค้าในระบบ </dt>
                                <dd><?php echo $rs_pd['instock']; ?> ชิ้น</dd>
                                <dt>ราคา</dt>
                                <dd><p style="font-size: 15px;font-weight: bold;color: red;"> <strike><?php if($rs_pd['priceP']>$rs_pd['price']){echo number_format($rs_pd['priceP']).".-"; } ?></strike>
                                <span style="font-size: 18px;font-weight: bold;color: navy;">  
                                <?php echo number_format($rs_pd['price'], 2); ?>  / ชิ้น
                                </p>
                            </dd>
                </span>
                            </dl>
                            <hr>
                            <form class="form-inline" role="form" action="<?php echo base_url();?>/cart/update/<?php echo $rs_pd['id'];?>" method="post">
                                <div class="form-group">
                                    <input type="number" class="form-control" id="text2" autocomplete="off" name="qty" placeholder="ใส่จำนวน" data-validation="number" data-validation-allowing="float"value="1">
                                </div>
                                <button type="submit" class="btn btn-success">หยิบใส่ตะกร้า</button>
                            </form>
                        </div>
                <!-- ร้านค้า -->
                        <div class="row">
                        <div class="col-sm-5">
                </div>
                        <div class="col-sm-7">

                        <hr>
                        <dd><a href="<?php echo base_url(); ?>/shop/<?php echo $rs_pd['shop']; ?>" ><?php echo $rs_pd['shopName']; ?></a></dd>
                        <a href="<?php echo base_url(); ?>/shop/<?php echo $rs_pd['shop']; ?>" >
                                <img src="<?php echo base_url(); ?>/upload/logo/thumb_<?php echo $rs_pd['logo']; ?>" class="img-responsive" >
                            </a>
                      
                        </div>
                </div>
                        <div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div class="fb-share-button" 
	data-href="<?php echo $strCurrentURL;?>" 
	data-layout="button_count">
</div>
                        
                    </div>
                    <div class="row" style="font-size: 14px;">
                        <div class="col-sm-12">
                            <ul class="nav nav-tabs">
                                <li role="presentation" class="active"><a href="#detail" onclick="return false;">รายละเอียด</a>
                                <div id="detail">
                                <?php echo $rs_pd['detail']; ?>
                                </div>
                                </li>
                        </ul>
                           
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.blog-main -->
        <div class="col-sm-3 blog-sidebar">
            <div class="panel panel-primary">
                <!-- Default panel contents -->
                <div class="panel-heading">สินค้าที่เกี่ยวข้อง</div>
                <!-- List group -->
                <ul class="list-group">
                    <?php while ($rs_ps = $db->get($query_ps)) { ?>
                        <li class="list-block">
                            <a href="<?php echo $baseUrl; ?>/product/view/<?php echo $rs_ps['id']; ?>">
                                <img src="<?php echo base_url(); ?>/upload/product/thumb_<?php echo $rs_ps['image']; ?>" class="img-responsive" alt="Responsive image">
                                <?php $str_check = strlen($rs_ps['pname']);
                                if ($str_check >= 20) {
                                $sheck_num = mb_substr($rs_ps['pname'], 0, 20);
                                echo $sheck_num. "...";
                                    }
                                else {
                               echo $rs_ps['pname'];
                                   } ?>
                                (<?php echo $rs_ps['price']; ?> บาท)
                            </a>
                        </li>
                    <?php } ?>
                </ul>
            </div>
        </div><!-- /.blog-sidebar -->

    </div><!-- /.row -->

</div><!-- /.container -->
<script type="text/javascript">
    $(document).ready(function () {
        $('a').imageLightbox();
        $.validate();
    });
</script>
<?php
/*
 * footer***********************************************************************
 */
require 'template/front/footer.php';
/*
 * footer***********************************************************************
 */
