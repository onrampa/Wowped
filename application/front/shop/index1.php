<?php
/*
 * php code///////////**********************************************************
 */
$title = 'สินค้าชุมชน ขับเคลื่อนเศรษฐกิจฐานราก Village E-Commerce';

require 'library/pagination.php';

$db = new database();
$option_cat = array(
    "table" => "product_categories"
);
$query_cat = $db->select($option_cat); // catgorie

$option_shop = array(
    "table" => "users",
      "condition" => "Cclose='1'"
);
$query_shop = $db->select($option_shop); // shop

$sql_pc = "SELECT * ";
$sql_pc .= "FROM v_product ";
$query_pc = $db->query($sql_pc);
// echo $sql_pc;
$pagination = new Zebra_Pagination();

$rows_pc = $db->rows($query_pc);

$per_page = 24;
$page_start = (($pagination->get_page() - 1) * $per_page);
$sql_pc .= "ORDER BY sell DESC,id ASC LIMIT {$page_start},{$per_page} ";
$pagination->records($rows_pc);
$pagination->records_per_page($per_page);
$pagination->base_url('', FALSE);
$query_pc_page = $db->query($sql_pc);

$page = ($page_start!=0) ? $page_start : "1";
$pages = ceil($rows_pc/$per_page);

$uri = $_SERVER['REQUEST_URI']; // url
/*
 * php code///////////**********************************************************
 */

/*
 * header***********************************************************************
 */
require 'template/front/header.php';
/*
 * header***********************************************************************
 */
?>
<div class="container">

    <div class="blog-header">
      
        <p class="lead blog-description">สินค้าชุมชน ขับเคลื่อนเศรษฐกิจฐานราก Village E-Commerce</p>
    </div>
    <div class="row">

        <div class="col-sm-12 blog-main">
        
        
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
   <?php
                        $i = 0;
                        while ($rs_pc = $db->get($query_pc_page)) {
                            $tr = ($i % 2 == 0) ? "odd" : "even";
                            $dis=($rs_pc['priceP']-$rs_pc['price']);
                            $ddis=($dis*100)/$rs_pc['priceP'];
                            ?>
                            <div class="col-sm-5 col-md-2 col-xs-6"> 
                                <div class="thumbnail" style="height: 330px!important;">
                                <?php if($dis>0){  ?>    
                                    
                                    
                                    
                                    <?php } ?>
                                  
                                    <a href="<?php echo $baseUrl; ?>/product/view/<?php echo $rs_pc['../home/id']; ?>">
                                        <img style="height: 170px!important;" src="<?php echo $baseUrl; ?>/upload/product/thumb_<?php echo $rs_pc['../home/image']; ?>" alt="<?php echo $rs_pc['shopName']; ?>">
                                    </a>
                                    <div class="caption">
                                    <a href="<?php echo $baseUrl; ?>/shop/<?php echo $rs_cat['MemID']; ?>"  style="font-size: 13px;">
                                    <?php $str_check = strlen($rs_pc['shopName']);
                                                      if ($str_check > 20) {
                                                      $sheck_num = mb_substr($rs_pc['shopName'], 0, 20);
                                                      echo $sheck_num. "...";                   }
                                                      else {     echo $rs_pc['shopName'];          }                         
                                    ?>
                                    </a>
                                                                 
                                                                 
                          <p style="font-size: 13px;">: 
                          <?php $str_checkN = strlen($rs_pc['shopName']);
                                    if ($str_check >= 17) {
                                        $sheck_numN = mb_substr($rs_pc['shopName'], 0, 17);
                                                echo $sheck_numN. "...";                   }
                                else {     echo $rs_pc['shopName'];          }                         
                           ?>
                        
                          </p>
                          <p style="font-size: 14px;width=100%">
                          <input type="hidden" class="form-control" id="text2" autocomplete="off" name="qty" placeholder="ใส่จำนวน" data-validation="number" data-validation-allowing="float"value="1">
                                         <button type="submit" class="cart-button">
                                                 <svg class="cart-svg " width="16 " height="16 " viewBox="0 0 16 16 ">
                                                 <path d="M15.32 2.405H4.887C3 2.405 2.46.805 2.46.805L2.257.21C2.208.085 2.083 0 1.946 0H.336C.1 0-.064.24.024.46l.644 1.945L3.11 9.767c.047.137.175.23.32.23h8.418l-.493 1.958H3.768l.002.003c-.017 0-.033-.003-.05-.003-1.06 0-1.92.86-1.92 1.92s.86 1.92 1.92 1.92c.99 0 1.805-.75 1.91-1.712l5.55.076c.12.922.91 1.636 1.867 1.636 1.04 0 1.885-.844 1.885-1.885 0-.866-.584-1.593-1.38-1.814l2.423-8.832c.12-.433-.206-.86-.655-.86 " fill="#fff "></path>
                                             </svg>หยิบ
                                         </button>
                                                          </p>
                             </form>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    
                </div>
                               
                    <?php $pagination->render(); ?>
                </div>
            </div>
            </div>
        </div><!-- /.blog-main -->

    </div><!-- /.row -->

</div><!-- /.container -->
<?php
/*
 * footer***********************************************************************
 */
require 'template/front/footer.php';
/*
 * footer***********************************************************************
 */