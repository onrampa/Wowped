<?php
 if($_SESSION[_ef . 'levelaccess']== 'user'){
 header('location:'.$baseUrl.'/admin');
 }
/*
 * php code///////////**********************************************************
 */
$db = new database();
$option_pc = array(
    "table" => "product_categories"
);
$query_pc = $db->select($option_pc);

$option_spc = array(
    "table" => "product_subcategories"
);
$query_spc = $db->select($option_spc);

$option_s = array(
    "table" => "users",
    "condition" => "Cclose='1'"
);
$query_s = $db->select($option_s);

$option_pd = array(
    "table" => "v_producta",
    "condition" => "id='{$_GET['id']}' "
);
$query_pd = $db->select($option_pd);
$rs_pd = $db->get($query_pd);

$title = 'แก้ไขสินค้า : ' .$rs_pd['name'];
/*
 * php code///////////**********************************************************
 */

/*
 * header***********************************************************************
 */
require 'template/back/header.php';
/*
 * header***********************************************************************
 */
?>
<script type="text/javascript" src="<?php echo $baseUrl; ?>/js/jquery.form-validator.min.js"></script>
<script type="text/javascript" src="<?php echo $baseUrl; ?>/ckeditor/ckeditor.js"></script>
<div id="page-warpper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">แก้ไขข้อมูล <?php echo $rs_pd['name']; ?></h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="subhead">
                <a role="button" id="save" class="btn btn-success btn-xs new-data" href="#">
                 <i class="glyphicon glyphicon-floppy-save"></i>
                    บันทึก
                </a>
                <a role="button" class="search-button btn btn-default btn-xs" href="<?php echo $baseUrl; ?>/back/product">
                    <i class="glyphicon glyphicon-remove-circle"></i>
                    ยกเลิก
                </a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <div class="form-horizontal" style="margin-top: 10px;">
                <form id="product-form" action="<?php echo $baseUrl; ?>/back/product/form_update" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="id" value="<?php echo $rs_pd['id'];?>">
                    <div class="form-group">
                        <label for="product_image" class="col-sm-4 control-label required">รูปภาพประจำสินค้า</label>
                        <div class="col-sm-8">
                            <img src="<?php echo $baseUrl ?>/upload/product/md_<?php echo $rs_pd['image'];?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="Product_image" class="col-sm-4 control-label">รูปภาพใหม่</label>
                        <div class="col-sm-8">
                            <input type="file" name="image" id="image">
                        </div>
                    </div>
                  
                    <div class="form-group">
                        <label for="Product_product_categorie_id" class="col-sm-4 control-label required">หมวดหมู่<span class="required">*</span></label>
                        <div class="col-sm-8">
                            <select id="product_categorie_id" name="product_categorie_id" class="form-control input-sm" onChange="DisplaySubCategory(product_categorie_id.value)">
                                <?php
                                while ($rs_pc = $db->get($query_pc)) { 
                                $spc = ($rs_pd['product_categorie_id']==$rs_pc['id']) ? "selected" : "";    
                                    ?>
                                    <option value="<?php echo $rs_pc['id']; ?>" <?php echo $spc; ?>><?php echo $rs_pc['name']; ?></option>
                                <?php } ?>

                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="Product_product_categorie_sid" class="col-sm-4 control-label required">หมวดหมู่ย่อย<span class="required">*</span></label>
                        <div class="col-sm-8">
                            <select id="product_categorie_sid" name="product_categorie_sid" class="form-control input-sm" >
                                <option value="<?php echo $rs_pd['product_categorie_sid']; ?>" selected><?php echo $rs_pd['cat_s_name']; ?></option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="Product_name" class="col-sm-4 control-label required">ชื่อสินค้า <span class="required">*</span></label>
                        <div class="col-sm-8">
                            <input type="text" id="name" name="name" class="form-control input-sm" maxlength="100" data-validation="required" value="<?php echo $rs_pd['name']; ?>" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="Product_name" class="col-sm-4 control-label required">ราคาพิเศษ <span class="required">*</span></label>
                        <div class="col-sm-8">
                            <input type="text" id="price" name="price" class="form-control input-sm" data-validation="number" data-validation-allowing="float" value="<?php echo $rs_pd['price']; ?>">
                        </div>
                        </div>
                    <div class="form-group">
                        <label for="Product_name" class="col-sm-4 control-label required">ราคาต้นทุนวัตถุดิบ </label>
                        <div class="col-sm-8">
                            <input type="text" id="priceP" name="priceP" class="form-control input-sm" value="<?php echo $rs_pd['priceP']; ?>">
                        </div>
                    </div>

                    <div class="form-group">
                          <label for="Product_name" class="col-sm-4 control-label required">พร้อมขาย</label>
                        <div class="col-sm-8">
                            <input type="text" id="instock" name="instock" class="form-control input-sm" maxlength="8" value="<?php echo $rs_pd['instock']; ?>" >
                        </div>
                        </div>
                    <div class="form-group">
                        <div class="col-sm-12 col-sm-offset-2">
                            <textarea id="detail" name="detail" class="form-control input-sm" ><?php echo $rs_pd['detail']; ?></textarea>
                            <script>
                                // Replace the <textarea id="editor1"> with a CKEditor
                                // instance, using default configuration.
                                CKEDITOR.replace('detail');
                                function CKupdate() {
                                    for (instance in CKEDITOR.instances)
                                        CKEDITOR.instances[instance].updateElement();
                                }
                            </script>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-lg-6">


       
        <!-- ส่วนประกอบ  -->
    <?php
$option_st_p = array(
    "table" => "v_stock_products",
    "condition" => "product_id='{$_GET['id']}'"
);
$query_st_p = $db->select($option_st_p);
// $rs_st_p = $db->get($query_st_p);
    ?>
                <div class="subhead">
            <a class="btn btn-success btn-xs new-data" title="" href="#" data-toggle="modal" data-target="#AddModal">
            <i class="glyphicon glyphicon-plus-sign"></i> เพิ่มวัตถุดิบ</a>                                
            </div>
               <table class="table table-striped table-custom">
                <thead>
                    <tr>
                        <th id="user-grid_c1">
                            <a class="sort-link" href="<?php echo $uri; ?>">ชื่อส่วนประกอบ</a>
                        </th>
                        <th id="user-grid_c1">
                            <a class="sort-link" href="<?php echo $uri; ?>">ปริมาณที่ใช้</a>
                        </th>
                        <th id="user-grid_c4">
                            <a class="sort-link" href="<?php echo $uri; ?>">หน่วย</a>
                        </th>
                        <th id="user-grid_c4">
                            <a class="sort-link" href="<?php echo $uri; ?>">คงเเหลือ</a>
                        </th>

                        <th class="button-column" id="user-grid_c6">&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $i = 0;
                    while ($rs_psc = $db->get($query_st_p)) {
                        $tr = ($i % 2 == 0) ? "odd" : "even";
                        ?>
                        <tr class="<?php echo $tr; ?>">
                           
                            <td><?php echo $rs_psc['subname']; ?></td>
                            <td><?php echo $rs_psc['v_value']; ?></td>
                            <td><?php echo $rs_psc['unit']; ?></td>
                            <td><?php echo $rs_psc['instock']; ?></td>
                            <td><?php echo $rs_psc['unit']; ?></td>
                            <td class="button-column">
                                <a class="btn btn-warning btn-xs load_data" title="" href="#" data-toggle="modal" data-target="#editModal<?php echo $rs_psc['id'];?>"><i class="glyphicon glyphicon-edit"></i> แก้ไข</a>                                
                                <a class="btn btn-danger btn-xs confirm" title="" href="#" data-toggle="modal" data-target="#deleteModal<?php echo $rs_psc['id'];?>"><i class="glyphicon glyphicon-remove"></i> ลบ</a>
 
                                <!-- editModal -->
 <?php
     $option_st = array(
        "table" => "stock_cat"
    );
    $query_st = $db->select($option_st);


    $option_sst = array(
        "table" => "stock_subcat"
    );
    $query_sst = $db->select($option_sst);
    $query_sst2 = $db->select($option_sst);   

 ?>                               
    <div class="modal fade" id="editModal<?php echo $rs_psc['id'];?>">
    <div class="modal-dialog">
    <form id="sub-form" action="<?php echo $baseUrl; ?>/back/stock_product/update/<?php echo $rs_psc['id'].'?id='.$rs_psc['id']; ?>" method="post">                                       
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel">แจ้งเตือนการ แก้ไขข้อมูล</h4>
                </div>
                <div class="modal-body">  
                <input type="hidden" id="id" name="id" class="form-control input-sm" data-validation="required" value="<?php echo $_GET['id']; ?>">
                <input type="hidden" id="sid" name="sid" class="form-control input-sm" data-validation="required" value="<?php echo $rs_psc['id']; ?>">
                <div class="form-group">
                <label for="stock_cat_id" class="col-sm-4 control-label required">หมวดหมู่<span class="required">*</span></label>
                <div class="col-sm-8">
                    <select id="stock_cat_id" name="stock_cat_id" class="form-control input-sm" onChange="DisplaySubCate(stock_cat_id.value,sid.value)">
                        <?php
                        while ($rs_st = $db->get($query_st)) { 
                        $spc = ($rs_psc['stock_cat_id']==$rs_st['id']) ? "selected" : "";    
                            ?>
                            <option value="<?php echo $rs_st['id']; ?>" <?php echo $spc; ?>><?php echo $rs_st['name']; ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="stock_cat_sid_<?php echo $rs_psc['id']; ?>" class="col-sm-4 control-label required">หมวดหมู่ย่อย<span class="required">*</span></label>
                <div class="col-sm-8">
                    <select id="stock_cat_sid_<?php echo $rs_psc['id']; ?>" name="stock_cat_sid_<?php echo $rs_psc['id']; ?>" class="form-control input-sm" >

                        <option value="<?php echo $rs_psc['stock_sub_id']; ?>" selected><?php echo $rs_psc['subname']; ?></option>

                    </select>
                </div>
            </div>                                        
            <div class="form-group">                                            
            <label for="sub_name" class="col-sm-4 control-label required">จำนวนใช้ใน 1 จาน <span class="required">*</span></label>
                <div class="col-sm-8">
                    <input type="rs_or" id="v_value" name="v_value" class="form-control input-sm" data-validation="required"  value="<?php echo $rs_psc['v_value']; ?>">
                </div>
                </div>
                <div class="form-group">                                            
            <label for="sub_name" class="col-sm-4 control-label required">หน่วย <span class="required">*</span></label>
                <div class="col-sm-8">
                    <input type="text" id="unit" name="unit" class="form-control input-sm" data-validation="required"  value="<?php echo $rs_psc['unit']; ?>">
                </div>
                </div>                                    
    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">
                                        Close
                                        </button>
                                        <input type="submit" class="btn btn-primary" value=" Save">
                                    </div>
                                    </div>
                                </form>                                        
                            </div>                                   
                        </div>
                        <!-- ปิด editModal -->

                        <!-- deleteModal -->
                        <div class="modal fade" id="deleteModal<?php echo $rs_psc['id'];?>" >
                            <div class="modal-dialog">
                            <form id="sub-form" action="<?php echo $baseUrl; ?>/back/stock_subcat/delete/<?php echo $rs_pc['id'].'?sid='.$rs_psc['id']; ?>" method="post">        
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                            <h4 class="modal-title" id="myModalLabel">แจ้งเตือนการ ลบข้อมูล</h4>
                                        </div>
                                        <div class="modal-body">  
                                        <input type="text" id="sid" name="sid" class="form-control input-sm" data-validation="required" value="<?php echo $rs_psc['id']; ?>">
                                        <input type="text" id="name" name="name" class="form-control input-sm" data-validation="required" value="<?php echo $rs_psc['name']; ?>">
                                        </div>
                                        
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">
                                            Close
                                            </button>
                                            <input type="submit" class="btn btn-primary" value=" Delete">
                                    </div>
                                    </div>
                            </form>                                        
                            </div>
                        </div>
                        <!-- ปิด deleteModal -->
                    </td>
                </tr>
            <?php } ?>
                </tbody>
            </table>


    </div>
</div>
    <!-- Modal Zone -->
    <!-- AddModal -->
    <div class="modal fade" id="AddModal">
    <div class="modal-dialog">
    <form id="sub-form" action="<?php echo $baseUrl; ?>/back/stock_product/create/<?php echo $_GET['id']; ?>" method="post">        
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel">การ เพิ่มข้อมูล</h4>
                </div>
                
                <div class="modal-body">  
                <input type="hidden" id="id" name="id" class="form-control input-sm" data-validation="required" value="<?php echo $_GET['id']; ?>">
                <div class="form-group">
                <label for="stock_cat_ida" class="col-sm-4 control-label required">หมวดหมู่<span class="required">*</span></label>
                <div class="col-sm-8">
                    <select id="stock_cat_ida" name="stock_cat_ida" class="form-control input-sm" onChange="DisplaySubCateA(stock_cat_ida.value)">
                    <option value=""selected >-- เลือกหมวดหมู่ --</option>

                        <?php
                            $sql_st2 = "SELECT * FROM stock_cat  ";
                            $query_st2 = $db->query($sql_st2);
                        while ($rs_st2 = $db->get($query_st2)) { 
                        $spc = ($rs_st_p['stock_cat_id']==$rs_st2['id']) ? "selected" : "";    
                            ?>
                        <option value="<?php echo $rs_st2['id']; ?>" <?php echo $spc; ?>><?php echo $rs_st2['name']; ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="stock_cat_sida" class="col-sm-4 control-label required">หมวดหมู่ย่อย<span class="required">*</span></label>
                <div class="col-sm-8">
                    <select id="stock_cat_sida" name="stock_cat_sida" class="form-control input-sm" >
                    <option value="<?php echo $rs_st_p['stock_cat_id']; ?>" selected><?php echo $rs_st_p['subname']; ?></option>


                    </select>
                </div>
            </div>
                                         
                <div class="form-group">                                            
                <label for="sub_name" class="col-sm-4 control-label required">จำนวนที่ใช้ 1 จาน <span class="required">*</span></label>
                    <div class="col-sm-8">
                        <input type="text" id="v_value" name="v_value" class="form-control input-sm" data-validation="required" >
                    </div>
                    </div>
                    <div class="form-group">                                            
                <label for="sub_name" class="col-sm-4 control-label required">หน่วย <span class="required">*</span></label>
                    <div class="col-sm-8">
                        <input type="text" id="unit" name="unit" class="form-control input-sm" data-validation="required" >
                    </div>
                    </div>                                    
        </div>

                <div class="modal-footer">
                </p>
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                    Close
                    </button>
                    <input type="submit" class="btn btn-primary" value=" Save">
            </div>
            </form>                                        
        </div>                                   
    </div>
    <!-- ปิด AddModal -->

<script type="text/javascript">
    $(document).ready(function() {
        $("#save").click(function() {
            $("#product-form").submit();
            return false;
        });
        $("#save2").click(function() {
            $("#sub-form").submit();
            return false;
        });
    });
    $.validate();


var http = false;
    if(navigator.appName == "Microsoft Internet Explorer"){
      http = new ActiveXObject("Microsoft.XMLHTTP");
      }else{
      http = new XMLHttpRequest();
    }
    // ส่วนที่ 2.2 ฟังก์ชั่นสำหรับการแสดงผลช่อง DropDown ช่องที่ 2
function DisplaySubCategory(product_categorie_id) {
  http.abort();
  http.open("GET", "<?php echo $baseUrl; ?>/back/product/s_category/?product_categorie_id=" + product_categorie_id, true);
  http.onreadystatechange=function() {
    if(http.readyState == 4) {
      document.getElementById('product_categorie_sid').innerHTML = http.responseText;
    }
  }
  http.send(null);
}
function DisplaySubCate(stock_cat_id,id) {
  http.abort();
  http.open("GET", "<?php echo $baseUrl; ?>/back/product/s_st/?stock_cat_id=" + stock_cat_id, true);
  http.onreadystatechange=function() {
    if(http.readyState == 4) {
    var v = stock_cat_id+'_'+id;
    // var id= $( "#id" ).attr( "value" );
      document.getElementById('stock_cat_sid_'+id).innerHTML = http.responseText;
    }
  }
  http.send(null);
}
function DisplaySubCateA(stock_cat_ida) {
  http.abort();
  http.open("GET", "<?php echo $baseUrl; ?>/back/product/s_st/?stock_cat_id=" + stock_cat_ida, true);
  http.onreadystatechange=function() {
    if(http.readyState == 4) {
      document.getElementById('stock_cat_sida').innerHTML = http.responseText;
    }
  }
  http.send(null);
}
</script>
<?php
/*
 * footer***********************************************************************
 */
require 'template/back/footer.php';
/*
 * footer***********************************************************************
 */

