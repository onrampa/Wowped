<?php
 if($_SESSION[_ef . 'levelaccess']== 'user'){
 header('location:'.$baseUrl.'/admin');
 }
 else if($_SESSION[_ef . 'levelaccess']== 'shop'){
 header('location:'.$baseUrl.'/admin');
 }
/*
 * php code///////////**********************************************************
 */
$db = new database();
$option_pc = array(
    "table" => "users"
);
$query_pc = $db->select($option_pc);

$option_pd = array(
    "table" => "users",
    "condition" => "id='{$_GET['id']}' "
);
$query_pd = $db->select($option_pd);
$rs_pd = $db->get($query_pd);

$title = 'แก้ไขร้านค้า : ' .$rs_pd['shopName'];
/*
 * php code///////////**********************************************************
 */

/*
 * header***********************************************************************
 */
require 'template/back/header.php';
/*
 * header***********************************************************************
 */
?>
<script type="text/javascript" src="<?php echo $baseUrl; ?>/js/jquery.form-validator.min.js"></script>
<script type="text/javascript" src="<?php echo $baseUrl; ?>/ckeditor/ckeditor.js"></script>
<div id="page-warpper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">แก้ไขข้อมูล <?php echo $rs_pd['shopName']; ?></h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="subhead">
                <a role="button" id="save" class="btn btn-success btn-xs new-data" href="#">
                 <i class="glyphicon glyphicon-floppy-save"></i>
                    บันทึก
                </a>
                <a role="button" class="search-button btn btn-default btn-xs" href="<?php echo $baseUrl; ?>/back/shop">
                    <i class="glyphicon glyphicon-remove-circle"></i>
                    ยกเลิก
                </a>
                <a role="button" class="search-button btn btn-default btn-xs" target="_blank" href="<?php echo $baseUrl; ?>/back/shop/aa">
                    <i class="glyphicon glyphicon-remove-circle"></i>
                    ข้อตกลงและเงื่อนไขบริการ
                </a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="form-horizontal" style="margin-top: 10px;">
                <form id="shop-form" action="<?php echo $baseUrl; ?>/back/shop/form_update" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="id" value="<?php echo $rs_pd['id'];?>">
                    <input type="hidden" name="id" value="<?php echo $rs_pd['id'];?>">
                    <div class="form-group">
                        <label for="shop_image" class="col-sm-2 control-label required">รูปภาพประจำร้านค้า</label>
                        <div class="col-sm-4">
                            <img src="<?php echo $baseUrl ?>/upload/logo/md_<?php echo $rs_pd['image'];?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="Shop_image" class="col-sm-2 control-label">รูปภาพใหม่</label>
                        <div class="col-sm-4">
                            <input type="file" name="image" id="image">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="Shop_name" class="col-sm-2 control-label required">ชื่อร้านค้า <span class="required">*</span></label>
                        <div class="col-sm-4">
                            <input type="text" id="shopName" name="shopName" maxlength="100" class="form-control input-sm" data-validation="required" value="<?php echo $rs_pd['shopName']; ?>" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="Shop_name" class="col-sm-2 control-label required">เลขที่ บัตรประชาชนผู้เสียภาษี <span class="required">*</span></label>
                        <div class="col-sm-4">
                            <input type="text" id="cid" name="cid" maxlength="13" class="form-control input-sm" data-validation="number" value="<?php echo $rs_pd['cid']; ?>"  >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="Shop_name" class="col-sm-2 control-label required">เบอร์โทร <span class="required">*</span></label>
                        <div class="col-sm-4">
                            <input type="text" id="phone" name="phone" maxlength="20" class="form-control input-sm" data-validation="number" value="<?php echo $rs_pd['phone']; ?>"  >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="Shop_name" class="col-sm-2 control-label required">ชื่อธนาคาร </label>
                        <div class="col-sm-4">
                            <select class="form-control input-sm" name="bankCode" id="bankCode">
                                <option value="0" <?php if($rs_pd['bankCode']=="0" ){echo "selected='selected' ";}?> >-- ระบุ --</option>
                                <option value="KBANK" <?php if($rs_pd['bankCode']=="KBANK" ){echo "selected='selected' ";}?> >ธ.กสิกรไทย</option>
                                <option value="BBL" <?php if($rs_pd['bankCode']=="BBL" ){echo "selected='selected' ";}?> >ธ.กรุงเทพ</option>
                                <option value="KTB"<?php if($rs_pd['bankCode']=="KTB" ){echo "selected='selected' ";}?> >ธ.กรุงไทย</option>
                            </select>
                       </div>
                    </div>
                                        <div class="form-group">
                        <label for="Shop_name" class="col-sm-2 control-label required">เลขที่บัญชี </label>
                        <div class="col-sm-4">
                            <input type="text" id="bankNo" name="bankNo" maxlength="20" class="form-control input-sm" data="number" value="<?php echo $rs_pd['bankNo']; ?>"  >
                        </div>
                    </div>
                                        <div class="form-group">
                        <label for="Shop_name" class="col-sm-2 control-label required">ชื่อบัญชี </label>
                        <div class="col-sm-4">
                            <input type="text" id="bankName" name="bankName" maxlength="100" class="form-control input-sm"  value="<?php echo $rs_pd['bankName']; ?>"  >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="Shop_name" class="col-sm-2 control-label required">จังหวัด </label>
                        <div class="col-sm-4">
                            <input type="text" id="province" name="province" maxlength="100" class="form-control input-sm"  value="<?php echo $rs_pd['province']; ?>"  >
                        </div>
                    </div>  
                    <div class="form-group">
                        <label for="Shop_name" class="col-sm-2 control-label required">อำเภอ </label>
                        <div class="col-sm-4">
                            <input type="text" id="amphur" name="amphur" maxlength="100" class="form-control input-sm"  value="<?php echo $rs_pd['amphur']; ?>"  >
                        </div>
                    </div>  
                    <div class="form-group">
                        <label for="Shop_name" class="col-sm-2 control-label required">ตำบล </label>
                        <div class="col-sm-4">
                            <input type="text" id="district" name="district" maxlength="100" class="form-control input-sm"  value="<?php echo $rs_pd['district']; ?>"  >
                        </div>
                    </div>  
                    <div class="form-group">
                        <label for="Shop_name" class="col-sm-2 control-label required">ที่อยู่ </label>
                        <div class="col-sm-4">
                            <input type="text" id="address" name="address" maxlength="100" class="form-control input-sm"  value="<?php echo $rs_pd['address']; ?>"  >
                        </div>
                    </div>                
                    <div class="form-group">
                        <label for="Shop_name" class="col-sm-2 control-label required">รหัสไปรษณีย์ </label>
                        <div class="col-sm-4">
                            <input type="text" id="zipcode" name="zipcode" maxlength="100" class="form-control input-sm"  value="<?php echo $rs_pd['zipcode']; ?>"  >
                        </div>
                    </div>                     

                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $("#save").click(function() {
            $("#shop-form").submit();
            return false;
        });
    });
    $.validate();
</script>
<?php
/*
 * footer***********************************************************************
 */
require 'template/back/footer.php';
/*
 * footer***********************************************************************
 */
mysql_close();
