<?php

require(base_path() . "/library/uploadimg.php");
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $db = new database();
    if (checkimg() == TRUE) {
        $filename = date('YmdHis') . rand(0, 9);
        $type = end(explode(".", $_FILES["image"]["name"]));
        $image = $filename . "." . $type;

        $path = base_path() . "/upload/logo/";
        uploadimg($filename, 600, 600, $path);
        uploadimg("thumb_" . $filename, 400, 400, $path);
        uploadimg("md_" . $filename, 150, 150, $path);
        uploadimg("sm_" . $filename, 70, 70, $path);
    } else {
        $image = "ecimage.jpg";
    }
    $value_pd = array(
        "name" => trim($_POST['name']),
        "cid" => trim($_POST['cid']),
        "tel" => trim($_POST['tel']),
        // "districe" => trim($_POST['districe']),
        // "amphur" => trim($_POST['amphur']),
        // "province" => trim($_POST['province']),
        // "zipcode" => trim($_POST['zipcode']),
        // "bankNo" => trim($_POST['bankNo']),
        // "bankName" => trim($_POST['bankName']),
        // "bankCode" => trim($_POST['bankCode']),
        // "bankC" => trim($_POST['bankC']),
        // "address" => trim($_POST['address']),
        // "location_survey" => trim($_POST['location_survey']),
        // "material" => trim($_POST['material']),
        // "WIP" => trim($_POST['WIP']),
        // "created" => date('Y-m-d H:i:s'),
        // "image" => $image,
        "latitude" => trim($_POST['latitude']),
        "longitude" => trim($_POST['longitude'])
    );
    $query_pd = $db->insert("shop", $value_pd);

    if ($query_pd == TRUE) {
        header("location:" . $baseUrl . "/back/shop");
    }
    mysql_close();
}