<?php
 if($_SESSION[_ss . 'levelaccess']== 'user'){
 header('location:'.$baseUrl.'/admin');
 }
 if($_SESSION[_ss . 'shop']<> '0'){
    header('location:'.$baseUrl.'/admin');
    }
/*
 * php code///////////**********************************************************
 */
$db = new database();
$option_pc = array(
    "table" => "shop"
);
$query_pc = $db->select($option_pc);


$title = 'เพิ่มร้านค้าใหม่';
/*
 * php code///////////**********************************************************
 */

/*
 * header***********************************************************************
 */
require 'template/back/header.php';
/*
 * header***********************************************************************
 */
?>
<script type="text/javascript" src="<?php echo $baseUrl; ?>/js/jquery.form-validator.min.js"></script>
<script type="text/javascript" src="<?php echo $baseUrl; ?>/ckeditor/ckeditor.js"></script>
<div id="page-warpper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">เพิ่มข้อมูลใหม่</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="subhead">
                <a role="button" id="save" class="btn btn-success btn-xs new-data" href="#">
                    <i class="glyphicon glyphicon-floppy-save"></i>
                    บันทึก
                </a>
                <a role="button" class="search-button btn btn-default btn-xs" href="<?php echo $baseUrl; ?>/back/Shop">
                    <i class="glyphicon glyphicon-remove-circle"></i>
                    ยกเลิก
                </a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="form-horizontal" style="margin-top: 10px;">
                <form id="shop-form" action="<?php echo $baseUrl; ?>/back/shop/form_created" method="post" enctype="multipart/form-data">

                    <div class="form-group">
                        <label for="Shop_image" class="col-sm-2 control-label">รูปใบจดทะเบียนพาณิชย์</label>
                        <div class="col-sm-4">
                            <input type="file" name="image" id="image">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="Shop_name" class="col-sm-2 control-label required">ชื่อร้านค้า <span class="required">*</span></label>
                        <div class="col-sm-4">
                            <input type="text" id="name" name="name" maxlength="100" class="form-control input-sm" data-validation="required" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="Shop_name" class="col-sm-2 control-label required">เลขที่ บัตรประชาชนผู้เสียภาษี <span class="required">*</span></label>
                        <div class="col-sm-4">
                            <input type="text" id="cid" name="cid" maxlength="13" class="form-control input-sm" data-validation="number"  >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="Shop_name" class="col-sm-2 control-label required">เบอร์โทร <span class="required">*</span></label>
                        <div class="col-sm-4">
                            <input type="text" id="tel" name="tel" maxlength="20" class="form-control input-sm" data-validation="number" >
                        </div>
                    </div>
                     <div class="form-group">
                        <label for="Shop_name" class="col-sm-2 control-label required">ชื่อธนาคาร <span class="required">*</span></label>
                        <div class="col-sm-4">
                        <select class="form-control input-sm" name="bankCode" id="bankCode">
                            <option value="0"  >-- ระบุ --</option>
                            <option value="KBANK"  >ธ.กสิกรไทย</option>
                            <option value="BBL"  >ธ.กรุงเทพ</option>
                            <option value="BAY" >ธ.กรุงศรีอยุธยา</option>
                            <option value="TMB" >ธ.ทหารไทย</option>
                            <option value="TBANK" >ธ.ทหารไทย</option>
                            <option value="SCB" >ธ.ไทยพาณิช</option>
                        </select>
                   </div>
                </div>
                <div class="form-group">
                    <label for="Shop_name" class="col-sm-2 control-label required">ธนาคารอื่นๆ (ระบุ)</label>
                    <div class="col-sm-4">
                        <input type="text" id="bankNo" name="bankC" maxlength="50" class="form-control input-sm"   >
                    </div>
                </div>
                                        <div class="form-group">
                        <label for="Shop_name" class="col-sm-2 control-label required">เลขที่บัญชี <span class="required">*</span></label>
                        <div class="col-sm-4">
                            <input type="text" id="bankNo" name="bankNo" maxlength="20" class="form-control input-sm" data-validation="number"  >
                        </div>
                    </div>
                                        <div class="form-group">
                        <label for="Shop_name" class="col-sm-2 control-label required">ชื่อบัญชี <span class="required">*</span></label>
                        <div class="col-sm-4">
                            <input type="text" id="bankName" name="bankName" maxlength="100" class="form-control input-sm"  data-validation="required"  >
                        </div>
                    </div>



                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $("#save").click(function () {
            $("#shop-form").submit();
            return false;
        });
    });
    $.validate();
</script>
<?php
/*
 * footer***********************************************************************
 */
require 'template/back/footer.php';
/*
 * footer***********************************************************************
 */
mysql_close();
