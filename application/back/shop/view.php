<?php
/*
 * php code///////////**********************************************************
 */
if (!isset($_GET['id'])) {
    header("location:" . $baseUrl . "/back/shop");
}
$db = new database();
$sql_od = "SELECT * FROM users ";
$sql_od .="WHERE id='{$_GET['id']}' ";
$query_od = $db->query($sql_od);

$option_os = array(
    "table" => "users",
    "condition" => "id='{$_GET['id']}'"
);
$query_os = $db->select($option_os);
$rows_os = $db->rows($query_os);
if($rows_os != 1){
    header("location:" . $baseUrl . "/back/shop");
}else{
    $rs_os = $db->get($query_os);
}

$title = 'รายละเอียด ร้านค้า';
/*
 * php code///////////**********************************************************
 */

/*
 * header***********************************************************************
 */
require 'template/back/header.php';
/*
 * header***********************************************************************
 */
?>
<div id="page-warpper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">รายละเอียด ร้านค้า</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="subhead">
                <a role="button" class="search-button btn btn-danger btn-xs" href="javascript:history.back()" >
                    <i class="glyphicon glyphicon-circle-arrow-left"></i>
                    ย้อนกลับ
                </a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <div class="form-horizontal" style="margin-top: 10px;">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">ข้อมูลที่ตั้งร้านค้า</h3>
                    </div>
                    <div class="panel-heading">
                     <img src="<?php echo base_url(); ?>/upload/users/sm_<?php echo $rs_od['image']; ?>">
                     </div>
                    <ul class="list-group">
                        <li class="list-group-item"><strong>สมาชิกชื่อ</strong> : <?php echo $rs_os['id'].":".$rs_os['firstname'];?></li>
                        <li class="list-group-item"><strong>ชื่อ</strong> : <?php echo $rs_os['shopName'];?></li>
                        <li class="list-group-item"><strong>เบอร์ติดต่อ</strong> : <?php echo $rs_os['phone'];?></li>
                        <li class="list-group-item"><strong>ที่อยู่</strong> : <?php echo $rs_os['address'];?></li>
                        <li class="list-group-item"><strong>ตำบล</strong> : <?php echo $rs_os['district'];?></li>
                        <li class="list-group-item"><strong>อำเภอ</strong> : <?php echo $rs_os['amphur'];?></li>
                        <li class="list-group-item"><strong>จังหวัด</strong> : <?php echo $rs_os['province'];?></li>
                        <li class="list-group-item"><strong>รหัสไปรษณีย์</strong> : <?php echo $rs_os['zipcode'];?></li>
                        <li class="list-group-item">*วันที่เพิ่มข้อมูล <?php echo thaidate($rs_os['created']);?></li>
                    </ul>
                </div>
            </div>
        </div>
       
    </div>
</div>
<?php
/*
 * footer***********************************************************************
 */
require 'template/back/footer.php';
/*
 * footer***********************************************************************
 */
mysql_close();
