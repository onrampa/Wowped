<?php
/*
 * php code///////////**********************************************************
 */

$id=$_SESSION[_ef . 'id'];

$title = 'รายละเอียด ร้านค้า';
/*
 * php code///////////**********************************************************
 */

/*
 * header***********************************************************************
 */
require 'template/back/header.php';
/*
 * header***********************************************************************
 */
?>
<div id="page-warpper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">รายละเอียด ร้านค้า</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="subhead">
                <a role="button" class="search-button btn btn-danger btn-xs" href="javascript:history.back()" >
                    <i class="glyphicon glyphicon-circle-arrow-left"></i>
                    ย้อนกลับ
                </a>
                <!-- <a role="button" class="search-button btn btn-info btn-xs" href="<?php echo $baseUrl; ?>/back/shop/AddpayShops.docx" >
                    <i class="glyphicon glyphicon-circle-arrow-left"></i>
                    Download
                </a>AddpayShops.docx -->
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="form-horizontal" style="margin-top: 10px;">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">ข้อตกลงและเงื่อนไขความเกี่ยวข้องของคุณ กับ Addpay Shop</h3>
                    </div>
                    1. นิยาม
1.1 เว็บไซต์ของบริษัท แอดเพย์ เซอร์วิสพอยท์ จำกัด (Addpay Service Point Co., Ltd) ในชื่อ "https://addpay.co.th" ซึ่งต่อไปจะเรียกว่า "ผู้ให้บริการ"
1.2 สิ่งที่คุณใช้ ไม่ว่าจะเป็น ผลิตภัณฑ์ ซอฟต์แวร์ บริการ หรือเว็บไซต์ ของผู้ให้บริการ ซึ่งต่อไปจะเรียกว่า "บริการ"
1.3 คุณซึ่งเป็นผู้ใช้บริการไม่ว่าจะส่วนใดส่วนหนึ่งหรือทั้งหมดของผู้ให้บริการ ซึ่งต่อไปจะเรียกว่า "ผู้ใช้บริการ" โดยจะแยกได้ดังนี้
1.3.1 เจ้าของสินค้าหรือผู้ที่นำสินค้ามาจัดจำหน่ายบนเว็บไซต์ผ่านบริการของผู้ให้บริการ ซึ่งต่อไปจะเรียกว่า "ผู้ขาย"
1.3.2 ผู้สั่งซื้อสินค้า และ/หรือ ใช้บริการเว็บไซต์ของผู้ขาย ซึ่งต่อไปจะเรียกว่า "ผู้ซื้อ"

2. ผู้ใช้บริการตกลงจะปฏิบัติตามข้อตกลงและเงื่อนไขต่างๆ
2.1 การใช้บริการของผู้ให้บริการนั้นผู้ใช้บริการจำเป็นต้องเข้าใจและยอมรับข้อตกลงและเงื่อนไขต่างๆ ของผู้ให้บริการทั้งหมด
2.2 จะถือว่าผู้ใช้บริการเข้าใจและยอมรับข้อตกลงและเงื่อนไขต่างๆ หากดำเนินการดังต่อไปนี้
2.2.1 ผู้ใช้บริการคลิกปุ่มตกลง และยอมรับเงื่อนไข ซึ่งทางผู้ให้บริการจะมีทางเลือกที่จะยอมรับหรือไม่ก่อน หรือ แสดงแจ้งเตือนการยอมรับของผู้ใช้บริการก่อนการคลิกปุ่มตกลง
2.2.2 ผู้ใช้บริการเข้าใช้บริการของผู้ให้บริการซึ่งในกรณีนี้ผู้ใช้บริการต้องยอมรับว่าทางผู้ให้บริการจะถือว่าผู้ใช้บริการเข้าใจและยอมรับข้อตกลงและเงื่อนไขต่างๆ ตั้งแต่เริ่มใช้บริการเป็นต้นไป
2.3 ผู้ใช้บริการควรพิมพ์ข้อตกลงและเงื่อนไขต่างๆ ของผู้ให้บริการเพื่อเป็นบันทึกช่วยจำของผู้ใช้บริการ

3. ข้อตกลงและเงื่อนไขของผู้ให้บริการ
ผู้ให้บริการขอสงวนสิทธิ์ไม่ให้บริการเว็บไซต์ร้านค้า ดังต่อไปนี้
3.1 เว็บไซต์ที่ใช้ในจุดประสงค์อื่นๆ ที่ไม่ใช่เพื่อการซื้อ-ขายสินค้า เช่น เว็บไซต์รับประกาศสินค้า  เว็บไซต์รวมลิงค์  เว็บไซต์สื่อกลางระหว่างเว็บไซต์ เว็บไซต์ขายสินค้าแบบ Affiliate เป็นต้น
3.2 ผิดกฎหมายหรือขัดต่อศีลธรรมอันดีของไทย เช่น การละเมิดลิขสิทธิ์ของผู้อื่น การข่มขู่ เว็บไซต์ที่เกี่ยวกับสื่อลามก หรือ เนื้อหาส่อไปทางลามก เซ็กส์ ขายบริการทางเพศ รวมทั้งเว็บไซต์ที่มีลิงค์ไปยังเว็บไซต์ที่เกี่ยวกับสื่อลามก
3.3 เกี่ยวข้องกับธุรกิจเครือข่าย และผลิตภัณฑ์ที่จำหน่ายโดยมีความสัมพันธ์ตามค่าคอมมิชชั่น ซึ่งรวมทั้งหน้ารายการที่ทำขึ้นมาจากโฆษณาเป็นหลัก
3.4 พบทุจริต ฉ้อโกง ในการซื้อ-ขาย ไม่ว่ากรณีใดๆ ทั้งสิ้น (ทั้งในอดีตและปัจจุบัน) ทั้งนี้ผู้ให้บริการขอสงวนสิทธิ์ในการตัดสินใจว่าสิ่งใดถือว่าเป็นการทุจริต
3.5 เกี่ยวกับ Hacker , Warez , Crack,  เผยแพร่ไวรัส หนอนอินเตอร์เน็ต โทร์จัน หรืออื่นๆ ที่เกี่ยวข้องกับซอฟท์แวร์ผิดกฏหมาย
3.6 หลอกลวงผู้อื่นด้วยการแสดงข้อความอันเป็นเท็จ โฆษณาเกินจริง หรือปกปิดข้อความจริงซึ่งควรบอกให้แจ้ง
3.7 ใช้งานที่ส่งผลกระทบกระเทือนกับต่อระบบและทรัพยากรของระบบ อาทิเช่น การใช้งาน CPU เกินขอบเขต การใช้งาน Email เกินขอบเขต การเจาะระบบ (Hacking) การใช้งานโปรแกรม IRC Bots หรือการใช้งานโปรแกรมใดๆ ที่ส่งผลกระทบต่อการทำงานของระบบ และผู้ให้บริการขอสงวนสิทธิ์ในการพิจารณาว่า การใช้งานใดเป็นการใช้งานอย่างเกินขอบเขต
3.8 มีการนำพื้นที่ อีเมล หรือ ทรัพยากรอื่น ๆ ที่ได้รับบริการ ไปแจกจ่ายต่อไปยังบุคคลอื่นในลักษณะฟรี หรือที่เรียกกันว่า Free email และ Free Hosting
3.9 มีการทำ Commercial Mass Emailing หรือ "Spamming" ไม่ว่ากรณีใดๆ ทั้งสิ้น เช่น การส่งโฆษณาไปยังอีเมลผู้อื่นโดยผู้อื่นไม่ได้ขอ, การส่งอีเมลโดยใช้ที่อยู่ส่งกลับที่ไม่มีจริงทางด้านธุรกิจ, การส่งข้อความลูกโซ่เพื่อจะทำให้เกิดการตอบกลับจำนวนมาก และการส่งข้อความซ้ำๆ เดิมๆ ไปยังผู้อื่น เป็นต้น
3.10 มีข้อมูลและไฟล์ต่างๆ บนเครื่องเซิฟเวอร์ของผู้ให้บริการ ติดไวรัส มัลแวร์ ไมว่ากรณีใดๆ
3.12 มีการ เพิ่ม เปลี่ยนแปลง หรือลบ การทำงานหรือการแสดงผลส่วนต่างๆ ของระบบร้านค้า เช่น การนำตะกร้าสินค้า, การสร้างสัญลักษณ์หรือภาพของตนเองเจตนาหลอกลวงว่าเป็นของระบบ

หากผู้ให้บริการตรวจพบเว็บไซต์ร้านค้าที่ผิดข้อตกลงตามดังกล่าว จะดำเนินการงดการให้บริการทันที โดยไม่มีการคืนค่าบริการใดๆ ทั้งสิ้น และไม่จำเป็นต้องแจ้งให้ทราบล่วงหน้า ทั้งนี้ผู้ให้บริการขอสงวนสิทธิ์ในการพิจารณาตรวจสอบร้านค้าว่าผิดตามข้อตกลงหรือไม่ หากมีข้อโต้แย้งใดๆ ให้ถือเอาคำตัดสินของผู้ให้บริการเป็นที่สิ้นสุด

4. ข้อตกลงในการจำหน่ายสินค้า
ผู้ให้บริการมีความประสงค์ที่ให้ผู้ใช้บริการสามารถจำหน่ายสินค้าผ่านระบบร้านค้าเท่านั้น อย่างไรก็ตามผู้ให้บริการอนุโลมให้สามารถใช้บริการในรูปแบบการประกาศขายสินค้าของตนได้  โดยผู้ให้บริการขอสงวนสิทธิ์ไม่ให้บริการเว็บไซต์ร้านค้า ที่จำหน่ายหรือประชาสัมพันธ์สินค้า ดังต่อไปนี้
4.1 สินค้าผิดกฎหมายหรือขัดต่อศีลธรรมอันดีของไทย
4.2 สินค้าแบบ Affiliate จากเว็บไซต์ออนไลน์อื่นๆ เช่น Amazon.com เป็นต้น
4.3 สินค้าที่เกี่ยวข้องกับการพนันทุกประเภท เช่น การเล่นคาสิโนออนไลน์ สถานที่ที่รับพนันขันต่อ บิงโก
4.4 สินค้าที่เกี่ยวข้องกับ Download MP3 , Software , VDO & Voice Clips , Music Online ที่ถูกต้องตามกฎหมายและผิดกฎหมายทุกประเภท
4.5 สินค้าลอกเลียนแบบ ไม่ได้รับอนุญาต ละเมิดเครื่องหมายการค้า สิทธิบัตร ลิขสิทธิ์ หรือ ทรัพย์สินทางปัญญา ของผู้อื่นหรือคณะบุคคลอื่นในทุกประเทศทั่วโลก
4.6 ฝิ่น ยาสูบ ยาเสพติดทุกชนิด หรือบุหรี่ เหล้า เบียร์ที่ผิดกฎหมาย
4.7 สัตว์ที่ใกล้สูญพันธุ์หรือสัตว์ที่ได้รับความคุ้มครองตามกฎหมาย รวมทั้งผลิตภัณฑ์ที่ผลิตจากสัตว์เหล่านี้
4.8 อุปกรณ์พลุไฟ วัสดุและสสารที่มีพิษ ระเบิดได้ ติดไฟ และมีกัมมันตรังสี ซึ่งรวมไปถึงข้อมูลเกี่ยวกับวิธีสร้างอุปกรณ์ระเบิด
4.9 อาวุธหรืออุปกรณ์อันตรายที่ผิดกฎหมาย
4.10 สินค้าที่มีการโฆษณาเกินจริง
4.11 บารากู่ บารากู่ไฟฟ้าหรือบุหรี่ไฟฟ้า หรือตัวยาบารากู่ น้ำยาสำหรับเติมบารากู่ไฟฟ้าหรือบุหรี่ไฟฟ้า

หากผู้ให้บริการตรวจพบเว็บไซต์ร้านค้าที่ผิดข้อตกลงตามดังกล่าว จะดำเนินการงดการให้บริการทันที โดยไม่มีการคืนค่าบริการใดๆ ทั้งสิ้น และไม่จำเป็นต้องแจ้งให้ทราบล่วงหน้า ทั้งนี้ผู้ให้บริการขอสงวนสิทธิ์ในการพิจารณาตรวจสอบร้านค้าว่าผิดตามข้อตกลงหรือไม่ หากมีข้อโต้แย้งใดๆ ให้ถือเอาคำตัดสินของผู้ให้บริการเป็นที่สิ้นสุด

5 ข้อตกลงในการคิดค่าบริการและเปลี่ยนรูปแบบของการให้บริการ
ผู้ให้บริการให้บริการต่างๆ ซึ่งแบ่งรูปแบบการให้บริการและการคิดค่าบริการดังต่อไปนี้
5.1 บริการร้านค้าออนไลน์ พื้นที่เก็บเว็บไซต์และการใช้งานรับ-ส่งข้อมูลผ่านเซิฟเวอร์ของผู้ให้บริการ รวมไปถึงระบบการทำงานต่างๆ ของร้านค้าออนไลน์ ทั้งหมดนี้ผู้ให้บริการจะไม่คิดค่าบริการใดๆ
5.2 บริการเสริมพิเศษ คือ ระบบการทำงานในร้านค้าออนไลน์ หรือบริการอื่นๆ ที่ทางผู้ให้บริการได้กำหนดไว้ จะคิดค่าบริการเป็นช่วงเวลาต่างๆ ซึ่งอาจเป็น วัน เดือน หรือปี หากหมดอายุการใช้บริการ ผู้ใช้บริการจะต้องต่ออายุจึงจะสามารถใช้งานบริการเสริมได้ ทั้งนี้ผู้ใช้บริการสามารถที่จะใช้บริการเสริมที่ทางผู้ให้บริการกำหนดไว้หรือไม่ก็ได้ ไม่มีผลกับการให้บริการร้านค้าออนไลน์ที่ไม่คิดค่าบริการตามข้อ 5.1.
5.3 ผู้ให้บริการอาจปรับเปลี่ยนรูปแบบของการให้บริการ โดยอาจปรับอัตราค่าบริการในระดับที่สูงขึ้น หรือต่ำลงซึ่งจะแจ้งให้ทราบล่วงหน้า 7 วัน
5.4 ในกรณีที่ปรับลดราคาค่าบริการ ผู้ให้บริการขอสงวนสิทธิ์ในการคืนค่าบริการและให้ผู้ใช้บริการชำระเงินตามอัตราค่าบริการใหม่เมื่อทำการต่ออายุ
5.5 ในกรณีที่ปรับเพิ่มราคาค่าบริการ ผู้ใช้บริการเก่าให้ใช้อัตราค่าบริการเดิมในการต่ออายุ ยกเว้นแต่จะแจ้งล่วงหน้าไว้เป็นอย่างอื่น และผู้ใช้บริการใหม่ให้ใช้ตามอัตราค่าบริการใหม่ ทั้งนี้เริ่มนับการใช้บริการตั้งแต่วันที่มีการชำระค่าบริการ

6. ข้อตกลงขอบเขตความรับผิดชอบของผู้ให้บริการ
ผู้ใช้บริการต้องเข้าใจและยอมรับ การให้บริการของผู้ให้บริการดังนี้
6.1 ผู้ให้บริการมีหน้าที่เพียงให้บริการในการอำนวยความสะดวกในการซื้อขายสินค้าระหว่างผู้ซื้อและผู้ขายเท่านั้น ผู้ให้บริการไม่มีส่วนเกี่ยวข้องใดๆ กับสินค้าของผู้ขายแต่อย่างใด
6.2 สินค้าที่ผู้ขายนำมาจัดจำหน่ายผ่านเว็บไซต์ผ่านบริการของผู้ให้บริการ รวมถึงข้อมูลใดๆ ให้ถือว่าผู้ขายเป็นเจ้าของสินค้าหรือข้อมูลนั้นๆ และผู้ขายได้เข้าใจกฎข้อบังคับของผู้ให้บริการเป็นอย่างดีแล้ว
6.3 หากผู้ใช้บริการได้กระทำผิดตามข้อตกลงของสัญญาฉบับนี้ หรือทุจริตในการซื้อ-ขายสินค้า ผู้ใช้บริการจำเป็นต้องชดเชยต่อความเสียหายอันเกิดจากการกระทำดังกล่าวต่อผู้ให้บริการ และ/หรือบุคคลอื่นที่ได้รับความเสียหาย หรือถูกละเมิดจากกรณีดังกล่าวทั้งหมด รวมถึงจะต้องถูกดำเนินคดีตามกฎหมายต่อไป
6.4 ผู้ให้บริการจะไม่รับผิดชอบค่าเสียหายหรือค่าตอบแทนใดๆ ทั้งสิ้น หากผู้ใช้บริการถูกหลอกลวงหรือฉ้อโกงทรัพย์สินจากผู้ใช้บริการอื่น แต่ทางผู้ให้บริการยินดีที่จะให้ความช่วยเหลือถึงที่สุดเพื่อให้ได้ทรัพย์สินของผู้ใช้บริการกลับคืนมา นอกจากนั้นทางผู้ให้บริการจะแสดงระดับความน่าเชื่อถือของร้านค้าไว้ที่ร้านค้าออนไลน์นั้นๆ เพื่อเป็นข้อมูลให้ผู้ใช้บริการประกอบการตัดสินใจในการเลือกซื้อสินค้า
6.5 ผู้ให้บริการไม่อาจให้การรับประกันว่า บริการจะมีอย่างต่อเนื่องโดยไม่มีการหยุด หรือไม่มีข้อบกพร่องใดๆ และผู้ให้บริการจะไม่รับผิดชอบต่อการกระทำโดยจงใจ หรือโดยประมาทเลินเล่อในการทำหน้าที่ของบุคคลที่สาม เช่น ปัญหาการเชื่อมต่ออินเตอร์เน็ต การติดขัดในช่องสัญญาณ เครือข่ายอินเตอร์เน็ต การสื่อสารผิดพลาด ภัยธรรมชาติ การก่อการร้าย อัคคีภัย หรือการหยุดให้บริการชั่วคราว และจากปัญหาที่มิได้เป็นปัญหาของผู้ให้บริการ
6.6 ผู้ให้บริการจะจ่ายคืนค่าใช้บริการตามส่วน หากทางผู้ให้บริการเป็นผู้ยกเลิกสัญญาการให้บริการเองเท่านั้น เช่น การยุติการให้บริการ ล้มเลิก หรือไม่สามารถให้บริการต่อไปได้ และผู้ให้บริการจะไม่รับผิดชอบในการชดใช้ค่าเสียหายอื่น ๆ นอกเหนือจากค่าบริการที่ผู้ใช้บริการได้ชำระกับผู้ให้บริการแล้วเท่านั้น

7. ข้อตกลงในการสำรองข้อมูล
ผู้ให้บริการไม่รับผิดชอบในกรณีที่ข้อมูลภายในเว็บไซต์นั้นสูญหายไปไม่ว่ากรณีใดๆ หรือเนื่องจากความผิดพลาดของอุปกรณ์ อันเป็นเหตุสุดวิสัย ถึงแม้ผู้ให้บริการจะมีบริการสำรองข้อมูลของผู้ใช้บริการ แต่ก็ไม่สามารถรับประกันได้ว่า ข้อมูลนั้นจะมีความถูกต้องครบถ้วน ดังนั้นผู้ใช้บริการจึงควรมีข้อมูลสำรองไว้อย่างน้อยจำนวน 1 ชุดเสมอ อย่างไรก็ตามผู้ให้บริการยินดีให้ความช่วยเหลือถึงที่สุดในการนำข้อมูลกลับคืนมา

8. ข้อตกลงในการรักษาความปลอดภัยในข้อมูลและรหัสผ่าน
8.1 ผู้ใช้บริการต้องเก็บรักษาข้อมูลและรหัสผ่านที่ใช้บริการกับทางผู้ให้บริการไว้เป็นอย่างดี ทางผู้ให้บริการจัดเก็บข้อมูลไว้ในที่ปลอดภัย และผู้ให้บริการจะไม่รับผิดชอบใดๆ หากเกิดความเสียหายแก่เว็บไซต์ หรือข้อมูล ที่เกิดขึ้นจากความประมาทของผู้ใช้บริการ
8.2 ผู้ใช้บริการยอมรับว่าจะต้องรับผิดชอบต่อผู้ให้บริการ สำหรับกิจกรรมทั้งหมดที่เกิดขึ้นภายใต้บัญชีของผู้ใช้บริการ
8.3 ในกรณีที่ผู้ใช้บริการลืมรหัสผ่านใดๆ ผู้ใช้บริการสามารถขอรหัสผ่านใหม่ได้ทางวิธีที่ทางผู้ให้บริการกำหนด โดยผู้ให้บริการจะทำการตั้งรหัสผ่านใหม่ และแจ้งให้ทางอีเมลที่ใช้ในการสั่งซื้อบริการนั้นๆ เท่านั้น
8.4 หากผู้ใช้บริการมีความสงสัยว่าจะข้อมูลหรือรหัสผ่านได้ถูกขโมย ให้ผู้ใช้บริการดำเนินการเปลี่ยนรหัสผ่านโดยทันที และจำเป็นต้องแจ้งให้ผู้ให้บริการทราบด้วย

9. ข้อตกลงในการเก็บรักษาข้อมูลและเนื้อหาที่มีความเป็นส่วนตัว
9.1 สำหรับข้อมูลที่ทางผู้ให้บริการเก็บรักษาไว้ ให้ผู้ใช้บริการอ่านนโยบายความเป็นส่วนตัวของเว็บไซต์ นโยบายนี้จะอธิบายเกี่ยวกับวิธีการเก็บข้อมูล และการรักษาข้อมูลส่วนตัวต่างๆ ที่ผู้ใช้บริการใช้บริการ
9.2 ผู้ใช้บริการต้องยอมรับที่จะใช้ข้อมูลต่างๆ ภายใต้นโยบายความเป็นส่วนตัวของเว็บไซต์บริษัท Addpay Service Point Co., Ltd

10. ข้อตกลงในการขอคืนค่าบริการ
ผู้ให้บริการขอสงวนสิทธิ์ในการคืนค่าบริการในกรณีที่ผู้ใช้ต้องการยกเลิกใช้บริการ หรือปรับลดบริการลง ก่อนครบกำหนดสัญญา และผู้ใช้บริการไม่มีสิทธิ์เรียกค่าบริการคืน ไม่ว่าบางส่วน หรือทั้งหมดได้ เว้นแต่ทางผู้ให้บริการจะเป็นผู้ยกเลิกบริการเอง โดยจะคืนค่าบริการตามสมควรให้เท่านั้น และผู้ใช้บริการจะไม่สามารถเรียกร้องค่าเสียหายอื่นๆ ที่เกิดขึ้นกับการใช้บริการนั้นๆ นอกเหนือจากค่าบริการที่ผู้ใช้บริการได้ชำระกับผู้ให้บริการแล้วเท่านั้น
กรณีมีการละเมิดข้อตกลงการใช้งานผู้ให้บริการจะแจ้งเตือน และจะหยุดให้บริการหากผู้ใช้บริการไม่ทำการแก้ไขให้ถูกต้อง และผู้ใช้บริการจะไม่สามารถเรียกร้องสิทธิ์ในการคืนเงินได้ และหากเป็นการกระทำที่ส่งผลกระทบต่อ ระบบ เครือข่าย และชื่อเสียงของผู้ให้บริการ ผู้ใช้บริการจะต้องรับผิดชอบความเสียหายที่เกิดขึ้นทั้งหมด

11. ข้อตกลงในกรณีที่ผู้ใช้บริการไม่เข้าสู่ระบบเป็นเวลานาน
เนื่องจากการใช้งานร้านค้า Addpay Shop คุณจะไม่สามารถตั้งชื่อ URL ร้านค้าเป็นของตนเองได้ ซึ่งผู้ที่ต้องการเปิดร้านหรือเปลี่ยนชื่อ URL ก็จะไม่สามารถใช้ชื่อนี้ได้ซ้ำได้ ดังนั้นทางทีมงานจึงมีข้อตกลงในการยกเลิก URL ร้านค้าของคุณ เพื่อให้ผู้อื่นสามารถใช้งานแทนได้ โดยจะดำเนินการเป็นขั้นตอนดังนี้

11.1 หากภายในระยะเวลา 3 เดือน ผู้ใช้บริการไม่มีการเข้าสู่ระบบจัดการร้านค้า ผู้ให้บริการขอสงวนสิทธิในการปิดร้านค้าของท่านชั่วคราว โดยร้านค้าสามารถเข้ามาเปิดร้านได้เอง แต่ต้องไม่เกินกว่า 12 เดือน
11.2 หากระยะเวลาเกินกว่า 12 เดือน (1 ปี) ผู้ใช้บริการยังไม่มีการเข้าสู่ระบบจัดการร้านค้า ทางผู้ให้บริการจะยกเลิกการใช้ชื่อ URL ของร้านค้าของผู้ใช้บริการทันที และ URL ที่ใช้ในการเปิดร้านจะถูกเปิดเป็นสาธารณะ โดยผู้ใช้บริการท่านอื่นจะสามารถขอใช้งานชื่อ URL ดังกล่าวแทนได้
11.3 ผู้ใช้บริการจะถูกงดการให้บริการทันที หากผู้ให้บริการพบเห็นการทุจริต และจะต้องถูกดำเนินคดีตามกฎหมายต่อไป
หมายเหตุ: ข้อมูลร้านค้าของคุณทั้งหมดไม่ว่าจะเป็น ข้อมูลสินค้า ข้อมูลหน้าร้าน รูปภาพ และรายละเอียดอื่นๆ ในร้าน จะไม่มีการลบออกไปแต่อย่างใด แม้ว่าผู้ใช้บริการไม่มีการเข้าสู่ระบบจัดการร้านค้าเลยก็ตาม ตามนโยบายการให้บริการของ Addpay Shop ที่จะไม่ลบร้านค้าของคุณ

12. ข้อตกลงในการระงับการให้บริการ
ผู้ให้บริการสามารถระงับการให้บริการแก่ร้านค้าใดๆ ตามเห็นสมควร ถึงแม้ว่าร้านค้าจะไม่ได้ละเมิดข้อตกลงใดๆ เลยก็ตาม โดยผู้ให้บริการจะคืนค่าบริการเฉพาะในส่วนที่ยังไม่ได้ใช้งานตามจริงเท่านั้น ตัวอย่างเช่น บริการเสริมที่ยังเหลืออายุอยู่ เป็นต้น ยกเว้นบริการโดเมนเนม ที่จะไม่มีการคืนค่าบริการ ทั้งนี้เจ้าของร้านค้าสามารถแจ้งให้ผู้ให้บริการชี้โดเมนเนม (เปลี่ยนค่า Name Server) ไปยังผู้ให้บริการรายอื่น หรือย้ายโดเมนเนมไปยังผู้ให้บริการรายอื่นได้ และผู้ใช้บริการจะไม่สามารถเรียกร้องค่าเสียหายอื่นๆ นอกเหนือจากค่าบริการที่ผู้ใช้บริการได้ชำระกับผู้ให้บริการแล้วเท่านั้น

13. ข้อตกลงในการเปลี่ยนแปลงข้อตกลงและเงื่อนไขต่างๆ
13.1 ผู้ให้บริการสามารถเปลี่ยนแปลงนโยบายข้อตกลงและเงื่อนไขต่างๆ ในการให้บริการได้โดยไม่จำเป็นต้องแจ้งให้ทราบล่วงหน้า
13.2 ผู้ใช้บริการต้องเข้าใจและยอมรับข้อตกลงและเงื่อนไขต่างๆ ของผู้ให้บริการที่ได้มีการเปลี่ยนแปลงแล้ว หากผู้ใช้บริการเข้าใช้บริการหลังมีการเปลี่ยนแปลง

 
                </div>
            </div>
        </div>
       
    </div>
</div>
<?php
/*
 * footer***********************************************************************
 */
require 'template/back/footer.php';
/*
 * footer***********************************************************************
 */

