<?php
require(base_path() . "/library/uploadimg.php");

$db = new database();
$shop=trim($_POST['bankName']);

$id=$_POST['id'];
$option_im = array(
    "table" => "users",
    "fields" => "image",
    "condition" => "id='{$_POST['id']}'"
);
$query_im = $db->select($option_im);
$rs_im = $db->get($query_im);

if (checkimg() == TRUE) {
    $filename = date('YmdHis') . rand(0, 9);
    $type = end(explode(".", $_FILES["image"]["name"]));
    $image = $filename . "." . $type;

    $path = base_path() . "/upload/logo/";
    uploadimg($filename, 600, 600, $path);
    uploadimg("thumb_" . $filename, 400, 400, $path);
    uploadimg("md_" . $filename, 150, 150, $path);
    uploadimg("sm_" . $filename, 70, 70, $path);

    if ($rs_im['image'] != "ecimage.jpg") {
        @unlink($path . $rs_im['image']);
        @unlink($path . "thumb_" . $rs_im['image']);
        @unlink($path . "md_" . $rs_im['image']);
        @unlink($path . "sm_" . $rs_im['image']);
    }
} else {
    $image = $rs_im['image'];
}
    $value_pd = array(
        "shopName" => trim($_POST['shopName']),
        "cid" => trim($_POST['cid']),
        "Cclose" => trim($_POST['Cclose']),
        "bankNo" => trim($_POST['bankNo']),
        "bankName" => trim($_POST['bankName']),
        "shop" => trim($_POST['MemID']),
        "bankCode" => trim($_POST['bankCode']),
        "image" => $image        
   
    );
    $query_pd = $db->update("users", $value_pd, "id='{$_POST['id']}'");

    // $sql_or = "UPDATE users set shopName= $_POST['shopName'], shopName= $_POST['shopName'], shopName= $_POST['shopName'], shopName= $_POST['shopName'], shopName= $_POST['shopName'], ";
    // $sql_or .= "WHERE  id = '$id' ";
    // $query_or = $db->query($sql_or);

    if ($query_pd == TRUE) {
        header("location:" . $baseUrl . "/back/shop");
    }
    
