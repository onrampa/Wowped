<?php

 $id=$_SESSION[_ef . 'id'];

/*
 * php code///////////**********************************************************
 */
$db = new database();
$option_pc = array(
    "table" => "users"
);
$query_pc = $db->select($option_pc);

$option_pd = array(
    "table" => "users",
    "condition" => "id='{$id}' "
);
$query_pd = $db->select($option_pd);
$rs_pd = $db->get($query_pd);

$title = 'แก้ไขร้านค้า : ' .$rs_pd['shopName'].'ID :'.$id ;
/*
 * php code///////////**********************************************************
 */

/*
 * header***********************************************************************
 */
require 'template/back/header.php';
/*
 * header***********************************************************************
 */
?>
<script type="text/javascript" src="<?php echo $baseUrl; ?>/js/jquery.form-validator.min.js"></script>
<script type="text/javascript" src="<?php echo $baseUrl; ?>/ckeditor/ckeditor.js"></script>
<div id="page-warpper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">ร้านค้าออนไลน์ <?php echo $rs_pd['shopName'].'ID :'.$id ; ?></h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="subhead">
                <a role="button" id="save" class="btn btn-success btn-xs new-data" href="#">
                 <i class="glyphicon glyphicon-floppy-save"></i>
                    บันทึก
                </a>
                <a role="button" class="search-button btn btn-default btn-xs" href="<?php echo $baseUrl; ?>/back/shop">
                    <i class="glyphicon glyphicon-remove-circle"></i>
                    ยกเลิก
                </a>
                <a role="button" class="search-button btn btn-info btn-xs" href="<?php echo $baseUrl; ?>/back/shop/aa">
                    <i class="glyphicon glyphicon-zoom-in"></i>
                    ข้อตกลงและเงื่อนไขบริการ
                </a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="form-horizontal" style="margin-top: 10px;">
                <form id="shop-form" action="<?php echo $baseUrl; ?>/back/shop/form_updated" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="id" value="<?php echo $rs_pd['id'];?>">
                    <input type="hidden" name="MemID" value="<?php echo $rs_pd['MemID'];?>">

                    <div class="form-group">
                        <label for="shop_image" class="col-sm-2 control-label required">รูปภาพประจำร้านค้า</label>
                        <div class="col-sm-4">
                            <img src="<?php echo $baseUrl ?>/upload/logo/md_<?php echo $rs_pd['image'];?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="Shop_image" class="col-sm-2 control-label">รูปภาพใหม่</label>
                        <div class="col-sm-4">
                            <input type="file" name="image" id="image">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="Shop_name" class="col-sm-2 control-label required">ชื่อร้านค้า <span class="required">*</span></label>
                        <div class="col-sm-4">
                            <input type="text" id="shopName" name="shopName" maxlength="100" class="form-control input-sm" data-validation="required" value="<?php echo $rs_pd['shopName']; ?>" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="Shop_name" class="col-sm-2 control-label required">เลขที่ บัตรประชาชนผู้เสียภาษี <span class="required">*</span></label>
                        <div class="col-sm-4">
                            <input type="text" id="cid" name="cid" maxlength="20" class="form-control input-sm" data-validation="number" value="<?php echo $rs_pd['cid']; ?>"  >
                        </div>
                    </div>

                     <div class="form-group">
                        <label for="Shop_name" class="col-sm-2 control-label required">ชื่อธนาคาร </label>
                        <div class="col-sm-4">
                            <select class="form-control input-sm" name="bankCode" id="bankCode">
                                <option value="0" <?php if($rs_pd['bankCode']=="0" ){echo "selected='selected' ";}?> >-- ระบุ --</option>
                                <option value="KBANK" <?php if($rs_pd['bankCode']=="KBANK" ){echo "selected='selected' ";}?> >ธ.กสิกรไทย</option>
                                <option value="BBL" <?php if($rs_pd['bankCode']=="BBL" ){echo "selected='selected' ";}?> >ธ.กรุงเทพ</option>
                                <option value="KTB"<?php if($rs_pd['bankCode']=="KTB" ){echo "selected='selected' ";}?> >ธ.กรุงไทย</option>
                                <option value="KBANK" <?php if($rs_pd['bankCode']=="SCB" ){echo "selected='selected' ";}?> >ธ.ไทยพาณิช</option>
                                <option value="BBL" <?php if($rs_pd['bankCode']=="BAY" ){echo "selected='selected' ";}?> >ธ.กรุงศรีอยุธยา</option>
                                <option value="KTB"<?php if($rs_pd['bankCode']=="Tbank" ){echo "selected='selected' ";}?> >ธ.ธนชาติ</option>                           </select>
                       </div>
                    </div>
                    <div class="form-group">
                        <label for="Shop_name" class="col-sm-2 control-label required">เลขที่บัญชี </label>
                        <div class="col-sm-4">
                            <input type="text" id="bankNo" name="bankNo" maxlength="20" class="form-control input-sm" data="number" value="<?php echo $rs_pd['bankNo']; ?>"  >
                        </div>
                    </div>
                 <div class="form-group">
                        <label for="Shop_name" class="col-sm-2 control-label required">ชื่อบัญชี </label>
                        <div class="col-sm-4">
                            <input type="text" id="bankName" name="bankName" maxlength="100" class="form-control input-sm"  value="<?php echo $rs_pd['bankName']; ?>"  >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="Shop_name" class="col-sm-2 control-label required">ชื่อที่ตั้ง </label>
                        <div class="col-sm-4">
                            <input type="text" id="address" name="address" maxlength="100" class="form-control input-sm"  value="<?php echo $rs_pd['address']; ?>"  >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="Shop_name" class="col-sm-2 control-label required">สถานะ </label>
                        <div class="col-sm-4">
                            <select class="form-control input-sm" name="Cclose" id="Cclose">
                                <option value="0" <?php if($rs_pd['Cclose']=="0" ){echo "selected='selected' ";}?> >-- ปิด --</option>
                                <option value="1" <?php if($rs_pd['Cclose']=="1" ){echo "selected='selected' ";}?> ><< เปิด >></option>
                         </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $("#save").click(function() {
            $("#shop-form").submit();
            return false;
        });
    });
    $.validate();
</script>
<?php
/*
 * footer***********************************************************************
 */
require 'template/back/footer.php';
/*
 * footer***********************************************************************
 */
mysql_close();
