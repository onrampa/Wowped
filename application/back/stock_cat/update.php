<?php
 if($_SESSION[_ef . 'levelaccess']== 'user'){
 header('location:'.$baseUrl.'/admin');
 }
 else if($_SESSION[_ef . 'levelaccess']== 'cashier'){
 header('location:'.$baseUrl.'/admin');
 }
/*
 * php code///////////**********************************************************
 */
$db = new database();
$_SESSION[_ef . 'categorie']=$_GET['id'];
$option_pc = array(
    "table" => "stock_cat",
    "condition" => "id='{$_GET['id']}' "
);
$query_pc = $db->select($option_pc);
$rs_pc = $db->get($query_pc);
$title = 'แก้ไขหมวดหมู่สินค้า : ' .$rs_pc['name'];
/*
 * php code///////////**********************************************************
 */

/*
 * header***********************************************************************
 */
require 'template/back/header.php';
/*
 * header***********************************************************************
 */
?>
<script type="text/javascript" src="<?php echo $baseUrl; ?>/js/jquery.form-validator.min.js"></script>
<div id="page-warpper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">แก้ไขข้อมูล <?php echo $rs_pc['name']; ?></h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="subhead">
                <a role="button" id="save" class="btn btn-success btn-xs new-data" href="#">
                    <i class="glyphicon glyphicon-floppy-save"></i>
                    บันทึก
                </a>
                <a role="button" class="search-button btn btn-default btn-xs" href="<?php echo $baseUrl; ?>/back/stock_cat">
                    <i class="glyphicon glyphicon-remove-circle"></i>
                    ยกเลิก
                </a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="form-horizontal" style="margin-top: 10px;">
                <form id="product-form" action="<?php echo $baseUrl; ?>/back/stock_cat/form_update" method="post">
                    <input type="hidden" name="id" value="<?php echo $rs_pc['id'];?>">
                    <div class="form-group">
                        <label for="Product_name" class="col-sm-2 control-label required">ชื่อหมวดหมู่วัตถุดิบ <span class="required">*</span></label>
                        <div class="col-sm-4">
                            <input type="text" id="name" name="name" class="form-control input-sm" data-validation="required" value="<?php echo $rs_pc['name']; ?>" >
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php 
$option_psc = array(
    "table" => "stock_subcat",
    "condition" => "catID='{$_GET['id']}' "
);

$query_psc = $db->select($option_psc);
// $rs_psc = $db->get($query_psc);
?>
<div class="row">
<div class="col-lg-12">
            <div class="subhead">
            <a class="btn btn-success btn-xs new-data" title="" href="#" data-toggle="modal" data-target="#AddModal">
            <i class="glyphicon glyphicon-plus-sign"></i> เพิ่มหมวดย่อย</a>                                
            </div>
        </div>

    <div class="col-lg-12">
        <div id="user-grid" class="grid-view">
            <table class="table table-striped table-custom">
                <thead>
                    <tr>
                        <th id="user-grid_c0">
                            <a class="sort-link" href="<?php echo $uri; ?>">ชื่อ</a>
                        </th>
                        <th id="user-grid_c1">
                            <a class="sort-link" href="<?php echo $uri; ?>">คงเหลือ</a>
                        </th>
                        <th id="user-grid_c1">
                            <a class="sort-link" href="<?php echo $uri; ?>">หน่วย</a>
                        </th>
                        <th id="user-grid_c4">
                            <a class="sort-link" href="<?php echo $uri; ?>">ปรับปรุง</a>
                        </th>
                        <th class="button-column" id="user-grid_c6">&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $i = 0;
                    while ($rs_psc = $db->get($query_psc)) {
                        $tr = ($i % 2 == 0) ? "odd" : "even";
                        ?>
                        <tr class="<?php echo $tr; ?>">
                            <td>
                                <?php echo $rs_psc['name']; ?>
                            </td>
                            <td><?php echo $rs_psc['instock']; ?></td>
                            <td><?php echo $rs_psc['unit']; ?></td>
                            <td><?php echo thaidate($rs_psc['updated']); ?></td>
                            <td class="button-column">
                                <a class="btn btn-warning btn-xs load_data" title="" href="#" data-toggle="modal" data-target="#editModal<?php echo $rs_psc['id'];?>"><i class="glyphicon glyphicon-edit"></i> แก้ไข</a>                                
                                <a class="btn btn-danger btn-xs confirm" title="" href="#" data-toggle="modal" data-target="#deleteModal<?php echo $rs_psc['id'];?>"><i class="glyphicon glyphicon-remove"></i> ลบ</a>
 
                                <!-- editModal -->
                                
                                <div class="modal fade" id="editModal<?php echo $rs_psc['id'];?>">
                                <div class="modal-dialog">
                                <form id="sub-form" action="<?php echo $baseUrl; ?>/back/stock_subcat/update/<?php echo $rs_pc['id'].'?id='.$rs_psc['id']; ?>" method="post">                                       
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                <h4 class="modal-title" id="myModalLabel">แจ้งเตือนการ แก้ไขข้อมูล</h4>
                                            </div>

                                            <div class="modal-body">  
                                            <input type="hidden" id="sid" name="sid" class="form-control input-sm" data-validation="required" value="<?php echo $rs_pc['id']; ?>">
                                            <div class="form-group">                                            
                                                <label for="sub_name" class="col-sm-4 control-label required">ชื่อหมวดหมู่ย่อย <span class="required">*</span></label>
                                                <div class="col-sm-6">
                                                     <input type="text" id="name" name="name" class="form-control input-sm" data-validation="required"  value="<?php echo $rs_psc['name']; ?>">
                                                </div> </div>                                          
                                            <div class="form-group">                                            
                                            <label for="sub_name" class="col-sm-4 control-label required">จำนวนที่มีอยู่ <span class="required">*</span></label>
                                                <div class="col-sm-6">
                                                    <input type="number" id="instock" name="instock" class="form-control input-sm" data-validation="required"  value="<?php echo $rs_psc['instock']; ?>">
                                                </div>
                                             </div>
                                             <div class="form-group">                                            
                                            <label for="sub_name" class="col-sm-4 control-label required">หน่วย <span class="required">*</span></label>
                                                <div class="col-sm-6">
                                                    <input type="text" id="unit" name="unit" class="form-control input-sm" data-validation="required"  value="<?php echo $rs_psc['unit']; ?>">
                                                </div>
                                             </div>                                    
                                    </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">
                                                Close
                                                </button>
                                                <input type="submit" class="btn btn-primary" value=" Save">
                                            </div>
                                            </div>
                                        </form>                                        
                                    </div>                                   
                                </div>
                                <!-- ปิด editModal -->

                                <!-- deleteModal -->
                                <div class="modal fade" id="deleteModal<?php echo $rs_psc['id'];?>" >
                                    <div class="modal-dialog">
                                    <form id="sub-form" action="<?php echo $baseUrl; ?>/back/stock_subcat/delete/<?php echo $rs_pc['id'].'?sid='.$rs_psc['id']; ?>" method="post">        
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                    <h4 class="modal-title" id="myModalLabel">แจ้งเตือนการ ลบข้อมูล</h4>
                                                </div>
                                                <div class="modal-body">  
                                                <input type="text" id="sid" name="sid" class="form-control input-sm" data-validation="required" value="<?php echo $rs_psc['id']; ?>">
                                                <input type="text" id="name" name="name" class="form-control input-sm" data-validation="required" value="<?php echo $rs_psc['name']; ?>">
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">
                                                    Close
                                                    </button>
                                                    <input type="submit" class="btn btn-primary" value=" Delete">
                                            </div>
                                            </div>
                                    </form>                                        
                                    </div>
                                </div>
                                <!-- ปิด deleteModal -->
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>


</div>


    <!-- Modal Zone -->
    <!-- AddModal -->
    <div class="modal fade" id="AddModal">
    <div class="modal-dialog">
    <form id="sub-form" action="<?php echo $baseUrl; ?>/back/stock_subcat/create/<?php echo $rs_pc['id']; ?>" method="post">        
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel">การ เพิ่มข้อมูล</h4>
                </div>
                
                <div class="modal-body">  
                <input type="hidden" id="sid" name="sid" class="form-control input-sm" data-validation="required" value="<?php echo $rs_pc['id']; ?>">
                <div class="form-group">                                            
                    <label for="sub_name" class="col-sm-4 control-label required">ชื่อหมวดหมู่ย่อย <span class="required">*</span></label>
                    <div class="col-sm-6">
                            <input type="text" id="name" name="name" class="form-control input-sm" data-validation="required" >
                    </div> </div>                                          
                <div class="form-group">                                            
                <label for="sub_name" class="col-sm-4 control-label required">จำนวนที่มีอยู่ <span class="required">*</span></label>
                    <div class="col-sm-6">
                        <input type="number" id="instock" name="instock" class="form-control input-sm" data-validation="required" >
                    </div>
                    </div>
                    <div class="form-group">                                            
                <label for="sub_name" class="col-sm-4 control-label required">หน่วย <span class="required">*</span></label>
                    <div class="col-sm-6">
                        <input type="text" id="unit" name="unit" class="form-control input-sm" data-validation="required" >
                    </div>
                    </div>                                    
        </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                    Close
                    </button>
                    <input type="submit" class="btn btn-primary" value=" Save">
            </div>
            </form>                                        
        </div>                                   
    </div>
    <!-- ปิด AddModal -->

<script type="text/javascript">
    $(document).ready(function() {
        $("#save").click(function() {
            $("#product-form").submit();
            return false;
        });
        $("#save2").click(function() {
            $("#sub-form").submit();
            return false;
        });
    });
    $.validate();
    
</script>
<?php
/*
 * footer***********************************************************************
 */
require 'template/back/footer.php';
/*
 * footer***********************************************************************
 */
mysql_close();
