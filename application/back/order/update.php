<?php

if($_SESSION[_ef . 'levelaccess']== 'user'){
header('location:'.$baseUrl.'/admin');
}
else if($_SESSION[_ef . 'levelaccess']== 'shop'){
header('location:'.$baseUrl.'/back/orderd');
}
$db = new database();
$title = 'ตรวจสอบข้อมูลการสั่งซื้อ';


$q_or="SELECT p.name,p.price,p.instock,p.Weight,o.* FROM order_details o
LEFT JOIN products p ON o.product_id=p.id where o.order_id ='{$_GET['id']}' ORDER BY o.product_id ASC ";   
$query_or = $db->query($q_or);   
// $rs_or = $db->get($query_or);
$or_count = $db->rows($query_or);
// echo $q_or;
$q_ct="select users.shopName,products.*  from products LEFT JOIN users ON products.shop=users.MemID  ORDER BY products.shop ASC ";   
 $query_ct = $db->query($q_ct);   
    // $query_or = $db->seleor($option_or);
    $me_count = $db->rows($query_ct);



/*
 * php code///////////**********************************************************
 */

/*
 * header***********************************************************************
 */
require 'template/back/header.php';
/*
 * header***********************************************************************
 */
?>
<script type="text/javascript" src="<?php echo base_url(); ?>/js/imagelightbox.min.js"></script>
<script type="text/javascript" src="<?php echo $baseUrl; ?>/js/jquery.form-validator.min.js"></script>
<style>
    #imagelightbox
    {
        position: fixed;
        z-index: 9999;

        -ms-touch-aorion: none;
        touch-aorion: none;
    }
</style>

<div id="page-warpper">
    
    <div class="row">
 

            <div class="row" style="font-size:13px;">
                <hr>
                    <form aorion="<?php echo base_url(); ?>/order/save" method="post" name="cartform" id="cartform" role="form" class="form-horizontal">
       
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>สินค้า</th>
                                    <th>ร้านค้า</th>
                                    <th style="text-align:center;">ราคา/หน่วย</th>
                                    <th style="width: 100px;text-align: center;">จำนวน</th>
                                    <th style="width: 100px;text-align: center;">น้ำหนัก (กรัม)</th>
                                    <th style="text-align:center;">จำนวนเงินรวม</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 0;
                                $total_price = 0;
                                while ($rs_or = $db->get($query_or)) {
                                    $key = $rs_or['quantity'];
                                   
                                    ?>
                                    <tr>
                                        <td>
                                            <a href="<?php echo base_url(); ?>/product/view/<?php echo $rs_or['id']; ?>">
                                                <?php echo $rs_or['name']; ?>
                                                <input type="hidden" name="product_<?php echo $i;?>" value="<?php echo $rs_or['id']?>"> 
                                            </a>
                                        </td>
                                         <td>
                                            <a href="<?php echo base_url(); ?>/product/shop/<?php echo $rs_or['shop']; ?>">
                                                <?php echo $rs_or['shop']; ?>
                                                <input type="hidden" name="shop_<?php echo $i;?>" value="<?php echo $rs_or['shop']?>"> 
                                            </a>
                                        </td>
                                        <td style="text-align:right;">
                                            <?php echo number_format($rs_or['price'], 2); ?>
                                            <input type="hidden" name="price_<?php echo $i;?>" value="<?php echo $rs_or['price'];?>">
                                        </td>
                                        <td style="text-align: right;">
                                            <?php echo $rs_or['quantity']; ?>
                                            <input type="hidden" name="qty_<?php echo $i;?>" value="<?php echo $rs_or['quantity']; ?>">
                                        </td>
                                       <td style="text-align:right;">
                                            <?php 
                                            $logisP[$i]=$rs_or['Weight'] * $rs_or['quantity'];
                                            echo number_format($logisP[$i], 2); ?>
<?php
$sumWP=0;
$input_logis=3;
$option_logis = array(
        "table" => "logisprice",
        "condition" => "weight >= {$logisP[$i]} and logis IN ({$input_logis})"
    );
 $qlogis="select * from logisprice where weight >= $logisP[$i] and logis IN ({$input_logis}) ORDER BY price ASC ";   
 $query_logis = $db->query($qlogis);
 $rows_pc = $db->rows($query_logis);   
 if($rows_pc<>0)
 {
//  $query_logis = $db->seleor($option_logis);
$a=0;
 while ($rs_logis = $db->get($query_logis)) {
     $price[$a]=$rs_logis['price'];
     $a++;
 }
//  $WP=$price[0];
 $WP=0;
 }
 else {$WP=0;}
// $logis= $WP."(ค่าส่งพัสดุ)";
 if ($rs_or['quantity']>=$rs_or['logis'] and $rs_or['logis'] > 0)
//  if(($rs_or['quantity']>=$rs_or['logisFree']) and ($rs_or['logisFree']> 0))
 { 
     $WP=0;
     
     }
if($rs_or['logis'] > 0)
{
     $Free=$rs_or['logis'];
     $logis= $WP." (".$Free." ชิ้น ส่งฟรี)";
}
else{
    $logis= $WP." (ค่าพัสดุ)";
}
?>
                                       </td>
                                        <td style="text-align:right;">                                      
                                            <input type="hidden" name="logis_<?php echo $i;?>" value="<?php echo $WP;?>"> 
                                             <?php
                                           $sumPrice[$i]=$rs_or['price'] * $rs_or['quantity'];
                                            echo number_format($sumPrice[$i], 2)." + ".$logis ;
                                             $total_price = $total_price + ($rs_or['price'] * $rs_or['quantity'])+$WP;
                                             ?>
                                        </td>
                                    </tr>

                                
                                <?php
                                $i++;
                            }
                            ?>
                            <tr>
                                <td colspan="7" style="text-align: right;">
                                    <h4>จำนวนเงินรวมทั้งหมด <?php echo number_format($total_price); ?> บาท</h4>
                                    <input type="hidden" name="total" value="<?php echo $total_price;?>">
                                    <input type="hidden" name="count_item" value="<?php echo $i;?>">
                                    <input type="hidden" name="sumWP" value="<?php echo $sumWP;?>">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="7" style="text-align: right;">
                               <a role="button" class="btn btn-info btn-xs load_data" href="<?php echo $baseUrl; ?>/back/order/payment1/<?php echo $_GET['id']; ?>">
                                   <i class="glyphicon glyphicon-remove-circle"></i>
                                   ย้อนกลับ
                               </a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </form>

            </div>
            </div>   


<script type="text/javascript">
    $(document).ready(funorion () {
        $('a').imageLightbox();
        $.validate();
        $('.saveform').click(funorion () {
            $('#cartform').submit();
        });
        $('.goback').click(funorion () {
            window.location = 'cart';
        });
    });
</script>
<?php
/*
 * footer***********************************************************************
 */
require 'template/front/footer.php';
/*
 * footer***********************************************************************
 */
