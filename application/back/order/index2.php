<?php
 if($_SESSION[_ef . 'levelaccess']== 'user'){
 header('location:'.$baseUrl.'/admin');
 }
 else if($_SESSION[_ef . 'levelaccess']== 'shop'){
 header('location:'.$baseUrl.'/back/orderd');
 }
/*
 * include file start***********************************************************
 */
require 'library/pagination.php';
/*
 * php code///////////**********************************************************
 */
$title = 'ระบบจัดการร้านค้า : รายการโอนเงินให้ร้านค้า';
$db = new database();
$pagination = new Zebra_Pagination();

$order_id = isset($_GET['order_id'])? "AND id='{$_GET['order_id']}'" : "";
$c_date = date('Y-m-d');
$d1=$c_date."  00:00:00";
$c_date2 = date ("Y-m-d", strtotime("+1 day"));
$d2=$c_date2."  00:00:00";
$bt=" AND  order_date BETWEEN '{$d1}' AND '{$d2}'";
if($_GET['que_id']>0) {$bt=" AND que = '{$_GET['que_id']}' " ; }
if($_GET['order_id']>0) {$bt=" AND id = '{$_GET['order_id']}' " ; }

$sql_or = "SELECT * FROM v_orderdetail o ";
$sql_or .= "WHERE 1=1 {$bt}  AND o.checkB ='0'  AND o.aw_status in ('P','C','T') ";
$sql_or .= isset($_GET['name']) ? "AND o.order_id LIKE '%{$_GET['name']}%' " : "";

$query_or = $db->query($sql_or);
$rows_pc = $db->rows($query_or);

$per_page = 20;
$page_start = (($pagination->get_page() - 1) * $per_page);
$sql_or .= "ORDER BY tb,id ASC LIMIT {$page_start},{$per_page} ";
$pagination->records($rows_pc);
$pagination->records_per_page($per_page);
$pagination->base_url('', FALSE);
$query_or_page = $db->query($sql_or);

$page = ($page_start != 0) ? $page_start : "1";
$pages = ceil($rows_pc / $per_page);

$uri = $_SERVER['REQUEST_URI']; // url

/*
 * php code///////////**********************************************************
 */

/*
 * header***********************************************************************
 */
require 'template/back/header.php';
/*
 * header***********************************************************************
 */
?>

<div id="page-warpper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">ข้อมูล บิล-แยกรายการ </h1>
           
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="subhead">
                <a role="button" class="search-button btn btn-default btn-xs" href="#">
                    <i class="glyphicon glyphicon-search"></i>
                    ค้นหาขั้นสูง
                </a>
                <a role="button" class="btn btn-default btn-xs" 
                   href="<?php echo $baseUrl; ?>/back/order/index2">
                    <i class="glyphicon glyphicon-refresh"></i>
                    โหลดหน้าจอใหม่
                </a>
               
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="search-form" style="display:none">

                <form id="yw0" action="<?php echo $baseUrl; ?>/back/order/index2" method="get">
                    <div class="form-horizontal" style="margin-top: 10px;">
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">รหัสสั่งซื้อ</label>
                            <div class="col-sm-4">
                                <input class="form-control input-sm" name="order_id" id="order_id" type="text" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">รหัส QUE</label>
                            <div class="col-sm-4">
                                <input class="form-control input-sm" name="que_id" id="que_id" type="text" />
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-4">
                                <button type="submit" class="btn btn-primary searchbtn"><i class="glyphicon glyphicon-search"></i> ค้นหาเดี๋ยวนี้!</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div><!-- search-form -->
            <div id="user-grid" class="grid-view">
                <div class="summary">หน้า <?php echo $page; ?> จากทั้งหมด <?php echo $pages; ?> หน้า</div>
                <table class="table table-striped table-custom">
                    <thead>
                        <tr>
                            <th id="user-grid_c0">
                                <a class="sort-link" href="<?php echo $uri; ?>">สถานะ</a>
                            </th>
                            <th id="user-grid_c1">
                                <a class="sort-link" href="<?php echo $uri; ?>">โต๊ะ</a>
                            </th>
                            <th id="user-grid_c2">
                                <a class="sort-link" href="<?php echo $uri; ?>">รหัสสั่งซื้อ</a>
                            </th>
                            <th id="user-grid_c4">
                                <a class="sort-link" href="<?php echo $uri; ?>">วันที่สั่งซื้อ</a>
                            </th>
                            <th id="user-grid_c4">
                                <a class="sort-link" href="<?php echo $uri; ?>">ผู้สั่งซื้อ</a>
                            </th>
                            <th id="user-grid_c4">
                                <a class="sort-link" href="<?php echo $uri; ?>">(ราคา*จำนวน)</a>
                            </th>
                            <th id="user-grid_c4" style="text-align: right;">
                                <a class="sort-link" href="<?php echo $uri; ?>">รวมราคา(บาท)</a>
                            </th>
                            <th class="button-column" id="user-grid_c6">&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 0;
                        while ($rs_or = $db->get($query_or_page)) {
                            $tr = ($i % 2 == 0) ? "odd" : "even";
                            //สถานะ             //สถานะ W=รอ P=โอนแล้ว C=กำลังขนส่ง T=รับสินค้า R=คืน S=ได้เงินบริษัท
                            switch ($rs_or['aw_status']) {
                                case 'P':
                                    $aw_status = 'ยังไม่ทำอาหาร';
                                    $btnorder = 'ยังไม่ทำอาหาร';
                                    $btnstat = '';
                                    $btnstat2 = '';                                    
                                    break;
                                case 'C':
                                    $aw_status = 'ครัวทำอาหาร';
                                    $btnorder = 'ลูกค้ารอรับ';
                                    $btnstat = '';
                                    $btnstat2 = '';
                                    break;
                                case 'T':
                                    $aw_status = 'ครัวเสิร์ฟแล้ว';
                                    $btnorder = 'รอคิดเงิน';
                                    $btnstat = '';
                                    $btnstat2 = '';
                                    break;
                                case 'S':
                                    $aw_status = 'ลูกค้ารับอาหารแล้ว';
                                    $btnorder = 'ชำระเงินแล้ว';
                                    $btnstat = 'disabled';
                                    $btnstat2 = 'disabled';
                                    break;
                                default:
                                    $aw_status = 'ผิดพลาด';
                                    $btnorder = '-';
                                    $btnstat = 'disabled';
                                    $btnstat2 = 'disabled';
                                    break;
                            }
                            ?>
                            <tr class="<?php echo $tr; ?>">
                                <td><?php echo $aw_status; ?></td>
                                <td><?php echo $rs_or['tb']; ?></td>
                                <td>
                                <a class="load_data" href="<?php echo $baseUrl; ?>/back/order/view/<?php echo $rs_or['order_id']; ?>"><?php echo $rs_or['order_id']; ?></a>
                                <?php echo $rs_or['productName']; ?>
                                </td>
                                <td><?php echo thaidate($rs_or['order_date']); ?></td>
                                <td><?php echo $rs_or['fullname'];?></td>
                                <td style="text-align: right;"><?php echo number_format($rs_or['price'],2)." * ".$rs_or['quantity']; ?></td>                                
                                <td style="text-align: right;"><?php echo number_format($rs_or['priceQ'],2); ?></td>
                                <td class="button-column">
                                    <a class="btn btn-info btn-xs load_data" title="" href="<?php echo $baseUrl; ?>/back/order/view/<?php echo $rs_or['order_id']; ?>"><i class="glyphicon glyphicon-zoom-in"></i> รายละเอียด</a>
                                    <a class="btn btn-danger btn-xs confirm <?php echo $btnstat2;?>" title="" href="#" data-toggle="modal" data-target="#deleteModal<?php echo $rs_or['id']; ?>"><i class="glyphicon glyphicon-remove"></i> ยกเลิก </a>
                                    <!-- Modal -->
                                    <div class="modal fade" id="deleteModal<?php echo $rs_or['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                        <form action="<?php echo base_url(); ?>/back/order/delete2/<?php echo $rs_or['id']; ?>" method="post" name="form" id="form" role="form"
                                        <div class="modal-content">
                                        
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                <h4 class="modal-title" id="myModalLabel">แจ้งเตือนการ ยกเลิกรายการ</h4>
                                            </div>
                                            
                                            <div class="modal-body">
                                            
                                                คุณต้องการยกเลิกแบบ คืน Stock หรือไม่? กรุณาเลือก 
                                                <select class="form-control input-md" name="aw_status" id="aw_status">
                                                    <option value="R" >คืน Stock   </option>
                                                    <option value="D" >ไม่ต้องคืน Stock</option>
                                                </select>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">ไม่ใช่</button>
                                                <button type="submit" class="btn btn-primary saveform"  >ใช่ ยืนยันการลบ</button>
                                            </div>
                                           
                                        </div>
                                        </form>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
                <div>
                    <?php $pagination->render(); ?>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.search-button').click(function () {
                $('.search-form').toggle();
                return false;
            });
            $('.saveform').click(function () {
                $('#form').submit();
            });
        });
    </script>
</div>

<?php
/*
 * footer***********************************************************************
 */
require 'template/back/footer.php';
/*
 * footer***********************************************************************
 */
