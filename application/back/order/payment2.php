<?php

/*
 * php code///////////**********************************************************
 */
if(!isset($_GET['id'])){
    header("location:" . $baseUrl . "/back/order");
}
$db = new database();


$sql_o = "SELECT * FROM orders  ";
$sql_o .="WHERE  billNo='{$_GET['id']}' ";
$sql_o2 = "SELECT * FROM orders  ";
$sql_o2 .="WHERE   billNo='{$_GET['id']}' ";
$query_o = $db->query($sql_o);
// $rs_o = $db->get($query_o);
// echo $sql_o;

$sql_tax = "SELECT * FROM pay_tax  ";
$sql_tax .="WHERE billNo='{$_GET['id']}' ";
$query_tax   = $db->query($sql_tax);
$rs_tax      = $db->get($query_tax);
$taxID      = $rs_tax['taxID'];
$nameCus    = $rs_tax['nameCus'];
$addCus     = $rs_tax['addCus'];
$taxCus     = $rs_tax['taxCus'];
$dateAdd     = $rs_tax['dateAdd'];

$sql_op = "SELECT * FROM payments  ";
$sql_op .="WHERE id='{$_GET['id']}' ORDER BY id DESC ";
$query_op = $db->query($sql_op);
$rs_op = $db->get($query_op); 

$sql_od = "SELECT price,percentage ,SUM(quantity) AS quantity, discount, vat7, SUM(total) AS total ,priceP, totalNet,aw_status, id, productName,image,order_id,product_id,tb,que,order_date,order_status,fullname,user_id FROM v_orderdetail ";
$sql_od .="WHERE order_status ='success' AND aw_status NOT IN ('R','D') AND billNo='{$_GET['id']}' GROUP BY  product_id,price ORDER BY product_categorie_id , productName ";

$query_od = $db->query($sql_od);

$title = ' : ใบกำกับภาษีแบบเต็ม';
/*
 * php code///////////**********************************************************
 */

/*
 * header***********************************************************************
 */
require 'template/back/header.php';
/*
 * header***********************************************************************
 */
?>
<link rel="stylesheet" href="<?php echo $baseUrl; ?>/css/jquery.datetimepicker.css" type="text/css" />

<script type="text/javascript" src="<?php echo $baseUrl; ?>/js/jquery.form-validator.min.js"></script>
<script type="text/javascript" src="<?php echo $baseUrl; ?>/ckeditor/ckeditor.js"></script>
<script type='text/javascript' src="<?php echo $baseUrl; ?>/js/jquery.datetimepicker.js"></script>
<div id="page-warpper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">รายละเอียดการชำระเงิน   <?php echo "Bill:".$_GET['id']; ?> </h1>

        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="subhead">
                <a role="button" id="save" class="btn btn-success btn-md new-data" href="#">
                    <i class="glyphicon glyphicon-floppy-save"></i>
                    บันทึก
                </a>
                <a role="button" class="search-button btn btn-danger btn-md" href="javascript:history.back()">
                    <i class="glyphicon glyphicon-remove-circle"></i>
                    ยกเลิก
                </a>
                <a role="button"  class="btn btn-info btn-md new-data"  onclick="printContent('p2')">
                <i class="glyphicon btn-md  glyphicon-print"></i> Print</a>   
                <?php 
                // echo $sql_od;
                ?>   
            </div>
        </div>
    </div>
    <div class="row">
    
        <div class="col-lg-6" id="p2">
    
        <div class="form-group">
        <div class="col-sm-2 col-lg-2" >
        <center> <img src="<?php echo $baseUrl; ?>/images/w1.png" width="50" ></center>
        </div>
        <div class="col-sm-2 col-lg-10" >
        <center>บริษัท ว้าวอุบล จำกัด</center>
        </div>
    </div>
        <div class="form-group"style="font-size: 12px;"> <center>TAX# 0345560001276  </center>   
         <center>หมายเลขประจำเครื่อง : 201320726541.3  </center> 

            </div> 
        <div class="form-group"style="font-size: 12px;"> <center>สำนักงานใหญ่<br> 222 ม.4 ต.ขามใหญ่ อ.เมือง <br>จ.อุบลราชธานี 34000 </center>       </div>    
        <div class="form-group"style="font-size: 12px;"> <center>Tel.082-5853523  </center>      </div>     
        <div class="row">
           <div class="col-md-12">  
         <div  class="pull-left form-group"style="font-size: 10px;">
        นามลูกค้า ...<?php echo @$nameCus; ?>... 
        </div> 


        </div>
        </div>
        <div  class="form-group"style="font-size: 11px;">
        ที่อยู่ลูกค้า ...<?php echo @$addCus; ?>...
        </div>
        <div  class="form-group"style="font-size: 11px;">
        TaxID ลูกค้า ...<?php echo @$taxCus; ?>...
        </div>
        <div class="form-group"style="font-size: 11px;">
       <strong> <center>ใบเสร็จรับเงิน/ ใบกำกับภาษี  </center> </strong>   
       <strong> <center>ใบกำกับภาษีเลขที่ : <?php echo $taxID; ?> </center> </strong>
      
        </div>
        <p style="text-align: center; font-size: 10px;"> วันที่ออกใบกำกับภาษี : <?php echo $dateAdd; ?></p>
            <table class="table" style="font-size: 10px;" >
                <thead>
                    <tr>
                        <th>@</th>
                        <th>รายการ (ราคา/หน่วย)</th>                       
                        <th style="text-align: right;">รวม</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                $vat7 = $rs_op['vat7'];
            $discount =$rs_op['discount'];      
            $grand_total = 0;
            $grand_totalNet = $rs_op['totalNet']; 
            $grand_totalP = $rs_op['cash'];   
            $percentage = $rs_od['percentage'];
            while ($rs_od = $db->get($query_od)) {
            $total_price = $rs_od['price'] * $rs_od['quantity'];
            // $discount = ($rs_od['discount']* $rs_od['quantity']) + $discount;                       
            // $vat7 = ($rs_od['vat7'] * $rs_od['quantity'] )+ $vat7;

            $total_priceNet  = $rs_od['totalNet'] * $rs_od['quantity'];
            // $grand_totalNet = $total_priceNet + $grand_totalNet ;
            $grand_total = ($grand_totalNet-$vat7);

            
            // $dis = ($grand_totalNet * ($percentage/100));
            // $grand_totalP=($grand_totalNet-$discount);
            
            ?>

                        <tr>
                        <td style="text-align: left;"><?php echo number_format($rs_od['quantity'], 0); ?></td>
                            <td>
                            <?php 
                             echo $rs_od['productName']." (".number_format($rs_od['totalNet'], 2).") "; ?>
                          
                            <td style="text-align: right;"><?php echo number_format($total_priceNet, 2); ?></td>
                        </tr>
                    <?php } ?>
                    <tr class="info">
                    <td colspan="5" style="text-align: right;">ยอดรวม ฿ <?php echo number_format($grand_totalNet, 2); ?>
                </td>
                </tr>
                <tr >
                    <td colspan="5" style="text-align: right;"> Vat 7% ฿ <?php echo number_format($vat7, 2); ?>
                </td>
                </tr>
                <tr class="info">
                    <td colspan="5" style="text-align: right;">ราคาสินค้า ฿ <?php echo number_format($grand_total, 2); ?> 
                    </td>
                </tr>
                <tr >
                    <td colspan="5" style="text-align: right;">หัก (ส่วนลด <?php echo $percentage."%) - ".number_format($discount, 2); ?>
                    </td>
                </tr>
                <tr class="info">
                    <td colspan="5" style="font-size: 14px; text-align: right;">รวมสุทธิ ฿ <?php echo number_format($grand_totalP, 2); ?>
                    </td>
                </tr>
                </tbody>
            </table>
            
            <p  style="font-size: 14px;"> ชำระโดย <?php if($rs_op['credit']>0){ echo "#Credit:>".number_format($rs_op['credit'], 2)." ";} if($rs_op['cash']>0){ echo "#Cash:>".number_format($rs_op['cash'], 2);} ?></p>
            <p  style="font-size: 12px;"><?php if($rs_op['cash']>0){ echo "Received: ".number_format($rs_op['payment'], 2)."  Change: ".number_format($rs_op['re_money'], 2);} ?></p>
<div class="form-group"style="font-size: 11px;">
<p  style="font-size: 10px;"><center>ราคารวมภาษีมูลค่าเพิ่มแล้ว (Including vat)  </center></p>  

<center> -------------------------------------</center>
<center> เป็นการยกเลิกและออกใบกำกับภาษีฉบับใหม่</center>
<center> แทนใบกำกับภาษีอย่างย่อ เลขที่ <?php echo $_GET['id']; ?> </center>
<center>-------------------------------------</center>
</div>
 <p  style="font-size: 10px;">  by :  <?php echo $_SESSION[_ef . 'fullname']; ?> วันที่พิมพ์ <?php echo date('Y-m-d H:i:s') ?></p>  
 <br>         
 <p  style="font-size: 10px;">  ลงชื่อ .............................</p>           

     <center> <H4><img src="<?php echo $baseUrl; ?>/images/favicon-32x32.png" width="32" >  Thank you  </H4></center>
     <br><br><br><br><br><br>


     <hr>
    
        </div>
        <div class="col-lg-6">
            <div class="form-horizontal" style="margin-top: 10px;">
                <form id="order-form" action="<?php echo $baseUrl; ?>/back/order/form_payment2-1" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="id" value="<?php echo $_GET['id']; ?>">
   



                    <div class="form-group row tdtopic " bid="111">
                              
                        <div class="col-sm-3">
                            นามลูกค้า
                        </div>
                        <div class="col-sm-9">
                            <input type="text" id="nameCus" name="nameCus" class="form-control input-sm" value="<?php echo $nameCus; ?>" >
                        </div>
                        <div class="col-sm-3">
                            ที่อยู่ลูกค้า
                        </div>
                        <div class="col-sm-9">
                            <input type="text" id="addCus" name="addCus" class="form-control input-sm" value="<?php echo $addCus; ?>" >
                        </div>
                    </div>

                    <div class="form-group row tdtopic " bid="111">
                        <div class="col-sm-3">
                           TAX ID
                        </div>
                        <div class="col-sm-9">
                        <input type="text" id="taxCus" name="taxCus" class="form-control input-sm" value="<?php echo $taxCus; ?>" >            
                        </div>
                    </div>
                    <div class="form-group row tdtopic " bid="111">
                        <div class="col-sm-3">
                           
                        </div>
                        <div class="col-sm-9">
                                        </div>
                    </div>
                   
                </form>
            </div>
        </div>
    
    </div>
</div>

<script>
    jQuery('#pay_date').datetimepicker({
        format: 'd/m/Y',
        lang: 'th',
        timepicker: false
    });
    jQuery('#pay_time').datetimepicker({
        format: 'H:i',
        datepicker: false,
        step:1
    });
    $(document).ready(function () {
        $("#save").click(function () {
            $("#order-form").submit();
            return false;
        });
    });

    $.validate();
    function printContent(el){
    var restorepage = document.body.innerHTML;
    var printcontent = document.getElementById(el).innerHTML;
    document.body.innerHTML = printcontent;
    window.print();
    document.body.innerHTML = restorepage;
    window.location.reload()
}
</script>
<?php
/*
 * footer***********************************************************************
 */
require 'template/back/footer.php';
/*
 * footer***********************************************************************
 */
