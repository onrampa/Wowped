<?php

/*
 * php code///////////**********************************************************
 */
if(!isset($_GET['id'])){
    header("location:" . $baseUrl . "/back/order");
}
$db = new database();
$sql_o = "SELECT * FROM orders  ";
$sql_o .="WHERE  order_status <> 'success' AND tb='{$_GET['id']}' ";
$query_o = $db->query($sql_o);
// $rs_o = $db->get($query_o);
// echo $sql_o;

$sql_op = "SELECT * FROM payments  ";
$sql_op .="WHERE accept = '0' AND tb_id='{$_GET['id']}' ";
$query_op = $db->query($sql_op);
$rs_op = $db->get($query_op);

// $query_p = $db->query($sql_op);
// $rs_p = $db->get($query_p);
$sql_od = "SELECT * FROM v_orderdetail  ";
$sql_od .="WHERE order_status <> 'success' AND tb='{$_GET['id']}' ";
$query_od = $db->query($sql_od);

// $sql_od = "SELECT d.*,p.id,p.name,p.image FROM order_details d INNER JOIN products p ";
// $sql_od .= "ON d.product_id=p.id ";
// $sql_od .="WHERE d.order_id='{$_GET['id']}' ";
// $query_od = $db->query($sql_od);
$title = 'รายละเอียดรายการสั่งอาหาร';
/*
 * php code///////////**********************************************************
 */

/*
 * header***********************************************************************
 */
require 'template/back/header.php';
/*
 * header***********************************************************************
 */
?>
<link rel="stylesheet" href="<?php echo $baseUrl; ?>/css/jquery.datetimepicker.css" type="text/css" />

<script type="text/javascript" src="<?php echo $baseUrl; ?>/js/jquery.form-validator.min.js"></script>
<script type="text/javascript" src="<?php echo $baseUrl; ?>/ckeditor/ckeditor.js"></script>
<script type='text/javascript' src="<?php echo $baseUrl; ?>/js/jquery.datetimepicker.js"></script>
<div id="page-warpper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">รายละเอียดการชำระเงิน   <?php echo "Bill:".$_GET['id']; ?></h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="subhead">
                <a role="button" id="save" class="btn btn-success btn-xs new-data" href="#">
                    <i class="glyphicon glyphicon-floppy-save"></i>
                    บันทึก
                </a>
                <a role="button" class="search-button btn btn-danger btn-xs" href="javascript:history.back()">
                    <i class="glyphicon glyphicon-remove-circle"></i>
                    ยกเลิก
                </a>
                <button onclick="printContent('p2')"><i class="glyphicon glyphicon-print"></i> Print</button>  
    
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <div class="form-horizontal" style="margin-top: 10px;">
                <form id="order-form" action="<?php echo $baseUrl; ?>/back/order/form_payment1" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="tb" value="<?php echo $_GET['id']; ?>">
                    <?php
                    $a=1;
                    $orders='';
                    while ($rs_o = $db->get($query_o)) {
                        $order = $rs_o['id']; 
                        $orders = $orders." #".$rs_o['id'];
                        
                        ?>
                    <input type="hidden" name="order_id_<?php echo $a; ?>" value="<?php echo $order; ?>">
                    
                        <?php $a++; } ?>
                    <input type="hidden" name="aa" value="<?php echo $a; ?>">
                    <input type="hidden" name="orders" value="<?php echo $orders; ?>">


                    <div class="form-group row tdtopic " bid="111">
                                    
                        <div class="col-sm-6">
                            <img src="images/kbank.gif" width="16" height="16"> บัตรเครดิต/ สินเชื่อ
                        </div>
                        <div class="col-sm-4">
                            <input type="hidden" id="pay_money" name="pay_money" class="form-control input-sm" value="<?php echo $rs_op['pay_money']; ?>" >
                            <input type="text" id="credit" name="credit" class="form-control input-sm" value="<?php echo $rs_op['credit']; ?>" >
                        </div>
                    </div>

                    <div class="form-group row tdtopic " bid="111">
                        <div class="col-sm-6">
                            <img src="images/bbl.gif" width="16" height="16"> เงินสด
                        </div>
                        <div class="col-sm-4">
                            <input type="text" id="cash" name="cash" class="form-control input-sm" value="<?php echo $rs_op['cash']; ?>" >
                        </div>
                    </div>
                </form>
            </div>
        </div>
        
        <div class="col-lg-6" id="p2">
        <h2>WoW เป็ด  ปตท.</h2>
        <h4>ใบเสร็จรับเงิน/ใบเรียกเก็บเงิน #Tbl <?php echo $_GET['id']; ?></h4>
            <p >  <?php echo date('Y-m-d H:i:s') ?></p>
            <table class="table" style="font-size: 12px;" >
                <thead>
                    <tr>
                        <th>รายการ</th>
                        <th style="text-align: right;">ราคา(บาท)</th>
                        <th style="text-align: right;">จำนวน</th>
                        <th style="text-align: right;">รวม</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $grand_total = 0;
                    while ($rs_od = $db->get($query_od)) {
                        $total_price = $rs_od['price'] * $rs_od['quantity'];
                        $grand_total = $total_price + $grand_total;
                        ?>
                        <tr>
                            <td>
                            <?php if($rs_od['aw_status']){ echo "@";} echo $rs_od['productName']; ?>
                            <td style="text-align: right;"><?php echo number_format($rs_od['price'], 2); ?></td>
                            <td style="text-align: right;"><?php echo $rs_od['quantity']; ?></td>
                            <td style="text-align: right;"><?php echo number_format($total_price, 2); ?></td>
                        </tr>
                    <?php } ?>
                    <tr class="info">
                        <td colspan="5" style="text-align: right;">ราคารวมทั้งหมด <strong><?php echo number_format($grand_total, 2); ?></strong> บาท</td>
                    </tr>
                </tbody>
            </table>
                   
        </div>
 
    </div>
</div>

<script>
    jQuery('#pay_date').datetimepicker({
        format: 'd/m/Y',
        lang: 'th',
        timepicker: false
    });
    jQuery('#pay_time').datetimepicker({
        format: 'H:i',
        datepicker: false,
        step:1
    });
    $(document).ready(function () {
        $("#save").click(function () {
            $("#order-form").submit();
            return false;
        });
    });
    $.validate();
    function printContent(el){
    var restorepage = document.body.innerHTML;
    var printcontent = document.getElementById(el).innerHTML;
    document.body.innerHTML = printcontent;
    window.print();
    document.body.innerHTML = restorepage;
}
</script>
<?php
/*
 * footer***********************************************************************
 */
require 'template/back/footer.php';
/*
 * footer***********************************************************************
 */
