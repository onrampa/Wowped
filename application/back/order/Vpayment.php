<?php
if(!isset($_GET['id'])){
    header("location:" . $baseUrl . "/back/order");
}
$db = new database();
// $sql_tb = " SELECT * FROM v_table ";
// $sql_tb .= "WHERE 1=1 AND tb='{$_GET['id']}'  ";
// $query_tb = $db->query($sql_tb);
// $tb_ototal = $db->get($query_tb);
// $que=$tb_ototal['que'];
$que=$_GET['id'];

$sql_o = "SELECT nameC,person FROM orders  ";
$sql_o .="WHERE  order_status not in ('success','payments') AND que='{$que}' ORDER BY person DESC,nameC DESC ";
$query_o = $db->query($sql_o);
$rs_o = $db->get($query_o);
$nameC = $rs_o['nameC'];
$person = $rs_o['person'];

$sql_op = "SELECT * FROM payments  ";
$sql_op .="WHERE accept = '0' AND que='{$que}' ";
$query_op = $db->query($sql_op);
$rs_op = $db->get($query_op);

$sql_od = "SELECT  nameC ,price,percentage ,SUM(quantity) AS quantity, SUM(total) AS total ,priceP,totalNet,aw_status, id, productName,image,order_id,product_id,tb,que,order_date,order_status,fullname,user_id FROM v_orderdetail ";
$sql_od .="WHERE order_status  not in ('success','cancel')  AND aw_status NOT IN ('R','D')   AND que='{$que}' GROUP BY  product_id,aw_status,price ORDER BY product_categorie_id , productName ";

// $sql_od = "SELECT * FROM v_orderdetail  ";
// $sql_od .="WHERE order_status <> 'success' AND tb='{$_GET['id']}'  ORDER BY id  ";
$query_od1 = $db->query($sql_od);

$receipp=0;
while ($rs_od1 = $db->get($query_od1)) {
    $tb=$rs_od1['tb'];
    if($rs_od1['aw_status']=='P')
    {
        $receipp=1;
    }
}
$query_od = $db->query($sql_od);
$title = 'รายละเอียดรายการสั่งอาหาร';

require 'template/back/header.php';

?>
<link rel="stylesheet" href="<?php echo $baseUrl; ?>/css/jquery.datetimepicker.css" type="text/css" />

<script type="text/javascript" src="<?php echo $baseUrl; ?>/js/jquery.form-validator.min.js"></script>
<script type="text/javascript" src="<?php echo $baseUrl; ?>/ckeditor/ckeditor.js"></script>
<script type='text/javascript' src="<?php echo $baseUrl; ?>/js/jquery.datetimepicker.js"></script>
<div id="page-warpper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">รายละเอียดการชำระเงิน   <?php echo "Bill:".$_GET['id']; ?></h1>
         
        </div>
    </div>
    <div class="row">
        <div class="col-lg-8">
            <div class="subhead">

                <a role="button" class="search-button btn btn-danger btn-md" href="javascript:history.back()">
                    
                    << ย้อนกลับ
                </a>
                <?php if($receipp==0) {?>
                <a role="button" class="btn btn-success btn-md new-data" href="<?php echo base_url();?>/back/order/payment/<?php echo $_GET['id']; ?>">
                    <i class="glyphicon glyphicon-floppy-info"></i>
                    ออกใบเรียกเก็บเงิน
                </a>
                <?php } else { ?>
                    <a role="button" class="btn btn-info btn-md new-data" href="#">
                    <i class="glyphicon glyphicon-floppy-info"></i>
                    รอยืนยัน เสริฟอาหาร
                </a>   

                <?php 
                }
                $sql_tb = " SELECT * FROM v_table ";
                $sql_tb .= "WHERE 1=1 and open='0'  ORDER BY tb  ";               
                $query_tb = $db->query($sql_tb); 
                ?>   
            </div>
    <form id="order-form" action="<?php echo $baseUrl; ?>/back/order/Vtable/<?php echo $que; ?>" method="post" enctype="multipart/form-data">
            <div class="form-group">
                        <label for="table_id" class="col-sm-2 control-label required">ย้ายโต๊ะ</label>
                        <div class="col-sm-4">
                            <select id="table_id" name="table_id" class="form-control input-sm">
                             <option value="00">-- ไม่ระบุ --</option>
                                <?php while ($rs_tb = $db->get($query_tb)) { ?>
                                    <option value="<?php echo $rs_tb['tb']; ?>"><?php echo $rs_tb['tb']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="col-sm-4">
                        <input type="hidden" name="que" value="<?php echo $que; ?>">
                        <input type="hidden" name="tb" value="<?php echo $tb; ?>">
                        <a role="button" id="save" class="btn btn-success btn-md new-data" href="#">
                        <i class="glyphicon glyphicon-floppy-save"></i>
                        ยืนยัน ย้ายโต๊ะ
                        </a>
                        </div>
                    </div> 
            </form>
           
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <div class="form-horizontal" style="margin-top: 10px;">
            </div>
        </div>
        
        <div class="col-lg-6" id="p2">
        <center>
        <div class="form-group">
            <div class="col-sm-2 col-lg-2" >
            </br>
            <img src="<?php echo $baseUrl; ?>/images/w1.png" width="50" >
            </div>
            <div class="col-sm-2 col-lg-10" style="font-size: 14px;">  
             ว้าวอุบล จำกัด
            
            </div>
        </div>
            <div class="form-group" style="font-size: 12px;"> 
            TAX# 0345560001276 </div>      </center> 
           <div class="form-group" style="font-size: 12px;">
            นามลูกค้า ....<?php echo @$nameC; ?>................<?php echo @$person; ?> ที่
            </div>
            <p style="font-size: 12px;">  <?php echo date('Y-m-d H:i:s') ?></p>
            <p style="font-size: 12px;"> ชื่อพนักงาน : <?php echo $_SESSION[_ef . 'fullname']; ?></p>
            <div class="form-group">
             ใบเรียกเก็บเงิน #โต๊ะ : <?php echo $tb; ?>
            </div>
    
            <table class="table" style="font-size: 12px;" >
                <thead>
                    <tr>
                        <th style="text-align: left;">@</th>
                        <th>รายการ (ราคา)</th>
                        <th style="text-align: right;">รวม</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    // echo $sql_od;
                    $grand_total = 0;
                    $grand_totalNet = 0;
                    $grand_totalNetP = 0;
                    while ($rs_od = $db->get($query_od)) {
                        if($rs_od['aw_status']=='C'||$rs_od['aw_status']=='P')
                        {
                            $receipp=0;
                        }
                        $percentage = $rs_od['percentage'];

                        $total_price = $rs_od['price'] * $rs_od['quantity'];
                        $grand_total = $total_price + $grand_total;

                        $total_priceNet  = $rs_od['totalNet'] * $rs_od['quantity'];
                        $grand_totalNet = $total_priceNet  + $grand_totalNet ;

                        $grand_total=($grand_totalNet/1.07);
                        $vat=($grand_totalNet-$grand_total);

                        $dis = ($grand_total * ($percentage/100));
                        $grand_totalP = ($grand_totalNet-$dis);
                        ?>
                        <tr>
                            <td style="text-align: left;"><?php echo $rs_od['quantity']; ?></td>
                    
                            <td>
                            <?php 
                            if($rs_od['aw_status']=='P'){
                            $check="# รอเสริฟ";
                            }
                            elseif($rs_od['aw_status']=='C'){
                                $check="# กำลังทำอาหาร";
                                }
                            else {$check=""; }
                            echo $rs_od['productName']." (".number_format($rs_od['totalNet'], 2).") ".$check; ?>
                            <td style="text-align: right;"><?php echo number_format($total_priceNet, 2); ?></td>
                        </tr>
                    <?php } ?>
                    <tr class="info">
                        <td colspan="5" style="text-align: right;">[ Vat 7% ฿ <?php echo number_format($vat, 2); ?> ราคาสินค้า ฿ <?php echo number_format($grand_total, 2); ?> ] ยอดรวม ฿ <?php echo number_format($grand_totalNet, 2); ?>
                    </td>
                    </tr>

                    <tr >
                        <td colspan="5" style="text-align: right;">ราคาสินค้า ฿ <?php echo number_format($grand_total, 2); ?>
                    </td>
                    </tr>
                    <tr class="info">
                        <td colspan="5" style="text-align: right;">หัก (ส่วนลด <?php echo @$percentage."%) - ".number_format(@$dis, 2); ?>
                        </td>
                    </tr>
                    <tr >
                        <td colspan="5" style="font-size: 14px; text-align: right;">รวมสุทธิ ฿ <?php echo number_format($grand_totalP, 2); ?>
                        </td>
                    </tr>
                </tbody>
            </table>
            <p  style="font-size: 14px;"><center><img src="<?php echo $baseUrl; ?>/images/favicon-32x32.png" width="32" > กรุณาตรวจสอบ </center></p>
            </br> </br><center>..............................

        </div>

    </div>
</div>



<script>

    jQuery('#pay_date').datetimepicker({
        format: 'd/m/Y',
        lang: 'th',
        timepicker: false
    });
    jQuery('#pay_time').datetimepicker({
        format: 'H:i',
        datepicker: false,
        step:1
    });
    $(document).ready(function () {
        $("#save").click(function () {
            $("#order-form").submit();
            return false;
        });
    });
    function fncSum(str){
        if (str == "") {
            alert("ไม่มี ค่าที่ส่งมา" + str);
                return;
            }
            if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            }
            else {// code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    document.getElementById("txtHint4").innerHTML = xmlhttp.responseText;
                }
            }
            xmlhttp.open("GET", "<?php echo $baseUrl; ?>/back/order/form_paymentO/" + str, true);
        //    xmlhttp.open("GET", "/eshop/back/check/username/" + str, true);
        //    alert("/eshop/back/check/username/" + str);
            xmlhttp.send();

            var credit = document.getElementById("credit").value;
            var payment = document.getElementById("payment").value;
            var sumT = document.getElementById("sumT").value;
            var re_money = ((credit+payment) - sumT);            
            // document.order-form.re_money.value = re_money;
            alert("เงินทอน "+ re_money + " บาท ");
            document.getElementById("re_money").value = re_money;
                return;            
        }
        function fncRe(str){
        if (str == "") {
            alert("ไม่มี ค่าที่ส่งมา" + str);
                return;
            }
            if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            }
            else {// code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    document.getElementById("txtHint4").innerHTML = xmlhttp.responseText;
                }
            }
            xmlhttp.open("GET", "<?php echo $baseUrl; ?>/back/order/form_paymentRe/" + str, true);
            xmlhttp.send();
      
        }
    $.validate();

    function printContent(el){
    var restorepage = document.body.innerHTML;
    var printcontent = document.getElementById(el).innerHTML;
    document.body.innerHTML = printcontent;
    window.print();
    document.body.innerHTML = restorepage;
    window.location.reload()
}

</script>
<?php
/*
 * footer***********************************************************************
 */
require 'template/back/footer.php';
/*
 * footer***********************************************************************
 */
