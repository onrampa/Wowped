<?php
//  if($_SESSION[_ss . 'levelaccess']== 'user'){
//  header('location:'.$baseUrl.'/admin');
//  }

/*
 * php code///////////**********************************************************
 */
if(!isset($_GET['id'])){
    header("location:" . $baseUrl . "/back/order");
}
$db = new database();
$sql_p = "SELECT * FROM v_payments  ";
$sql_p .="WHERE order_id='{$_GET['id']}' ";
$query_p = $db->query($sql_p);
$rs_p = $db->get($query_p);

$sql_od = "SELECT d.*,p.id,p.name,p.image FROM order_details d INNER JOIN products p ";
$sql_od .= "ON d.product_id=p.id ";
$sql_od .="WHERE d.order_id='{$_GET['id']}' ";
$query_od = $db->query($sql_od);
$title = 'รายละเอียดการชำระเงิน';
/*
 * php code///////////**********************************************************
 */

/*
 * header***********************************************************************
 */
require 'template/back/header.php';
/*
 * header***********************************************************************
 */
?>
<link rel="stylesheet" href="<?php echo $baseUrl; ?>/css/jquery.datetimepicker.css" type="text/css" />

<script type="text/javascript" src="<?php echo $baseUrl; ?>/js/jquery.form-validator.min.js"></script>
<script type="text/javascript" src="<?php echo $baseUrl; ?>/ckeditor/ckeditor.js"></script>
<script type='text/javascript' src="<?php echo $baseUrl; ?>/js/jquery.datetimepicker.js"></script>
<div id="page-warpper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">รายละเอียดการชำระเงิน</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="subhead">
                <a role="button" id="save" class="btn btn-success btn-xs new-data" href="#">
                    <i class="glyphicon glyphicon-floppy-save"></i>
                    บันทึก
                </a>
                <a role="button" class="search-button btn btn-default btn-xs" href="<?php echo $baseUrl; ?>/back/order">
                    <i class="glyphicon glyphicon-remove-circle"></i>
                    ยกเลิก
                </a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <div class="form-horizontal" style="margin-top: 10px;">
                <form id="order-form" action="<?php echo $baseUrl; ?>/back/order/form_paymentUp" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="order_id" value="<?php echo $_GET['id']; ?>">
                    <input type="hidden" name="id" value="<?php echo $rs_p['id']; ?>">
                    <div class="form-group">
                        <label for="slip_image" class="col-sm-6 control-label required">รูปภาพ slip</label>
                        <div class="col-sm-6">
                            <img src="<?php echo $baseUrl ?>/upload/slip/md_<?php echo $rs_p['slip'];?>">
                        </div>
                    </div>
                                        <div class="form-group">
                        <label for="Slip_image" class="col-sm-6 control-label">รูปภาพ slip ใหม่</label>
                        <div class="col-sm-6">
                            <input type="file" name="image" id="image">
                        </div>
                    </div>
                    <div class="form-group row tdtopic " bid="111">
                        <label class="col-sm-6 form-control-label"> สำนักงานย่อย ตลาด Big C อุบลฯ
                          </label>
                        <div class="col-sm-6">
                            <input name="blno" type="radio" id="blno" value="1" <?php if($rs_p['blno']=="1"){ echo "checked"; }?>/>
                            <img src="images/kbank.gif" width="16" height="16"> KBANK 006-1-28582-2
                        </div>
                    </div>

                    <div class="form-group row tdtopic " bid="111">
                        <label class="col-sm-6 form-control-label">สาขา Central Plaza อุบลราชธานี</label>
                        <div class="col-sm-6">
                            <input name="blno" type="radio" id="blno" value="2" <?php if($rs_p['blno']=="2"){ echo "checked"; }?>/>
                            <img src="images/bbl.gif" width="16" height="16"> BBL 673-7-07788-0
                        </div>
                    </div>

                    <div class="form-group row tdtopic " bid="111">
                        <label class="col-sm-6 form-control-label">สาขา Central Plaza อุบลราชธานี</label>
                        <div class="col-sm-6">
                            <input name="blno" type="radio" id="blno" value="3"<?php if($rs_p['blno']=="3"){ echo "checked"; }?>/>
                            <img src="images/ktb.gif" width="16" height="16"> KTB 984-1-43426-1
                        </div>
                    </div>
                    <div class="form-group row tdtopic " bid="111">
                        <label class="col-sm-4 form-control-label">ชื่อบัญชี </label>
                        <div class="col-sm-4">                            
                            บจก. แอดเพย์ เซอร์วิสพอยท์
                            จำกัด
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="Product_name" class="col-sm-4 control-label required">จำนวนเงิน <span class="required">*</span></label>
                        <div class="col-sm-8">
                            <input type="text" id="pay_money" name="pay_money" class="form-control input-sm" value="<?php echo $rs_p['pay_money']; ?>" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="Product_name" class="col-sm-4 control-label required">เลขที่โอน </label>
                        <div class="col-sm-8">
                            <input type="text" id="detail" name="detail" class="form-control input-sm" value="<?php echo $rs_p['detail']; ?>" >
                        </div>
                    </div>

                </form>
            </div>
        </div>
        <div class="col-lg-6">
            <table class="table" style="font-size: 12px;">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>ชื่อสินค้า</th>
                        <th style="text-align: right;">ราคา(บาท)</th>
                        <th style="text-align: right;">จำนวน</th>
                        <th style="text-align: right;">รวม</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $grand_total = 0;
                    while ($rs_od = $db->get($query_od)) {
                        $total_price = $rs_od['price'] * $rs_od['quantity'];
                        $grand_total = $total_price + $grand_total;
                        ?>
                        <tr>
                            <td>
                                <img src="<?php echo base_url(); ?>/upload/product/sm_<?php echo $rs_od['image']; ?>">
                            </td>
                            <td><?php echo $rs_od['name']; ?></td>
                            <td style="text-align: right;"><?php echo number_format($rs_od['price'], 2); ?></td>
                            <td style="text-align: right;"><?php echo $rs_od['quantity']; ?></td>
                            <td style="text-align: right;"><?php echo number_format($total_price, 2); ?></td>
                        </tr>
                    <?php } ?>
                    <tr class="info">
                        <td colspan="5" style="text-align: right;">ราคารวมทั้งหมด <strong><?php echo number_format($grand_total, 2); ?></strong> บาท</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<script>
    jQuery('#pay_date').datetimepicker({
        format: 'd/m/Y',
        lang: 'th',
        timepicker: false
    });
    jQuery('#pay_time').datetimepicker({
        format: 'H:i',
        datepicker: false,
        step:1
    });
    $(document).ready(function () {
        $("#save").click(function () {
            $("#order-form").submit();
            return false;
        });
    });
    $.validate();
</script>
<?php
/*
 * footer***********************************************************************
 */
require 'template/back/footer.php';
/*
 * footer***********************************************************************
 */
mysql_close();
