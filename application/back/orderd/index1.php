
<?php
/*
 * include file start***********************************************************
 */
require 'library/pagination.php';
/*
 * php code///////////**********************************************************
 */
$title = '@Food รับ Order';
$db = new database();
$pagination = new Zebra_Pagination();

$order_id = isset($_GET['order_id'])? "AND id='{$_GET['order_id']}'" : "";


/*
 * header***********************************************************************
 */
require 'template/back/header.php';
/*
 * header***********************************************************************
 */
?>
<meta http-equiv="refresh" content="10" />

<div id="page-warpper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">รายการสั่งอาหาร</h1>
            <div>
                <audio id="audio1" style="width:25%" controls>Canvas not supported</audio>
          </div>
          <div>
            <input type="hidden" id="audioFile" value="<?php echo base_url(); ?>/bell.mp3"  />
            <button id="playbutton" onclick="togglePlay();">Play</button>  
          </div>
   
            <div id="rate"></div>
          
               <script type="text/javascript">
                 // Create a couple of global variables to use. 
                 var audioElm = document.getElementById("audio1"); // Audio element
                 var ratedisplay = document.getElementById("rate"); // Rate display area

                 // Hook the ratechange event and display the current playbackRate after each change
                 audioElm.addEventListener("ratechange", function () {
                   ratedisplay.innerHTML = "Rate: " + audioElm.playbackRate;
                 }, false);
          
                 //  Alternates between play and pause based on the value of the paused property
                 function togglePlay() {
                   if (document.getElementById("audio1")) {

                       if (audioElm.paused == true) {
                            playAudio(audioElm);    //  if player is paused, then play the file
                        } else {
                            pauseAudio(audioElm);   //  if player is playing, then pause
                        }
                   }
                 }
          
                 function playAudio(audioElm) {
                   document.getElementById("playbutton").innerHTML = "Pause"; // Set button text == Pause
                   // Get file from text box and assign it to the source of the audio element 
                   audioElm.src = document.getElementById('audioFile').value;
                   audioElm.play();
                 }
          
                 function pauseAudio(audioElm) {
                   document.getElementById("playbutton").innerHTML = "play"; // Set button text == Play
                   audioElm.pause();
                 }
          
                 // Increment playbackRate by 1 
                 function increaseSpeed() {
                   audioElm.playbackRate += 1;
                 }
          
                 // Cut playback rate in half
                 function decreaseSpeed() {
                   if (audioElm.playbackRate <= 1) {
                     var temp = audioElm.playbackRate;
                     audioElm.playbackRate = (temp / 2); 
                   } else {
                     audioElm.playbackRate -= 1;
                   }
                 }
          
               </script>
        </div>
    </div>
    <div class="col-sm-12 blog-main">

    <?php
    $c_date = date('Y-m-d');
    $d1=$c_date."  00:00:00";
    $c_date2 = date ("Y-m-d", strtotime("+1 day"));
    $d2=$c_date2."  00:00:00";
$ss=0;
    $sql_ss = "SELECT max(order_id) as sid FROM v_orderdetail WHERE  product_categorie_id IN (34,35) AND  order_date BETWEEN '{$d1}' AND '{$d2}'  
    AND aw_status in ('P','C')   ";    
    
     $query_ss = $db->query($sql_ss);  
     $rs_ss = $db->get($query_ss);
     $sid= $rs_ss['sid'];
    if($sid > $_SESSION['sing']) {
     $_SESSION['sing']=$sid;
     $ss=1;
    }
    
 $sql_pd = "SELECT * FROM v_orderdetail WHERE  product_categorie_id IN (34,35) AND  order_date BETWEEN '{$d1}' AND '{$d2}'  
  AND aw_status in ('P','C')  ORDER BY id ASC  ";    

   $query_pd = $db->query($sql_pd);   

   while ($rs_pd = $db->get($query_pd)) {
  
   $ppid= $rs_pd['product_id'];
   $order_id= $rs_pd['order_id'];
   $uindex="index1";
   $_SESSION['uindex']=$uindex;
   ?>

 <div class="col-sm-2 col-md-2 col-xs-2" style="height: 170px!important;">

 <!-- <a style="position: absolute;left:17;" class="btn btn-danger btn-sm confirm " title="" href="#" data-toggle="modal" data-target="#closeModal<?php echo $rs_pd['id']; ?>"><i class="glyphicon glyphicon-minus-sign"></i></a>-->
 
 <!-- Modal -->
 <div class="modal fade" id="closeModal<?php echo $rs_pd['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
     <div class="modal-dialog">
         <form action="<?php echo base_url(); ?>/back/order/delete2/<?php echo $rs_pd['id']; ?>" method="post" name="form" id="form" role="form"
         <div class="modal-content">
         
             <div class="modal-header">
                 <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                 <h4 class="modal-title" id="myModalLabel">แจ้งเตือน ยกเลิกรายการอาหาร</h4>
             </div>
             
             <div class="modal-body">
             คุณต้องการยกเลิกแบบ คืน Stock หรือไม่? กรุณาเลือก 
             <select class="form-control input-md" name="aw_status" id="aw_status">
                 <option value="R" >คืน Stock   </option>
                 <option value="D" >ไม่ต้องคืน Stock</option>
             </select>

                 </div>
             <div class="modal-footer">
                 <button type="button" class="btn btn-default" data-dismiss="modal">ไม่ใช่</button>
                 <button type="submit" class="btn btn-primary saveform"  > ยืนยัน </button>
             </div>
             
         </div>
         </form>
     </div>  

 <!-- จบ Modal -->

                            <form class="form-inline" role="form" action="<?php echo base_url();?>/back/orderd/form_payment" method="post">
                            <input type="hidden" name="order_id" value="<?php echo $rs_pd['order_id']; ?>">
                            <input type="hidden" name="pid" value="<?php echo $rs_pd['product_id']; ?>">
                                <div class="thumbnail" style="height: 170px!important;">
                               
                            <?php if($rs_pd['aw_status']=='P' & $rs_pd['order_status']=='pending'){ ?>  
                                <button type="submit" class="cartN-button"><?php echo "โต๊ะ ". $rs_pd['tb']."</br>".$rs_pd['productName'].$rs_pd['detail']." @ ".$rs_pd['quantity']." ที่";?>  </button>
                                    <input type="hidden" name="detail" class="col-sm-12"  value="<?php echo $rs_pd['detail']; ?>">                                
                                    <img style="height: 100px!important;" src="<?php echo $baseUrl; ?>/upload/product/thumb_<?php echo $rs_pd['image']; ?>" >
                                   
                            <?php } else if($rs_pd['aw_status']=='P' & $rs_pd['order_status']<>'pending'){ ?> 
                                <button type="submit" class="cartM-button"><?php echo "@Cash "." #โต๊ะ". $rs_pd['tb']."</br>".$rs_pd['productName'].$rs_pd['detail']." ".$rs_pd['quantity']." ที่";?>  </button>                                
                                    <input type="hidden" name="detail" class="col-sm-12"  value="<?php echo $rs_pd['detail']; ?>">                                
                                    <img style="height: 100px!important;" src="<?php echo $baseUrl; ?>/upload/product/thumb_<?php echo $rs_pd['image']; ?>" >
                            <?php } else if($rs_pd['aw_status']=='C'){
                                $id=$rs_pd['id']; ?>
                                <center>
                                <button onclick="printContent('p1_<?php echo $id; ?>')"><i class="glyphicon glyphicon-print"></i> Print</button>    
                <a class="btn btn-info btn-md confirm" title="" href="<?php echo $baseUrl; ?>/back/orderd/form_payment2/<?php echo $rs_pd['id']; ?>" <i class="glyphicon glyphicon-share"></i> เสิร์ฟ</a>                                
                <!--<a class="btn btn-info btn-md confirm" title="" href="#" data-toggle="modal" data-target="#PModal<?php echo $rs_pd['id'];?>"><i class="glyphicon glyphicon-share"></i> เสิร์ฟ</a>                                -->

                               <div  id="p1_<?php echo $id; ?>" style=" text-align: center; font-size: 18px;">
                                   
                                   <p style="font-size: 18px;">
                                <?php echo "โต๊ะ ". $rs_pd['tb']."</br>".$rs_pd['productName'].$rs_pd['detail']."</br>@ ". $rs_pd['quantity']." ที่";?></p>
                                <p style="font-size: 16px;">  <?php echo date('Y-m-d H:i:s') ?></p>
                                </br></br></br></br> </br></br>  </br></br></br></br> </br></br></br></br></br></br> </br></br>................
                                
                                </div>
                                </center>
                            <?php } ?>                                


                                </div>
                            </div> 

                            </form>
                            <!-- Modal -->
                            <div class="modal fade" id="PModal<?php echo $rs_pd['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                               <div class="modal-dialog">
                                   <div class="modal-content">
                                       <div class="modal-header">
                                           <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                           <h4 class="modal-title" id="myModalLabel">แจ้งเตือน</h4>
                                       </div>
                                       <div class="modal-body"> คุณยืนยันการเสิร์ฟ ใช่หรือไม่? </div>
                                       <div class="modal-footer">
                                              <button type="button" class="btn btn-default" data-dismiss="modal">ไม่ใช่</button>
                                               <a role="button" class="btn btn-primary" href="<?php echo $baseUrl; ?>/back/orderd/form_payment2/<?php echo $rs_pd['id']; ?>">ใช่ ยืนยัน</a>
                                       </div>
                                   </div>
                               </div>
                           </div>

            <?php } ?>
            
                </div>
                </div>
                
    </div>

    <input type="hidden" id="sid" value="<?php echo $sid; ?>" /> 
     <input type="hidden" id="ssing" value="<?php echo $ss; ?>" />
<script>
// Activate Carousel
$("#myCarousel").carousel();

// Enable Carousel Indicators
$(".item").click(function(){
    $("#myCarousel").carousel(1);
});

// Enable Carousel Controls
$(".left").click(function(){
    $("#myCarousel").carousel("prev");
});
</script>

<script type="text/javascript">
          
        $(document).ready(function () {

            var ssing=document.getElementById('ssing').value;
            if(ssing == 1){
            togglePlay();
            }
            $('.search-button').click(function () {
                $('.search-form').toggle();
                return false;
            });
            $('.saveform').click(function () {
                $('#form').submit();
            });
        });
function printContent(el){
    var restorepage = document.body.innerHTML;
    var printcontent = document.getElementById(el).innerHTML;
    document.body.innerHTML = printcontent;
    window.print();
    document.body.innerHTML = restorepage;
}


</script>
</div>



<?php
/*
 * footer***********************************************************************
 */
require 'template/back/footer.php';
/*
 * footer***********************************************************************
 */
// mysql_close();
