<?php
/*
 * include file start***********************************************************
 */
require 'library/pagination.php';
/*
 * php code///////////**********************************************************
 */
$title = 'ระบบจัดการร้านค้า : อาหารที่ส่งลูกค้าแล้ว';
$db = new database();
$pagination = new Zebra_Pagination();

$order_id = isset($_GET['order_id'])? "AND o.order_id='{$_GET['order_id']}'" : "";
$c_date = date('Y-m-d');
$d1=$c_date."  00:00:00";
$c_date2 = date ("Y-m-d", strtotime("+1 day"));
$d2=$c_date2."  00:00:00";
$bt=" AND  order_date BETWEEN '{$d1}' AND '{$d2}'";
if(isset($_GET['order_id'])) {$bt=" AND order_id= '{$_GET['order_id']}' " ; }

$sql_or = "SELECT * FROM v_orderdetail o ";
$sql_or .= "WHERE 1=1 {$order_id} {$bt} AND aw_status in ('T','S','R','D','O')  ";
// $sql_or .= isset($_GET['order_id']) ? "AND order_id LIKE '%{$_GET['order_id']}%' " : "";

$query_or = $db->query($sql_or);
$rows_pc = $db->rows($query_or);

$per_page = 20;
$page_start = (($pagination->get_page() - 1) * $per_page);
$sql_or .= "ORDER BY id DESC LIMIT {$page_start},{$per_page} ";
$pagination->records($rows_pc);
$pagination->records_per_page($per_page);
$pagination->base_url('', FALSE);
$query_or_page = $db->query($sql_or);

$page = ($page_start != 0) ? $page_start : "1";
$pages = ceil($rows_pc / $per_page);

$uri = $_SERVER['REQUEST_URI']; // url

// echo $sql_or;
/*
 * php code///////////**********************************************************
 */

/*
 * header***********************************************************************
 */
require 'template/back/header.php';
/*
 * header***********************************************************************
 */
?>

<div id="page-warpper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">รายการสั่งอาหาร</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="subhead">
                <a role="button" class="search-button btn btn-default btn-xs" href="#">
                    <i class="glyphicon glyphicon-search"></i>
                    ค้นหาขั้นสูง
                </a>
                <a role="button" class="btn btn-default btn-xs" 
                   href="<?php echo $baseUrl; ?>/back/orderd/index3">
                    <i class="glyphicon glyphicon-refresh"></i>
                    โหลดหน้าจอใหม่
                </a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="search-form" style="display:none">

                <form id="yw0" action="<?php echo $baseUrl; ?>/back/orderd/index3" method="get">
                    <div class="form-horizontal" style="margin-top: 10px;">
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">รหัสสั่งอาหาร</label>
                            <div class="col-sm-4">
                                <input class="form-control input-sm" name="order_id" id="order_id" type="text" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-4">
                                <button type="submit" class="btn btn-primary searchbtn"><i class="glyphicon glyphicon-search"></i> ค้นหาเดี๋ยวนี้!</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div><!-- search-form -->
            <div id="user-grid" class="grid-view">
                <div class="summary">หน้า <?php echo $page; ?> จากทั้งหมด <?php echo $pages; ?> หน้า</div>
                <table class="table table-striped table-custom">
                    <thead>
                        <tr>
                            <th id="user-grid_c0">
                                <a class="sort-link" href="<?php echo $uri; ?>">สถานะ</a>
                            </th>
                            <th id="user-grid_c1">
                                <a class="sort-link" href="<?php echo $uri; ?>">รหัสสั่งซื้อ</a>
                            </th>
                            <th id="user-grid_c4">
                                <a class="sort-link" href="<?php echo $uri; ?>">ชื่ออาหาร</a>
                            </th>
                            <th id="user-grid_c4">
                                <a class="sort-link" href="<?php echo $uri; ?>">วันที่สั่ง</a>
                            </th>
                            <th id="user-grid_c4">
                                <a class="sort-link" href="<?php echo $uri; ?>">ผู้สั่ง</a>
                            </th>
                            <th id="user-grid_c4" style="text-align: right;">
                                <a class="sort-link" href="<?php echo $uri; ?>">ราคา(บาท)</a>
                            </th>
                            <th class="button-column" id="user-grid_c6">&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 0;
                        while ($rs_or = $db->get($query_or_page)) {
                            $tr = ($i % 2 == 0) ? "odd" : "even";
                            //สถานะ
                            switch ($rs_or['aw_status']) {
                            case 'T':
                                $aw_status = 'เสิร์ฟแล้ว';
                                $btnorder = 'รอเช็คบิล..';
                                $btnstat = 'disabled';
                                break;
                            case 'S':
                                $aw_status = 'เสิร์ฟแล้ว';
                                $btnorder = 'เช็คบิลแล้ว';
                                $btnstat = 'disabled';
                                break;
                            case 'R':
                                $aw_status = 'ยกเลิก';
                                $btnorder = 'ยกเลิก';
                                $btnstat = 'disabled';
                                break;
                            case 'D':
                                $aw_status = 'ยกเลิก';
                                $btnorder = 'ยกเลิก';
                                $btnstat = 'disabled';
                                break;
                            case 'O':
                                $aw_status = 'กำลังเช็คบิล..';
                                $btnorder = 'การเงิน..';
                                $btnstat = 'disabled';
                                break;
                            default:
                                $aw_status = 'ผิดพลาด';
                                $btnorder = '-';
                                $btnstat = 'disabled';
                                break;
                        }
                            ?>
                            <tr class="<?php echo $tr; ?>">
                                <td><?php echo $btnorder."/".$aw_status; ?></td>
                                <td>
                                    <a class="load_data" href="<?php echo $baseUrl; ?>/back/order/view/<?php echo $rs_or['order_id']; ?>"><?php echo $rs_or['order_id']; ?></a>
                                </td>
                                <td><?php echo $rs_or['productName']; ?></td>
                                <td><?php echo thaidate($rs_or['order_date']); ?></td>
                                <td>TB.<?php echo $rs_or['tb'];?></td>
                                <td style="text-align: right;"><?php echo number_format($rs_or['priceQ'],2); ?></td>
                                <td class="button-column">
                                    <a class="btn btn-warning btn-xs load_data <?php echo $btnstat;?>" title="" href="<?php echo $baseUrl; ?>/back/order/payment2/<?php echo $rs_or['order_id']; ?>"><i class="glyphicon glyphicon-usd"></i> <?php echo $btnorder;?></a>
                                    <a class="btn btn-info btn-xs load_data" title="" href="<?php echo $baseUrl; ?>/back/order/view/<?php echo $rs_or['order_id']; ?>"><i class="glyphicon glyphicon-zoom-in"></i> รายละเอียด</a>
                                    <!-- Modal -->
                                    <div class="modal fade" id="deleteModal<?php echo $rs_or['order_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                    <h4 class="modal-title" id="myModalLabel">แจ้งเตือนการลบข้อมูล</h4>
                                                </div>
                                                <div class="modal-body">
                                                    คุณยืนยันต้องการจะลบข้อมูลนี้ ใช่หรือไม่?
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">ไม่ใช่</button>
                                                    <a role="button" class="btn btn-primary" href="<?php echo $baseUrl; ?>/back/order/delete/<?php echo $rs_or['id']; ?>">ใช่ ยืนยันการลบ</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
                <div>
                    <?php $pagination->render(); ?>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.search-button').click(function () {
                $('.search-form').toggle();
                return false;
            });
        });
    </script>
</div>

<?php
/*
 * footer***********************************************************************
 */
require 'template/back/footer.php';
/*
 * footer***********************************************************************
 */
mysql_close();
