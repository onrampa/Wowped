<?php
/*
 * include file start***********************************************************
 */
require 'library/pagination.php';
/*
 * php code///////////**********************************************************
 */
$title = '@Food รับ Order';
$db = new database();
$pagination = new Zebra_Pagination();

$order_id = isset($_GET['order_id'])? "AND id='{$_GET['order_id']}'" : "";
/*
 * php code///////////**********************************************************
 */

/*
 * header***********************************************************************
 */
require 'template/back/header.php';
/*
 * header***********************************************************************
 */
?>

<div id="page-warpper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">รายการสั่งอาหาร</h1>
        </div>
    </div>
    <div class="col-sm-12 blog-main">

    <?php
    $c_date = date('Y-m-d');
    $d1=$c_date."  00:00:00";
    $c_date2 = date ("Y-m-d", strtotime("+1 day"));
    $d2=$c_date2."  00:00:00";
    
   $sql_pd = "SELECT * FROM v_orderdetail WHERE  product_categorie_id IN (34,35) AND  order_date BETWEEN '{$d1}' AND '{$d2}'  AND aw_status in ('P','C') GROUP BY product_id ORDER BY id ASC  ";
   
   $query_pd = $db->query($sql_pd);   
   while ($rs_pd = $db->get($query_pd)) {
   $ppid= $rs_pd['product_id'];
   
   $c_date = date('Y-m-d');
   $d1=$c_date."  00:00:00";
   $c_date2 = date ("Y-m-d", strtotime("+1 day"));
   $d2=$c_date2."  00:00:00";
      
   $sql_pc = " SELECT * FROM v_orderdetail o ";
   $sql_pc .= "WHERE 1=1 {$order_id} AND  order_date BETWEEN '{$d1}' AND '{$d2}' AND product_id in ({$ppid})  AND o.aw_status in ('P','C') ORDER BY o.order_id ASC LIMIT 4  ";
   
   $query_pc = $db->query($sql_pc);  
   ?>
   <div class="col-sm-6 panel panel-default">   
                <div class="panel-body">
                    <div class="row">
                    <div class="form-group">
                    <div class="col-sm-offset-3">
                    <?php 
                    echo " #".$rs_pd['productName'];
                    ?>                    </div>
                </div>


<?php
  $i = 0;
                        while ($rs_pc = $db->get($query_pc)) {
                            $tr = ($i % 2 == 0) ? "odd" : "even";
                            $dis=($rs_pc['priceP']-$rs_pc['price']);
                            if($rs_pc['priceP']>0){$ddis=($dis*100)/$rs_pc['priceP'];}
                            ?>
                            <div class="col-sm-3 col-md-3 col-xs-5" style="height: 110px!important;">
                              
                            <form class="form-inline" role="form" action="<?php echo base_url();?>/back/orderd/form_payment" method="post">
                            <input type="hidden" name="order_id" value="<?php echo $rs_pc['order_id']; ?>">
                            <input type="hidden" name="pid" value="<?php echo $rs_pc['product_id']; ?>">
                                <div class="thumbnail" style="height: 100px!important;">

                            <?php if($rs_pc['aw_status']=='P' & $rs_pc['order_status']=='pending'){ ?>    
                                <button type="submit" class="cart-button"><?php echo $rs_pc['quantity']." #Tb". $rs_pc['quantity'];?>  </button>
                                    <input type="hidden" name="detail" class="col-sm-12"  value="<?php echo $rs_pc['detail']; ?>">                                
                                    <a href="<?php echo $baseUrl; ?>/product/view/<?php echo $rs_pc['product_id']; ?>" >
                                    <img style="height: 50px!important;" src="<?php echo $baseUrl; ?>/upload/product/thumb_<?php echo $rs_pc['image']; ?>" >
                                    </a>
                            <?php } else if($rs_pc['aw_status']=='P' & $rs_pc['order_status']<>'pending'){ ?> 
                                <button type="submit" class="carto-button"><?php echo $rs_pc['quantity']." #Tb". $rs_pc['quantity'];?>  </button>                                
                                    <input type="hidden" name="detail" class="col-sm-12"  value="<?php echo $rs_pc['detail']; ?>">                                
                                    <a href="<?php echo $baseUrl; ?>/product/view/<?php echo $rs_pc['product_id']; ?>" >
                                    <img style="height: 50px!important;" src="<?php echo $baseUrl; ?>/upload/product/thumb_<?php echo $rs_pc['image']; ?>" >
                                    </a>
                            <?php } else if($rs_pc['aw_status']=='C'){ ?>
                                <p id="p1"><?php echo $rs_pc['productName'].":".$rs_pc['quantity']." ที่ #Tb". $rs_pc['quantity'];?></br></br></p>
                                <button onclick="printContent('p1')"><i class="glyphicon glyphicon-print"></i> Print</button>    
                                <a class="btn btn-info btn-xs confirm" title="" href="#" data-toggle="modal" data-target="#PModal<?php echo $rs_pc['id'];?>"><i class="glyphicon glyphicon-share"></i> เสิร์ฟ</a>                                
                            <?php } ?>                                


                                </div>
                            </div> 

                            </form>
                            <!-- Modal -->
                            <div class="modal fade" id="PModal<?php echo $rs_pc['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                               <div class="modal-dialog">
                                   <div class="modal-content">
                                       <div class="modal-header">
                                           <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                           <h4 class="modal-title" id="myModalLabel">แจ้งเตือน</h4>
                                       </div>
                                       <div class="modal-body"> คุณยืนยันการเสิร์ฟ ใช่หรือไม่? </div>
                                       <div class="modal-footer">
                                              <button type="button" class="btn btn-default" data-dismiss="modal">ไม่ใช่</button>
                                               <a role="button" class="btn btn-primary" href="<?php echo $baseUrl; ?>/back/orderd/form_payment2/<?php echo $rs_pc['id']; ?>">ใช่ ยืนยัน</a>
                                       </div>
                                   </div>
                               </div>
                           </div>
                        <?php } ?>
                    </div>
                </div>
                </div>
                
<?php } ?>
    </div>

<script type="text/javascript">
        $(document).ready(function () {
            $('.search-button').click(function () {
                $('.search-form').toggle();
                return false;
            });
        });
function printContent(el){
    var restorepage = document.body.innerHTML;
    var printcontent = document.getElementById(el).innerHTML;
    document.body.innerHTML = printcontent;
    window.print();
    document.body.innerHTML = restorepage;
}
</script>
</div>



<?php
/*
 * footer***********************************************************************
 */
require 'template/back/footer.php';
/*
 * footer***********************************************************************
 */
// mysql_close();
