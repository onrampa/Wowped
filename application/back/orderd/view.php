<?php
/*
 * php code///////////**********************************************************
 */
if (!isset($_GET['id'])) {
    header("location:" . $baseUrl . "/back/order");
}
$db = new database();
$shopID =$_SESSION[_ef . 'MemID'];

$sql_od = "SELECT d.*,p.id,p.name,p.image,p.Weight FROM order_details d INNER JOIN products p ";
$sql_od .= "ON d.product_id=p.id ";
$sql_od .="WHERE d.order_id='{$_GET['id']}'  and d.shop='$shopID'";
$query_od = $db->query($sql_od);

$option_os = array(
    "table" => "orders",
    "condition" => "id='{$_GET['id']}'"
);
$query_os = $db->select($option_os);
$rows_os = $db->rows($query_os);
if($rows_os != 1){
    header("location:" . $baseUrl . "/back/order");
}else{
    $rs_os = $db->get($query_os);
}

/*
 * php code///////////**********************************************************
 */

/*
 * header***********************************************************************
 */
/*
 * header***********************************************************************
 */
?>
<div id="page-warpper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">TB</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6">
            <table class="table" style="font-size: 12px;">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>ชื่อสินค้า</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $grand_total = 0;
                    while ($rs_od = $db->get($query_od)) {
                        $total_price = $rs_od['price'] * $rs_od['quantity'];
                        $grand_total = $total_price + $grand_total;
                        $total_w = $rs_od['Weight'] * $rs_od['quantity'];
                        $grand_w = $total_w + $grand_w;

                        ?>
                        <tr>
                            <td>
                                <img src="<?php echo base_url(); ?>/upload/product/sm_<?php echo $rs_od['image']; ?>">
                            </td>
                            <td><?php echo $rs_od['name']; ?></td>
                            <td style="text-align: right;"><?php echo number_format($rs_od['price'], 2); ?></td>
                            <td style="text-align: right;"><?php echo $rs_od['quantity']; ?></td>
                            <td style="text-align: right;"><?php echo number_format($total_price, 2); ?></td>
                        </tr>
                    <?php } ?>
                    <tr class="info">
                        <td colspan="5" style="text-align: right;">
                         นน=<strong><?php echo number_format($grand_w, 0); ?> </strong> กรัม     ราคารวมทั้งหมด <strong><?php echo number_format($grand_total, 2); ?></strong> บาท
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php
/*
 * footer***********************************************************************
 */
require 'template/back/footer.php';
/*
 * footer***********************************************************************
 */
mysql_close();
