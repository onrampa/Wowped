<?php
 if($_SESSION[_ef . 'levelaccess']== 'user'){
 header('location:'.$baseUrl.'/admin');
 }
 else if($_SESSION[_ef . 'levelaccess']== 'shop'){
 header('location:'.$baseUrl.'/back/orderd');
 }
/*
 * include file start***********************************************************
 */
require 'library/pagination.php';
/*
 * php code///////////**********************************************************
 */
$title = 'ระบบจัดการร้านค้า : รายการรอชำระเงิน';
$db = new database();
$pagination = new Zebra_Pagination();

$order_id = isset($_GET['order_id'])? "AND id='{$_GET['order_id']}'" : "";


$sql_or = "SELECT * FROM v_table o ";
$sql_or .= "WHERE 1=1 {$order_id}  AND o.order_status in ('waiting','pending','shipping','pay','payments')  ";
$sql_or .= isset($_GET['name']) ? "AND o.id LIKE '%{$_GET['name']}%' " : "";
$sql_or .= " group by o.tb  ";
$query_or = $db->query($sql_or);
$rows_pc = $db->rows($query_or);

// echo $sql_or ;
$per_page = 20;
$page_start = (($pagination->get_page() - 1) * $per_page);
$sql_or .= "ORDER BY tb ASC LIMIT {$page_start},{$per_page} ";
$pagination->records($rows_pc);
$pagination->records_per_page($per_page);
$pagination->base_url('', FALSE);
$query_or_page = $db->query($sql_or);

$page = ($page_start != 0) ? $page_start : "1";
$pages = ceil($rows_pc / $per_page);

$uri = $_SERVER['REQUEST_URI']; // url

/*
 * php code///////////**********************************************************
 */

/*
 * header***********************************************************************
 */
require 'template/back/header.php';
/*
 * header***********************************************************************
 */
?>

<div id="page-warpper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">ข้อมูล บิลทั้งหมด</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="subhead">
                <a role="button" class="search-button btn btn-default btn-xs" href="#">
                    <i class="glyphicon glyphicon-search"></i>
                    ค้นหาขั้นสูง
                </a>
                <a role="button" class="btn btn-default btn-xs" 
                   href="<?php echo $baseUrl; ?>/back/order">
                    <i class="glyphicon glyphicon-refresh"></i>
                    โหลดหน้าจอใหม่
                </a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="search-form" style="display:none">

                <form id="yw0" action="<?php echo $baseUrl; ?>/back/order/index" method="get">
                    <div class="form-horizontal" style="margin-top: 10px;">
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">รหัสสั่งซื้อ</label>
                            <div class="col-sm-4">
                                <input class="form-control input-sm" name="order_id" id="order_id" type="text" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-4">
                                <button type="submit" class="btn btn-primary searchbtn"><i class="glyphicon glyphicon-search"></i> ค้นหาเดี๋ยวนี้!</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div><!-- search-form -->
            <div id="user-grid" class="grid-view">
                <div class="summary">หน้า <?php echo $page; ?> จากทั้งหมด <?php echo $pages; ?> หน้า</div>
                <table class="table table-striped table-custom">
                    <thead>
                        <tr>
                            <th id="user-grid_c0">
                                <a class="sort-link" href="<?php echo $uri; ?>">#TB</a>
                            </th>
                            <th id="user-grid_c0">
                                <a class="sort-link" href="<?php echo $uri; ?>">สถานะ</a>
                            </th>
                            <th id="user-grid_c1">                               
                                <a class="sort-link" href="<?php echo $uri; ?>">จำนวน (ใบ)</a>
                              
                            </th>
                            <th id="user-grid_c4">
                                <a class="sort-link" href="<?php echo $uri; ?>">วันที่สั่งซื้อล่าสุด</a>
                            </th>
                            <th id="user-grid_c4">
                                <a class="sort-link" href="<?php echo $uri; ?>">ชื่อลูกค้า</a>
                            </th>
                            <th id="user-grid_c4" style="text-align: right;">
                                <a class="sort-link" href="<?php echo $uri; ?>">ราคา(บาท)</a>
                            </th>
                            <th class="button-column" id="user-grid_c6">&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 0;
                        while ($rs_or = $db->get($query_or_page)) {
                            $tr = ($i % 2 == 0) ? "odd" : "even";
                            //สถานะ
                            switch ($rs_or['order_status']) {
                                case 'pending':
                                    $order_status = 'ยังไม่ชำระเงิน';
                                    $btnorder = 'แจ้งชำระเงิน';
                                    $btnstat2 = '';                                    
                                    $pay = 'payment';
                                    break;
                                case 'waiting':
                                    $order_status = 'ยังไม่ชำระเงิน';
                                    $btnorder = 'แจ้งชำระเงิน';
                                    $btnstat2 = '';                                                                        
                                    $pay = 'payment1';
                                    break;
                                case 'pay':
                                    $order_status = 'กำลังชำระเงิน...';
                                    $btnorder = 'แจ้งชำระเงิน';
                                    $btnstat2 = 'disabled';                                                                        
                                    break;                                    
                                case 'payments':
                                    $order_status = 'กำลังชำระเงิน...';
                                    $btnorder = 'แจ้งชำระเงิน';
                                    $btnstat2 = 'disabled';                                                                        
                                    break;
                                case 'success':
                                    $order_status = 'ชำระเงินแล้ว';
                                    $btnorder = 'ชำระเงินแล้ว';
                                    $btnstat2 = 'disabled';                                    
                                    break;
                                case 'shipping':
                                    $order_status = 'แจ้งจัดส่งสินค้าแล้ว';
                                    $btnorder = 'รอแจ้งโอนเงินให้ร้านค้า';
                                    $btnstat2 = 'disabled';  
                                    $pay = 'payment2';
                                    break;
                                case 'delivery':
                                    $order_status = 'รับสินค้าแล้ว';
                                    $btnorder = 'ชำระเงินแล้ว';
                                    $btnstat2 = 'disabled';  
                                    break;
                                default:
                                    $order_status = 'ผิดพลาด';
                                    $btnorder = '-';
                                    $btnstat2 = 'disabled';  
                                    break;
                            }
                            ?>
                            <tr class="<?php echo $tr; ?>">
                                <td><?php echo $rs_or['tb']; ?></td>
                                <td><?php echo $order_status; ?></td>
                                <td><?php echo $rs_or['c_id']; ?></td>
                                <td><?php echo thaidate($rs_or['order_date']); ?></td>
                                <td><?php echo $rs_or['nameC'];?></td>
                                <td style="text-align: right;"><?php echo number_format($rs_or['total'],2); ?></td>
                                <td class="button-column">
                                    <a class="btn btn-info btn-xs load_data <?php echo $btnstat2;?>" title="" href="<?php echo $baseUrl; ?>/back/order/payment/<?php echo $rs_or['tb']; ?>"><i class="glyphicon glyphicon-zoom-in"></i>แจ้งชำระเงิน</a>                                
                                    <a class="btn btn-danger btn-xs confirm <?php echo $btnstat2;?>" title="" href="#" data-toggle="modal" data-target="#deleteModal<?php echo $rs_or['que']; ?>"><i class="glyphicon glyphicon-remove"></i> ยกเลิก</a>
                                    <!-- Modal -->
                                    <div class="modal fade" id="deleteModal<?php echo $rs_or['que']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <form action="<?php echo base_url(); ?>/back/order/delete1/<?php echo $rs_or['que']; ?>" method="post" name="form" id="form" role="form"
                                            <div class="modal-content">
                                            
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                    <h4 class="modal-title" id="myModalLabel">แจ้งเตือนการลบข้อมูล</h4>
                                                </div>
                                                
                                                <div class="modal-body">
                                                    คุณยืนยันต้องการจะยกเลิกทุกบิล แบบไม่คืน Stock ใช่หรือไม่? กรุณาใส่เหตุผล 
                                                    <input type="text" class="form-control" data-validation="required"  name="reason"  placeholder="กรุณาใส่เหตุผล">

                                                    </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">ไม่ใช่</button>
                                                    <button type="submit" class="btn btn-primary saveform"  >ใช่ ยืนยันการลบ</button>
                                                </div>
                                               
                                            </div>
                                            </form>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
                <div>
                    <?php $pagination->render(); ?>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.search-button').click(function () {
                $('.search-form').toggle();
                return false;
            });
            $('.saveform').click(function () {
                $('#form').submit();
            });
        });
    </script>
</div>

<?php
/*
 * footer***********************************************************************
 */
require 'template/back/footer.php';
/*
 * footer***********************************************************************
 */
