<?php
if(!isset($_GET['id'])){
$c_date = date('Y-m-d');
} else {
$c_date=$_GET['id'];
}
$db = new database();

// $c_date = date('Y-m-d');
$d1=$c_date."  00:00:00";
$c_date2 = date ($c_date, strtotime("+1 day"));
$d2=$c_date."  24:00:00";
$bt=" AND  pay_dateD BETWEEN '{$c_date}' AND '{$c_date}' ";
$btp=" AND  pay_dateD BETWEEN '{$c_date}' AND '{$c_date}' ";
// $bt=" ";
if(isset($_GET['order_id'])) {$bt=" AND order_id= '{$_GET['order_id']}'" ; }


$sql_ot = "SELECT sum(total) as totals FROM orders WHERE  1=1 {$bt} AND order_status = 'success'  ";
$query_ot = $db->query($sql_ot);
$rs_ototal = $db->get($query_ot);
$totals=$rs_ototal['totals'];


$sql_od = "SELECT  price ,SUM(quantity) AS quantity, SUM(total) AS total ,priceP,aw_status, id, productName,image,order_id,product_id,tb,que,order_date,order_status,fullname,user_id FROM v_orderdetail ";
$sql_od .="WHERE   1=1 {$bt} AND order_status IN ('success')  AND aw_status NOT IN ('R','D')  GROUP BY  product_id,price ORDER BY product_categorie_id,productName  ";

// $sql_od = "SELECT * FROM v_orderdetail  ";
// $sql_od .="WHERE que='{$que}' ";
$query_od = $db->query($sql_od);



$title = 'รายละเอียดรายการสั่งอาหาร';

require 'template/back/header.php';

?>
<link rel="stylesheet" href="<?php echo $baseUrl; ?>/css/jquery.datetimepicker.css" type="text/css" />
<script type="text/javascript" src="<?php echo $baseUrl; ?>/js/jquery.form-validator.min.js"></script>
<script type="text/javascript" src="<?php echo $baseUrl; ?>/ckeditor/ckeditor.js"></script>
<script type='text/javascript' src="<?php echo $baseUrl; ?>/js/jquery.datetimepicker.js"></script>
<div id="page-warpper">
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">รายละเอียดการชำระเงิน   <?php echo "Bill:".$c_date; ?></h1>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="subhead">

            <a role="button" class="search-button btn btn-danger btn-md" href="javascript:history.back()">
            << ยกเลิก
            </a>
            <a role="button" class="btn btn-info btn-md new-data" href="#" onclick="printContent('p2')">
            <i class="glyphicon glyphicon-print"></i> Print
            </a>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-6">
        <div class="form-horizontal" style="margin-top: 10px;">
    
        </div>
    </div>
    
    <div class="col-lg-6" id="p2">
    <center>
    <div class="form-group">
    <div class="col-sm-2 col-lg-2" >
    <img src="<?php echo $baseUrl; ?>/images/w1.png" width="50" >
    </div>
    <div class="col-sm-2 col-lg-10" >
     ว้าวอุบล จำกัด
    </div>
</div>
     </center>      
<h4>สรุปรายการ/ รายรับทั้งหมด</h4>
        <p > วันที่ #<?php echo $c_date; ?></p>
        <table class="table" style="font-size: 12px;" >
            <thead>
                <tr>
                    <th>รายการ</th>
                    <th style="text-align: right;">ราคา(บาท)</th>
                    <th style="text-align: right;">จำนวน</th>
                    <th style="text-align: right;">รวม</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $grand_total = 0;
                $no=1;
                while ($rs_od = $db->get($query_od)) {
                    $total_price = $rs_od['price'] * $rs_od['quantity'];
                    $grand_total = $total_price + $grand_total;
                    ?>
                    <tr>
                        <td>
                        <?php echo $no." ) ".$rs_od['productName']; ?>
                        <td style="text-align: right;"><?php echo number_format($rs_od['price'], 2); ?></td>
                        <td style="text-align: right;"><?php echo $rs_od['quantity']; ?></td>
                        <td style="text-align: right;"><?php echo number_format($total_price, 2); ?></td>
                    </tr>
                <?php $no++;} ?>
                <tr class="info">
                    <td colspan="5" style="text-align: right;">รวมทั้งหมด <strong><?php echo number_format($grand_total, 2); ?></strong> บาท</td>
                </tr>
            </tbody>
        </table>
        <p  style="font-size: 14px;"> ห้องครัว .............................</p></br>
        <p  style="font-size: 14px;"> ผู้จัดการ .............................</p></br>
        <p  style="font-size: 14px;"> การเงิน .............................</p></br>
        </br></br></br></br></br></br>
        <p  style="font-size: 10px;"> print by :  <?php echo $_SESSION[_ef . 'fullname']; ?> </p>           
<center> <H4><img src="<?php echo $baseUrl; ?>/images/favicon-32x32.png" width="32" >  <?php echo date('Y-m-d H:i:s') ?> </H4></center>
 <hr>
 <p  style="font-size: 10px;"><center>Powered by Addpay  </center></p>
    </div>  

</div>
</div>

<script>
jQuery('#pay_date').datetimepicker({
    format: 'd/m/Y',
    lang: 'th',
    timepicker: false
});
jQuery('#pay_time').datetimepicker({
    format: 'H:i',
    datepicker: false,
    step:1
});
$(document).ready(function () {
    $("#save").click(function () {
        $("#order-form").submit();
        return false;
    });
});
$.validate();
function printContent(el){
var restorepage = document.body.innerHTML;
var printcontent = document.getElementById(el).innerHTML;
document.body.innerHTML = printcontent;
window.print();
document.body.innerHTML = restorepage;
}
</script>
<?php
/*
* footer***********************************************************************
*/
require 'template/back/footer.php';
/*
* footer***********************************************************************
*/
