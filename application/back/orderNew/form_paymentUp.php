<?php
require(base_path() . "/library/uploadimg.php");

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $db = new database();
    $option_im = array(
        "table" => "payments",
        "fields" => "slip",
        "condition" => "id='{$_POST['id']}'"
    );
    $query_im = $db->select($option_im);
    $rs_im = $db->get($query_im);

    if (checkimg() == TRUE) {
        $filename = date('YmdHis') . rand(0, 9);
        $type = end(explode(".", $_FILES["image"]["name"]));
        $image = $filename . "." . $type;
        $path = base_path() . "/upload/slip/";
        uploadimg($filename, 600, 600, $path);
        uploadimg("thumb_" . $filename, 400, 400, $path);
        uploadimg("md_" . $filename, 150, 150, $path);
        uploadimg("sm_" . $filename, 70, 70, $path);
    
        if ($rs_im['slip'] != "ecimage.jpg") {
            @unlink($path . $rs_im['slip']);
            @unlink($path . "thumb_" . $rs_im['slip']);
            @unlink($path . "md_" . $rs_im['slip']);
            @unlink($path . "sm_" . $rs_im['slip']);
        }
    } else {
        $image = $rs_im['slip'];
    }
        
    $value_pm = array(
        "pay_money" => trim($_POST['pay_money']),
        "detail" => trim($_POST['detail']),
        "blno" => trim($_POST['blno']),
        "slip" => $image
        
    );
    $query_pm = $db->update("payments", $value_pm, "id='{$_POST['id']}'");

    if ($query_pm == TRUE) {
     
        header("location:" . $baseUrl . "/back/order/index1");
    }
    mysql_close();
}