<?php
if(!isset($_GET['id'])){
    header("location:" . $baseUrl . "/back/order");
}
$db = new database();
$sql_tb = " SELECT * FROM v_table ";
$sql_tb .= "WHERE 1=1 AND tb='{$_GET['id']}'  ";
$query_tb = $db->query($sql_tb);
$tb_ototal = $db->get($query_tb);
$que=$tb_ototal['que'];

$sql_ot = "SELECT sum(total) as totals,que,nameC FROM orders  WHERE  order_status  not in ('success','payments','cancel') AND tb='{$_GET['id']}' ";
$query_ot = $db->query($sql_ot);
$rs_ototal = $db->get($query_ot);
$totals=$rs_ototal['totals'];
// $que=$rs_ototal['que'];
$nameC=$rs_ototal['nameC'];
// $db->update("v_orderdetail", array("aw_status"=>"O")," aw_status NOT IN ('R','D') and que='{$que}'");
// $db->update("orders", array("order_status"=>"pay")," order_status NOT IN  ('success','payments')  and que='{$que}' ");

$sql_o = "SELECT * FROM orders  ";
$sql_o .="WHERE  order_status not in ('success','payments') AND que='{$que}' ";
$query_o = $db->query($sql_o);

$sql_op = "SELECT * FROM payments  ";
$sql_op .="WHERE accept = '0' AND que='{$que}' ";
$query_op = $db->query($sql_op);
$rs_op = $db->get($query_op);

$sql_od = "SELECT  nameC ,price,percentage ,SUM(quantity) AS quantity, SUM(total) AS total ,priceP,totalNet,aw_status, id, productName,image,order_id,product_id,tb,que,order_date,order_status,fullname,user_id FROM v_orderdetail ";
$sql_od .="WHERE order_status  not in ('success','cancel')  AND aw_status NOT IN ('R','D')   AND que='{$que}' GROUP BY  product_id,price ORDER BY product_categorie_id , productName ";

// $sql_od = "SELECT * FROM v_orderdetail  ";
// $sql_od .="WHERE order_status <> 'success' AND tb='{$_GET['id']}'  ORDER BY id  ";
$query_od = $db->query($sql_od);

$title = 'รายละเอียดรายการสั่งอาหาร';

require 'template/back/header.php';

?>
<link rel="stylesheet" href="<?php echo $baseUrl; ?>/css/jquery.datetimepicker.css" type="text/css" />

<script type="text/javascript" src="<?php echo $baseUrl; ?>/js/jquery.form-validator.min.js"></script>
<script type="text/javascript" src="<?php echo $baseUrl; ?>/ckeditor/ckeditor.js"></script>
<script type='text/javascript' src="<?php echo $baseUrl; ?>/js/jquery.datetimepicker.js"></script>
<div id="page-warpper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">รายละเอียดการชำระเงิน   <?php echo "Bill:".$_GET['id']; ?></h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="subhead">

                <a role="button" class="search-button btn btn-danger btn-md" href="javascript:history.back()">
                    
                    << ย้อนกลับ
                </a>
                <a role="button" class="btn btn-success btn-md new-data" href="<?php echo base_url();?>/back/order/payment/<?php echo $_GET['id']; ?>">
                    <i class="glyphicon glyphicon-floppy-info"></i>
                    ออกใบเรียกเก็บเงิน
                </a>
                <?php 
                // echo $sql_od; 
                ?>    
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <div class="form-horizontal" style="margin-top: 10px;">

            </div>
        </div>
        
        <div class="col-lg-6" id="p2">
        <center>
        <div class="form-group">
            <div class="col-sm-2 col-lg-2" >
            </br>
            <img src="<?php echo $baseUrl; ?>/images/w1.png" width="50" >
            </div>
            <div class="col-sm-2 col-lg-10" style="font-size: 14px;">  
             ว้าวอุบล จำกัด
            
            </div>
        </div>
            <div class="form-group" style="font-size: 12px;"> 
            TAX# 0345560001276 </div>      </center> 
           <div class="form-group" style="font-size: 12px;">
            นามลูกค้า ....<?php echo @$nameC; ?>...................
            </div>
            <p style="font-size: 12px;">  <?php echo date('Y-m-d H:i:s') ?></p>
            <p style="font-size: 12px;"> ชื่อพนักงาน : <?php echo $_SESSION[_ef . 'fullname']; ?></p>
            <div class="form-group">
             ใบเรียกเก็บเงิน #โต๊ะ : <?php echo $_GET['id']; ?>
            </div>
    
            <table class="table" style="font-size: 12px;" >
                <thead>
                    <tr>
                        <th style="text-align: left;">@</th>
                        <th>รายการ (ราคา)</th>
                        <th style="text-align: right;">รวม</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    // echo $sql_od;
                    $grand_total = 0;
                    $grand_totalNet = 0;
                    while ($rs_od = $db->get($query_od)) {
                        $percentage = $rs_od['percentage'];

                        $total_price = $rs_od['price'] * $rs_od['quantity'];
                        $grand_total = $total_price + $grand_total;

                        $total_priceNet  = $rs_od['totalNet'] * $rs_od['quantity'];
                        $grand_totalNet = $total_priceNet  + $grand_totalNet ;

                        $dis = ($grand_totalNet * ($percentage/100));
                        ?>
                        <tr>
                            <td style="text-align: left;"><?php echo $rs_od['quantity']; ?></td>
                    
                            <td>
                            <?php 
                            if($rs_od['priceP']>$rs_od['price']){
                            $discount="<strike>".number_format($rs_od['priceP'], 2)."</strike>";
                            }
                            else {$discount=""; }
                            echo $rs_od['productName'].$discount." (".number_format($rs_od['totalNet'], 2).") "; ?>
                            <td style="text-align: right;"><?php echo number_format($total_priceNet, 2); ?></td>
                        </tr>
                    <?php } ?>
                    <tr class="info">
                        <td colspan="5" style="text-align: right;">ยอดรวม ฿ <?php echo number_format($grand_totalNet, 2); ?>
                    </td>
                    </tr>
                    <tr>
                        <td colspan="5" style="text-align: right;">หัก (ส่วนลด <?php echo @$percentage."%) - ".number_format(@$dis, 2); ?>
                        </td>
                    </tr>
                    <tr class="info">
                        <td colspan="5" style="font-size: 14px; text-align: right;">รวมสุทธิ ฿ <?php echo number_format($grand_total, 2); ?>
                        </td>
                    </tr>
                </tbody>
            </table>
            <p  style="font-size: 14px;"><center><img src="<?php echo $baseUrl; ?>/images/favicon-32x32.png" width="32" > กรุณาตรวจสอบ </center></p>
            </br> </br><center>..............................

        </div>

    </div>
</div>

<!-- Modal -->
    <div class="modal fade" id="EditModal<?php echo $_GET['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <form action="<?php echo base_url(); ?>/back/order/form_paymentE/<?php echo $_GET['id']; ?>" method="post" name="form" id="form" role="form"
            <div class="modal-content">            
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel">ปรับปรุงยอด ใบเรียกเก็บเงิน</h4>
                </div>
                
                <div class="modal-body">
                <table class="table" style="font-size: 12px;" >
                <thead>
                    <tr>
                    <th>รายการ </th>
                    <th style="text-align: right;">ราคา/ หน่วย</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
//  $sql_odM = "SELECT * FROM v_orderdetail  ";
//  $sql_odM .="WHERE order_status <> 'success' AND tb='{$_GET['id']}' ";

 $sql_odM = "SELECT  price ,SUM(quantity) AS quantity, SUM(total) AS total ,priceP,aw_status, id, productName,image,order_id,product_id,tb,que,order_date,order_status,fullname,user_id FROM v_orderdetail ";
 $sql_odM .="WHERE order_status  not in ('success','payments')  AND aw_status NOT IN ('R','D')   AND tb='{$_GET['id']}'GROUP BY  product_id,price ORDER BY product_categorie_id , productName ";
 $query_odM = $db->query($sql_odM);
                    
                    $grand_total = 0;
                    $k=0;
                    while ($rs_odM = $db->get($query_odM)) {
                        $total_price = $rs_odM['price'] * $rs_odM['quantity'];
                        $grand_total = $total_price + $grand_total;
                        ?>
                        <tr>
                            <td>

                            <input type="hidden" style="text-align: right;" class="form-control"  name="did_<?php echo $k; ?>"
                            value="<?php echo $rs_odM['id']; ?>">

                            <?php 
                            if($rs_odM['priceP']>0){
                            $discount=" (# ".number_format($rs_odM['priceP'], 2)." )";
                            }
                            else {$discount=""; }
                            if($rs_odM['aw_status']){ echo "@";} echo $rs_odM['productName'].$discount; ?>
                            <td style="text-align: right;width:100px;">
                            <input type="text" style="text-align: right;" class="form-control"  name="price_<?php echo $k; ?>"
                            placeholder="ราคาขาย"  value="<?php echo number_format($total_price, 2); ?>">
                            </td>
                        </tr>
                    <?php $k++;} ?>


                </tbody>
            </table>
            </div>
                <div class="modal-footer">

                    <button type="button" class="btn btn-default" data-dismiss="modal">ยกเลิก</button>
                    <button type="submit" class="btn btn-primary saveform"  >บันทึก</button>
                </div>
                
            </div>
            </form>
        </div>
    </div>
<!-- Modal -->

<!-- Modal Dis-->
<div class="modal fade" id="DisModal<?php echo $_GET['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <form action="<?php echo base_url(); ?>/back/order/form_paymentD/<?php echo $_GET['id']; ?>" method="post" name="form" id="form" role="form"
            <div class="modal-content">            
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel">ปรับปรุงส่วนลดใบเรียกเก็บเงิน</h4>
                </div>
<?php

 $sql_ot = "SELECT nameC,sum(totalNet) as totals,percentage,que FROM v_orderdetail WHERE  order_status  not in ('success','payments')  AND aw_status NOT IN ('R','D')   AND tb='{$_GET['id']}' ";
 $query_ot = $db->query($sql_ot);
 $rs_ototal = $db->get($query_ot);
 $totals=$rs_ototal['totals'];
 $nameC=$rs_ototal['nameC'];
 $percentage=$rs_ototal['percentage'];
 $que=$rs_ototal['que'];
 ?>
            <div class="modal-body">
                <table class="table" style="font-size: 16px;" >
                <thead>
                <tr>
                    <th> ชื่อลูกค้า </th>
                    <th style="text-align: right;">                       
                     <input type="text" style="text-align: right;" class="form-control"  name="nameC" value="<?php echo $nameC; ?>">
                    </th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td> ส่วนลด %</td>
                    <td style="text-align: right;width:100px;">
                         <input type="text" style="text-align: right;" class="form-control" name="percentage" value="<?php echo $percentage; ?>">
                    </td>
                </tr>
                <tr>
                    <td> ราคารวมทั้งหมด </td>
                    <td style="text-align: right;width:100px;">
                        <input type="text" style="text-align: right;" class="form-control" name="totalNet" value="<?php echo number_format($totals, 2); ?>">
                    </td>
                </tr>
                <tr>

                </tbody>
            </table>
            </div>
                <div class="modal-footer">
                <input type="hidden" name="que" value="<?php echo $que; ?>">
                    <button type="button" class="btn btn-default" data-dismiss="modal">ยกเลิก</button>
                    <button type="submit" class="btn btn-primary saveform"  >บันทึก</button>
                </div>
                
            </div>
            </form>
        </div>
    </div>
<!-- Modal -->

<script>

    jQuery('#pay_date').datetimepicker({
        format: 'd/m/Y',
        lang: 'th',
        timepicker: false
    });
    jQuery('#pay_time').datetimepicker({
        format: 'H:i',
        datepicker: false,
        step:1
    });
    $(document).ready(function () {
        $("#save").click(function () {
            $("#order-form").submit();
            return false;
        });
    });
    function fncSum(str){
        if (str == "") {
            alert("ไม่มี ค่าที่ส่งมา" + str);
                return;
            }
            if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            }
            else {// code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    document.getElementById("txtHint4").innerHTML = xmlhttp.responseText;
                }
            }
            xmlhttp.open("GET", "<?php echo $baseUrl; ?>/back/order/form_paymentO/" + str, true);
        //    xmlhttp.open("GET", "/eshop/back/check/username/" + str, true);
        //    alert("/eshop/back/check/username/" + str);
            xmlhttp.send();

            var credit = document.getElementById("credit").value;
            var payment = document.getElementById("payment").value;
            var sumT = document.getElementById("sumT").value;
            var re_money = ((credit+payment) - sumT);            
            // document.order-form.re_money.value = re_money;
            alert("เงินทอน "+ re_money + " บาท ");
            document.getElementById("re_money").value = re_money;
                return;            
        }
        function fncRe(str){
        if (str == "") {
            alert("ไม่มี ค่าที่ส่งมา" + str);
                return;
            }
            if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            }
            else {// code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    document.getElementById("txtHint4").innerHTML = xmlhttp.responseText;
                }
            }
            xmlhttp.open("GET", "<?php echo $baseUrl; ?>/back/order/form_paymentRe/" + str, true);
            xmlhttp.send();
      
        }
    $.validate();

    function printContent(el){
    var restorepage = document.body.innerHTML;
    var printcontent = document.getElementById(el).innerHTML;
    document.body.innerHTML = printcontent;
    window.print();
    document.body.innerHTML = restorepage;
    window.location.reload()
}

</script>
<?php
/*
 * footer***********************************************************************
 */
require 'template/back/footer.php';
/*
 * footer***********************************************************************
 */
