<?php 
$Membertype=trim(@$_REQUEST['MemID']);
$MemID=trim(@$_REQUEST['MemID']);
$uname=trim(@$_REQUEST['username']);
$pname=trim(@$_REQUEST['password']);

if(@$_SESSION["Membertype"]<> ''){
    header('location:'.$baseUrl.'/admin');
    }
    ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="language" content="en" />
        <link rel="stylesheet" type="text/css" href="<?php echo $baseUrl; ?>/css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo $baseUrl; ?>/css/bootstrap-theme.min.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo $baseUrl; ?>/css/defaults.css" />
        <script type="text/javascript" src="<?php echo $baseUrl; ?>/js/jquery.1.11.1.min.js"></script>
        <script type="text/javascript" src="<?php echo $baseUrl; ?>/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php echo $baseUrl; ?>/js/loading.js"></script>
        <title>เข้าสู่ระบบ Add-Product</title>
        <style>
            /* CSS used here will be applied after bootstrap.css */
            body { 
                background: url('<?php echo $baseUrl; ?>/images/bg_suburb.jpg') no-repeat center center fixed; 
                -webkit-background-size: cover;
                -moz-background-size: cover;
                -o-background-size: cover;
                background-size: cover;
            }

            .panel-default {
                opacity: 0.9;
                margin-top:30px;
            }
            .form-group.last {
                margin-bottom:0px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="panel panel-default">
                        <div class="panel-heading"> <strong class="">เข้าสู่ระบบ </strong>
                        </div>
                        <div class="panel-body">
                            <form class="form-horizontal" id="myform1" name="myform1" role="form" action="<?php echo $baseUrl; ?>/back/user/form_login/1" method="post">
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-3 control-label">ชื่อผู้ใช้</label>
                                    <div class="col-sm-9">
                                        <input class="form-control" id="username" placeholder="Username" required="" type="text" name="username" value="<?php echo @$_REQUEST['username']; ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-3 control-label">รหัสผ่าน</label>
                                    <div class="col-sm-9">
                                        <input class="form-control" id="password" placeholder="Password" required="" type="password" name="password" value="<?php echo @$_REQUEST['password']; ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-9">
                                        <div class="checkbox">
                                            <label class="">
                                                <input class="" type="checkbox" name="remember">จำไว้ในระบบ</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group last">
                                    <div class="col-sm-offset-3 col-sm-9">
                                    <input type="submit" name="btn_submit" id="btn_submit" value="Submit" style="visibility:hidden;">
                                        <button type="submit" class="btn btn-success btn-sm">ตกลง</button>
                                        <button type="reset" class="btn btn-default btn-sm">ล้างค่า</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!--<div class="panel-footer">ต้องการติดต่อผู้พัฒนาระบบ? <a href="http://www.addpay.co.th" class="">คลิกที่นี้</a>-->
                        <!--</div>-->
                        <div class="panel-footer">สมัครสมาชิกฟรี !  <a href="https://www.addpay.co.th/ap_system/ap_user/register01_ve.php" class="">คลิกที่นี้</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>


 
<br />
</div>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>      
<script type="text/javascript">
$(function(){  
 
// ตั้งเวลาให้ปุ่ม sumit กดเองอัตโนมัติใน 1 วินาที
    setTimeout(function(){
        $("#btn_submit").trigger("click");                  
    },1000);
     
    // เมื่อเริ่มส่งค่า
    $("#myform1").submit(function(){
        $.post("<?php echo $baseUrl; ?>/back/user/form_login/",$(this).serialize()
        ,function(data){
            $(".container").html(data); // ทดสอบแสดงค่าที่กลับมา
            
            if(data<6){ // ถ้าค่าที่กลับมายังไม่เป็นไปตามเงื่อนไข
                setTimeout(function(){ // ตั้งเวลาให้ปุ่ม กดเองอัตโนมัติใน 1 วินาที
                    $("#btn_submit").trigger("click");                  
                },1000);
            }else{
                
                // alert("Success");  
            }
        });
        return false;
    });
 
});
</script>    
    </body>
</html>