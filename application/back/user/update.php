<?php
 if($_SESSION[_ef . 'levelaccess']== 'user'){
 header('location:'.$baseUrl.'/admin');
 }
 else if($_SESSION[_ef . 'levelaccess']== 'shop'){
 header('location:'.$baseUrl.'/admin');
 }
/*
 * php code///////////**********************************************************
 */
$db = new database();

$option_s = array(
    "table" => "users",
    "condition" => "Cclose='1'"
);
$query_s = $db->select($option_s);


$option_user = array(
    "table" => "users",
    "condition" => "id='{$_GET['id']}'"
);
$query_user = $db->select($option_user);

$rs_user = $db->get($query_user);

$title = 'แก้ไขผู้ใช้งาน : ' . $rs_user['username'];
/*
 * php code///////////**********************************************************
 */

/*
 * header***********************************************************************
 */
require 'template/back/header.php';
/*
 * header***********************************************************************
 */
?>

<div id="page-warpper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">แก้ไขข้อมูล <?php echo $rs_user['username']; ?></h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="subhead">
                <a role="button" id="save" class="btn btn-success btn-xs new-data" href="#">
                    <i class="glyphicon glyphicon-floppy-save"></i>
                    Save
                </a>
                <a role="button" class="search-button btn btn-default btn-xs" href="<?php echo $baseUrl; ?>/back/user">
                    <i class="glyphicon glyphicon-remove-circle"></i>
                    Cancel
                </a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="form-horizontal" style="margin-top: 10px;">
                <form id="user-form" action="<?php echo $baseUrl; ?>/back/user/form_update/<?php echo $rs_user['id']; ?>" method="post">
                    <input type="hidden" name="id" value="<?php echo $rs_user['id'];?>">
                    <div class="form-group">
                        <label for="shop_image" class="col-sm-2 control-label required">รูปภาพประจำตัว</label>
                        <div class="col-sm-4">
                            <img src="<?php echo $baseUrl ?>/upload/users/md_<?php echo $rs_user['image'];?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="Shop_image" class="col-sm-2 control-label">รูปภาพใหม่</label>
                        <div class="col-sm-4">
                            <input type="file" name="image" id="image">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label required" for="User_username">Username <span class="required">*</span></label>
                        <div class="col-sm-4">
                            <input class="form-control input-sm" maxlength="50" name="username" id="username" type="text" value="<?php echo $rs_user['username'];?>" readonly = "readonly" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label required" for="User_password">Password <span class="required">*</span></label>
                        <div class="col-sm-4">
                            <input class="form-control input-sm" maxlength="50" name="password" id="password" type="text" value="<?php echo $rs_user['password'];?>" readonly = "readonly" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label required" for="User_firstname">ชื่อจริง <span class="required">*</span></label>
                        <div class="col-sm-4">
                            <input maxlength="100" class="form-control input-sm" name="firstname" id="firstname" type="text" value="<?php echo $rs_user['firstname'];?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="User_email">อีเมล์</label>
                        <div class="col-sm-4">
                            <input class="form-control input-sm" maxlength="100" name="email" id="email" type="text" value="<?php echo $rs_user['email'];?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="User_phone">โทรศัพท์</label>
                        <div class="col-sm-4">
                            <input class="form-control input-sm" maxlength="20" name="phone" id="phone" type="tel" value="<?php echo $rs_user['phone'];?>" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="Shop_name" class="col-sm-2 control-label required">ประเภทสมาชิก </label>
                        <div class="col-sm-4">
                        <select class="form-control input-sm" name="user_type" id="user_type">
                        <option value="user"<?php if($rs_user['user_type']=="user"){ echo "selected";} ?>>ผู้ใช้ทั่วไป</option>
                        <option value="employee" <?php if($rs_user['user_type']=="employee"){ echo "selected";} ?> >พนักงาน</option>
                        <option value="cashier"<?php if($rs_user['user_type']=="cashier"){ echo "selected";} ?>>การเงิน</option>
                        <option value="chef"<?php if($rs_user['user_type']=="chef"){ echo "selected";} ?> >ห้องครัว</option>
                        <option value="admin" <?php if($rs_user['user_type']=="admin"){ echo "selected";} ?>>ผู้ดูแลระบบ</option>
                    </select>
                         </div>
                    </div>
                    <!-- <div class="form-group">
                        <label for="Shop_name" class="col-sm-2 control-label required">ชื่อร้านค้า <span class="required">*</span></label>
                        <div class="col-sm-4">
                            <input type="text" id="shopName" name="shopName" class="form-control input-sm" value="<?php echo $rs_user['shopName']; ?>"  >
                        </div>
                    </div> -->
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $("#save").click(function() {
            $("#user-form").submit();
            return false;
        });
    });
</script>
<?php
/*
 * footer***********************************************************************
 */
require 'template/back/footer.php';
/*
 * footer***********************************************************************
 */