<?php
require(base_path() . "/library/uploadimg.php");
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $db = new database();
    $option_im = array(
        "table" => "users",
        "fields" => "image",
        "condition" => "id='{$_POST['id']}'"
    );
    $query_im = $db->select($option_im);
    $rs_im = $db->get($query_im);
    if (checkimg() == TRUE) {
        $filename = date('YmdHis') . rand(0, 9);
        $type = end(explode(".", $_FILES["image"]["name"]));
        $image = $filename . "." . $type;
        $path = base_path() . "/upload/users/";
        uploadimg($filename, 600, 600, $path);
        uploadimg("thumb_" . $filename, 400, 400, $path);
        uploadimg("md_" . $filename, 150, 150, $path);
        uploadimg("sm_" . $filename, 70, 70, $path);

        if ($rs_im['image'] != "ecimage.jpg") {
            @unlink($path . $rs_im['image']);
            @unlink($path . "thumb_" . $rs_im['image']);
            @unlink($path . "md_" . $rs_im['image']);
            @unlink($path . "sm_" . $rs_im['image']);
        }
    } else {
        $image = $rs_im['image'];
    }

$value_user = array(
    "firstname" => trim($_POST['firstname']),
    "username" => trim($_POST['username']),
    "password" => trim($_POST['password']),
    "email" => trim($_POST['email']),    
    "address" => trim($_POST['address']),
    "district" => trim($_POST['district']),
    "amphur" => trim($_POST['amphur']),
    "province" => trim($_POST['province']),
    "zipcode" => trim($_POST['zipcode']),
    "image" => $image
);

$con_user = "id='{$_GET['id']}'";
$query_user = $db->update("users", $value_user, $con_user);

if($query_user == TRUE){
    header("location:" . $baseUrl . "/back/user");
}
mysql_close();
}