<?php
 if($_SESSION[_ef . 'levelaccess']== 'user'){
 header('location:'.$baseUrl.'/admin');
 }
 else if($_SESSION[_ef . 'levelaccess']== 'shop'){
 header('location:'.$baseUrl.'/admin');
 }
/*
 * php code///////////**********************************************************
 */
$db = new database();
$option_pc = array(
    "table" => "users"
);
$query_pc = $db->select($option_pc);

//$db = new database();
$option_s = array(
    "table" => "shop"
);
$query_s = $db->select($option_s);

$title = 'เพิ่ม ผู้ใช้ใหม่';
/*
 * php code///////////**********************************************************
 */

/*
 * header***********************************************************************
 */
require 'template/back/header.php';
/*
 * header***********************************************************************
 */
?>
<script type="text/javascript" src="<?php echo $baseUrl; ?>/js/jquery.form-validator.min.js"></script>
<script type="text/javascript" src="<?php echo $baseUrl; ?>/ckeditor/ckeditor.js"></script>
<div id="page-warpper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">เพิ่มผู้ใช้ใหม่</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="subhead">
                <a role="button" id="save" class="btn btn-success btn-xs new-data" href="#">
                    <i class="glyphicon glyphicon-floppy-save"></i>
                    บันทึก
                </a>
                <a role="button" class="search-button btn btn-default btn-xs" href="<?php echo $baseUrl; ?>/back/user">
                    <i class="glyphicon glyphicon-remove-circle"></i>
                    ยกเลิก
                </a>
            </div>
        </div>
    </div>
    <div class="row">
   <div class="col-lg-12">
     <div class="form-horizontal" style="margin-top: 10px;">
         <form id="user-form" action="<?php echo $baseUrl; ?>/back/user/form_create" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                        <label for="Shop_image" class="col-sm-2 control-label">รูป สมาชิก</label>
                        <div class="col-sm-4">
                            <input type="file" name="image" id="image">
                        </div>
                    </div>
 
         <div class="form-group">
           <label class="col-sm-2 control-label required" for="User_username">Username <span class="required">*</span></label>
           <div class="col-sm-3">
                <input class="form-control input-sm" maxlength="50" name="username" id="username" type="text"  onkeyup="checkid(this.value)" data-validation="required"/>
          <font color='#FF0000'><b><?php echo  @$_SESSION[_ef . 'userr'] ;?></b></font>

           <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
            <div id="txtHint4"></div>
             </div>
             </div>
        <div class="form-group">
            <label class="col-sm-2 control-label required" for="User_username">Password <span class="required">*</span></label>
            <div class="col-sm-3">
                <input class="form-control input-sm" maxlength="50" name="password" id="password" type="password"  data-validation="required"/>
             </div>
        </div>
          <div class="form-group">
           <label class="col-sm-2 control-label required" for="User_firstname">ชื่อ-นามสกุล <span class="required">*</span></label>
           <div class="col-sm-3">
             <input maxlength="100" class="form-control input-sm" name="firstname" id="firstname" type="text" data-validation="required" />
           </div>
           </div>


        <div class="form-group">
             <label class="col-sm-2 control-label" for="User_email">อีเมล์<span class="required">*</span></label>
             <div class="col-sm-3">
             <input class="form-control input-sm" maxlength="100" name="email" id="email" type="text"  data-validation="required"/>
         </div>
         </div>
          <div class="form-group">
              <label class="col-sm-2 control-label" for="User_phone">โทรศัพท์<span class="required">*</span></label>
            <div class="col-sm-3">
               <input class="form-control input-sm" maxlength="20" name="phone" id="phone" type="text" data-validation="required" />
             </div>
          </div>


       <div class="form-group">
                        <label class="col-sm-2 control-label" for="User_user_type">ประเภทสมาชิก</label>
                        <div class="col-sm-3">
                            <select class="form-control input-sm" name="user_type" id="user_type">
                                <option value="user" selected="selected">ผู้ใช้ทั่วไป</option>
                                <option value="employee" >พนักงาน</option>
                                <option value="cashier">การเงิน</option>
                                <option value="chef" >ห้องครัว</option>
                                <option value="admin">ผู้ดูแลระบบ</option>
                            </select>
          </div>
          </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $("#save").click(function () {
            $("#user-form").submit();
            return false;
        });
    });
    $.validate();

   function checkid(str) {

            if (str == "") {
                document.getElementById("txtHint4").innerHTML = "";
                return;
            }
            if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            }
            else {// code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    document.getElementById("txtHint4").innerHTML = xmlhttp.responseText;
                }
            }
            xmlhttp.open("GET", "<?php echo $baseUrl; ?>/back/check/username/" + str, true);
        //    xmlhttp.open("GET", "/eshop/back/check/username/" + str, true);
        //    alert("/eshop/back/check/username/" + str);
            xmlhttp.send();
        }
</script>
<?php
/*
 * footer***********************************************************************
 */
require 'template/back/footer.php';
/*
 * footer***********************************************************************
 */

