<?php

require(base_path() . "/library/uploadimg.php");
if ($_SERVER['REQUEST_METHOD'] == 'POST') 
{
    $db = new database();
    $option_q = array(
        "table" => "users",
        "fields" => "username",
        "condition" => "username='{$_POST['username']}' and baan='1'"
    );
    $query_q = $db->select($option_q);
    $rows_q = $db->rows($query_q);
    $sdata="";
    if ($rows_q > 0) {
        $_SESSION[_ef . 'userr'] = "Username ซ้ำ ";
        header("location:" . $baseUrl . "/back/user/create");
    }
    else
    {
        if (checkimg() == TRUE) {
            $filename = date('YmdHis') . rand(0, 9);
            $type = end(explode(".", $_FILES["image"]["name"]));
            $image = $filename . "." . $type;

            $path = base_path() . "/upload/users/";
            uploadimg($filename, 600, 600, $path);
            uploadimg("thumb_" . $filename, 400, 400, $path);
            uploadimg("md_" . $filename, 150, 150, $path);
            uploadimg("sm_" . $filename, 70, 70, $path);
        } else {
            $image = "ecimage.jpg";
        }

        $value_user = array(
        "firstname" => trim($_POST['firstname']),
        "username" => trim($_POST['username']),
        "password" => trim($_POST['password']),
        "phone" => trim($_POST['phone']),
        "email" => trim($_POST['email']),
        "user_type" => trim($_POST['user_type']),
        "image" => $image
        );

        $query_user = $db->insert("users", $value_user);

            if ($query_user == TRUE) {
                header("location:" . $baseUrl . "/back/user");
            }else {
                header("location:" . $baseUrl . "/back/user");
            }
    
    }
}