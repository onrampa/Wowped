<?php
/*
 * php code///////////**********************************************************
 */
$db = new database();
$userID= $_SESSION[_ef . 'id'];
$option_user = array(
    "table" => "users",
    "condition" => "id='$userID'"
);
$query_user = $db->select($option_user);
$rs_user = $db->get($query_user);


$title = 'แก้ไขผู้ใช้งาน : ' . $rs_user['username'];
/*
 * php code///////////**********************************************************
 */

/*
 * header***********************************************************************
 */
require 'template/back/header.php';
/*
 * header***********************************************************************
 */
?>

<div id="page-warpper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">แก้ไขข้อมูล <?php echo $rs_user['username']; ?></h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="subhead">
                <a role="button" id="save" class="btn btn-success btn-xs new-data" href="#">
                    <i class="glyphicon glyphicon-floppy-save"></i>
                    Save
                </a>
                <a role="button" class="search-button btn btn-default btn-xs" href="<?php echo $baseUrl; ?>/back/user">
                    <i class="glyphicon glyphicon-remove-circle"></i>
                    Cancel
                </a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="form-horizontal" style="margin-top: 10px;">
                <form id="user-form" action="<?php echo $baseUrl; ?>/back/user/form_profile/<?php echo $rs_user['id']; ?>" method="post">
                    <input type="hidden" name="user_type" value="<?php echo $rs_user['user_type'];?>">
                    <input type="hidden" name="user_type" value="<?php echo $rs_user['user_type'];?>">
                        <!-- <div class="form-group">
                        <label for="shop_image" class="col-sm-2 control-label required">รูปภาพประจำตัว</label>
                        <div class="col-sm-4">
                            <img src="<?php echo $baseUrl ?>/upload/users/md_<?php echo $rs_user['image'];?>">
                        </div>
                    </div> -->
                    <!-- <div class="form-group">
                        <label for="Shop_image" class="col-sm-2 control-label">รูปภาพใหม่</label>
                        <div class="col-sm-4">
                            <input type="file" name="image" id="image">
                        </div>
                    </div>                 -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label required" for="User_firstname">ชื่อจริง <span class="required">*</span></label>
                        <div class="col-sm-4">
                            <input maxlength="100" class="form-control input-sm" name="firstname" id="firstname" type="text" value="<?php echo $rs_user['firstname'];?>" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label required" for="User_username">Username <span class="required">*</span></label>
                        <div class="col-sm-4">
                            <input class="form-control input-sm" maxlength="50" name="username" id="username" type="text" value="<?php echo $rs_user['username'];?>"readonly = "readonly" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label required" for="Shop_name">Password <span class="required">*</span></label>
                        <div class="col-sm-4">
                            <input maxlength="100" class="form-control input-sm" name="password" id="password" type="text" value="<?php echo $rs_user['password'];?>" />
                        </div>
                    </div>
                    <div class="form-group">
                    <label class="col-sm-2 control-label" for="User_email">E-mail</label>
                    <div class="col-sm-4">
                        <input class="form-control input-sm" maxlength="100" name="email" id="email" type="text" value="<?php echo $rs_user['email'];?>" />
                    </div>
                </div>
                 <div class="form-group">
                        <label class="col-sm-2 control-label" for="User_province">จังหวัด</label>
                        <div class="col-sm-4">
                            <input class="form-control input-sm" maxlength="100" name="province" id="province" type="text" value="<?php echo $rs_user['province'];?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="User_district">อำเภอ</label>
                        <div class="col-sm-4">
                            <input class="form-control input-sm" maxlength="100" name="amphur" id="amphur" type="text" value="<?php echo $rs_user['district'];?>" />
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="col-sm-2 control-label" for="User_district">ตำบล</label>
                        <div class="col-sm-4">
                            <input class="form-control input-sm" maxlength="100" name="district" id="district" type="text" value="<?php echo $rs_user['district'];?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="User_address">ที่อยู่</label>
                        <div class="col-sm-4">
                            <input class="form-control input-sm" maxlength="200" name="address" id="address" type="text" value="<?php echo $rs_user['address'];?>" />
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="User_zipcode">รหัสไปรษณีย์</label>
                        <div class="col-sm-4">
                            <input class="form-control input-sm" maxlength="5" name="zipcode" id="zipcode" type="text" value="<?php echo $rs_user['zipcode'];?>" />
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $("#save").click(function() {
            $("#user-form").submit();
            return false;
        });
    });
</script>
<?php
/*
 * footer***********************************************************************
 */
require 'template/back/footer.php';
/*
 * footer***********************************************************************
 */