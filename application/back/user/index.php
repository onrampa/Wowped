<?php
require 'library/pagination.php';
 if($_SESSION[_ef . 'levelaccess']== 'user'){
 header('location:'.$baseUrl.'/admin');
 }
 else if($_SESSION[_ef . 'levelaccess']== 'shop'){
 header('location:'.$baseUrl.'/admin');
 }
/*
 * php code///////////**********************************************************
 */
$title = 'ระบบจัดการ : ผู้ใช้';
$db = new database();
$pagination = new Zebra_Pagination();
// $option_user = array(
//     "table" => "users",
//     "condition" => "username != 'guest'"
// );
// $query_user = $db->select($option_user);

$sql_pc = "SELECT * from users WHERE 1=1 AND baan ='1' ";

$sql_pc .= (@$_GET['UserPhone']<>"") ? "AND phone LIKE '%{$_GET['UserPhone']}%' " : "";
$sql_pc .= (@$_GET['Userfirstname']<>"") ? "AND firstname LIKE '%{$_GET['Userfirstname']}%' " : "";
$sql_pc .= (@$_GET['username']<>"") ? "AND username LIKE '%{$_GET['username']}%' " : "";
$sql_pc .= (@$_GET['Userphone']<>"") ? "AND phone LIKE '%{$_GET['Userphone']}%' " : "";
$sql_pc .= (@$_GET['user_type']<>"") ? "AND user_type LIKE '%{$_GET['user_type']}%' " : "";
$query_user = $db->query($sql_pc);
// echo $sql_pc;
// แบ่งหน้า
$pagination = new Zebra_Pagination();

$query_pc2 = $db->query($sql_pc);

$rows_pc2 = $db->rows($query_pc2);

$per_page = 20;
$page_start = (($pagination->get_page() - 1) * $per_page);
$sql_pc .= "ORDER BY id DESC LIMIT {$page_start},{$per_page} ";
$pagination->records($rows_pc2);
$pagination->records_per_page($per_page);
$pagination->base_url('', FALSE);
$query_pc_page = $db->query($sql_pc);

$page = ($page_start!=0) ? $page_start : "1";
$pages = ceil($rows_pc2/$per_page);

$uri = $_SERVER['REQUEST_URI']; // url
/*
 * php code///////////**********************************************************
 */

/*
 * header***********************************************************************
 */
require 'template/back/header.php';
/*
 * header***********************************************************************
 */
?>

<div id="page-warpper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">จัดการผู้ใช้</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="subhead">
               <a role="button" class="btn btn-success btn-xs new-data"
                   href="<?php echo $baseUrl; ?>/back/user/create" >
                    <i class="glyphicon glyphicon-plus-sign"></i>
                    เพิ่มใหม่
                </a>
                <a role="button" class="search-button btn btn-default btn-xs" href="#">
                    <i class="glyphicon glyphicon-search"></i>
                    ค้นหา ขั้นสูง
                </a>
                <a role="button" class="btn btn-default btn-xs" 
                   href="<?php echo $baseUrl; ?>/back/user">
                    <i class="glyphicon glyphicon-refresh"></i>
                    โหลดหน้าจอใหม่
                </a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="search-form" style="display:none">

                <form id="yw0" action="<?php echo $baseUrl; ?>/back/user/index" method="get"><div class="form-horizontal" style="margin-top: 10px;">
                <div class="form-group">
                            <label for="MemID" class="col-sm-2 control-label">Tel.</label>
                            <div class="col-sm-4">
                                <input size="50" maxlength="50" class="form-control input-sm" name="UserPhone" id="UserPhone" type="number" />        </div>
                        </div>
                <div class="form-group">
                            <label for="Username" class="col-sm-2 control-label">Username</label>
                            <div class="col-sm-4">
                                <input size="50" maxlength="50" class="form-control input-sm" name="Userusername" id="Userusername" type="text" />        </div>
                        </div>

                        <div class="form-group">
                            <label for="Username" class="col-sm-2 control-label">ชื่อจริง</label>
                            <div class="col-sm-4">
                                <input size="60" maxlength="100" class="form-control input-sm" name="Userfirstname" id="Userfirstname" type="text" />        </div>
                        </div>

                        <div class="form-group">
                            <label for="Username" class="col-sm-2 control-label">เบอร์โทร</label>
                            <div class="col-sm-4">
                                <input size="60" maxlength="100" class="form-control input-sm" name="Userphone" id="Userphone" type="number" />        </div>
                        </div>

                        <div class="form-group">
                            <label for="Username" class="col-sm-2 control-label">ประเภท</label>
                            <div class="col-sm-4">
                                <select class="form-control input-sm" name="user_type" id="user_type">
                                    <option value="" selected="selected">All</option>
                                    <option value="user">สมาชิก (ลูกค้า)</option>
                                    <option value="employee">พนักงาน</option>
                                    <option value="cashier">การเงิน</option>
                                    <option value="chef">ห้องครัว</option>
                                    <option value="admin">ผู้ดูแลระบบ</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-4">
                                <button type="submit" class="btn btn-primary searchbtn"><i class="glyphicon glyphicon-search"></i> ค้นหาเดี๋ยวนี้!</button>
                            </div>
                        </div>

                    </div>
                </form>
            </div><!-- search-form -->
            <div id="user-grid" class="grid-view">
             <div class="summary">หน้า <?php echo $page;?> จากทั้งหมด <?php echo $rows_pc2;?> รายการ : <?php echo $pages; ?> หน้า</div>
                <table class="table table-striped table-custom">
                    <thead>
                        <tr>
                        <th id="user-grid_c2">
                                <a class="sort-link" href="<?php echo $baseUrl; ?>/back/user/index?User_sort=phone">Tel.</a>
                            <th id="user-grid_c0">
                                <a class="sort-link" href="<?php echo $baseUrl; ?>/back/user/index?User_sort=username">Username</a>
                            </th>
                            <th id="user-grid_c1">
                                <a class="sort-link" href="<?php echo $baseUrl; ?>/back/user/index?User_sort=firstname">Password</a>
                            </th>
                            </th><th id="user-grid_c3">
                                <a class="sort-link" href="<?php echo $baseUrl; ?>/back/user/index?User_sort=username">ชื่อจริง</a>
                            </th><th id="user-grid_c4">
                                <a class="sort-link" href="<?php echo $baseUrl; ?>/back/user/index?User_sort=user_type">ประเภท</a>
                            </th>

                            <th class="button-column" id="user-grid_c6">&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                        $i = 0;
                        while ($rs_user = $db->get($query_pc_page)) {
                            $tr = ($i % 2 == 0) ? "odd" : "even";
                            ?>

                            <tr class="<?php echo $tr; ?>">
                            <td><?php echo $rs_user['phone']; ?></td>
                                <td>
                                    <a class="load_data" href="<?php echo $baseUrl; ?>/back/user/view/<?php echo $rs_user['id']; ?>"><?php echo $rs_user['username']; ?></a>
                                </td>
                                <td><?php echo $rs_user['password']; ?></td>
                                <td><?php echo $rs_user['firstname']; ?></td>
                                <td><?php echo $rs_user['user_type']; ?></td>
                                <td class="button-column">
                                    <a class="btn btn-info btn-xs load_data" title="" href="<?php echo $baseUrl; ?>/back/user/view/<?php echo $rs_user['id']; ?>"><i class="glyphicon glyphicon-zoom-in"></i> รายละเอียด</a>
                                    <a class="btn btn-warning btn-xs load_data" title="" href="<?php echo $baseUrl; ?>/back/user/update/<?php echo $rs_user['id']; ?>"><i class="glyphicon glyphicon-edit"></i> แก้ไข</a>
                               <?php if($rs_user['user_type']<>"admin"){ ?>
                                    <a class="btn btn-danger btn-xs confirm" title="" href="#" data-toggle="modal" data-target="#deleteModal<?php echo $rs_user['id'];?>"><i class="glyphicon glyphicon-remove"></i> ลบ</a>
                               <?php } ?>
                                    <!-- Modal -->
                                    <div class="modal fade" id="deleteModal<?php echo $rs_user['id'];?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                    <h4 class="modal-title" id="myModalLabel">แจ้งเตือนการลบข้อมูล</h4>
                                                </div>
                                                <div class="modal-body">
                                                    คุณยืนยันต้องการจะลบข้อมูลนี้ ใช่หรือไม่?
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">ไม่ใช่</button>
                                                    <a role="button" class="btn btn-primary" href="<?php echo $baseUrl; ?>/back/user/delete/<?php echo $rs_user['id']; ?>">ใช่ ยืนยันการลบ</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--ปิด Modal-->
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
                <div>
                    <?php $pagination->render(); ?>
                </div>
                <div class="keys" style="display:none" title="<?php echo $baseUrl; ?>/back/user"><span>2</span><span>3</span></div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.search-button').click(function () {
                $('.search-form').toggle();
                return false;
            });
        });
    </script>
</div>

<?php
/*
 * footer***********************************************************************
 */
require 'template/back/footer.php';
/*
 * footer***********************************************************************
 */
mysql_close();