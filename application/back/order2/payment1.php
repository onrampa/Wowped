<?php

/*
 * php code///////////**********************************************************
 */
if(!isset($_GET['id'])){
    header("location:" . $baseUrl . "/back/order");
}
$db = new database();
$sql_tb = " SELECT * FROM v_table ";
$sql_tb .= "WHERE 1=1 AND tb='{$_GET['id']}'  ";
$query_tb = $db->query($sql_tb);
$tb_ototal = $db->get($query_tb);
$que=$tb_ototal['que'];

$sql_o = "SELECT * FROM orders  ";
$sql_o .="WHERE  order_status <> 'success' AND que='{$que}' ";
$sql_o2 = "SELECT * FROM orders  ";
$sql_o2 .="WHERE  order_status <> 'success' AND tb='{$_GET['id']}' ";
$query_o = $db->query($sql_o);
// $rs_o = $db->get($query_o);
// echo $sql_o;

$sql_op = "SELECT * FROM payments  ";
$sql_op .="WHERE accept = '0' AND tb_id='{$_GET['id']}' ORDER BY id DESC ";
$query_op = $db->query($sql_op);
$rs_op = $db->get($query_op);

$sql_od = "SELECT  nameC ,price,percentage ,SUM(quantity) AS quantity, SUM(total) AS total ,priceP,totalNet,aw_status, id, productName,image,order_id,product_id,tb,que,order_date,order_status,fullname,user_id FROM v_orderdetail ";
$sql_od .="WHERE order_status <> 'success' AND aw_status NOT IN ('R','D')  AND que='{$que}' GROUP BY  product_id,price ORDER BY product_categorie_id , productName ";

$query_od = $db->query($sql_od);

$title = ' : ใบเสร็จ';
/*
 * php code///////////**********************************************************
 */

/*
 * header***********************************************************************
 */
require 'template/back/header.php';
/*
 * header***********************************************************************
 */
?>
<link rel="stylesheet" href="<?php echo $baseUrl; ?>/css/jquery.datetimepicker.css" type="text/css" />

<script type="text/javascript" src="<?php echo $baseUrl; ?>/js/jquery.form-validator.min.js"></script>
<script type="text/javascript" src="<?php echo $baseUrl; ?>/ckeditor/ckeditor.js"></script>
<script type='text/javascript' src="<?php echo $baseUrl; ?>/js/jquery.datetimepicker.js"></script>
<div id="page-warpper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">รายละเอียดการชำระเงิน   <?php echo "Bill:".$_GET['id']; ?></h1>

        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="subhead">
                <a role="button" id="save" class="btn btn-success btn-md new-data" href="#">
                    <i class="glyphicon glyphicon-floppy-save"></i>
                    บันทึก Close
                </a>
                <a role="button" class="search-button btn btn-danger btn-md" href="javascript:history.back()">
                    <i class="glyphicon glyphicon-remove-circle"></i>
                    ยกเลิก
                </a>
                <a role="button"  class="btn btn-info btn-md new-data"  onclick="printContent('p2')">
                <i class="glyphicon btn-md  glyphicon-print"></i> Print</a>      
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <div class="form-horizontal" style="margin-top: 10px;">
                <form id="order-form" action="<?php echo $baseUrl; ?>/back/order/form_payment1" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="tb" value="<?php echo $_GET['id']; ?>">
                    <?php
                    $a=1;
                    $orders='';
                    while ($rs_o = $db->get($query_o)) {
                        $order = $rs_o['id']; 
                        $orders = $orders." #".$rs_o['id'];
                        $nameC=$rs_o['nameC'];                       
                        ?>
                    <input type="hidden" name="order_id_<?php echo $a; ?>" value="<?php echo $order; ?>">
                    
                        <?php $a++; } ?>
                    <input type="hidden" name="aa" value="<?php echo $a; ?>">
                    <input type="hidden" name="orders" value="<?php echo $orders; ?>">


                    <div class="form-group row tdtopic " bid="111">
                              
                        <div class="col-sm-6">
                            <img src="images/kbank.gif" width="16" height="16"> บัตรเครดิต/ สินเชื่อ
                        </div>
                        <div class="col-sm-4">
                            <input type="hidden" id="pay_money" name="pay_money" class="form-control input-sm" value="<?php echo $rs_op['pay_money']; ?>" >
                            <input type="hidden" id="que" name="que" value="<?php echo $rs_op['que']; ?>" >
                          <?php echo $rs_op['credit']; ?>
                        </div>
                    </div>

                    <div class="form-group row tdtopic " bid="111">
                        <div class="col-sm-6">
                          <img src="images/bbl.gif" width="16" height="16"> เงินสด
                        </div>
                        <div class="col-sm-4">
                            <?php echo $rs_op['cash']; ?>             
                        </div>
                    </div>
                </form>
            </div>
        </div>
        
        <div class="col-lg-6" id="p2">
    
        <div class="form-group">
        <div class="col-sm-2 col-lg-2" >
        <center> <img src="<?php echo $baseUrl; ?>/images/w1.png" width="50" ></center>
        </div>
        <div class="col-sm-2 col-lg-10" >
        <center>ว้าวอุบล จำกัด</center>
        </div>
    </div>
        <div class="form-group"style="font-size: 12px;"> <center>TAX# 0345560001276  </center>       </div>     
        <div class="form-group"style="font-size: 12px;"> <center>Tel.061-8182888  </center>      </div>     
        <div class="form-group"style="font-size: 12px;"> <center>เปิดบริการทุกวัน 09.00-15.00น. </center>     </div>    
          
         <div class="form-group"style="font-size: 10px;">
        นามลูกค้า ....<?php echo @$nameC; ?>.......
        </div>
        <div class="form-group"style="font-size: 12px;">
        เสร็จรับเงิน #โต๊ะ <?php echo $_GET['id']; ?>
        </div>
        <p style="font-size: 12px;">  <?php echo date('Y-m-d H:i:s') ?></p>
            <table class="table" style="font-size: 10px;" >
                <thead>
                    <tr>
                        <th>@</th>
                        <th>รายการ (ราคา/หน่วย)</th>
                       
                        <th style="text-align: right;">รวม</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $grand_total = 0;
                    $grand_totalNet = 0;
                    while ($rs_od = $db->get($query_od)) {
                        $total_price = $rs_od['price'] * $rs_od['quantity'];
                        $grand_total = $total_price + $grand_total;
                        $total_priceNet  = $rs_od['totalNet'] * $rs_od['quantity'];
                        $grand_totalNet = $total_priceNet  + $grand_totalNet ;
                        $percentage = $rs_od['percentage'];
                        $dis = ($grand_totalNet * ($percentage/100));

                        ?>
                        <tr>
                        <td style="text-align: left;"><?php echo number_format($rs_od['quantity'], 0); ?></td>
                            <td>
                            <?php 
                            if($rs_od['priceP']>$rs_od['price']){
                                // $discount="<strike>".number_format($rs_od['priceP'], 2)."</strike>";
                                $discount="";
                                }
                                else {$discount=""; }
                             echo $rs_od['productName'].$discount." (".number_format($rs_od['totalNet'], 2).") "; ?>
                          
                            <td style="text-align: right;"><?php echo number_format($total_priceNet, 2); ?></td>
                        </tr>
                    <?php } ?>
                    <tr class="info">
                    <td colspan="5" style="text-align: right;">ยอดรวม ฿ <?php echo number_format($grand_totalNet, 2); ?>
                </td>
                </tr>
                <tr>
                    <td colspan="5" style="text-align: right;">หัก (ส่วนลด <?php echo $percentage."%) - ".number_format($dis, 2); ?>
                    </td>
                </tr>
                <tr class="info">
                    <td colspan="5" style="font-size: 14px; text-align: right;">รวมสุทธิ ฿ <?php echo number_format($grand_total, 2); ?>
                    </td>
                </tr>
                </tbody>
            </table>
            <p  style="font-size: 14px;"> ชำระโดย <?php if($rs_op['credit']>0){ echo "#Credit:>".$rs_op['credit']." ";} if($rs_op['cash']>0){ echo "#Cash:>".$rs_op['cash'];} ?></p>
            <p  style="font-size: 12px;"><?php if($rs_op['cash']>0){ echo "Received: ".number_format($rs_op['payment'], 2)."  Change: ".number_format($rs_op['re_money'], 2);} ?></p>
            <p  style="font-size: 12px;">billNo# <?php echo $rs_op['id']; ?></p>
            <p  style="font-size: 10px;">  by :  <?php echo $_SESSION[_ef . 'fullname']; ?> @orderID<?php echo $rs_op['order_id']; ?></p>           
     <center> <H4><img src="<?php echo $baseUrl; ?>/images/favicon-32x32.png" width="32" >  Thank you  </H4></center>
     <br><br><br><br><br><br>


     <hr>
     <p  style="font-size: 10px;"><center>Powered by Addpay  </center></p>
        </div>
 
    </div>
</div>

<script>
    jQuery('#pay_date').datetimepicker({
        format: 'd/m/Y',
        lang: 'th',
        timepicker: false
    });
    jQuery('#pay_time').datetimepicker({
        format: 'H:i',
        datepicker: false,
        step:1
    });
    $(document).ready(function () {
        $("#save").click(function () {
            $("#order-form").submit();
            return false;
        });
    });

    $.validate();
    function printContent(el){
    var restorepage = document.body.innerHTML;
    var printcontent = document.getElementById(el).innerHTML;
    document.body.innerHTML = printcontent;
    window.print();
    document.body.innerHTML = restorepage;
    window.location.reload()
}
</script>
<?php
/*
 * footer***********************************************************************
 */
require 'template/back/footer.php';
/*
 * footer***********************************************************************
 */
