<?php
require(base_path() . "/library/uploadimg.php");
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $pay_date_explode = explode("/", trim($_POST['pay_money']));
    $pay_date = $pay_date_explode[2] . "-" . $pay_date_explode[1] . "-" . $pay_date_explode[0] . " " . trim($_POST['pay_time']);
    $db = new database();
    if (checkimg() == TRUE) {
        $filename = date('YmdHis') . rand(0, 9);
        $type = end(explode(".", $_FILES["image"]["name"]));
        $image = $filename . "." . $type;
        $path = base_path() . "/upload/slip/";
        uploadimg($filename, 600, 600, $path);
        uploadimg("thumb_" . $filename, 400, 400, $path);
        uploadimg("md_" . $filename, 150, 150, $path);
        uploadimg("sm_" . $filename, 70, 70, $path);
    } else {
        $image = "ecimage.jpg";
    }
$aa=$_POST['aa'];
$fullname = $_SESSION[_ef . 'fullname'];
$pay_money=trim($_POST['pay_money']);
$credit=trim($_POST['credit']);
$cash=trim($_POST['cash']);
$sum_pay=($credit+$cash);
$re_money=($pay_money-$sum_pay);
$value_pm = array(
    "re_money" => $re_money,
    "pay_date" => $pay_date,
    "credit" => $credit,
    "cash" => $cash,
    "tb_id" => trim($_POST['tb']),
    "order_id" => $_POST['orders'],
    "nameC" => trim($_POST['nameC']),
    "receip_user" => $fullname,
    "person" => trim($_POST['person']),
    "slip" => $image
);
$pm=trim($_POST['pm']);
$query_pm = $db->update("payments",$value_pm," id='{$pm}'");

    if ($query_pm == TRUE) {
        for ($i = 0; $i < $aa; $i++) {
        $db->update("orders", array("order_status"=>"payments")," id='{$_POST['order_id_' . $i]}'");
        $db->update("order_detail", array("aw_status"=>"S")," order_id='{$_POST['order_id_' . $i]}'");        
        }
        $db->update("utable", array("open"=>"3")," tb='{$_POST['tb']}'");
    }
    if($_SESSION[_ef . 'levelaccess']== 'admin'){
        header('location:'.$baseUrl.'/back/order/index1');
        }
        else {
        header('location:'.$baseUrl.'/back/utable/view');
        }

    mysql_close();
}