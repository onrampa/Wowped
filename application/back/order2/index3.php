

<?php

if(!isset($_GET['id'])){
    $c_date = date('Y-m-d');
    $c_date2 = date('Y-m-d');
    } else {
    $c_date=$_GET['id'];
    $c_date2=$_GET['id'];
    }
 if(isset($_GET['sdate'])){
    $c_date = $_GET['sdate'];
    $c_date2 = $_GET['edate'];
    } 

 if($_SESSION[_ef . 'levelaccess']== 'user'){
 header('location:'.$baseUrl.'/admin');
 }
 else if($_SESSION[_ef . 'levelaccess']== 'shop'){
 header('location:'.$baseUrl.'/back/orderd');
 }
/*
 * include file start***********************************************************
 */
require 'library/pagination.php';
/*
 * php code///////////**********************************************************
 */
$title = 'ระบบจัดการร้านค้า : เสร็จสิ้น';
$db = new database();
$pagination = new Zebra_Pagination();

// $c_date = date('Y-m-d');
$d1=$c_date."  00:00:00";
$d2=$c_date2."  23:59:59";
$bt=" AND  o.pay_date BETWEEN '{$d1}' AND '{$d2}'";
if($_GET['billNo']>0) {
    $bt=" AND o.id= '{$_GET['billNo']}'" ; 
    }


$sql_or = "SELECT  * FROM payments o ";
$sql_or .= "WHERE 1=1 {$bt} AND o.accept in ('1') ";
$query_or = $db->query($sql_or);
$rows_pc = $db->rows($query_or);

// หาผลรวมทั้งหมดของวัน
$sql_sum = "SELECT SUM(credit) AS credit, SUM(cash) AS cash, SUM(discount) AS discount   FROM payments o  WHERE 1=1 {$bt}   AND o.accept in ('1') ";
$query_sum = $db->query($sql_sum);
$rs_sum = $db->get($query_sum);
$total_credit = $rs_sum['credit'];
$total_cash = $rs_sum['cash'];
$total_discount = $rs_sum['discount'];
$total_sum = ($total_credit+$total_cash);


$per_page = 40;
$page_start = (($pagination->get_page() - 1) * $per_page);
$sql_or .= "ORDER BY o.id DESC LIMIT {$page_start},{$per_page} ";
$pagination->records($rows_pc);
$pagination->records_per_page($per_page);
$pagination->base_url('', FALSE);
$query_or_page = $db->query($sql_or);

$page = ($page_start != 0) ? $page_start : "1";
$pages = ceil($rows_pc / $per_page);

$uri = $_SERVER['REQUEST_URI']; // url

// echo $sql_or;
/*
 * php code///////////**********************************************************
 */

/*
 * header***********************************************************************
 */
require 'template/back/header.php';
/*
 * header***********************************************************************
 */
?>

<div id="page-warpper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">รายการ ชำระเงินแล้ว  </h1>
 <p><strong> ช่วง ระหว่าง <?php echo $c_date." ถึง  ".$c_date2; ?>  </strong></p>
        

        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="subhead">
                <a role="button" class="search-button btn btn-default btn-xs" href="#">
                    <i class="glyphicon glyphicon-search"></i>
                    ค้นหาขั้นสูง
                </a>
                <a role="button" class="btn btn-default btn-xs" 
                   href="<?php echo $baseUrl; ?>/back/order/index3">
                    <i class="glyphicon glyphicon-refresh"></i>
                    โหลดหน้าจอใหม่
                </a>
                <a class="btn btn-info btn-md new-data" title="" href="#" data-toggle="modal" 
                data-target="#PrintModal"><i class="glyphicon btn-md  glyphicon-print"></i> Print</a>                              

            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="search-form" style="display:none">

                <form id="yw0" action="<?php echo $baseUrl; ?>/back/order/index3" method="get">
                    <div class="form-horizontal" style="margin-top: 10px;">
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">รหัสสั่งชำระเงิน Bill</label>
                            <div class="col-sm-4">
                                <input class="form-control input-sm" name="billNo" id="billNo" type="text" />
                            </div>
                        </div>
                    <div class="form-group">
                        <label class="col-sm-1 form-control-label"> วันที่ </label>
                        <div class="col-sm-2 ">
                        <input  name="sdate" id="sdate" class="form-control css-require datepicker" data-date-format="yy-mm-dd">
                        </div>
                        <label class="col-sm-1 form-control-label"> ถึง วันที่ </label>
                        <div class="col-sm-2 ">
                        <input  name="edate" id="edate" class="form-control css-require datepicker" data-date-format="yy-mm-dd">
                        </div>
                    </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-4">
                                <button type="submit" class="btn btn-primary searchbtn"><i class="glyphicon glyphicon-search"></i> ค้นหาเดี๋ยวนี้!</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div><!-- search-form -->
            <div id="user-grid" class="grid-view">
                <div class="summary">หน้า <?php echo $page; ?> จากทั้งหมด <?php echo $pages; ?> หน้า</div>
                <table class="table table-striped table-custom">
                    <thead>
                        <tr>
                            <th id="user-grid_c0">
                                <a class="sort-link" href="<?php echo $uri; ?>">#Bill</a>
                            </th>
                            <th id="user-grid_c0">
                                <a class="sort-link" href="<?php echo $uri; ?>">สถานะ</a>
                            </th>
                            <th id="user-grid_c1">                               
                                <a class="sort-link" href="<?php echo $uri; ?>">รหัสใบ Order</a>
                              
                            </th>
                            <th id="user-grid_c4">
                                <a class="sort-link" href="<?php echo $uri; ?>">วันที่สั่งซื้อ</a>
                            </th>
                            <th id="user-grid_c4">
                                <a class="sort-link" href="<?php echo $uri; ?>">ชื่อลูกค้า</a>
                            </th>
                            <th id="user-grid_c4" style="text-align: right;">
                                <a class="sort-link" href="<?php echo $uri; ?>">ส่วนลด</a>
                            </th>
                            <th id="user-grid_c4" style="text-align: right;">
                                <a class="sort-link" href="<?php echo $uri; ?>">Credit</a>
                            </th>
                            <th id="user-grid_c4" style="text-align: right;">
                                <a class="sort-link" href="<?php echo $uri; ?>">เงินสด</a>
                            </th>
                            <th class="button-column" id="user-grid_c6">&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td style="text-align: right;"><?php echo number_format($total_discount,2); ?></td>
                                <td style="text-align: right;"><?php echo number_format($total_credit,2); ?></td>
                                <td style="text-align: right;"><?php echo number_format($total_cash,2); ?></td>
                                <td style="text-align: center;"><?php echo number_format($total_sum,2); ?> ฿ รวมรับ (ทั้งวัน)</td>
                            </tr>

                        <?php
                        $i = 0;
                        while ($rs_or = $db->get($query_or_page)) {
                            $tr = ($i % 2 == 0) ? "odd" : "even";
                            //สถานะ
                            switch ($rs_or['accept']) {
                                case '0':
                                    $order_status = 'ยังไม่ชำระเงิน';
                                    $btnorder = 'แจ้งชำระเงิน';
                                    $btnstat = '';
                                    $pay = 'payment';
                                    break;
                                case '1':
                                    $order_status = 'ชำระเงินแล้ว';
                                    $btnorder = 'ชำระเงินแล้ว';
                                    $btnstat = 'disabled';
                                    $pay = 'payment';
                                    break;
                                default:
                                    $order_status = 'ผิดพลาด';
                                    $btnorder = '-';
                                    $btnstat = 'disabled';
                                    break;
                            }
                            ?>
                            <tr class="<?php echo $tr; ?>">
                                <td><?php echo $rs_or['id']; ?></td>
                                <td><?php echo $order_status; ?></td>
                                <td><?php echo $rs_or['order_id']; ?></td>
                                <td><?php echo thaidate($rs_or['pay_date']); ?></td>
                                <td><?php echo $rs_or['nameC'];?></td>
                                <td style="text-align: right;"><?php echo number_format($rs_or['discount'],2); ?></td>                                
                                <td style="text-align: right;"><?php echo number_format($rs_or['credit'],2); ?></td>
                                <td style="text-align: right;"><?php echo number_format($rs_or['cash'],2); ?></td>
                                <td class="button-column">
                                    <a class="btn btn-info btn-xs load_data" title="" href="<?php echo $baseUrl; ?>/back/order/view3/<?php echo $rs_or['id']; ?>"><i class="glyphicon glyphicon-zoom-in"></i> รายละเอียด</a>

                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
        <!-- Modal -->
<div class="modal fade" id="PrintModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<?php 
$query_or1 = $db->query($sql_or);
// $rows_pc = $db->rows($query_or);

// หาผลรวมทั้งหมดของวัน
$sql_sum = "SELECT SUM(credit) AS credit, SUM(cash) AS cash   FROM payments o  WHERE 1=1 {$bt}   AND o.accept in ('1') ";
$query_sum = $db->query($sql_sum);
$rs_sum = $db->get($query_sum);
$total_credit = $rs_sum['credit'];
$total_cash = $rs_sum['cash'];
$total_sum = ($total_credit+$total_cash);
?>
        <div class="modal-dialog" >
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel">ปริ้นต์รายการรายวัน</h4>
                </div>
                <div class="modal-body" id="p2" >
 
 <table class="table table-striped table-custom">
                    <thead>
                        <tr>
                            <th>
                            #Bill
                            </th>
                            <th>
                            ส่วนลด
                            </th>

                            <th>
                            เครดิต
                            </th>
                            <th>
                            เงินสด
                            </th>


                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $i = 0;
                        while ($rs_or1 = $db->get($query_or1)) {
                            $tr = ($i % 2 == 0) ? "odd" : "even";
                            ?>
                            <tr class="<?php echo $tr; ?>">
                                <td><?php echo $rs_or1['id']; ?></td>
                                <td style="text-align: right;"><?php echo number_format($rs_or1['discount'],2); ?></td>
                                <td style="text-align: right;"><?php echo number_format($rs_or1['credit'],2); ?></td>
                                <td style="text-align: right;"><?php echo number_format($rs_or1['cash'],2); ?></td>
                            </tr>
                        <?php } ?>
                        <hr>
                           <tr>
                                <td>Total</td>
                                <td style="text-align: right;text-decoration: underline"><?php echo number_format($total_discount,2); ?></td>
                                <td style="text-align: right;text-decoration: underline"><?php echo number_format($total_credit,2); ?></td>
                                <td style="text-align: right;text-decoration: underline"><?php echo number_format($total_cash,2); ?></td>
                            </tr>

                           
                           
                    </tbody>
                </table>
                <p style="text-align: right;">
                รวม <?php echo number_format($total_sum,2); ?> ฿ (เครดิต+เงินสด)
                </p>
                <br><br><br>
                <br><br><br>
                <center>------------------</center>

                
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">ไม่ใช่</button>
                    <a role="button"  class="btn btn-info btn-md new-data"  onclick="printContent('p2')">
                    <i class="glyphicon btn-md  glyphicon-print"></i> Print</a>   
                     
                </div>
            </div>
        </div>
    </div>
    <!-- ปิด modal -->

                <div>
                    <?php $pagination->render(); ?>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.search-button').click(function () {
                $('.search-form').toggle();
                return false;
            });
        });

        $('.datepicker').datepicker({
            
            format: 'yyyy-mm-dd'
            // startDate: '-3d'
        });
$.validate();
function printContent(el){
var restorepage = document.body.innerHTML;
var printcontent = document.getElementById(el).innerHTML;
document.body.innerHTML = printcontent;
window.print();
document.body.innerHTML = restorepage;
window.location.reload()
}
// $('.datepicker').datepicker();
</script>

</div>

<?php
/*
 * footer***********************************************************************
 */
require 'template/back/footer.php';
/*
 * footer***********************************************************************
 */

