<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>ระบบลงทะเบียนร้านค้า ผู้ประกอบการ</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link href="css/main.menu.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    
	<div id="flipkart-navbar">
    <div class="container">
        <div class="row row1">
            <ul class="largenav pull-right">
                <li class="upper-links"><a class="links" href="index.html">หน้าหลัก</a></li>
                <li class="upper-links"><a class="links" href="#">สินค้า</a></li>
                <li class="upper-links"><a class="links" href="cart.html">ตะกร้าสินค้า</a></li>
                <li class="upper-links"><a class="links" href="#">วิธีซื้อสินค้า</a></li>
                <li class="upper-links"><a class="links" href="#">เกี่ยวกับเรา</a></li>
                <li class="upper-links">
                   
                </li>
            </ul>
        </div>

        <div class="row row2">
            <div class="col-sm-2">
                <h2 style="margin:0px;"><span class="smallnav menu" onclick="openNav()">☰ AddPay Shops</span></h2>
                <div style="margin-top:-10px; margin-left:25px;"><a href="#"><img src="images/logo/logo_addpay1.png" class="largenav"></a></div>              
            </div>
            
            
            <div class="flipkart-navbar-search smallsearch col-sm-10 col-xs-11">
                <div class="row">
                    <input class="flipkart-navbar-input col-xs-11" type="" placeholder="ค้นหา" name="">
                    <button class="flipkart-navbar-button col-xs-1">
                        <svg width="15px" height="15px">
                            <path d="M11.618 9.897l4.224 4.212c.092.09.1.23.02.312l-1.464 1.46c-.08.08-.222.072-.314-.02L9.868 11.66M6.486 10.9c-2.42 0-4.38-1.955-4.38-4.367 0-2.413 1.96-4.37 4.38-4.37s4.38 1.957 4.38 4.37c0 2.412-1.96 4.368-4.38 4.368m0-10.834C2.904.066 0 2.96 0 6.533 0 10.105 2.904 13 6.486 13s6.487-2.895 6.487-6.467c0-3.572-2.905-6.467-6.487-6.467 "></path>
                        </svg>
                    </button>
                </div>
            </div>
          
        </div>
    </div>
</div>




<div id="mySidenav" class="sidenav">
    <div class="container" style="background-color: #9c1313; padding-top: 10px;">
        <span class="sidenav-heading">เมนู</span>
        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">×</a>
    </div>
    <a href="#">หน้าหลัก</a>
	<a href="#">อิเล็กทรอนิกส์</a>
	<a href="#">แฟชั่นสุภาพสตรี</a>
	<a href="#">แฟชั่นสุภาพบุรุษ</a>
	<a href="#">เครื่องใช้ไฟฟ้าและของในบ้าน</a>
	<a href="#">สุขภาพและความงาม</a>
	<a href="#">เด็กและของเล่น</a>
	<a href="#">กีฬาและการเดินทาง</a>
	<a href="#">ยานยนต์</a>
	<a href="#">สัตว์เลี้ยงและสื่อบันเทิง</a>
</div>
    

    <!-- Page Content -->
    <div class="container">

        <div class="row top10">

            <div class="col-md-2 hidden-xs">
                <p class="lead">AddPay Shops</p>
                <div class="list-group">
                    <a href="#" class="list-group-item">อุปกรณ์อิเล็กทรอนิกส์</a>
                    <a href="#" class="list-group-item">อาหาร</a>
                    <a href="#" class="list-group-item">ของฝาก</a>
                    <a href="#" class="list-group-item">สุขภาพและความงาม</a>
                    <a href="#" class="list-group-item">แฟชั่นสุภาพสตรี</a>
                    <a href="#" class="list-group-item">แฟชั่นสุภาพบุรุษ</a>
                    <a href="#" class="list-group-item">สินค้าอื่นๆ</a>
                </div>
            </div>

            <div class="col-md-9">
                

                <div class="row top10">

                    <div class="col-sm-4 col-lg-4 col-md-4">
                        <div class='container'>
    <div class='row' style='padding-top:25px; padding-bottom:25px;'>
        <div class='col-md-12'>
            <div id='mainContentWrapper'>
                <div class="col-md-8 col-md-offset-0">
                    <h2 style="text-align: center;">
                    <img src="images/market.png" width="100" height="97"> <img src="images/logo_thongchom.png" width="150"> <img src="images/IMG_31_110860_20160322_091552.jpg" width="100"><br> แบบคำขอลงทะเบียนผู้ประกอบการ โครงการตลาดต้องชม</h2>
               
                    
                    
                        <form class="form-horizontal" role="form" action="add_regist.php" method="post" id="payment-form">
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-default">
                                    
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            <div class="items">
                                                <div class="col-md-12">
                                                    <table class="table table-striped" style="font-weight: bold;">
                                        
                                            <tr>
                                                <td style="width: 175px;">
                                                    <label for="id_state">จังหวัด:</label ></td>
                                                <td>
                                                    <select class="form-control" id="province_regis" name="province_regis">
                                                    	<option value="อุบลราชธานี">อุบลราชธานี</option>
                                                       <option value="กรุงเทพมหานคร">กรุงเทพมหานคร</option>
                                                   	  <option value="กระบี่">กระบี่ </option>
                                                           <option value="กาญจนบุรี">กาญจนบุรี </option>
                                                           <option value="กาฬสินธุ์">กาฬสินธุ์ </option>
                                                           <option value="กำแพงเพชร">กำแพงเพชร </option>
                                                           <option value="ขอนแก่น">ขอนแก่น</option>
                                                           <option value="จันทบุรี">จันทบุรี</option>
                                                           <option value="ฉะเชิงเทรา">ฉะเชิงเทรา </option>
                                                           <option value="ชัยนาท">ชัยนาท </option>
                                                           <option value="ชัยภูมิ">ชัยภูมิ </option>
                                                           <option value="ชุมพร">ชุมพร </option>
                                                           <option value="ชลบุรี">ชลบุรี </option>
                                                           <option value="เชียงใหม่">เชียงใหม่ </option>
                                                           <option value="เชียงราย">เชียงราย </option>
                                                           <option value="ตรัง">ตรัง </option>
                                                           <option value="ตราด">ตราด </option>
                                                           <option value="ตาก">ตาก </option>
                                                           <option value="นครนายก">นครนายก </option>
                                                           <option value="นครปฐม">นครปฐม </option>
                                                           <option value="นครพนม">นครพนม </option>
                                                           <option value="นครราชสีมา">นครราชสีมา </option>
                                                           <option value="นครศรีธรรมราช">นครศรีธรรมราช </option>
                                                           <option value="นครสวรรค์">นครสวรรค์ </option>
                                                           <option value="นราธิวาส">นราธิวาส </option>
                                                           <option value="น่าน">น่าน </option>
                                                           <option value="นนทบุรี">นนทบุรี </option>
                                                           <option value="บึงกาฬ">บึงกาฬ</option>
                                                           <option value="บุรีรัมย์">บุรีรัมย์</option>
                                                           <option value="ประจวบคีรีขันธ์">ประจวบคีรีขันธ์ </option>
                                                           <option value="ปทุมธานี">ปทุมธานี </option>
                                                           <option value="ปราจีนบุรี">ปราจีนบุรี </option>
                                                           <option value="ปัตตานี">ปัตตานี </option>
                                                           <option value="พะเยา">พะเยา </option>
                                                           <option value="พระนครศรีอยุธยา">พระนครศรีอยุธยา </option>
                                                           <option value="พังงา">พังงา </option>
                                                           <option value="พิจิตร">พิจิตร </option>
                                                           <option value="พิษณุโลก">พิษณุโลก </option>
                                                           <option value="เพชรบุรี">เพชรบุรี </option>
                                                           <option value="เพชรบูรณ์">เพชรบูรณ์ </option>
                                                           <option value="แพร่">แพร่ </option>
                                                           <option value="พัทลุง">พัทลุง </option>
                                                           <option value="ภูเก็ต">ภูเก็ต </option>
                                                           <option value="มหาสารคาม">มหาสารคาม </option>
                                                           <option value="มุกดาหาร">มุกดาหาร </option>
                                                           <option value="แม่ฮ่องสอน">แม่ฮ่องสอน </option>
                                                           <option value="ยโสธร">ยโสธร </option>
                                                           <option value="ยะลา">ยะลา </option>
                                                           <option value="ร้อยเอ็ด">ร้อยเอ็ด </option>
                                                           <option value="ระนอง">ระนอง </option>
                                                           <option value="ระยอง">ระยอง </option>
                                                           <option value="ราชบุรี">ราชบุรี</option>
                                                           <option value="ลพบุรี">ลพบุรี </option>
                                                           <option value="ลำปาง">ลำปาง </option>
                                                           <option value="ลำพูน">ลำพูน </option>
                                                           <option value="เลย">เลย </option>
                                                           <option value="ศรีสะเกษ">ศรีสะเกษ</option>
                                                           <option value="สกลนคร">สกลนคร</option>
                                                           <option value="สงขลา">สงขลา </option>
                                                           <option value="สมุทรสาคร">สมุทรสาคร </option>
                                                           <option value="สมุทรปราการ">สมุทรปราการ </option>
                                                           <option value="สมุทรสงคราม">สมุทรสงคราม </option>
                                                           <option value="สระแก้ว">สระแก้ว </option>
                                                           <option value="สระบุรี">สระบุรี </option>
                                                           <option value="สิงห์บุรี">สิงห์บุรี </option>
                                                           <option value="สุโขทัย">สุโขทัย </option>
                                                           <option value="สุพรรณบุรี">สุพรรณบุรี </option>
                                                           <option value="สุราษฎร์ธานี">สุราษฎร์ธานี </option>
                                                           <option value="สุรินทร์">สุรินทร์ </option>
                                                           <option value="สตูล">สตูล </option>
                                                           <option value="หนองคาย">หนองคาย </option>
                                                           <option value="หนองบัวลำภู">หนองบัวลำภู </option>
                                                           <option value="อำนาจเจริญ">อำนาจเจริญ </option>
                                                           <option value="อุดรธานี">อุดรธานี </option>
                                                           <option value="อุตรดิตถ์">อุตรดิตถ์ </option>
                                                           <option value="อุทัยธานี">อุทัยธานี </option>
                                                           <option value="อ่างทอง">อ่างทอง </option>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 175px;">
                                                    <label for="date_regis">วันที่ยื่นคำขอ :</label></td>
                                                <td>
      
            										<input class="form-control" name="date_regis" id="date_regis" type="date">
            		                            </td>
                                            </tr>
                                        </table>
                                        
                                        <legend>ส่วนที่ ๑ ข้อมูลพื้นฐานผู้ประกอบการ</legend>
                                        <table class="table">
                                            <tr>
                                                <td style="width: 175px;">
                                                    <label for="name">๑.๑ ชื่อ:</label></td>
                                                <td>
                                                    <input placeholder="กรุณาพิมพ์ ชื่อ" class="form-control" id="name" name="name"
                                                           required="required" type="text"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 175px;">
                                                    <label for="sername">นามสกุล:</label></td>
                                                <td>
                                                    <input placeholder="กรุณาพิมพ์ นามสกุล" class="form-control" id="sername" name="sername"
                                                           required="required" type="text"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 175px;">
                                                    <label for="id_card">เลขประจำตัวประชาชน *:</label></td>
                                                <td>
                                                    <input placeholder="13 หลักโดยไม่ต้องเว้นวรรค" class="form-control" id="id_card" name="id_card" maxlength="13"
                                                           required="required" type="number"/>
                                                </td>
                                            </tr>
                                           
                                            </table>
                                            
                                           <table class="table">
                                            <tr>
                                                <td style="width: 175px;">
                                                   <label for="type_person">๑.๒ ประเภท:</label></td>
                                                <td> 
                                               
                                                	  <select class="form-control" id="type_person" name="type_person">
                                                       <option value="เกษตรกร">เกษตรกร</option>
                                                     	<option value="ผู้มีรายได้น้อย">ผู้มีรายได้น้อย </option>
                                                        <option value="วิสาหกิจชุมชน">วิสาหกิจชุมชน </option>
                                                        <option value="กลุ่ม OTOP">กลุ่ม OTOP </option>
                                                        <option value="SMEs">SMEs </option>
                                                        <option value="พ่อค้า/แม่ค้า">พ่อค้า/แม่ค้า </option>
                                                        <option value="อื่นๆ">อื่นๆ</option>
                                                    </select>
                                                	  
                                               	  <br>
                                                   
                                              	  
                                              
                                              </td>
                                            </tr>
                                             <tr>
                                                <td style="width: 175px;">
                                                   <label for="income_per">รายได้ต่อเดือน:</label></td>
                                                <td> 
                                               	  
                                               	  <input placeholder="ระบุรายได้ต่อเดือน" class="form-control" id="income_per" name="income_per"
                                                           required="required" type="text"/>
                                              </td>
                                            </tr>
                                            
                                            </table>
                                           
                                           
                                            <table class="table table-striped" style="font-weight: bold;">
                                             <tr>
                                                <td style="width: 175px;">
                                                   <label for="address">๑.๓ ที่อยู่ปัจจุบันที่ติดต่อได้</label></td>
                                                <td> 
                                               	  
                                               	  <input placeholder="เลขที่/หมู่บ้าน/ชุมชน" class="form-control" id="address" name="address" required type="text"/>
                                              </td>
                                            </tr>

                                            <tr>
                                                <td style="width: 175px;">
                                                    <label for="sub_district">ตำบล:</label></td>
                                                <td>
                                                    <input placeholder="แขวง/ตำบล" class="form-control" id="sub_district" name="sub_district" type="text"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 175px;">
                                                    <label for="district">อำเภอ:</label></td>
                                                <td>
                                                    <input placeholder="เขต/อำเภอ" class="form-control" id="district" name="district"
                                                           required="required" type="text"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 175px;">
                                                    <label for="province">จังหวัด:</label></td>
                                                <td>
                                                    <select class="form-control" id="province" name="province">
                                                    	<option value="อุบลราชธานี">อุบลราชธานี</option>
                                                       <option value="กรุงเทพมหานคร">กรุงเทพมหานคร</option>
                                                     	<option value="กระบี่">กระบี่ </option>
                                                           <option value="กาญจนบุรี">กาญจนบุรี </option>
                                                           <option value="กาฬสินธุ์">กาฬสินธุ์ </option>
                                                           <option value="กำแพงเพชร">กำแพงเพชร </option>
                                                           <option value="ขอนแก่น">ขอนแก่น</option>
                                                           <option value="จันทบุรี">จันทบุรี</option>
                                                           <option value="ฉะเชิงเทรา">ฉะเชิงเทรา </option>
                                                           <option value="ชัยนาท">ชัยนาท </option>
                                                           <option value="ชัยภูมิ">ชัยภูมิ </option>
                                                           <option value="ชุมพร">ชุมพร </option>
                                                           <option value="ชลบุรี">ชลบุรี </option>
                                                           <option value="เชียงใหม่">เชียงใหม่ </option>
                                                           <option value="เชียงราย">เชียงราย </option>
                                                           <option value="ตรัง">ตรัง </option>
                                                           <option value="ตราด">ตราด </option>
                                                           <option value="ตาก">ตาก </option>
                                                           <option value="นครนายก">นครนายก </option>
                                                           <option value="นครปฐม">นครปฐม </option>
                                                           <option value="นครพนม">นครพนม </option>
                                                           <option value="นครราชสีมา">นครราชสีมา </option>
                                                           <option value="นครศรีธรรมราช">นครศรีธรรมราช </option>
                                                           <option value="นครสวรรค์">นครสวรรค์ </option>
                                                           <option value="นราธิวาส">นราธิวาส </option>
                                                           <option value="น่าน">น่าน </option>
                                                           <option value="นนทบุรี">นนทบุรี </option>
                                                           <option value="บึงกาฬ">บึงกาฬ</option>
                                                           <option value="บุรีรัมย์">บุรีรัมย์</option>
                                                           <option value="ประจวบคีรีขันธ์">ประจวบคีรีขันธ์ </option>
                                                           <option value="ปทุมธานี">ปทุมธานี </option>
                                                           <option value="ปราจีนบุรี">ปราจีนบุรี </option>
                                                           <option value="ปัตตานี">ปัตตานี </option>
                                                           <option value="พะเยา">พะเยา </option>
                                                           <option value="พระนครศรีอยุธยา">พระนครศรีอยุธยา </option>
                                                           <option value="พังงา">พังงา </option>
                                                           <option value="พิจิตร">พิจิตร </option>
                                                           <option value="พิษณุโลก">พิษณุโลก </option>
                                                           <option value="เพชรบุรี">เพชรบุรี </option>
                                                           <option value="เพชรบูรณ์">เพชรบูรณ์ </option>
                                                           <option value="แพร่">แพร่ </option>
                                                           <option value="พัทลุง">พัทลุง </option>
                                                           <option value="ภูเก็ต">ภูเก็ต </option>
                                                           <option value="มหาสารคาม">มหาสารคาม </option>
                                                           <option value="มุกดาหาร">มุกดาหาร </option>
                                                           <option value="แม่ฮ่องสอน">แม่ฮ่องสอน </option>
                                                           <option value="ยโสธร">ยโสธร </option>
                                                           <option value="ยะลา">ยะลา </option>
                                                           <option value="ร้อยเอ็ด">ร้อยเอ็ด </option>
                                                           <option value="ระนอง">ระนอง </option>
                                                           <option value="ระยอง">ระยอง </option>
                                                           <option value="ราชบุรี">ราชบุรี</option>
                                                           <option value="ลพบุรี">ลพบุรี </option>
                                                           <option value="ลำปาง">ลำปาง </option>
                                                           <option value="ลำพูน">ลำพูน </option>
                                                           <option value="เลย">เลย </option>
                                                           <option value="ศรีสะเกษ">ศรีสะเกษ</option>
                                                           <option value="สกลนคร">สกลนคร</option>
                                                           <option value="สงขลา">สงขลา </option>
                                                           <option value="สมุทรสาคร">สมุทรสาคร </option>
                                                           <option value="สมุทรปราการ">สมุทรปราการ </option>
                                                           <option value="สมุทรสงคราม">สมุทรสงคราม </option>
                                                           <option value="สระแก้ว">สระแก้ว </option>
                                                           <option value="สระบุรี">สระบุรี </option>
                                                           <option value="สิงห์บุรี">สิงห์บุรี </option>
                                                           <option value="สุโขทัย">สุโขทัย </option>
                                                           <option value="สุพรรณบุรี">สุพรรณบุรี </option>
                                                           <option value="สุราษฎร์ธานี">สุราษฎร์ธานี </option>
                                                           <option value="สุรินทร์">สุรินทร์ </option>
                                                           <option value="สตูล">สตูล </option>
                                                           <option value="หนองคาย">หนองคาย </option>
                                                           <option value="หนองบัวลำภู">หนองบัวลำภู </option>
                                                           <option value="อำนาจเจริญ">อำนาจเจริญ </option>
                                                           <option value="อุดรธานี">อุดรธานี </option>
                                                           <option value="อุตรดิตถ์">อุตรดิตถ์ </option>
                                                           <option value="อุทัยธานี">อุทัยธานี </option>
                                                           <option value="อ่างทอง">อ่างทอง </option>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 175px;">
                                                    <label for="zipcode">รหัสไปรษณีย์:</label></td>
                                                <td>
                                                    <input placeholder="รหัสไปรษณีย์" class="form-control" id="zipcode" name="zipcode"
                                                           required="required" type="text"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 175px;">
                                                    <label for="phone_num">หมายเลชโทรศัพท์:</label></td>
                                                <td>
                                                    <input placeholder="10 หลักโดยไม่ต้องเว้นวรรค" class="form-control" id="phone_num" name="phone_num" type="text"/>
                                                </td>
                                            </tr>
                                             <tr>
                                                <td style="width: 175px;">
                                                    <label for="line_id">Line ID :</label></td>
                                                <td>
                                                    <input class="form-control" id="line_id" name="line_id"
                                                           required="required" type="text"/>
                                                </td>
                                            </tr>
                                             <tr>
                                                <td style="width: 175px;">
                                                    <label for="e_mail">อีเมล:</label></td>
                                                <td>
                                                    <input placeholder="name@domain.com" class="form-control" id="e_mail" name="e_mail"
                                                           required="required" type="text"/>
                                                </td>
                                            </tr>

                                        </table>
                                        
                                        <table class="table table-striped" style="font-weight: bold;">
                                             <tr>
                                                <td style="width: 175px;">
                                                   <label for="status_market">๑.๔ สถานภาพผู้ประกอบการ</label></td>
                                                <td> 
                                               	  
                                              </td>
                                            </tr>
                                      
                                            
                                            <tr>
                                              <td style="width: 175px;">
                                                    <label for="status_market">สถานภาพผู้ประกอบการ *:</label></td>
                                                <td>
                                                
                                                	<p>
                                                	  <label>
                                                	    <input type="radio" name="status_market" value="เป็นผู้ประกอบการรายใหม่" id="status_market_01">
                                                	    เป็นผู้ประกอบการรายใหม่</label>
                                                	  <br>
                                                	  <label>
                                                	    <input type="radio" name="status_market" type="radio" value="เป็นผู้ประกอบการที่ปัจจุบันไม่มีสถานที่ค้าขาย" id="status_market_02">
                                                	    เป็นผู้ประกอบการที่ปัจจุบันไม่มีสถานที่ค้าขาย</label>
                                                	  <br>
                                              	  </p>
                                               	  
                                              </td>
                                            </tr>
                                            
                                            <tr>
                                                <td style="width: 175px;">
                                                    <label for="s_product">ชื่อร้านค้า:</label></td>
                                                <td>
                                                    <input placeholder="ระบุชื่อร้านค้า" class="form-control" id="s_product" name="s_product"
                                                           required="required" type="text"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 175px;">
                                                  <label for="p_product">สินค้าที่จำหน่าย :</label></td>
                                                <td>
                                                    <input placeholder="ระบุชื่อสินค้า" class="form-control" id="n_product" name="n_product"
                                                           required="required" type="text"/>
                                                </td>
                                            </tr>
                                            

                                        </table>
                                        
                                        
                                        
                                        
                                        	<legend>ส่วนที่ ๒ ข้อมูลความสนใจจองพื้นที่ตลาด</legend>
                                        <table class="table table-striped" style="font-weight: bold;">
                                             
                                      
                                            <tr>
                                                <td colspan="2" style="width: 175px;">
                                                   <label for="g_product">๒.๑ กลุ่มสินค้าที่ท่านต้องการจำหน่าย</label>                                              </td>
                                            </tr>
                                            
                                            
                                            <tr>
                                                <td style="width: 175px;">
                                                    <label for="g_product">กลุ่มสินค้าที่ต้องการจำหน่าย</label></td>
                                                <td>
                                                  <p>
                                                    <label>
                                                      <input type="checkbox" name="g_product" value="สินค้าเกษตร" id="g_product_0">
                                                      สินค้าเกษตร</label>
                                                    <br>
                                                    <label>
                                                      <input type="checkbox" name="g_product" value="สินค้า OTOP / CPOT" id="g_product_1">
                                                      สินค้า OTOP / CPOT</label>
                                                    <br>
                                                    <label>
                                                      <input type="checkbox" name="g_product" value="สินค้าแปรรูป วิสาหกิจชุมชน" id="g_product_2">
                                                      สินค้าแปรรูป วิสาหกิจชุมชน</label>
                                                    <br>
                                                    <label>
                                                      <input type="checkbox" name="g_product" value="สินค้าเกษตรอินทรีย์" id="g_product_3">
                                                      สินค้าเกษตรอินทรีย์</label>
                                                    <br>
                                                    <label>
                                                      <input type="checkbox" name="g_product" value="เนื้อสัตว์ หมู ไก่ ปลา อาหารทะเล" id="g_product_4">
                                                      เนื้อสัตว์ หมู ไก่ ปลา อาหารทะเล</label>
                                                    <br>
                                                    <label>
                                                      <input type="checkbox" name="g_product" value="บรรจุภัณฑ์" id="g_product_5">
                                                      บรรจุภัณฑ์</label>
                                                    <br>
                                                    <label>
                                                      <input type="checkbox" name="g_product" value="ผัก ผลไม้ปลอดภัย" id="g_product_6">
                                                      ผัก ผลไม้ปลอดภัย</label>
                                                    <br>
                                                    <label>
                                                      <input type="checkbox" name="g_product" value="ข้าวสาร ของแห้ง เครื่องปรุงต่างๆ" id="g_product_7">
                                                      ข้าวสาร ของแห้ง เครื่องปรุงต่างๆ</label>
                                                    <br>
                                                    <label>
                                                      <input type="checkbox" name="g_product" value="อาหารพร้อมรับประทาน ข้าวแกง" id="g_product_8">
                                                      อาหารพร้อมรับประทาน ข้าวแกง</label>
                                                    <br>
                                                    <label>
                                                      <input type="checkbox" name="g_product" value="อาหารสดแปรรูป" id="g_product_9">
                                                      อาหารสดแปรรูป</label>
                                                    <br>
                                                    <label>
                                                      <input type="checkbox" name="g_product" value="อาหารบริโภคสด" id="g_product_10">
                                                      อาหารบริโภคสด</label>
                                                    <br>
                                                    <label>
                                                      <input type="checkbox" name="g_product" value="อาหารแปรรูปเบื้องต้น/กึ่งสำเร็จรูป/สำเร็จรูป" id="g_product_11">
                                                      อาหารแปรรูปเบื้องต้น/กึ่งสำเร็จรูป/สำเร็จรูป</label>
                                                    <br>
                                                    <label>
                                                      <input type="checkbox" name="g_product" value="เครื่องดื่มมีแอลกอฮอล์" id="g_product_12">
                                                      เครื่องดื่มมีแอลกอฮอล์</label>
                                                    <br>
                                                    <label>
                                                      <input type="checkbox" name="g_product" value="เครื่องดื่มไม่มีแอลกอฮอล์" id="g_product_13">
                                                      เครื่องดื่มไม่มีแอลกอฮอล์</label>
                                                    <br>
                                                    <label>
                                                      <input type="checkbox" name="g_product" value="ผ้า/เครื่องแต่งกาย/ผ้าผืนและเครื่องนุ่งห่ม" id="g_product_14">
                                                      ผ้า/เครื่องแต่งกาย/ผ้าผืนและเครื่องนุ่งห่ม</label>
                                                    <br>
                                                    <label>
                                                      <input type="checkbox" name="g_product" value="ผ้า/เครื่องแต่งกาย/เครื่องแต่งการที่ทำจากวัสดุทุกชนิด" id="g_product_15">
                                                      ผ้า/เครื่องแต่งกาย/เครื่องแต่งการที่ทำจากวัสดุทุกชนิด</label>
                                                    <br>
                                                    <label>
                                                      <input type="checkbox" name="g_product" value="ผลิตภัณฑ์จากศิลปะประดิษฐ์/ผลิดภัณฑ์ไม้" id="g_product_16">
                                                      ผลิตภัณฑ์จากศิลปะประดิษฐ์/ผลิดภัณฑ์ไม้</label>
                                                    <br>
                                                    <label>
                                                      <input type="checkbox" name="g_product" value="ผลิตภัณฑ์จากศิลปะประดิษฐ์/ผลิตภัณฑ์จักสาน ถักสาน" id="g_product_17">
                                                      ผลิตภัณฑ์จากศิลปะประดิษฐ์/ผลิตภัณฑ์จักสาน ถักสาน</label>
                                                    <br>
                                                    <label>
                                                      <input type="checkbox" name="g_product" value="ผลิตภัณฑ์จากศิลปะประดิษฐ์/ผลิตภัณฑ์กระดาษ" id="g_product_18">
                                                      ผลิตภัณฑ์จากศิลปะประดิษฐ์/ผลิตภัณฑ์กระดาษ</label>
                                                    <br>
                                                    <label>
                                                      <input type="checkbox" name="g_product" value="ผลิตภัณฑ์จากศิลปะประดิษฐ์/ผลิตภัณฑ์โลหะ" id="g_product_19">
                                                      ผลิตภัณฑ์จากศิลปะประดิษฐ์/ผลิตภัณฑ์โลหะ</label>
                                                    <br>
                                                    <label>
                                                      <input type="checkbox" name="g_product" value="ผลิตภัณฑ์จากศิลปะประดิษฐ์/ไม่ไผ่" id="g_product_20">
                                                      ผลิตภัณฑ์จากศิลปะประดิษฐ์/ไม่ไผ่</label>
                                                    <br>
                                                    <label>
                                                      <input type="checkbox" name="g_product" value="ผลิตภัณฑ์จากศิลปะประดิษฐ์/ใยพืช" id="g_product_21">
                                                      ผลิตภัณฑ์จากศิลปะประดิษฐ์/ใยพืช</label>
                                                    <br>
                                                    <label>
                                                      <input type="checkbox" name="g_product" value="ผลิตภัณฑ์จากศิลปะประดิษฐ์/เครื่องปั้นดินเผา/เซรามิก" id="g_product_22">
                                                      ผลิตภัณฑ์จากศิลปะประดิษฐ์/เครื่องปั้นดินเผา/เซรามิก</label>
                                                    <br>
                                                    <label>
                                                      <input type="checkbox" name="g_product" value="ผลิตภัณฑ์จากศิลปะประดิษฐ์/ผลิตภัณฑ์หนังที่ไม่ใช่เครื่องแต่งกาย" id="g_product_23">
                                                      ผลิตภัณฑ์จากศิลปะประดิษฐ์/ผลิตภัณฑ์หนังที่ไม่ใช่เครื่องแต่งกาย</label>
                                                    <br>
                                                    <label>
                                                      <input type="checkbox" name="g_product" value="ผลิตภัณฑ์จากศิลปะประดิษฐ์/เครื่องเรือน" id="g_product_24">
                                                      ผลิตภัณฑ์จากศิลปะประดิษฐ์/เครื่องเรือน</label>
                                                    <br>
                                                    <label>
                                                      <input type="checkbox" name="g_product" value="ผลิตภัณฑ์จากศิลปะประดิษฐ์/ผลิตภัณฑ์ดอกไม้ประดิษฐ์" id="g_product_25">
                                                      ผลิตภัณฑ์จากศิลปะประดิษฐ์/ผลิตภัณฑ์ดอกไม้ประดิษฐ์</label>
                                                    <br>
                                                    <label>
                                                      <input type="checkbox" name="g_product" value="สมุนไพรที่ไม่ใช่อาหาร" id="g_product_26">
                                                      สมุนไพรที่ไม่ใช่อาหาร</label>
                                                    <br>
                                                    <label>
                                                      <input type="checkbox" name="g_product" value="กลุ่มบริการ/กลุ่มบริการโปรแกรมท่องเที่ยว" id="g_product_27">
                                                      กลุ่มบริการ/กลุ่มบริการโปรแกรมท่องเที่ยว</label>
                                                    <br>
                                                    <label>
                                                      <input type="checkbox" name="g_product" value="กลุ่มบริการ/กลุ่มบริการที่พัก" id="g_product_28">
                                                      กลุ่มบริการ/กลุ่มบริการที่พัก</label>
                                                    <br>
                                                    <label>
                                                      <input type="checkbox" name="g_product" value="กลุ่มบริการ/กลุ่มบริการด้านอื่นๆ" id="g_product_29">
                                                      กลุ่มบริการ/กลุ่มบริการด้านอื่นๆ</label>
                                                    <br>
                                                    <label>
                                                      <input type="checkbox" name="g_product" value="อื่นๆ" id="g_product_30">
                                                      อื่นๆ</label>
                                                    <br>
                                                  </p>
</p></td>
                                            </tr>
                                            <tr>
                                                <td style="width: 175px;">
                                                    <label for="org_agency">หน่วยงานที่รับลงทะเบียน *:</label></td>                                                <td>
                                                    <select class="form-control" id="org_agency" name="org_agency">
                                                        <option value="สำนักงานพาณิชย์จังหวัดอุบลราชธานี">สำนักงานพาณิชย์จังหวัดอุบลราชธานี </option>
                                                        <option value="สำนักงานวัฒนธรรมจังหวัดอุบลราชธานี">สำนักงานวัฒนธรรมจังหวัดอุบลราชธานี </option>
                                                        <option value="ธนาคารเพื่อการเกษตรและสหกรณ์ สาขาพิบูลมังสาหาร">ธนาคารเพื่อการเกษตรและสหกรณ์ สาขาพิบูลมังสาหาร </option>
                                                        <option value="บริษัทประชารัฐรักสามัคคีจังหวัดอุบลราชธานีจำกัด">บริษัทประชารัฐรักสามัคคีจังหวัดอุบลราชธานี จำกัด </option>
                                                        
                                                        <option value="อื่นๆ">อื่นๆ</option>
                                                    </select>
                                                </td>
                                            </tr>
                                            

                                        </table>
                                        <div style="text-align: left;"><br/>
                                        
                                            *** ข้าพเจ้าขอรับรองว่าได้ยื่นลงทะเบียนเพียงแห่งเดียวและข้อมูลข้างต้นนี้เป็นความจริงทุกประการ โดยยินยอมให้เจ้าหน้าที่ตัดสิทธิ์การลงทะเบียนหากพบว่าข้อมูลในแบบคำขอไม่เป็นความจริงหรือผู้ลงทะเบียนยื่นแบบคำขอมากกว่า 1 แห่ง
                          </div><br/>
                                        <button type="submit" class="btn btn-success btn-lg" style="width:100%;">ตกลง
                          </button>
                                        
                                        </form>

                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                         
                           
        </div>
    </div>



                    </div>


               
                </div>

            </div>

        </div>

    </div>
    <!-- /.container -->

    <div class="container">

        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                
<p>© 2016 Addpay Service Point Co., Ltd. All rights reserved.</p>
<p>บริษัท แอดเพย์ เซอร์วิสพอยท์ จำกัด เลขที่ 406 หมู่ 18 ต.ขามใหญ่ อ.เมือง จ.อุบลราชธานี 34000</p>
<p>โทร. 045-317123,061-8182888 Fax.045-317678</p>
                
            </div>
        </footer>

    </div>
    <!-- /.container -->





    
    

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
        <script src="js/mainmenu.js"></script>
        <script src="http://momentjs.com/downloads/moment-with-locales.js"></script>
		<script src="http://momentjs.com/downloads/moment-timezone-with-data.js"></script>

  </body>
</html>