<?php

class security {
    private function setadmin() {
        if ($_SESSION[_ef . 'levelaccess'] == 'admin') {
            return TRUE;
        } else if ($_SESSION[_ef . 'levelaccess'] == 'user') {
            return TRUE;
        } else if ($_SESSION[_ef . 'levelaccess'] == 'chef') {
            return TRUE;
        } else if ($_SESSION[_ef . 'levelaccess'] == 'employee') {
            return TRUE;
        } else if ($_SESSION[_ef . 'levelaccess'] == 'cashier') {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    private function setuser() {
        if ($_SESSION[_ef . 'levelaccess'] == '') {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function check($type = "user") {
        if ($type == "admin"OR"user"OR"chef"OR"employee"OR"cashier") {
            $check = $this->setadmin();
            $dir = "back";
        } else {
            $check = $this->setuser();
            $dir = "front";
        }

        if ($check == FALSE) {
            header("location:" . base_url() . "/" . $dir . "/user/login");
        }
    }

}
